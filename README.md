# ikalkulator

Serwis ikalkulator.pl oparty o CMS Wordpress.

## Instalacja

1. Zainstaluj Wordpressa
2. Zaloguj się do panelu administracyjnego
3. Uruchom theme "ikalkulator"
4. Zainstaluj niezbędne pluginy:
    * ACF Pro
    * Custom sidebars ( [http://incsub.com](http://incsub.com))
    * Google Captcha (reCAPTCHA) by BestWebSoft
    * Mailchimp for WP
    * SVG Support 
    * Page Builder by SiteOrigin
    * Wecode widgets ( część tego projektu )
    * Wordpress popular posts
    * WP no base permalink
5. Opcjonalnie zainstaluj dodatkowe zalecane pluginy
    * Accelerated Mobile Pages for WordPress
    * ACF Options Page Admin
    * Sierotki
    * SiteOrigin Widgets Bundle
    * Yoast SEO
    * WP User Avatar
6. Wyeksportuj wybrane definicje / obiekty (definicje pól, ACF, Strony, Posty, Produkty) z istniejącej instancji aplikacji (developerskiej lub produkcyjnej)
7. Zaimportuj powyższe

## Motyw ikalkulator

Architekutra motywu:
* katalog główny `/`
    * definicje archiwów - pliki `archive-*` dla poszczególnych typów postów
    * template pojedynczej firmy `single-company.php`
    * template pojedynczego produktu `single-product.php` - uniwersalny dla wszystkich typów produktów 
    * wyniki wyszukiwania `search.php`
    * template strony głównej `page-home.php`
    * pozostałe template'y zgodne z konwencją WP
* `/app`
    * `api.php` - definicje WP REST API
    * `comments.php` - funkcje związane z komentarzami i ocenami produktów
    * `config.php` - funkcje konfigurujące instalację WP, m.in. sidebary, nawigację, shortcode'y
    * `func.php` - funkcje pomocnicze używane w template'ach do pobierania / przetwarzania wyświetlancyh obiektów
    * `identify-external-links.php`  - funkcje obsługujące linki wychodzące z serwisu
    * `post-types.php`  - funkcje definiujące Custom Post Types
* `/assets` - katalog ze statycznymi elementami projektu, w tym generowanymi plikami css i js
* `/product-api` - endpoint do pobierania przez AJAX przefiltrowanych produktów z poszczególnych kategorii (zwraca HTML)
* `/src` - źródła interfejsu 
* `/template-page` - dodatkowe template'y do użycia w CMS
* `/template-parts` - reużywalne elementy interfejsu  
    * `/category` - elementy do wyświetlania listy postów/produktów w formie m.in. Megaboxa czy Promoboxa (na home)
    * `/head` - header 
    * `/icon` - ikony svg 
    * `/navigation` - elementy nawigacji
    * `/post` - elementy użyte w widoku Single Post
    * `/products` - elementy używane w poszczególnych wyszukwiarkach produktów
        * `/products-html-filters` - filtry poszczególnych wyszukiwarek 
        * `/products-html-list` - lista wyników po filtrowaniu
        * `/products-setup` - przygotowanie filtrów na podstawie przekazanych parametrów GET
        * `/products-single-metrics` - metryki produktów do wyświetlania dla widoku Single Product z podziałem na typy produktów
        * `/schema-definition` - definicje używane do Danych Strukturalnych
    * `/searchbox` - box wyszukiwania na Home
    * `/specific` - definicje gridów do wyświetlania widoku firm 

## Konstrukcja wyszukiwarki

Wyszukiwarki znajdują się w plikach `archive-*.php` motywu. 

Każda wyszukiwarka składa się z trzech bloków: 
* setup - ustawienie parametrów domyślnych i ew. przeładowanie ich parametrami przekazanymi w zapytaniu oraz pobranie za pomocą `get_posts()` listy pasujących wyników
* filtry - include odpowiedniego pliku z `template-parts/products/products-html-filters/`
* wyświetlenie listy pasujących produktów poprzez include odpowiedniego pliku  z `template-parts/products/products-html-list`
  W każdym z tych plików iterujemy po tablicy postów definiowanej w pierwszym punkcie i dla każdego produktu sprawdzamy czy spełnia on kolejne warunki filtrowania. Ten sposób jest mniej wydajny niż tworzenie bezpośredniego zapytania do bazy, ale łatwiej i czytelniej pozwala zarządzać parametrami.
  Przy założeniu, że lista aktywnych produktów nie będzie przekraczała 30 oraz małej częstotliwości zmian produktów (możliwość długotrwałego cacehowania) kwestię złożoności obliczeniowej można pominąć.

## Modyfikacja interfejsu

Żródła plików CSS / JS znajdują się w katalogu `/src` motywu.

### Modyfikacja CSS

Definicje CSS są tworzone przy użyciu preprocesora LESS. 
W katalogu `/src/less` znajduje się plik `style.css` w którym definiowane są wszystkie źródła podlegające kompilacji do pliku wynikowego.

### Modyfikacja JS

Żródłowe pliki JS aplikacji znajdują się w katalogu `/src/js/app` i kompilują się do pliku `/assets/js/app.js`

### NPM + Gulp

W katalogu głównym repozytorium znajdują się definicje pakietów NPM i taski GULP. Można użyć ich do budowania CSS i JS:
`npm install` - instalacja pakietów NPM
`gulp dev` - bez  minifikacji źródeł
`gulp prod` - z  minifikacji źródeł

### Zewnętrzne biblioteki

Dodatkowe biblioteki można umieszczać w katalogu `/src/vendor` motywu, a następnie dopisywać je do tasków `css_concat_vendor` i `css_concat_vendor` w plikut gulpfile.js w głównym katalogu repozytorium.


## Modules
### Five tiles text fields
The fields was registered in `web/wp-content/themes/ikalkulator/app/config.php`, and displayed in: `web/wp-content/themes/ikalkulator/template-parts/specific/tiles.php`, `web/wp-content/themes/ikalkulator/template-parts/post/blocks-list.php`

### Countdown
The script responsible for displaying countdown is available from `web/wp-content/themes/ikalkulator/src/js/app/countdown.js`, styles from `web/wp-content/themes/ikalkulator/src/less/partial/countdown.less`.
Countdown module work independently, we can add any number of it on the website.
Code's needed to embed:
`<div class="ikal__countdown__wrap" id="UniqueIdOfCountdown" data-start="StartDateValue" data-end="EndDateValue" data-before-msg="BeforeMsgValue" data-while-msg="WhileMsgValue" data-after-msg="AfterMsgValue"></div><script type="text/javascript"> countdown("UniqueIdOfCountdown");</script>`
     
Can be displayed by a shortcode `[ik_countdown start-date="StartDateValue" end-date="EndDateValue" before-msg="BeforeMsgValue" while-msg="WhileMsgValue" after-msg="AfterMsgValue"]`

Arguments:
- StartDateValue

Starting date of the event e.g. 2022/06/24 13:03:00

- EndDateValue (optional)

Starting date of the event e.g. 2025/12/25 13:03:10

- BeforeMsgValue (optional)

Message displayed before the event starts e.g. The offer starts with:

- WhileMsgValue (optional)

Message displayed during the event e.g. The offer ends with:

- AfterMsgValue (optional)

Message displayed after the event is finished e.g. The offer has ended



### Calculators
The script responsible for displaying and operation of the calculators is available from `web/wp-content/themes/ikalkulator/src/js/app/calculators.js`, styles from `web/wp-content/themes/ikalkulator/src/less/partial/calculators.less`.
Calculators work independently, we can add any number of them on the website.
Code's needed to embed each of them:


percentOfCalculator- embeded by: 
`<div id="UniqueIdOfCalculator"></div><script type="text/javascript">document.addEventListener("DOMContentLoaded",function(){new percentOfCalculator("UniqueIdOfCalculator")})</script>`    

Can be displayed by a shortcode `[ik_calc_percof]`




whatPercentCalculator- embeded by:
`<div id="UniqueIdOfCalculator"></div><script type="text/javascript">document.addEventListener("DOMContentLoaded",function(){new whatPercentCalculator("UniqueIdOfCalculator")})</script>`

Can be displayed by a shortcode `[ik_calc_whatperc]`




percentRaiseCalculator- embeded by:
`<div id="UniqueIdOfCalculator"></div><script type="text/javascript">document.addEventListener("DOMContentLoaded",function(){new percentRaiseCalculator("UniqueIdOfCalculator")})</script>`

Can be displayed by a shortcode `[ik_calc_percraise]`




percentAdditionCalculator- embeded by:
`<div id="UniqueIdOfCalculator"></div><script type="text/javascript">document.addEventListener("DOMContentLoaded",function(){new percentAdditionCalculator("UniqueIdOfCalculator")})</script>`

Can be displayed by a shortcode `[ik_calc_percaddition]`




percentSubtractCalculator- embeded by:
`<div id="UniqueIdOfCalculator"></div><script type="text/javascript">document.addEventListener("DOMContentLoaded",function(){new percentSubtractCalculator("UniqueIdOfCalculator")})</script>`

Can be displayed by a shortcode `[ik_calc_percsubtract]`




compoundInterestCalculator- embeded by:
`<div id="UniqueIdOfCalculator"></div><script type="text/javascript">document.addEventListener("DOMContentLoaded",function(){new compoundInterestCalculator("UniqueIdOfCalculator")})</script>`

Can be displayed by a shortcode `[ik_calc_compinterest]`




investmentCalculator- embeded by:
`<div id="UniqueIdOfCalculator"></div><script type="text/javascript">document.addEventListener("DOMContentLoaded",function(){new investmentCalculator("UniqueIdOfCalculator")})</script>`

Can be displayed by a shortcode `[ik_calc_investment]`




regularSavingsCalculator- embeded by:
`<div id="UniqueIdOfCalculator"></div><script type="text/javascript">document.addEventListener("DOMContentLoaded",function(){new regularSavingsCalculator("UniqueIdOfCalculator")})</script>`

Can be displayed by a shortcode `[ik_calc_regularsavings]`




regularSavingsByAmountCalculator- embeded by:
`<div id="UniqueIdOfCalculator"></div><script type="text/javascript">document.addEventListener("DOMContentLoaded",function(){new regularSavingsByAmountCalculator("UniqueIdOfCalculator")})</script>`

Can be displayed by a shortcode `[ik_calc_regularsavingsbyamount]`



