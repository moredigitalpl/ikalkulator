<?php if ( is_active_sidebar( "sidebar-right" ) ) : ?>
	<div class="sidebar sidebar-right widget-area" role="complementary">
		<?php dynamic_sidebar( "sidebar-right" ); ?>
	</div>
<?php endif; ?>