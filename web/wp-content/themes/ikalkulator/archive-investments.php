<?php

$bodyClass = "archive-investments";
get_header();
include('template-parts/products/products-setup/investments.php');
?>

<div id="page" class="product-list investments" data-product-slug="investments">
    <div class="page-head">
        <div class="content">
            <?php the_breadcrumbs(); ?>
            <h1 class="title">Lokaty <span class="count">(<?php echo product_count_display(count($posts_array)); ?>)</span> </h1>

            <div class="description">
                <?php the_field('products_descriptions_investments', 'options'); ?>
            </div>
            
        </div>
    </div>
    <?php

    include('template-parts/products/products-html-filters/investments.php');

    ?>
    <div class="container products header"><div class="mobile-label">Sortuj wg</div><ul><li>
                <div class="name data-row">
                   Nazwa
                </div>
                <div class="amount data-row">
                    Kwota
                </div>
                <div class="maxperiod data-row">
                    Okres spłat
                </div>
                <div class="percentage data-row sortable">
                    <a href="#" data-sort-by="percentage" data-sort-type="number">Oprocentowanie</a>
                </div>
                <div class="profit data-row sortable">
                    <a href="#" data-sort-by="profit" data-sort-type="number">Zysk</a>
                </div>
                <div class="rank data-row sortable">
                    <a href="#" data-sort-by="rank" data-sort-type="number">Ocena</a>
                </div>
                <div class="cta data-row">
                    Złóż wniosek
                </div>
            </li></ul></div>
    <div class="container products list">
        <?php
        include('template-parts/products/products-html-list/investments.php');
        echo '</div>';

        ?>
        <div class="container description main-content">
            <div class="content">
                <?php the_field('products_descriptions_investments_bottom', 'options'); ?>
            </div>
        </div>
    </div>

    <?php get_footer(); ?>
