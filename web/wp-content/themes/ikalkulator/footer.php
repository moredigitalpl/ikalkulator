        </div><?php /* .site-content */ ?>
        <footer class="site-footer">
            <div class="footer-top">
				<div class="site-logo">
					<?php if (get_theme_mod("website_logo")) : ?>
					<a href="<?php echo esc_url(home_url("/")); ?>" class="logo"
					   title="<?php echo esc_attr(get_bloginfo("name", "display")); ?>" rel="home">
						<img src="<?php echo get_theme_mod("website_logo"); ?>"
							 alt="<?php echo esc_attr(get_bloginfo("name", "display")); ?>" />
					</a>
					<?php else : ?>
						<a href="<?php echo esc_url(home_url("/")); ?>"></a>
					<?php endif; ?>
				</div>
                <div class="motto">
                    Dokładne analizy, szczegółowe porady oraz wygodne porównywarki.<br>
					iKalkulator - więcej pieniędzy w <strong>Twoim</strong> portfelu.
                </div>
            </div>
            <?php if ( has_nav_menu( "main-menu-footer" ) ) : ?>
                <div class="footer-nav">
                    <?php
                    wp_nav_menu( array(
                        "theme_location"	=> "main-menu-footer",
                        "menu"				=> "",
                        "container_class"	=> "",
                        "container"			=> "",
                        "link_before"       => "<span>",
                        "link_after"		=> "</span>",
                        "items_wrap"		=> '<ul class="%2$s">%3$s</ul>',
                    ));
                    ?>
                </div>
            <?php endif; ?>

            <div class="footer-bottom">
                <div class="copy-rules">
                    <div class="copyrights">
                        &copy; iKalkulator - 2018.
                    </div>
                    <?php if ( has_nav_menu( "footer-copy" ) ) : ?>
                        <div class="rules-nav">
                            <?php
                            wp_nav_menu( array(
                                "theme_location"	=> "footer-copy",
                                "menu"				=> "",
                                "container_class"	=> "",
                                "container"		=> "",
                                "link_before"      => "<span>",
                                "link_after"		=> "</span>",
                                "items_wrap"		=> '<ul class="%2$s">%3$s</ul>',
                            ));
                            ?>
                        </div>
                    <?php endif; ?>
                </div>
                <?php if ( has_nav_menu( "social" ) ) : ?>
                    <div class="social-nav">
                        <?php
                        wp_nav_menu( array(
                            "theme_location"	=> "social",
                                "menu"				=> "",
                                "container_class"	=> "",
                                "container"		=> "",
                                "link_before"      => "<span>",
                                "link_after"		=> "</span>",
                                "items_wrap"		=> '<ul class="%2$s">%3$s</ul>',
                        ));
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </footer>
        <?php echo create_search_bar("spotlight-search", true); ?>

    </div><?php /* .site */ ?>
    <?php wp_footer(); ?>
</body>
</html>
