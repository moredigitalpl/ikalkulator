<?php

$bodyClass = "archive-credits";
get_header();
include('template-parts/products/products-setup/credits.php');
?>

<div id="page" class="product-list credits" data-product-slug="credits">
    <div class="page-head">
        <div class="content">
            <?php the_breadcrumbs(); ?>
            <h1 class="title">Kredyty <span class="count">(<?php echo product_count_display(count($posts_array)); ?>)</span> </h1>

            <div class="description">
                <?php the_field('products_descriptions_credits', 'options'); ?>
            </div>
            
        </div>
    </div>
    <?php

    include('template-parts/products/products-html-filters/credits.php');

    ?>
    <div class="container products header"><div class="mobile-label">Sortuj wg</div><ul><li>
                <div class="name data-row">
                    Nazwa produktu
                </div>


                <div class="amount data-row">
                   Kwota
                </div>
                <div class="maxperiod data-row">
                    Okres spłat
                </div>
                <div class="interest data-row">
                    Oprocentowanie
                </div>
                <div class="comission data-row">
                    Prowizja
                </div>
                <div class="rank data-row sortable">
                    <a href="#" data-sort-by="rank" data-sort-type="number">Ocena</a>
                </div>
                <div class="cta data-row">
                    Złóż wniosek
                </div>
            </li></ul></div>
    <div class="container products list">
        <?php
        include('template-parts/products/products-html-list/credits.php');
        echo '</div>';

        ?>
        <div class="container description main-content">
            <div class="content">
                <?php the_field('products_descriptions_credits_bottom', 'options'); ?>
            </div>
        </div>
    </div>


    <?php get_footer(); ?>
