<?php
get_header();
$excluded_posts = array();
set_query_var("excluded_posts", $excluded_posts);
?>
<main class="page-main" role="main">
    <?php
    get_template_part( "template-parts/head/head" );
    ?>
    <div class="main-content category-content">
        <?php get_sidebar("top"); ?>
        <section class="category-content">
        <?php
            set_query_var("two_column_posts_list", true);
            get_template_part( "template-parts/category/posts-list-paginated");
        ?>
        </section>
        <?php get_sidebar("bottom"); ?>
    </div>
</main>
<?php get_footer();
