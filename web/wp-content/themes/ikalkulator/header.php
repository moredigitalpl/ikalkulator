<?php
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<title><?php wp_title(); ?></title>
	<meta charset="<?php bloginfo( "charset" ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php wp_head(); ?>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KBCDVLQ');</script>
<!-- End Google Tag Manager -->
	<?php if(is_front_page()): ?>
		<script type="application/ld+json">
		  {
		    "@context": "http://schema.org",
		    "@type": "Blog",
		    "url": "https://www.ikalkulator.pl/blog"
		  }
		</script>
	<?php endif ?>
<!--	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag()

		{dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-9039982-61', {'page_location': document.location.href});
	</script> -->
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KBCDVLQ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="site">
	<header class="site-header">
		<nav class="navigation-top" role="navigation">
			<div class="site-logo">
				<?php if (get_theme_mod("website_logo")) : ?>
					<a href="<?php echo esc_url(home_url("/")); ?>" class="logo"
					   title="<?php echo esc_attr(get_bloginfo("name", "display")); ?>" rel="home">
						<img src="<?php echo get_theme_mod("website_logo"); ?>"
						     alt="<?php echo esc_attr(get_bloginfo("name", "display")); ?>" />
					</a>
				<?php else : ?>
					<a href="<?php echo esc_url(home_url("/")); ?>"></a>
				<?php endif; ?>
			</div>
			<div class="btn-mobile-menu"><span></span></div>
			<?php if ( has_nav_menu( "main-menu" ) ) : ?>
				<div class="main-nav">
					<?php
					wp_nav_menu( array(
						"theme_location"	=> "main-menu",
						"menu"				=> "",
						"container_class"	=> "",
						"container"			=> "",
						"link_before"       => "<span>",
						"link_after"		=> "</span>",
						"items_wrap"		=> '<ul class="%2$s">%3$s</ul>',
						"walker"         	=> new Primary_Walker_Nav_Menu()
					));
					?>
				</div>
			<?php endif; ?>
		</nav>
	</header>
	<div class="site-content">
