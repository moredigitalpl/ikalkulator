ikalkulator = function ()
{
    function calculateChildrenHeight(container)
    {	
        var topOffset = bottomOffset = 0;

        container.children().each(function(i, child)
        {
            var element = jQuery(child),
            eTopOffset = element.offset().top,
            eBottomOffset = eTopOffset + element.outerHeight();

            if (eTopOffset < topOffset)
                topOffset = eTopOffset;

            if (eBottomOffset > bottomOffset)
                bottomOffset = eBottomOffset;
        });

        return  bottomOffset - topOffset - container.offset().top + parseInt(container.css("padding-bottom"));
    }    
    
    function handleCategoryWithSidebarMinHeight()
    {
        var sidebar = jQuery("body.category section.category-content .aside-panel");
        if (!sidebar.length) { return; }
        
        var categoryContent = jQuery("body.category section.category-content");        
        categoryContent.css("min-height", calculateChildrenHeight(categoryContent));
    }
    
    function handleListNumbering()
    {
        jQuery("ol[start]").each(function()
        {
            var val = parseFloat(jQuery(this).attr("start")) - 1;
            console.log(val);
            jQuery(this).css("counter-increment", "ol-counter " + val);
        });        
    }
    
    function handleProductsGrid()
    {
        var productGrid = jQuery(".product-grid");
        
        if (!productGrid.length)
            return;
        
        var showMore = jQuery(".products-show-more", productGrid);
        
        showMore.on("click", function()
        {
            var btn = jQuery(this);
            var moreProducts = jQuery(".posts-list-content.more-products", productGrid);
            
            if (moreProducts.hasClass("visible"))
            {
                jQuery(".show", btn).show(); 
                jQuery(".hide", btn).hide(); 
                moreProducts.removeClass("visible");
                moreProducts.slideUp();
                scrollTo(jQuery(".posts-list-content", productGrid), -30);
            }    
            else
            {
                jQuery(".show", btn).hide(); 
                jQuery(".hide", btn).show(); 
                moreProducts.addClass("visible");
                moreProducts.slideDown();
                scrollTo(jQuery(".posts-list-content", productGrid), -30);
            }   
        });
        
    }
    
    
	function handleShareLinks()
	{
		jQuery(".share .social-share").on("click", function(e)
        {
			e.preventDefault();
			window.open(jQuery(this).attr('href'), 'fbShareWindow', 'height=450, width=650, top=' + (jQuery(window).height() / 2 - 275) + ', left=' + (jQuery(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
			return false;
		});
	}
    
    function handleSpotlightSearch()
    {
        var searchBtn = jQuery(".site-header .search-btn a");
        var searchBar = jQuery(".search-bar.spotlight-search");
        
        if (!searchBtn.length || !searchBtn.length)
            return;
        
        function hideBar()
        {
            searchBar.removeClass("active");
            jQuery("input", searchBar).blur().attr("readonly", true);
        }
        
        jQuery("input", searchBar).attr("readonly", true);
        
        searchBtn.on("click", function(e)
        {
            e.stopPropagation();
            e.preventDefault();            
            searchBar.toggleClass("active");  
            
            if (searchBar.hasClass("active"))
            {
                jQuery("input", searchBar).attr("readonly", false); 
                
                window.setTimeout(function()
                {
                    jQuery("input[type=text]", searchBar).focus();
                } ,500);
                
        }
            else
                jQuery("input", searchBar).attr("readonly", true);
        });

        searchBar.on("click touch touchend", function(e)
        {
            e.stopPropagation();
        });        
        
        jQuery(document)
            .on("click touch touchend", function()
            {
                hideBar();
            })        
            .on("keydown", function(e)
            {
                if ( e.keyCode == 27 )
                {
                    hideBar();    
                }
            });
    }

    function checkElementsVisibilityOnScroll(elementsClass)
    {
        var elements = jQuery(elementsClass);

        if (!elements.length)
            return;

        elements
            .attr("data-onscreen", 0);

        var isScrolling  = false;

        jQuery(window).on("scroll", function()
        {
			if (isScrolling) { return; }

            var winHeight = jQuery(window).innerHeight();
            var scrollTop =  jQuery(window).scrollTop();
            var scrollBottom = scrollTop + winHeight;

			isScrolling = true;

			window.setTimeout(function()
			{
                elements.each(function()
                {
                    var element = jQuery(this);
                    var isVisible = element.attr("data-onscreen");
                    var elementHeight = element.outerHeight();
                    var elementTop = element.offset().top;
                    var elementBottom = elementTop + elementHeight;

                    if (isVisible == true)
                    {
                        if (elementBottom <= scrollTop || elementTop >= scrollBottom)
                        {
                            element
                                .trigger("ik:visiblity-change")
                                .trigger("ik:hidden")
                                .attr("data-onscreen", 0);
                        }
                    }
                    else
                    {
                        if (
                                (elementBottom > scrollTop && elementTop < scrollTop)
                                ||
                                (elementTop < scrollBottom && elementBottom > scrollBottom)
                                ||
                                (elementTop < scrollTop && elementBottom > scrollBottom)
                            )
                        {
                            element
                                .trigger("ik:visiblity-change")
                                .trigger("ik:visible")
                                .attr("data-onscreen", 1);
                        }
                    }

                    /*
                    if (elementBottom <= scrollBottom && elementTop >= scrollTop)
                        element.trigger("ik:reveal:full");
                    else*/
                });

				isScrolling = false;
			}, 5 );
        });

		jQuery(window).trigger("scroll");
    }
    function handleMenu()
    {
        var siteHeader = jQuery(".site-header");
        var menuItems = jQuery(".menu > .menu-item-has-children > a", siteHeader);
        
        
        menuItems.on("click", function(e)
        {
            if (window.innerWidth < 1024)
            {
                var isClicked = jQuery(this).parent().hasClass("clicked");
               
                
                if (!isClicked)
                {
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    e.stopPropagation();              
                    
                    jQuery(".menu > .menu-item-has-children").removeClass("clicked");
                    jQuery(this).parent().addClass("clicked");
                }
                else
                {
//                    jQuery(this).parent().removeClass("clicked");                    
                }
            }
        });
        
    }

	function handleMobileMenu()
	{
        var body = jQuery("body");
        var siteHeader = jQuery(".site-header");
		var btnMenu = jQuery(".btn-mobile-menu", siteHeader);
		var menuItems = jQuery(".menu-item a", siteHeader);

		btnMenu
			.on("click", function(e)
			{
				body.toggleClass("menu-opened");

				if (body.hasClass("menu-opened"))
				{
					jQuery(this).addClass("active");
					siteHeader.addClass("mobile-active");
				}
				else
				{
					jQuery(this).removeClass("active");
					siteHeader.removeClass("mobile-active");
				}
			});

		menuItems
			.on("click", function(e)
			{
                if(!jQuery(this).parent().hasClass("menu-item-has-children"))
                {
                    body.removeClass("menu-opened");
                    btnMenu.removeClass("active");
                    siteHeader.removeClass("mobile-active");

                }
			});
	};

    function handleAnalytics()
    {
        jQuery('.navigation-top li a').on('click', function(e)
        {
            var link = jQuery(this).attr("href");
            //e.preventDefault();
            gtag('event', 'TOP-menu', {
                'event_category': 'click',
                'event_label': jQuery(this).find("span").html(),
                //'event_callback': function(){document.location = link;}
            });
        });

        jQuery('.footer-nav li a').on('click', function(e)
        {
            //e.preventDefault();

            var link = jQuery(this).attr("href");

            gtag('event', 'footer-menu', {
                'event_category': 'click',
                'event_label': jQuery(this).find("span").html(),
                //'event_callback': function() {document.location = link;}
            });
        });

        jQuery('.products.list li .more .expander .expand').on('click', function(e)
        {
            var product_name = jQuery(this).closest("li").find(".name div").html();
            e.preventDefault();
            gtag('event', 'Zobacz więcej', {
                'event_category': 'click',
                'event_label': product_name + '  na ' + location.href
            });
        });

        jQuery('.products.list li .more .expander .fold').on('click', function(e)
        {
            var product_name = jQuery(this).closest("li").find(".name div").html();
            e.preventDefault();
            gtag('event', 'Zobacz mniej', {
                'event_category': 'click',
                'event_label': product_name + '  na ' + location.href
            });
        });

        jQuery('.products.list li .cta a').on('click', function(e)
        {
            //e.preventDefault();

            var link = jQuery(this).attr("href");
            var product_name = jQuery(this).closest("li").attr("id")
            var color = "biały";

            if(jQuery(this).hasClass("btn-inverse"))
                color = "zielony";

            gtag('event', 'compare-acquisition', {
                'event_category': 'click',
                'event_label': 'Złóż wniosek - ' + color + ' guzik - '  + product_name + ' na ' + location.href,
                //'event_callback': function(){document.location = link;}
            });
        });

        jQuery('.product .attachments ul li a').on('click', function(e)
        {
            //e.preventDefault();

            var link = jQuery(this).attr("href");

            gtag('event', 'PDF', {
                    'event_category' : 'pobranie',
                    'event_label' : location.href,
                    //'event_callback': function(){document.location = link;}
                }
            );
        });


        jQuery('.share .links a.social-share').on('click', function()
        {
            var media = jQuery(this).find("span").html();

            gtag('event', media,
                { 'event_category' : 'share', 'event_label' : location.href }
            );
        });

        jQuery('.post-content .external-link').on('click', function()
        {
            var link = jQuery(this).attr("href");
            var text = jQuery(this).text();
            gtag("event", "link_z_artykulu",
                {
                    "event_category" : "click",
                    "event_label" : link + " | " + text,
                    //"event_callback": function(){document.location = link;}
                }
            );
        });


    }

    function scrollTo(targetOrInt, margin, speed)
    {
        function isInt(n){ return Number(n) === n && n % 1 === 0; }
        function isjQuery(obj) { return (obj && (obj instanceof jQuery || obj.constructor.prototype.jquery)); }

        var target = targetOrInt;

        if (!isInt(target))
        {
            target = !isjQuery(target) ? jQuery(target).offset().top : target.offset().top;
        }

        margin = typeof margin !== "undefined" ? margin : 0;
        speed = typeof speed !== "undefined" ? speed : 750;

        if (navigator.userAgent.match(/(iPod|iPhone|iPad)/))
        {
            jQuery('body').animate(
            {
                scrollTop: target + margin
            }, speed);
        }
        else
        {
            jQuery('html, body').animate(
            {
                scrollTop: target + margin
            }, speed);
        }
    }

    function handleScrollToContent()
    {
        var scrollBtn = jQuery(".scroll-to-content");

        if (!scrollBtn.length)
            return;

        var content = jQuery(".main-content");

        if (!content.length)
            content = jQuery(".panel-layout");

        if (!content.length)
            return;

        scrollBtn.on("click", function()
        {
            scrollTo(content);
        })
    }

    function handleMageboxNavigation()
    {
        var megaboxes = jQuery(".megabox");

        if (!megaboxes.length)
            return;

        var autoPlay = false;
        var megaboxInterval;

        function toggleAutoPlay(megabox, turn_on)
        {
            turn_on = typeof turn_on !== "undefined" ? turn_on : true;

            if (turn_on)
            {
                clearInterval(megaboxInterval);
                megaboxInterval = setInterval(function()
                {
                    goToNextSlide(megabox, true);
                }, 3000);
            }
            else
            {
                clearInterval(megaboxInterval);
            }
        }

        function getActiveSlide(megabox)
        {
            return jQuery(".featured-posts-container .featured-post.active");
        }

        function getActiveSlideIndex(megabox)
        {
            return getActiveSlide(megabox).index();
        };

        function getSlideByIndex(megabox, index)
        {
            return jQuery(".featured-posts-container .featured-post", megabox).eq(index);
        };

        function updateActiveDotPosition(megabox)
        {
            jQuery(".dots .dot", megabox).removeClass("active")
                .eq(getActiveSlideIndex(megabox)).addClass("active");
        };

        function goToSlide(megabox, index)
        {
            if (getActiveSlideIndex(megabox) === index)
                return;

            var slides = jQuery(".featured-posts-container .featured-post", megabox);

            megabox.trigger( "ikalkulator:beforeMegaboxChange" );
            slides.removeClass("active");
            slides.eq(index).addClass("active");
            updateActiveDotPosition(megabox);
            megabox.trigger( "ikalkulator:afterSlideChange" );
        };

        function goToPrevSlide(megabox, loop)
        {
            loop = typeof loop !== "undefined" ? loop : true;
            var slides = jQuery(".featured-posts-container .featured-post", megabox);

            var activeIndex = getActiveSlideIndex(megabox);

            if (activeIndex >= 1 && getSlideByIndex(megabox, activeIndex - 1).length)
                goToSlide(megabox, activeIndex - 1);
            else
                if (loop)
                    goToSlide(megabox, slides.length - 1);
        };

        function goToNextSlide(megabox, loop)
        {
            loop = typeof loop !== "undefined" ? loop : true;
            var activeIndex = getActiveSlideIndex(megabox);

            if (getSlideByIndex(megabox, activeIndex + 1).length)
                goToSlide(megabox, activeIndex + 1);
            else
                if (loop)
                    goToSlide(megabox, 0);
        };

        megaboxes.each(function()
        {
            var megabox = jQuery(this);
            var dots = jQuery(".nav-panel .dot", megabox);
            var prev = jQuery(".nav-panel .prev", megabox);
            var next = jQuery(".nav-panel .next", megabox);

            prev.on("click", function()
            {
                goToPrevSlide(megabox, true);
            });

            next.on("click", function()
            {
                goToNextSlide(megabox, true);
            });

            dots.on("click", function(evnt)
            {
                var dotIndex =  jQuery(this).index();
                goToSlide(megabox, dotIndex);
            });

            if (autoPlay)
            {
                jQuery(".featured-posts-container, .nav-panel", megabox)
                    .on("mouseenter", function()
                    {
                        toggleAutoPlay(megabox, false);
                    })
                    .on("mouseleave", function()
                    {
                        toggleAutoPlay(megabox, true);
                    });

                toggleAutoPlay(megabox, true);
            }
            jQuery(".see-all .see-all-btn",megabox).on("click", function()
            {
                jQuery(".see-all",megabox).toggleClass("active");

            });

        });
    }

    function updatePostFloatingHeaderVisibility(value)
    {
        var floatingHeader = jQuery(".floating-header");

        if (value >= 0)
            floatingHeader.addClass("active");
        else
            floatingHeader.removeClass("active");
    }

    function updateProgressBar(progress, content, contentTop)
    {
        var winHeight = jQuery(window).innerHeight();

        var contentTopPadding = content.offset().top - contentTop;
        var contentHeight = contentTopPadding + content.outerHeight();
        var value = jQuery(window).scrollTop() - contentTop;
        progress.attr("max", contentHeight - winHeight);
        progress.attr("value", value);

        updatePostFloatingHeaderVisibility(value);

        return value;
    }

    function handleNewsletterScrollToResponse()
    {
        var resposeElement = jQuery(".mc4wp-response .mc4wp-alert");
        
        if (!resposeElement.length)
            return;
        
        if (resposeElement.html !== "")
        {
            scrollTo(resposeElement, -80);
        }
    }


    function handleReadingProgress()
    {
        var progress = jQuery(".reading-progress");
        var content = jQuery(".main-content .post-content");

        if (!progress.length || !content.length)
            return;

        var mainContent = content.closest(".main-content");

        var contentTop = mainContent.offset().top;
        updateProgressBar(progress, content, contentTop);

        jQuery(document).on("scroll", function()
        {
            contentTop = mainContent.offset().top;
            var value = jQuery(window).scrollTop() - contentTop;
            progress.attr("value",  value);
            updatePostFloatingHeaderVisibility(value);
        });

        jQuery(window).on("resize", function()
        {
            contentTop = mainContent.offset().top;
            updateProgressBar(progress, content, contentTop);
        });
    }
    
    function handleMobileSwapOrder()
    {
        var containers = jQuery(".mobile-swap");
        
        containers.each(function()
        {
            var current = jQuery(this);
            var elements = jQuery("> *", current);
            
            
            if (window.innerWidth < 768)
            {
                if (!current.hasClass("swapped"))
                {
                    elements.first().detach().insertAfter(elements.last());
                    current.addClass("swapped");
                }                
            }
            else
            {
                 if (current.hasClass("swapped"))
                {
                    elements.first().detach().insertAfter(elements.last());
                    current.removeClass("swapped");
                }                 
            }
        });
    }


    function disableImagesDragging()
    {
        jQuery(".post-list").on("mousedown", "img", function(event) { event.preventDefault(); });
    }
    
    function onResize()
    {	
        var isResizing  = false;

        jQuery(window).on("resize", function () 
        {
            if (isResizing) { return; }		
            isResizing = true;

            window.setTimeout(function()
            {
                //place functions here
                handleMobileSwapOrder();
                handleCategoryWithSidebarMinHeight();

                isResizing = false;
            }, 50 );
        });	
               
		jQuery(window).trigger("resize");
    }



    /* _________________________________________________________________________ public methods */

	var init = function()
	{
        handleMenu();
        handleMobileMenu();
        handleMageboxNavigation();
        handleScrollToContent();
        handleReadingProgress();
        handleSpotlightSearch();
        handleShareLinks();
        handleProductsGrid();
        handleAnalytics();
        handleListNumbering();
        handleCategoryWithSidebarMinHeight();
        
        checkElementsVisibilityOnScroll(".scroll-handle");
        disableImagesDragging();
        handleNewsletterScrollToResponse();

		onResize();
		jQuery(window).trigger("scroll");

	};

	init();
};

var ikalkulator;

jQuery(document).ready(function($)
{
	ikalkulator = new ikalkulator();
	product_filters = new productFilter();
	companies = new companies();
    searchbox = new searchbox();
    promobox = new promobox();
    comments = new comments();
});

class percentOfCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__percOf" action=""><div class="row">Oblicz <input type="text" class="ikalCalc__percOf__input1" name="ikalCalc__percOf__input1">% z <input type="text" class="ikalCalc__percOf__input2" name="ikalCalc__percOf__input2"></div><div class="row">Wynik: <input type="text" class="ikalCalc__percOf__output" name="ikalCalc__percOf__output" disabled></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.input1 = this.target.getElementsByClassName('ikalCalc__percOf__input1')[0];
    this.input2 = this.target.getElementsByClassName('ikalCalc__percOf__input2')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__percOf__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.input1.addEventListener('keyup', function() {
      self.result();
    });
    this.input2.addEventListener('keyup', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    let result = eval(( this.input1.value / 100 ) * this.input2.value);
    result =+ (Math.round(result + "e+6") + "e-6");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}

class whatPercentCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__whatPerc" action=""><div class="row"><input type="text" class="ikalCalc__whatPerc__input1" name="ikalCalc__whatPerc__input1"> jest jakim procentem z <input type="text" class="ikalCalc__whatPerc__input2" name="ikalCalc__whatPerc__input2"></div><div class="row">Wynik: <input type="text" class="ikalCalc__whatPerc__output" name="ikalCalc__whatPerc__output" disabled></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.input1 = this.target.getElementsByClassName('ikalCalc__whatPerc__input1')[0];
    this.input2 = this.target.getElementsByClassName('ikalCalc__whatPerc__input2')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__whatPerc__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.input1.addEventListener('keyup', function() {
      self.result();
    });
    this.input2.addEventListener('keyup', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    let result = eval( this.input1.value / (this.input2.value / 100) );
    result =+ (Math.round(result + "e+6") + "e-6");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}

class percentRaiseCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__percRaise" action=""><div class="row">O jaki procent wzrosła/zmalała liczba</div><div class="row">z <input type="text" class="ikalCalc__percRaise__input1" name="ikalCalc__percRaise__input1"> do <input type="text" class="ikalCalc__percRaise__input2" name="ikalCalc__percRaise__input2">?</div><div class="row">Wynik: <input type="text" class="ikalCalc__percRaise__output" name="ikalCalc__percRaise__output" disabled>%</div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.input1 = this.target.getElementsByClassName('ikalCalc__percRaise__input1')[0];
    this.input2 = this.target.getElementsByClassName('ikalCalc__percRaise__input2')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__percRaise__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.input1.addEventListener('keyup', function() {
      self.result();
    });
    this.input2.addEventListener('keyup', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    let result = eval( (this.input2.value * 100) / this.input1.value - 100);
    result =+ (Math.round(result + "e+6") + "e-6");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}


class percentAdditionCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__percAddition" action=""><div class="row">Dodaj procent do liczby</div><div class="row"><input type="text" class="ikalCalc__percAddition__input1" name="ikalCalc__percAddition__input1"> + <input type="text" class="ikalCalc__percAddition__input2" name="ikalCalc__percAddition__input2">%</div><div class="row">Wynik: <input type="text" class="ikalCalc__percAddition__output" name="ikalCalc__percAddition__output" disabled></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.input1 = this.target.getElementsByClassName('ikalCalc__percAddition__input1')[0];
    this.input2 = this.target.getElementsByClassName('ikalCalc__percAddition__input2')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__percAddition__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.input1.addEventListener('keyup', function() {
      self.result();
    });
    this.input2.addEventListener('keyup', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    let result = eval( this.input1.value * ( this.input2.value / 100 + 1 ) );
    result =+ (Math.round(result + "e+6") + "e-6");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}

class percentSubtractCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__percSubtract" action=""><div class="row">Odejmij procent od liczby</div><div class="row"><input type="text" min="0" max="100" class="ikalCalc__percSubtract__input1" name="ikalCalc__percSubtract__input1"> - <input type="text" class="ikalCalc__percSubtract__input2" name="ikalCalc__percSubtract__input2">%</div><div class="row">Wynik: <input type="text" class="ikalCalc__percSubtract__output" name="ikalCalc__percSubtract__output" disabled></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.input1 = this.target.getElementsByClassName('ikalCalc__percSubtract__input1')[0];
    this.input2 = this.target.getElementsByClassName('ikalCalc__percSubtract__input2')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__percSubtract__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.input1.addEventListener('keyup', function() {
      self.result();
    });
    this.input2.addEventListener('keyup', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    let result = eval( this.input1.value * ( 1 - this.input2.value / 100 ) );
    result =+ (Math.round(result + "e+6") + "e-6");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}

class compoundInterestCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__compInterest" action=""><div class="row"><span>Kapitał początkowy</span> <input type="text" class="ikalCalc__compInterest__initBalance" name="ikalCalc__compInterest__initBalance"></div><div class="row"><span>Oprocentowanie</span> <input type="text" class="ikalCalc__compInterest__intRate" name="ikalCalc__compInterest__intRate"></div><div class="row"><span>Kapitalizacja</span> <select class="ikalCalc__compInterest__compFreq" name="ikalCalc__compInterest__compFreq"><option value="365">dzienna</option><option value="52">tygodniowa</option><option value="12">miesięczna</option><option value="4">kwartalna</option><option value="2">półroczna</option><option value="1">roczna</option></select></div><div class="row"><span>Liczba lat</span> <input type="text" class="ikalCalc__compInterest__years" name="ikalCalc__compInterest__years"></div><div class="row"><span>Kapitał końcowy</span> <input type="text" class="ikalCalc__compInterest__output" name="ikalCalc__compInterest__output" disabled></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.initBalance = this.target.getElementsByClassName('ikalCalc__compInterest__initBalance')[0];
    this.intRate = this.target.getElementsByClassName('ikalCalc__compInterest__intRate')[0];
    this.compFreq = this.target.getElementsByClassName('ikalCalc__compInterest__compFreq')[0];
    this.years = this.target.getElementsByClassName('ikalCalc__compInterest__years')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__compInterest__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.initBalance.addEventListener('keyup', function() {
      self.result();
    });

    this.intRate.addEventListener('keyup', function() {
      self.result();
    });

    this.compFreq.addEventListener('change', function() {
      self.result();
    });

    this.years.addEventListener('keyup', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    const initBalance = this.initBalance.value;
    const intRate = this.intRate.value / 100;
    const compFreq = this.compFreq.value;
    const years = this.years.value;

    let calc1 = intRate / compFreq;
    let calc2 = 1 + calc1;
    let calc3 = compFreq * years;
    let calc4 = Math.pow(calc2, calc3);
    let calc5 = initBalance * calc4;

    let result = eval( calc5 );
    result =+ (Math.round(result + "e+2") + "e-2");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}


class investmentCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    console.log(this.targetID);
    this.target = document.getElementById(targetId);
    console.log(this.target);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__investment" action=""><div class="row"><span>Kapitał początkowy</span><input type="text" class="ikalCalc__investment__initBalance" name="ikalCalc__investment__initBalance"></div><div class="row"><span>Okres umowy</span><input type="text" class="ikalCalc__investment__time__value" name="ikalCalc__investment__time__value"><select class="ikalCalc__investment__time__unit" name="ikalCalc__investment__time__unit"><option value="days">dni</option><option value="months">miesiące</option><option value="years">lata</option></select></div><div class="row"><span>Oprocentowanie</span><input type="text" class="ikalCalc__investment__intRate" name="ikalCalc__investment__intRate"></div><div class="row"><span>Kapitalizacja</span><select class="ikalCalc__investment__compFreq" name="ikalCalc__investment__compFreq"><option value="yearly">roczna</option><option value="monthly">miesięczna</option><option value="daily">dzienna</option><option value="overall">na koniec okresu</option></select></div><div class="row"><div class="checkbox"><input type="checkbox" class="ikalCalc__investment__tax" id="ikalCalc__investment__tax" name="ikalCalc__investment__tax"><label for="ikalCalc__investment__tax">Uwzględnij podatek</label></div></div><div class="row"><label id="ikalCalc__investment__output">Kapitał końcowy</label><input type="text" class="ikalCalc__investment__output" name="ikalCalc__investment__output"></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.initBalance = this.target.getElementsByClassName('ikalCalc__investment__initBalance')[0];
    this.timeValue = this.target.getElementsByClassName('ikalCalc__investment__time__value')[0];
    this.timeUnit = this.target.getElementsByClassName('ikalCalc__investment__time__unit')[0];
    this.intRate = this.target.getElementsByClassName('ikalCalc__investment__intRate')[0];
    this.compFreq = this.target.getElementsByClassName('ikalCalc__investment__compFreq')[0];
    this.tax = this.target.getElementsByClassName('ikalCalc__investment__tax')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__investment__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.initBalance.addEventListener('keyup', function() {
      self.result();
    });

    this.timeValue.addEventListener('keyup', function() {
      self.result();
    });

    this.timeUnit.addEventListener('change', function() {
      self.result();
    });

    this.intRate.addEventListener('keyup', function() {
      self.result();
    });

    this.compFreq.addEventListener('change', function() {
      self.result();
    });

    this.tax.addEventListener('change', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    const initBalance = this.initBalance.value;

    let time = this.timeValue.value;
    const timeUnit = this.timeUnit.value;
    switch (timeUnit) {
      case 'days':
        time = time / 365;
        break;
      case 'months':
        time = time / 12;
        break;
    }

    const intRate = this.intRate.value*0.01;
    let compFreq = this.compFreq.value;
    switch(compFreq) {
      case 'yearly':
        compFreq = 1;
        break;
      case 'monthly':
        compFreq = 12;
        break;
      case 'daily':
        compFreq = 365;
        break;
      case 'overall':
        compFreq = 1/time;
        break;
    }

    const tax = this.tax.checked;
    let taxed = 0;
    let calc1 = 1 + intRate / compFreq;
    let calc2 = compFreq * time;
    let calc3 = Math.pow(calc1, calc2);
    let calc4 = initBalance * calc3;

    if(tax) {
      taxed =  ((calc4 - initBalance) * 0.19);
    }

    let result = eval( calc4 - taxed );
    result =+ (Math.round(result + "e+2") + "e-2");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}

class regularSavingsCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__regSavings" action=""><div class="row"><span>Kwota regularnych składek</span><input type="text" class="ikalCalc__regSavings__amount" name="ikalCalc__regSavings__amount"></div><div class="row"><span>Częstotliwość</span><select class="ikalCalc__regSavings__freq" name="ikalCalc__regSavings__freq"><option value="12">Miesięcznie</option><option value="1">Rocznie</option></select></div><div class="row"><span>Okres oszczędzania</span><input type="text" class="ikalCalc__regSavings__years" name="ikalCalc__regSavings__years"> lat</div><div class="row"><span>Stopa zwrotu</span><input type="text" class="ikalCalc__regSavings__intRate" name="ikalCalc__regSavings__intRate"></div><div class="row"><span>Kapitalizacja</span><select class="ikalCalc__regSavings__compFreq" name="ikalCalc__regSavings__compFreq"><option value="12">Miesięcznie</option><option value="1">Rocznie</option></select></div><div class="row"><span>Inflacja</span><input type="text" class="ikalCalc__regSavings__inflation" name="ikalCalc__regSavings__inflation"></div><div class="row"><div class="checkbox"><input type="checkbox" class="ikalCalc__regSavings__tax" id="ikalCalc__regSavings__tax" name="ikalCalc__regSavings__tax"><label for="ikalCalc__regSavings__tax">Uwzględnij podatek</label></div></div><div class="row"><span>Wynik</span><input type="text" class="ikalCalc__regSavings__output" name="ikalCalc__regSavings__output" disabled=""></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.amount = this.target.getElementsByClassName('ikalCalc__regSavings__amount')[0];
    this.freq = this.target.getElementsByClassName('ikalCalc__regSavings__freq')[0];
    this.years = this.target.getElementsByClassName('ikalCalc__regSavings__years')[0];
    this.intRate = this.target.getElementsByClassName('ikalCalc__regSavings__intRate')[0];
    this.compFreq = this.target.getElementsByClassName('ikalCalc__regSavings__compFreq')[0];
    this.inflation = this.target.getElementsByClassName('ikalCalc__regSavings__inflation')[0];
    this.tax = this.target.getElementsByClassName('ikalCalc__regSavings__tax')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__regSavings__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.amount.addEventListener('keyup', function() {
      self.result();
    });

    this.freq.addEventListener('change', function() {
      self.result();
    });

    this.years.addEventListener('keyup', function() {
      self.result();
    });

    this.intRate.addEventListener('keyup', function() {
      self.result();
    });

    this.compFreq.addEventListener('change', function() {
      self.result();
    });

    this.inflation.addEventListener('keyup', function() {
      self.result();
    });

    this.tax.addEventListener('change', function() {
      self.result();
    });

    this.output.addEventListener('keyup', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if(input.classList.contains('ikalCalc__regSavings__inflation')) {
        if (evt.which < 45 || ( evt.which > 46 && evt.which < 48) || evt.which > 57) {
          evt.preventDefault();
        }
      } else {
        if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
          evt.preventDefault();
        }
      }
    });
  }
  result() {
    const amount = eval(this.amount.value);
    const freq = eval(this.freq.value);
    const years = eval(this.years.value);
    const intRate = eval(this.intRate.value) * 0.01;
    const compFreq = eval(this.compFreq.value);
    const inflation = eval(this.inflation.value) * 0.01;
    const tax = this.tax.checked;

    let subscore = 0;

    if (compFreq == freq) {
      const subscore1 = 1 + intRate / compFreq;
      const subscore2 = Math.pow(subscore1, (compFreq * years)) - 1;
      const subscore3 = intRate / compFreq;
      subscore = amount * subscore1 * (subscore2 / subscore3);
    } else if (compFreq < freq) {
      const subscore1 = (freq / compFreq) + (((freq / compFreq - 1) * (intRate / compFreq)) / 2);
      const subscore2 = (Math.pow((1 + intRate / compFreq), (compFreq * years)) - 1) / (intRate / compFreq);
      subscore = amount * subscore1 * subscore2;
    } else {
      const subscore1 = Math.pow((1 + intRate / compFreq), (compFreq * years)) - 1;
      const subscore2 = Math.pow((1 + intRate / compFreq), (compFreq / freq)) - 1;
      subscore = amount * (subscore1 / subscore2);
    }

    const savedAmount = amount * freq * years;
    const diffence = subscore - savedAmount;

    let taxAmount = 0;
    if (tax) {
      taxAmount = diffence * 0.19;
    }

    let inflationAmount = 0;
    if (inflation && inflation != 0) {
      inflationAmount = subscore * inflation;
    }

    let result  = eval(subscore - taxAmount - inflationAmount);
    result =+ (Math.round(result + "e+2") + "e-2");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}


class regularSavingsByAmountCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__regSavingsByAmount" action=""><div class="row"><span>Cel</span><input type="text" class="ikalCalc__regSavingsByAmount__goal" name="ikalCalc__regSavings__goal"></div><div class="row"><span>Kwota regularnych składek</span><input type="text" class="ikalCalc__regSavingsByAmount__amount" name="ikalCalc__regSavingsByAmount__amount"></div><div class="row"><span>Częstotliwość</span><select class="ikalCalc__regSavingsByAmount__freq" name="ikalCalc__regSavingsByAmount__freq"><option value="12">Miesięcznie</option><option value="4">Kwartalnie</option><option value="1">Rocznie</option></select></div><div class="row"><span>Stopa zwrotu</span><input type="text" class="ikalCalc__regSavingsByAmount__intRate" name="ikalCalc__regSavingsByAmount__intRate"></div><div class="row"><span>Kapitalizacja</span><select class="ikalCalc__regSavingsByAmount__compFreq" name="ikalCalc__regSavingsByAmount__compFreq"><option value="12">Miesięcznie</option><option value="1">Rocznie</option></select></div><div class="row"><div class="ikalCalc__regSavingsByAmount__output"></div></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.goal = this.target.getElementsByClassName('ikalCalc__regSavingsByAmount__goal')[0];
    this.amount = this.target.getElementsByClassName('ikalCalc__regSavingsByAmount__amount')[0];
    this.freq = this.target.getElementsByClassName('ikalCalc__regSavingsByAmount__freq')[0];
    this.intRate = this.target.getElementsByClassName('ikalCalc__regSavingsByAmount__intRate')[0];
    this.compFreq = this.target.getElementsByClassName('ikalCalc__regSavingsByAmount__compFreq')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__regSavingsByAmount__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.goal.addEventListener('keyup', function() {
      self.result();
    });

    this.amount.addEventListener('keyup', function() {
      self.result();
    });

    this.freq.addEventListener('change', function() {
      self.result();
    });

    this.intRate.addEventListener('keyup', function() {
      self.result();
    });

    this.compFreq.addEventListener('change', function() {
      self.result();
    });

  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    const goal = eval(this.goal.value);
    const amount = eval(this.amount.value);
    const freq = eval(this.freq.value);
    const intRate = eval(this.intRate.value) * 0.01;
    const compFreq = eval(this.compFreq.value);

    let subscore = 0;
    let years = 0;

    if (compFreq == freq) {
      while(subscore < goal) {
        console.log('compfreq==freq years: ' + years + ' score: ' + subscore);
        years++;
        const subscore1 = 1 + intRate / compFreq;
        const subscore2 = Math.pow(subscore1, (compFreq * years)) - 1;
        const subscore3 = intRate / compFreq;
        subscore = amount * subscore1 * (subscore2 / subscore3);
      }
    } else if (compFreq < freq) {
      while(subscore < goal) {
        console.log('compfreq<freq years: ' + years + ' score: ' + subscore);
        years++;
        const subscore1 = (freq / compFreq) + (((freq / compFreq - 1) * (intRate / compFreq)) / 2);
        const subscore2 = (Math.pow((1 + intRate / compFreq), (compFreq * years)) - 1) / (intRate / compFreq);
        subscore = amount * subscore1 * subscore2;
      }
    } else {
      while(subscore < goal) {
        console.log('compfreq>freq years: ' + years + ' score: ' + subscore);
        years++;
        const subscore1 = Math.pow((1 + intRate / compFreq), (compFreq * years)) - 1;
        console.log('subscore1 ' + subscore1);
        const subscore2 = Math.pow((1 + intRate / compFreq), (compFreq / freq)) - 1;
        console.log('subscore2 ' + subscore2);
        subscore = amount * (subscore1 / subscore2);
      }
    }
    // let result = 'Musisz zbierać przez ' + years + ' lat. Uzbierana kwota będzie równa ' + subscore;
    let result  = eval(subscore);
    result =+ (Math.round(result + "e+2") + "e-2");
    if(!isNaN(result)) {
      if(years == 1) {
        this.output.innerHTML = 'Musisz zbierać przez 1 rok. Uzbierana kwota będzie wynosić ' + result;
      } else if (years > 1 && years < 5) {
        this.output.innerHTML = 'Musisz zbierać przez ' + years + ' lata. Uzbierana kwota będzie wynosić ' + result;
      } else {
        this.output.innerHTML = 'Musisz zbierać przez ' + years + ' lat. Uzbierana kwota będzie wynosić ' + result;
      }
    }
  }
}

comments = function ()
{
	var applySwitcher = function()
	{
		jQuery(".comments").each( function ()
		{
			var commentBox = jQuery(this);

			jQuery(".show-action button", commentBox).on("click", function()
			{
				jQuery(".comments-content", commentBox).stop().slideToggle();
				commentBox.toggleClass("visible");
			})
		})

	}

	var scrollToComments = function()
	{
		if(location.href.indexOf("replytocom=") > -1 || location.href.indexOf("#comment") > -1)
		{
			jQuery(".comments .comments-content").show();
			jQuery(".comments").addClass("visible");

			if(location.hash)
			{
				if(jQuery(location.hash).length)
				{
					jQuery("html,body").animate({ scrollTop: jQuery(location.hash).offset().top - 100})
				}
			}
		}
	}

	var handleDoRateRelease = function()
	{
		jQuery("#doRate").on("click", function() {
			jQuery(".comments .comments-content").show();
			jQuery(".comments").addClass("visible");
			jQuery("html,body").animate({ scrollTop: jQuery("#respond").offset().top - 100})
		});
	}

	var drawStars = function()
	{
		jQuery(".star-rating").each( function() {
			if(jQuery(this).data("rating"))
			{
				var rating = parseFloat(jQuery(this).data("rating"));
				var fulls = Math.floor(rating);
				for(var i = 0; i < fulls;i ++)
				{
					jQuery(this).find(".stars").append('<span class="full"></span>');
				}
				if(fulls < 5)
				{
					var fract = Math.round((rating - fulls) * 100);
					if(fract > 0) jQuery(this).find(".stars").append('<span class="part"><span style="width: ' + (fract+10) + '%"></span></span>');
					else jQuery(this).find(".stars").append('<span class="empty"></span>');
					fulls++;
					while(fulls < 5)
					{
						jQuery(this).find(".stars").append('<span class="empty"></span>');
						fulls++;
					}
				}
			}
		});
	}

	var handleRateInForm = function()
	{
		jQuery(".star-rating.selectable").each( function()
		{
			var rateContainer = jQuery(this);
			jQuery(".stars > span", rateContainer).each( function() {
				jQuery(this).on("mouseenter", function() {
					var selectedIndex = jQuery(this).index();
					jQuery(".stars > span",rateContainer).each( function() {
						if(jQuery(this).index() <= selectedIndex)
							jQuery(this).removeClass().addClass("full");
						else
							jQuery(this).removeClass().addClass("empty");
					});
				});

				jQuery(this).on("click", function() {
					var selectedIndex = jQuery(this).index();
					var selectedValue = selectedIndex + 1;
					rateContainer.data('rating', selectedIndex);
					jQuery('input[name=rating]').val(selectedValue);
				});

			});

			rateContainer.on("mouseleave", function() {
				var selectedIndex = jQuery(this).data('rating');
				jQuery(".stars > span",rateContainer).each( function() {
					if(jQuery(this).index() <= selectedIndex)
						jQuery(this).removeClass().addClass("full");
					else
						jQuery(this).removeClass().addClass("empty");
				});
			});


		})
	}

	var handleThumbs = function()
	{
		jQuery(".thumbs > button").on("click", function()
		{
			if(!jQuery(this).hasClass("locked-active") && !jQuery(this).hasClass("locked-inactive"))
			{
				jQuery(this).parent().find("button").fadeOut( function() {
					jQuery(this).fadeIn("fast");
				})
				jQuery(this).parent().find("button").addClass("locked-inactive")
				jQuery(this).addClass("active").removeClass("locked-inactive").addClass("locked-active")

				var dir = "down"
				if(jQuery(this).hasClass("up")) dir = "up";
				var postId = jQuery(this).closest(".thumbvote").data("postid");
				jQuery.post("/wp-json/ikal/v1/post/vote", {"post": postId, "dir": dir});
			}
		})
	}


	/* _________________________________________________________________________ public methods */

	var init = function()
	{
		handleDoRateRelease();
		scrollToComments();
		applySwitcher();
		drawStars();
		handleRateInForm();
		handleThumbs();
	};

	init();
};

var comments;


/*
 *	Dostosowana natywna fukcja wordpressa (wp-includes/js/comment-reply.js)
 */
var addComment = {
  /**
   * @param {string} commId The comment ID.
   * @param {string} parentId The parent ID.
   * @param {string} respondId The respond ID.
   * @param {string} postId The post ID.
   * @returns {boolean} Always returns false.
   */
  moveForm: function( commId, parentId, respondId, postId ) {
  	var cancel = jQuery('#cancel-comment-reply-link');
    cancel.on('click', function() {
      jQuery("#cancel-comment-reply-link .reply-to-author").remove();
      jQuery("#cancel-comment-reply-link").hide();
      jQuery('#comment_parent').val(null);
      jQuery('.comment-form-rating').show();
      jQuery('#rating').val(4);
      return false;
    })

  	var author = jQuery('#'+commId).find('.comment-author b').html();
    jQuery("html,body").animate({ scrollTop: jQuery("#respond").offset().top - 100});
    jQuery("#cancel-comment-reply-link").html(
    	jQuery("#cancel-comment-reply-link").text() + '<span class="reply-to-author"> użytkownikowi ' + author + '</span>'
		);
    jQuery("#cancel-comment-reply-link").show();

    jQuery('.comment-form-rating').hide();
    jQuery('#rating').val(null);

    jQuery('#comment_parent').val(parentId);

    return false;
  }
}


companies = function ()
{   
	function handleFixedFilter()
	{
        var pageHead =  jQuery(".page-head");
        var pageContent =  jQuery(".page-content");
        var filter =  jQuery(".filters");
        
        if (!filter.length)
            return;

		var switchAt = pageHead.offset().top + pageHead.outerHeight();

    
        if (switchAt < jQuery(window).scrollTop() && window.innerWidth >= 768)
        {
            if (!filter.hasClass("fixed"))
            {
                var diff = filter.outerHeight() -  (switchAt - filter.offset().top);
                pageContent
                    .css({
                        "padding-top" : diff + "px"
                    });
            }

            filter
                .addClass("fixed");
        }
        else
        {
            filter.removeClass("fixed");
            pageContent
                .css({
                    "padding-top" : ""
                });            
        }
    }    
    
    
    function filterGrid()
    {
        var filterItems = jQuery(".companies .filters .items-list .category");
        var gridItems = jQuery(".companies .tiles-grid .grid-item");
                
        if (!filterItems.length || !gridItems.length) { return; }
                
        var selectedCategories = [];
        
        filterItems.each(function()
        {
            currentItem = jQuery(this);            
            
            if (currentItem.hasClass("active"))
            {
                if (currentItem.attr("data-filter") == "all")
                {
                    selectedCategories = "all";
                    return false;
                }
                else
                {
                    selectedCategories.push(currentItem.attr("data-filter"));
                }
            }
        });
        
        if (selectedCategories == "all")
        {
            gridItems.addClass("active");
        }
        else
        {
            gridItems.each(function()
            {
                currentItem = jQuery(this);
                var categories = currentItem.attr("data-item-categories");

                var itemCategories = categories.split(",");
                
                var containAtLeastOneFilter = selectedCategories.some(function (v) { return itemCategories.indexOf(v) >= 0;} );

                if (containAtLeastOneFilter)
                    currentItem.addClass("active");
                else
                    currentItem.removeClass("active");

            });
        }
    }  
    
    
    
    function handleFilter(isConditionOR)
    {
        isConditionOR = typeof isConditionOR !== "undefined" ? isConditionOR : false;
        
        var filterItems = jQuery(".companies .filters .items-list .category");
        
        if (!filterItems.length) { return; }
        
        filterItems.on("click", function(e)
        {
            currentItem = jQuery(this);
            cirrentItemFilter = currentItem.attr("data-filter");        
                  
            if (cirrentItemFilter == "all")
            {
                if (currentItem.hasClass("active")) 
                { 
                    return;
                }
                else
                {
                    filterItems.removeClass("active");
                    currentItem.addClass("active");
                }
            }
            else
            {
                if (isConditionOR) //only last selected
                    filterItems.removeClass("active");
                else //all selected excepted "all"
                    filterItems.filter('[data-filter="all"]').removeClass("active"); 
                
                if (currentItem.hasClass("active"))
                {
                    if (filterItems.filter(".active").length > 1)
                    {
                        currentItem.removeClass("active");
                    }
                }
                else
                {
                    currentItem.addClass("active");                    
                }
            }
            
            filterGrid();
        });
        
        filterGrid();
    }    
    
/*
	function onScroll()
	{	
		var isScrolling  = false;

		jQuery(window).on("scroll", function () 
		{
			if (isScrolling) { return; }

			isScrolling = true;

			window.setTimeout(function()
			{
				//place functions here	
                handleFixedFilter();

				isScrolling = false;
			}, 10 );
		});
        
        jQuery(window).trigger("scroll");        
	}	
   
    
    function onResize()
    {	
        var isResizing  = false;

        jQuery(window).on("resize", function () 
        {
            if (isResizing) { return; }		
            isResizing = true;

            window.setTimeout(function()
            {
                //place functions here	
                handleFixedFilter();

                isResizing = false;
            }, 10 );
        });	
               
		jQuery(window).trigger("resize");
    }    
*/

	var init = function()
	{
        if (!jQuery(".page-main.companies").length) { return; }
        
		handleFilter(true);
        onScroll();
        onResize();

	};

	init();
};

var companies;


function countdown(targetId) {
  const target = document.getElementById(targetId);
  let days, hours, minutes, seconds;

  let startDate = target.getAttribute('data-start');
  startDate = new Date(startDate).getTime();

  let endDate = target.getAttribute('data-end');
  endDate = new Date(endDate).getTime();

  let nowDate = new Date();
  nowDate.getTime();

  let beforeMsg = target.getAttribute('data-before-msg');
  let whileMsg = target.getAttribute('data-while-msg');
  let afterMsg = target.getAttribute('data-after-msg');

  target.innerHTML = '<div class="ikal__countdown"><small class="msg"></small><br><div class="ikal__countdown__cell"><span class="ikal__countdown__days"></span> Dni</div><div class="ikal__countdown__cell"><span class="ikal__countdown__hours"></span> Godzin</div><div class="ikal__countdown__cell"><span class="ikal__countdown__minutes"></span> Minut</div><div class="ikal__countdown__cell"><span class="ikal__countdown__seconds"></span> Sekund</div></div>';

  function check() {
    if((!isNaN(startDate)) && (startDate - nowDate > 0)) {
      // obliczanie czasu przed rozpoczęciem wydarzenia
      target.getElementsByClassName("msg")[0].innerHTML = beforeMsg;
      calculate(startDate);
    } else if ((!isNaN(endDate)) && (endDate - nowDate > 0)) {
      // obliczanie czasu w trakcie wydarzenia
      target.getElementsByClassName("msg")[0].innerHTML = whileMsg;
      calculate(endDate);
    } else {
      // informacja po zakończeniu wydarzenia
      if(afterMsg) {
        target.innerHTML = '<p class="ikal__countdown"><small class="msg">' + afterMsg + '</small></p>';
      } else {
        target.innerHTML = '';
      }
      clearInterval(interval);
      return;
    }
  }

  const interval = setInterval(function() {
    nowDate.getTime();
    check();
  }, 1000);

  function calculate(calculatedDate) {
    nowDate = new Date();
    nowDate.getTime();

    let timeRemaining = parseInt((calculatedDate - nowDate) / 1000);

    if (timeRemaining >= 0) {
      days = parseInt(timeRemaining / 86400);
      timeRemaining = (timeRemaining % 86400);

      hours = parseInt(timeRemaining / 3600);
      timeRemaining = (timeRemaining % 3600);

      minutes = parseInt(timeRemaining / 60);
      timeRemaining = (timeRemaining % 60);

      seconds = parseInt(timeRemaining);

      target.getElementsByClassName("ikal__countdown__days")[0].innerHTML = parseInt(days, 10);
      target.getElementsByClassName("ikal__countdown__hours")[0].innerHTML = ("0" + hours).slice(-2);
      target.getElementsByClassName("ikal__countdown__minutes")[0].innerHTML = ("0" + minutes).slice(-2);
      target.getElementsByClassName("ikal__countdown__seconds")[0].innerHTML = ("0" + seconds).slice(-2);
    } else {
      return;
    }
  }
}

// (function () {
//   countdown('04/01/2333 05:00:00 PM');
// }());

productFilter = function ()
{
	var sortAttribute = null;
	var sortType = null;
	var sortDirection = null;

	var applySliders = function()
	{
		jQuery(".slider[data-filter-name]").each( function()
		{
			var options = {};
			var sliderContainer = jQuery(this);

			if(jQuery(this).data("filter-type") == "between")
			{
				options.range = true;
				options.values = [];

				if(typeof(jQuery(this).data("value-min")) != "undefined" && jQuery(this).data("value-min") >= 0 && jQuery(this).data("value-max"))
				{
					options.values.push(jQuery(this).data("value-min"));
					options.values.push(jQuery(this).data("value-max"));
				};
			}
			else if(jQuery(this).data("filter-type") == "to")
			{
				options.range = "min";

				if(jQuery(this).data("value"))
				{
					options.value = jQuery(this).data("value");
				};
			};

			if(jQuery(this).data("min") != null)
			{
				options.min = jQuery(this).data("min");
			};

			if(jQuery(this).data("max"))
			{
				options.max = jQuery(this).data("max");
			};



			var pointsValues = [];

			if(jQuery(this).data("step"))
			{

				var stepVal = jQuery(this).data("step");
				var length = Math.ceil((options.max-options.min)/stepVal);
				var start = Math.floor(options.min/stepVal);
				for(var i = start; i <length+start; i++)
				{
					if(i == start && options.min < i*stepVal)
					{
						pointsValues.push(options.min);
					}
					pointsValues.push(i*stepVal)
					if(i == length+start-1 && options.max > i*stepVal)
					{
						pointsValues.push(options.max);
					}
				}


				if(pointsValues.length)
				{
					options.min = 0;
					options.max = pointsValues.length-1;

					if(options.value)
					{
						options.value = pointsValues.indexOf(options.value);
					}

					if(options.values)
					{
						options.values[0] = pointsValues.indexOf(options.values[0]);
						options.values[1] = pointsValues.indexOf(options.values[1]);
					}
				}

			}


			if(jQuery(this).data("progressive") == true)
			{
				var rangesConfig =
				{
					default:
					[
						{"min": 0, "max": 900, "step": 100},
						{"min": 1000, "max": 9000, "step": 1000},
						{"min": 10000, "max": 48000, "step": 2000},
						{"min": 50000, "max": 50000000, "step": 5000},
					],
					investments: [
						{"min": 0, "max": 5000, "step": 500},
						{"min": 6000, "max": 19000, "step": 1000},
						{"min": 20000, "max": 95000, "step": 5000},
						{"min": 100000, "max": 475000, "step": 25000},
						{"min": 500000, "max": 10000000, "step": 50000},
					],
					investmentsMonths:
					[
						{"min": 0, "max": 23, "step": 1},
						{"min": 24, "max": 600, "step": 12}
					]
				};

				var ranges = rangesConfig.default;

				if(jQuery(this).data("progressive-values") && rangesConfig[jQuery(this).data("progressive-values")])
					ranges = rangesConfig[jQuery(this).data("progressive-values")];

				for(i=0; i < ranges.length; i++)
				{
					var current = 0;
					if(options.min > ranges[i].max || options.max < ranges[i].min)
					{
						continue;
					}
					else
					{
						current = ranges[i].min;
						while(current < options.min)
						{
							current += ranges[i].step;
						}

						if(options.min > ranges[i].min && current-ranges[i].step < options.min)
						{
							pointsValues.push(options.min);
						}

						while(current <= options.max && current <= ranges[i].max)
						{
							pointsValues.push(current);
							current += ranges[i].step;
						}
					}
				}


				if(pointsValues.length)
				{
					options.min = 0;
					options.max = pointsValues.length-1;

					if(options.value)
					{
						options.value = pointsValues.indexOf(options.value);
					}

					if(options.values)
					{
						options.values[0] = pointsValues.indexOf(options.values[0]);
						options.values[1] = pointsValues.indexOf(options.values[1]);
					}
				}


			}

			options.slide = function( event, ui )
			{
				if(ui.value || pointsValues[ui.value])
				{
					var exVal = ui.value;

					if(pointsValues.length)
					{
						exVal = pointsValues[ui.value];
					}

					sliderContainer.find(".value").html( formatNumber(exVal) );
					sliderContainer.parents("form").first().find("input[name='" + sliderContainer.data("filter-name") + "']").val(exVal)
				}
				if(ui.values)
				{
					var exValMin = ui.values[0];
					var exValMax = ui.values[1];

					if(pointsValues.length)
					{
						exValMin = pointsValues[ui.values[0]];
						exValMax = pointsValues[ui.values[1]];
					}

					sliderContainer.find(".value").html( formatNumber(exValMin) + " - " + formatNumber(exValMax) );
					sliderContainer.parents("form").first().find("input[name='min-" + sliderContainer.data("filter-name") + "']").val(exValMin)
					sliderContainer.parents("form").first().find("input[name='max-" + sliderContainer.data("filter-name") + "']").val(exValMax)
				}
			}

			options.change = function( event, ui )
			{
				sliderContainer.parents("form").first().trigger("ikal:filter:changed");
			}

			if(pointsValues)
			{
				if(pointsValues[options.min])
					sliderContainer.find(".range-min-value").html(pointsValues[options.min]);
				else
					sliderContainer.find(".range-min-value").html(pointsValues[(options.min + 1)] );

				sliderContainer.find(".range-max-value").html(pointsValues[options.max]);

			}
			else
			{
				sliderContainer.find(".range-min-value").html(options.min);
				sliderContainer.find(".range-max-value").html(options.max);

			}
			sliderContainer.find(".slider-bar").slider(options);
		});

	}

	var applyInputs = function()
	{
		jQuery(".filters form").each( function()
		{
			var filterForm = jQuery(this);
			jQuery("input[type=checkbox], input[type=radio], input[type=text]", filterForm).on("change", function() {
				filterForm.trigger("ikal:filter:changed");
			})
		});
	}

	var prepareForms = function()
	{
		jQuery(".filters form").each( function()
		{
			var filterForm = jQuery(this);

			filterForm.on("ikal:filter:changed", function()
			{
				var list = filterForm.parents(".product-list").first().find(".products.list");
				list.addClass("loading");
				if(jQuery("#page").data("product-slug"))
				{
					var url = "/wp-content/themes/ikalkulator/product-api/products.php?ptype=" + jQuery("#page").data("product-slug") + "&" + filterForm.serialize();
					jQuery.get(url, function(response)
					{
						list.first().html(response);
						refreshSorting(list);
						list.removeClass("loading");
					})
				}
			});
		});
	}

	var applySorting = function()
	{
		jQuery(".products.list").each( function()
		{
			var list = jQuery(this);
			var header = list.parent().find(".products.header");
			jQuery("ul li div.sortable a", header)
                .append('<span class="arrow"></span>')
                .on("click", jQuery.proxy(sortList, this, list));
		})
	}

	var sortList = function(list, event)
	{
		event.preventDefault();

		var sortby = jQuery(event.target).data("sort-by");
		var sorttyp = jQuery(event.target).data("sort-type");
		var sortdir = jQuery(event.target).data("sort-dir");

		if(sortAttribute == sortby)
		{
			if(sortDirection != "desc")
				sortDirection = "desc";
			else
				sortDirection = "asc";
		}
		else
		{
			if(sortdir == "desc")
				sortDirection = "desc";
			else
				sortDirection = "asc";
		}

		jQuery("div.sortable").removeClass("desc asc");

        jQuery(event.target).closest("div.sortable")
            .addClass(sortDirection);

		sortType = sorttyp;
		sortAttribute = sortby;

		jQuery(".sorted-field").removeClass("sorted-field");
		jQuery("." + sortAttribute).addClass("sorted-field");

		refreshSorting(list);
	}

	var refreshSorting = function(list)
	{
		list.find(" > ul > li").sort(jQuery.proxy(sorter, this, sortAttribute, sortType, sortDirection)).appendTo(list.find(" > ul"));
	}

	var sorter = function(sortby, sorttype, sortdir, a, b)
	{
		if(sorttype == "number")
		{
			if(sortdir == "asc") return (parseFloat(jQuery(b).closest("li").data(sortby)) < parseFloat(jQuery(a).closest("li").data(sortby))) ? 1 : -1;
			else return (parseFloat(jQuery(a).closest("li").data(sortby)) < parseFloat(jQuery(b).closest("li").data(sortby))) ? 1 : -1;
		}
	}

	var handleDetails = function()
	{
		jQuery(document).on("click", ".product-list .products.list ul li .more .expander", function()
		{
			jQuery(this).toggleClass("unfolded")
			jQuery(this).parent().find(".expanded").slideToggle();
		})
	}

	var handleOptional = function()
	{
		jQuery(".filters form .show-all").each( function()
		{
				var moveDownHeader = function()
				{
					if(jQuery(".container.products.header").hasClass("fixed"))
					{
						jQuery(".container.products.header").css("top", jQuery(".product-list .filters.container").outerHeight());
					}
				}
				var showAllBtn = jQuery(this);
				jQuery(showAllBtn).on("click", function() {
				showAllBtn.toggleClass("active");
				setInterval( moveDownHeader, 20);
				showAllBtn.closest("form").find(".togglable").slideToggle( function() {
					clearInterval( moveDownHeader );
					jQuery(window).trigger("scroll touchmove");
				});
			})
		});
	}

	var handleTooltip = function()
	{
		if(jQuery(".tooltip").length)
		{
			jQuery(window).on("click", function() {
				jQuery(".tooltip").css("z-index", "").find(".content").stop().slideUp();
			});

			jQuery(".tooltip .show").on("click", function(e)
			{
				e.stopPropagation();
				jQuery(".products .tooltip").css("z-index", "").find(".content").stop().slideUp();
				jQuery(this).closest(".tooltip").css("z-index", 19).find(".content").stop().slideToggle();
				if(jQuery(this).closest(".tooltip").find(".content").outerWidth() + jQuery(this).closest(".tooltip").find(".content").offset().left > jQuery(window).width())
				{
					jQuery(this).closest(".tooltip").find(".content").css("left", -jQuery(this).closest(".tooltip").find(".content").outerWidth()/2 + 40);
				}
			})

		}
	}

	var applyFixedFilter = function()
	{
        var filter =  jQuery(".product-list .filters");
        var productHeader = jQuery(".product-list .products.header");
        var productList = jQuery(".product-list .products.list");
        
        if (!filter.length && !productHeader.length)
            return;

		var margin = filter.offset().top - productList.offset().top;

		if(jQuery(window).width() < 768)
		{
			margin = -productHeader.height();
		}

		var execOnScroll = function()
		{
			if (productList.offset().top + margin <= jQuery(window).scrollTop() && jQuery(window).width() >= 768)
			{
				productList
					.css({
						"margin-top" : (filter.outerHeight(true) + productHeader.outerHeight(true)) + "px"
					});

				filter
					.addClass("fixed");

				productHeader
					.css({
						"top" : filter.outerHeight() + "px"
					})
					.addClass("fixed");

			}
			else
			{
				filter.removeClass("fixed");

				productHeader
					.css({
						"top" : ""
					})
					.removeClass("fixed");

				productList
					.css({
						"margin-top" : ""
					});

			}
		}
        
        jQuery(window).on("scroll", function() {
	        clearTimeout(execOnScroll);
	        setTimeout(execOnScroll, 50)
        });

		jQuery(window).on("touchmove", execOnScroll);
    }

	/* _________________________________________________________________________ public methods */

	var init = function()
	{
		applyInputs();
		applySliders();
		applySorting();
		applyFixedFilter();
		prepareForms();
		handleDetails();
		handleOptional();
		handleTooltip();
	};

	init();
};

var productFilter;

formatNumber = function (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")
}
promobox = function ()
{
    var draggableList,
        postsList,
        promoboxElement,
        info,
        filters,
        filtersPos = null,
        filtersStartPos = null,
        listWidth,
        distansceFromRight;
        dragging = false;
        itemWidth = null, seeAllItemWidth = null;
    var pep;
    
    function getPostsListWidth(list)
    {
        var width = 0;

        jQuery(".post-item:not(.hide):not(.all-promotions)", list).each(function()
        {
//            width += jQuery(this).outerWidth(true);
            width += itemWidth;///jQuery(this).outerWidth(); //333
        });
        
        width += seeAllItemWidth;
        width += parseFloat(list.css("padding-left")) + parseFloat(list.css("padding-right"));        
        
        return width;
    }  
    
    function updateVars()
    {
        draggableList = jQuery(".draggable-list");
        postsList = jQuery(".posts-list-content", draggableList);

        if (!postsList.length)
            return;   

        promoboxElement = draggableList.closest(".promobox");
        info = jQuery(".info", promoboxElement);
        
        if (filtersPos === null)
        {
            filters = jQuery(".filters", draggableList);  
            filtersPos = filters.position();
        }
        
        if (filtersStartPos === null)
        {
            filtersStartPos = filters.position().left;
        }
        if (itemWidth === null)
        {
            itemWidth = jQuery(".post-item", postsList).not(".all-promotions").eq(0).outerWidth();
            seeAllItemWidth = jQuery(".post-item.all-promotions", postsList).eq(0).outerWidth();
        }
                
        listWidth = getPostsListWidth(postsList);
        postsList.outerWidth(listWidth);
        distansceFromRight = (jQuery(window).innerWidth() -  (draggableList.offset().left + draggableList.outerWidth()));
    }      
    
    function updateGallery()
    {  
        if  (typeof pep !== "undefined")
        {
            jQuery.pep.unbind( pep ); 
        }

        if(!draggableList || !draggableList.length)
            return;
    
        updateVars();

        if (listWidth >=  draggableList.offset().left + draggableList.outerWidth())
        {
            constrainLeft = -listWidth + draggableList.outerWidth() + distansceFromRight;
        }
        else
        if (listWidth >= draggableList.outerWidth())
        {
            constrainLeft = -listWidth + draggableList.outerWidth();
        }
        else
//        if (listWidth < draggableList.outerWidth())
        {
            constrainLeft = 0;
        }

        if(jQuery(window).width() < 1024)
        {
            var spd = false;
        }
        else
        {
            var spd = true;
        }
                
        pep = postsList.pep({
            axis: "x",
            drag: drag,
            easing: moving,
            start: startDrag,
            revert: false,
//          revertIf:   function(){ return false; },
            constrainTo: [0, 0, 0, constrainLeft],
            shouldPreventDefault: spd
          });
       
        
        info.css({
            "opacity" : "",
            "transform" : ""
        });    

        postsList.css({
            "left" : "0",
            "transform": "matrix(1, 0, 0, 1, 0, 0)"
        });           
          
/*      
        filters.css({
            "left" : filtersStartPos + "px"
        }); 
*/        
    }   
    
    function startDrag(e, obj)
    {
//            info = jQuery(".info", promoboxElement);
//            listWidth = getPostsListWidth(postsList);
//            postsList.width(listWidth);
    }
    
    function drag(e, obj)
    {
        dragging = true;
        moving(e, obj);
    }

    //drag or easing
    function moving(e, obj)
    {
        var drag = jQuery(obj.el);
        var infoWidth = info.outerWidth();
        var dragPos = drag.position().left;
        var infoDistance = Math.min(Math.abs(dragPos), infoWidth);

        if (jQuery(window).innerWidth() >= 1024)
        {
            info.css({
                "opacity" : 1- infoDistance/infoWidth,
                "transform" : "scale(" + (1 - infoDistance/infoWidth/2) + ")"
            });
        }
        else
        {
            info.css({
                "opacity" : "",
                "transform" : ""
            });
        }
        
        filters.css({
            "left" : Math.max((filtersPos.left - Math.abs(dragPos)), 0) + "px"
        });
    }
    

    function handleDraggablePostsList()
    {
        dragging = false;
        updateVars();
        
        if (!postsList.length)
            return;  

        updateGallery();
        
        jQuery(window).on("resize", function()
        {
            updateGallery();
        });

        //click when click, drag when drag
        jQuery("a", postsList)
            .on("mousedown pointerdown touchstart",function(e) {
                dragging = false;
            })
            .on("click touch",function(e)
            {
                if (dragging)
                {
                    e.preventDefault();
                }                
            })
            ;
    }

	var applyFilter = function()
	{
		jQuery(".promobox").each( function ()
		{
			var promobox = jQuery(this);
			jQuery(".filters .value", promobox).on("click touch touchend", function(e)
			{
                e.preventDefault();
                e.stopPropagation();
                jQuery(".filters ul", promobox).stop().fadeIn();
			});

			jQuery(".filters ul li", promobox).on("click touch touchend", function(e)
			{
                e.preventDefault();
                e.stopPropagation();
                jQuery(this).toggleClass("active");
                window.location.hash = "#filtruj-" + jQuery(this).html();
				applyFilterToContent(promobox);
			});

            //jQuery(document).on("click touch touchend", function(e)
            //{
            //    e.stopPropagation();
            //})

            var hideFilter = function()
            {
                jQuery(".filters ul", promobox).stop().fadeOut();

                if (typeof filters !== "undefined" && parseInt(postsList.css("left")) == 0)
                {
                    filters.css({
                        "left" : filtersStartPos + "px"
                    });
                }
            }

			jQuery(".filters ul", promobox).on("mouseleave", hideFilter);
            jQuery(document).on("click touchend", hideFilter)
        });
	}

	var applyFilterToContent = function(promobox)
	{
		var cats = [];
        var postItemsLength = jQuery(".post-item", promobox).length;

		jQuery(".filters ul li", promobox).each( function() {
			if(jQuery(this).hasClass("active"))
				cats.push(jQuery(this).attr("data-type"));
		});

		if(!cats.length)
        {
			jQuery(".post-item", promobox).removeClass( "hide" );
            updateGallery();
        }
		else
		jQuery(".post-item", promobox).each( function(index, element)
        {
            var currentItem = jQuery(this);
            if (!currentItem.hasClass("all-promotions"))
            {
                var postCats = currentItem.attr("data-type").toString().split(",");
                var disablePost = true;
                for(i in cats)
                {
                    if(postCats.indexOf(cats[i].toString()) > -1)
                        disablePost = false;
                }
                if(disablePost) currentItem.addClass( "hide" );
                else jQuery(this).removeClass( "hide" );
            }
            
            if (index == postItemsLength -1)
            {
                updateGallery();
            }            
		});
//        updateVars();
//            updateGallery();
	}

	/* _________________________________________________________________________ public methods */

	var init = function()
	{
        updateGallery();
		applyFilter();
        handleDraggablePostsList();
	};

	init();
};

var promobox;

document.addEventListener("DOMContentLoaded",function(){

  function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
  }

  let gid = getCookie('_gid');

  if(gid) {
    const urls = document.getElementsByTagName('a');

    if(gid.includes('GA')) {
      gid = gid.substr(6);
    }

    for(let i = 0; i<urls.length; i++) {
      let href = urls[i].getAttribute('href');
      if(href.includes('affplus.pl/c') && href.includes('?')) {
        href += '&cid=' + gid;
        urls[i].setAttribute('href', href);
      } else if(href.includes('affplus.pl/c')) {
        href += '?cid=' + gid;
        urls[i].setAttribute('href', href);
      }
    }
  }

})

searchbox = function ()
{
	var currentPath = "";
	var currentLevel = 1;
    
    // get closest tu num number from array
    function closest(num, arr)
    {
        var curr = arr[0];
        var index = 0;
        var diff = Math.abs(num - curr);
        for (var val = 0; val < arr.length; val++) {
            var newdiff = Math.abs(num - arr[val]);
            if (newdiff < diff) {
                diff = newdiff;
                curr = arr[val];
                index = val;
            }
        }
        return index;
    }    

	var applySliders = function()
	{
		jQuery(".searchbox .slider").each( function()
		{
			var options = {};
			var sliderContainer = jQuery(this);

			if(sliderContainer.data("filter-type") == "between")
			{
				options.range = true;
				options.values = [];
				if(sliderContainer.data("value-min") && sliderContainer.data("value-max"))
				{
					options.values.push(sliderContainer.data("value-min"));
					options.values.push(sliderContainer.data("value-max"));
				};
			}
			else if(sliderContainer.data("filter-type") == "to")
			{                
				options.range = "min";
				if(sliderContainer.data("value"))
				{
					options.value = sliderContainer.data("value");
				};
			};
            
			if(sliderContainer.data("min") != null)
			{
				options.min = sliderContainer.data("min");
			};

			if(sliderContainer.data("max"))
			{
				options.max = sliderContainer.data("max");
			};
			var pointsValues = [];

			if(sliderContainer.data("step"))
			{

				var stepVal = sliderContainer.data("step");
				var length = Math.ceil((options.max-options.min)/stepVal);
				var start = Math.floor(options.min/stepVal);
                
				for(var i = start; i <length+start; i++)
				{
					if(i == start && options.min < i*stepVal)
					{
						pointsValues.push(options.min);
					}
                    
					pointsValues.push(i*stepVal);
                    
					if(i == length+start-1 && options.max > i*stepVal)
					{
						pointsValues.push(options.max);
					}
				}


				if(pointsValues.length)
				{
					options.min = 0;
					options.max = pointsValues.length-1;

					if(options.value)
					{
						options.value = pointsValues.indexOf(options.value);
					}
				}
			}

			if(sliderContainer.data("progressive") == true)
			{
				var rangesConfig =
				{
					default:
						[
							{"min": 0, "max": 900, "step": 100},
							{"min": 1000, "max": 9000, "step": 1000},
							{"min": 10000, "max": 48000, "step": 2000},
							{"min": 50000, "max": 50000000, "step": 5000},
						],
					investments: [
						{"min": 0, "max": 5000, "step": 500},
						{"min": 6000, "max": 19000, "step": 1000},
						{"min": 20000, "max": 95000, "step": 5000},
						{"min": 100000, "max": 475000, "step": 25000},
						{"min": 500000, "max": 10000000, "step": 50000},
					],
					investmentsMonths:
						[
							{"min": 1, "max": 23, "step": 1},
							{"min": 24, "max": 600, "step": 12}
						]
				};

				var ranges = rangesConfig.default;

				if(sliderContainer.data("progressive-values") && rangesConfig[sliderContainer.data("progressive-values")])
					ranges = rangesConfig[sliderContainer.data("progressive-values")];

				for(i=0; i < ranges.length; i++)
				{
					var current = 0;
					if(options.min > ranges[i].max || options.max < ranges[i].min)
					{
						continue;
					}
					else
					{ 
						current = ranges[i].min;
						while(current < options.min)
						{
							current += ranges[i].step;
						}

						if(options.min > ranges[i].min && current-ranges[i].step < options.min)
						{
							pointsValues.push(options.min);
						}

						while(current <= options.max && current <= ranges[i].max)
						{
							pointsValues.push(current);
							current += ranges[i].step;
						}
					}
				}

				if(pointsValues.length)
				{
					options.min = 0;
					options.max = pointsValues.length-1;
//					options.max = pointsValues[pointsValues.length-1];

					if(options.value)
					{
						options.value = pointsValues.indexOf(options.value);
//						options.value = pointsValues[pointsValues.indexOf(options.value)];
					}
				}
			}

			options.slide = function( event, ui )
			{
				if(ui.value)
				{
					var exVal = ui.value;

					if(pointsValues.length)
					{
						exVal = pointsValues[ui.value];
					}

					sliderContainer.find(".value").html( formatNumber(exVal) );
					sliderContainer.parents("[data-level]").first().find("input").val(exVal);
                    sliderContainer.data("value", exVal);
                }
				if(ui.values)
				{
					sliderContainer.find(".value").html( formatNumber(ui.values[0]) + " - " + formatNumber(ui.values[1]) );
					sliderContainer.parents("[data-level]").first().find("input[name^='min-']").val(ui.values[0]).trigger("change");
					sliderContainer.parents("[data-level]").first().find("input[name^='max-']").val(ui.values[1]).trigger("change");
				}
			}

			options.change = function( event, ui )
			{
				sliderContainer.parents("[data-level]").first().trigger("ikal:filter:changed");
			}


			sliderContainer.closest("[data-level]").find("input").on("change", function()
			{
                var input = jQuery(this);

				if(parseFloat(input.attr("min")) && input.val() < parseFloat(input.attr("min")))
					input.val(parseFloat(input.attr("min")));

				if(parseFloat(input.attr("max")) && input.val() > parseFloat(input.attr("max")))
					input.val(parseFloat(input.attr("max")));
                
                var val = input.val();
                if(pointsValues.length)
                {
                    val = closest(input.val(), pointsValues);
                }
                sliderContainer.find(".slider-bar").slider("value", val);
				sliderContainer.find(".value").html( formatNumber(input.val()) );
				sliderContainer.closest("[data-level]").trigger("ikal:filter:changed");
			});
            
			sliderContainer.find(".slider-bar").slider(options);   
            sliderContainer.closest("[data-level]").find("input").trigger("change");
		});
        

	}

	var applyChoices = function()
	{
		jQuery(".searchbox .choice").each( function()
		{
			var choicer = jQuery(this);
			jQuery(".value", choicer).on("click", function(e)
			{
				jQuery(".topped").stop().fadeOut().removeClass("topped");
				jQuery("ul", choicer).stop().fadeIn().addClass("topped");
			})

			jQuery("li[data-value]", choicer).on("click", function(e)
			{
				e.stopPropagation();
				var optionValue = jQuery(this).data("value");
				var targetPath = jQuery(this).data("target-path");
				var targetLevel = jQuery(this).data("target-level");
				jQuery("ul", choicer).stop().fadeOut().removeClass("topped");
				jQuery(".value > span", choicer).html(jQuery(this).html());

				choicer.first().find("input").val(optionValue)
				setPath(targetPath, targetLevel);
			});
		});
	}

	var applyTooltips = function()
	{
		jQuery(".tooltip-toggler").each( function() {
			var toggler = jQuery(this);
			jQuery(".do-toggle", toggler).on("click", function(e)
			{
				jQuery(".topped").stop().fadeOut().removeClass("topped");
				jQuery(".tooltip", toggler).fadeIn().addClass("topped");

				toggler.parents("[data-level]").first().one("ikal:filter:changed", function()
				{
					jQuery(".tooltip", toggler).fadeOut().removeClass("topped");
				});
			});
		});
	}

	var handleSubmit = function()
	{
		jQuery(".submit-search").on("click", function()
		{
			var pathParts = getPathParts();
			var queryParams = {};
			for(var i = 0; i<currentLevel; i++)
			{
				jQuery("[data-level='" + parseInt(i+1) +"']").each( function()
				{
					var lvl = jQuery(this).data("level");
					if(lvl == i+1 && jQuery(this).data("path").indexOf(pathParts[i]) == 0)
					{
						var currEl = jQuery(this);
						jQuery("input", currEl).each( function()
						{
							if(jQuery(this).data("prop-name"))
								queryParams[jQuery(this).data("prop-name")] = jQuery(this).val();
						});
					}
				});
			}
			var url = "";
			if(pathParts[1] == "quickloans") url = "/finansowanie/chwilowki/";
			if(pathParts[1] == "loans") url = "/finansowanie/pozyczki-na-raty/";
			if(pathParts[1] == "credits") url = "/finansowanie/kredyty-gotowkowe/";
			if(pathParts[1] == "investments") url = "/lokaty/";

			if(pathParts[1] == "accounts")
			{
				if(jQuery("input[name='accounts.type']").val() == "personal") url = "/konta-bankowe/konta-osobiste/";
				if(jQuery("input[name='accounts.type']").val() == "business") url = "/konta-bankowe/konta-firmowe/";
				if(jQuery("input[name='accounts.type']").val() == "currency") url = "/konta-bankowe/konta-walutowe/";
				if(jQuery("input[name='accounts.type']").val() == "saving") url = "/konta-bankowe/konta-oszczednosciowe/";
			}
			url += "?";

//			console.log(queryParams);
			jQuery.each(queryParams,  function(k,v)
			{
				url += k + "=" + v + "&";
			})

			location.href = url;

		})
	}

	var handleScrollDown = function()
	{
		jQuery(".searchbox .cta a").on("click", function(e)
		{
			e.preventDefault();
			window.location.hash = jQuery(this).attr("href");
			jQuery("body,html").animate({ scrollTop: jQuery(".main-content.page-content").offset().top });
		})
	}

	var handleZindex = function()
	{
		jQuery(".searchbox [data-level]").on("click", function(e)
		{
			e.stopPropagation();

			jQuery(".searchbox [data-level]").css("z-index", "");
			jQuery(this).css("z-index", 20);
		})
	}


	var setPath = function(targetPath, targetLevel)
	{
		if(typeof(targetPath) != "undefined" && typeof(targetLevel) != "undefined")
		{
			currentPath = targetPath;
			currentLevel = targetLevel;

			var pathParts = getPathParts();

			for(var i = 0; i<currentLevel; i++)
			{
				jQuery("[data-level='" + parseInt(i+1) +"']").each( function()
				{
					var lvl = jQuery(this).data("level");
					if(lvl == i+1 && jQuery(this).data("path").indexOf(pathParts[i]) == 0)
					{
						var currEl = jQuery(this);
						setTimeout(function()
						{
							currEl.fadeIn("fast").css("display", "inline-block");
						}, 350)
					}
					else
						jQuery(this).fadeOut(300);

				});
			}

		}
	}

	var getPathParts = function()
	{
		var slicedPath = currentPath.split(".");
		var pathParts = [""];
		var tmpPath = "";

		for(var k in slicedPath)
		{
			if(k == 0) tmpPath = slicedPath[0];
			else tmpPath += "." + slicedPath [k];
			pathParts.push(tmpPath);
		}

		return pathParts;
	}

	var handleClickOutside = function()
	{
		jQuery(window).on("click", function() {
			jQuery(".topped").stop().fadeOut().removeClass("topped");
		});
	}

	/* _________________________________________________________________________ public methods */

	var init = function()
	{
		applyChoices();
		applyTooltips();
		applySliders();
		handleSubmit();
		handleScrollDown();
		handleZindex();
		handleClickOutside();
	};

	init();
};

var searchbox;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJhc2UuanMiLCJjYWxjdWxhdG9ycy5qcyIsImNvbW1lbnRzLmpzIiwiY29tcGFuaWVzLmpzIiwiY291bnRkb3duLmpzIiwicHJvZHVjdF9saXN0LmpzIiwicHJvbW9ib3guanMiLCJyZXBsYWNlX3VybHMuanMiLCJzZWFyY2hib3guanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2h1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzlqQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMxTEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM1TUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzVFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMxY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN6U0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM5QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpa2Fsa3VsYXRvciA9IGZ1bmN0aW9uICgpXG57XG4gICAgZnVuY3Rpb24gY2FsY3VsYXRlQ2hpbGRyZW5IZWlnaHQoY29udGFpbmVyKVxuICAgIHtcdFxuICAgICAgICB2YXIgdG9wT2Zmc2V0ID0gYm90dG9tT2Zmc2V0ID0gMDtcblxuICAgICAgICBjb250YWluZXIuY2hpbGRyZW4oKS5lYWNoKGZ1bmN0aW9uKGksIGNoaWxkKVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgZWxlbWVudCA9IGpRdWVyeShjaGlsZCksXG4gICAgICAgICAgICBlVG9wT2Zmc2V0ID0gZWxlbWVudC5vZmZzZXQoKS50b3AsXG4gICAgICAgICAgICBlQm90dG9tT2Zmc2V0ID0gZVRvcE9mZnNldCArIGVsZW1lbnQub3V0ZXJIZWlnaHQoKTtcblxuICAgICAgICAgICAgaWYgKGVUb3BPZmZzZXQgPCB0b3BPZmZzZXQpXG4gICAgICAgICAgICAgICAgdG9wT2Zmc2V0ID0gZVRvcE9mZnNldDtcblxuICAgICAgICAgICAgaWYgKGVCb3R0b21PZmZzZXQgPiBib3R0b21PZmZzZXQpXG4gICAgICAgICAgICAgICAgYm90dG9tT2Zmc2V0ID0gZUJvdHRvbU9mZnNldDtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgcmV0dXJuICBib3R0b21PZmZzZXQgLSB0b3BPZmZzZXQgLSBjb250YWluZXIub2Zmc2V0KCkudG9wICsgcGFyc2VJbnQoY29udGFpbmVyLmNzcyhcInBhZGRpbmctYm90dG9tXCIpKTtcbiAgICB9ICAgIFxuICAgIFxuICAgIGZ1bmN0aW9uIGhhbmRsZUNhdGVnb3J5V2l0aFNpZGViYXJNaW5IZWlnaHQoKVxuICAgIHtcbiAgICAgICAgdmFyIHNpZGViYXIgPSBqUXVlcnkoXCJib2R5LmNhdGVnb3J5IHNlY3Rpb24uY2F0ZWdvcnktY29udGVudCAuYXNpZGUtcGFuZWxcIik7XG4gICAgICAgIGlmICghc2lkZWJhci5sZW5ndGgpIHsgcmV0dXJuOyB9XG4gICAgICAgIFxuICAgICAgICB2YXIgY2F0ZWdvcnlDb250ZW50ID0galF1ZXJ5KFwiYm9keS5jYXRlZ29yeSBzZWN0aW9uLmNhdGVnb3J5LWNvbnRlbnRcIik7ICAgICAgICBcbiAgICAgICAgY2F0ZWdvcnlDb250ZW50LmNzcyhcIm1pbi1oZWlnaHRcIiwgY2FsY3VsYXRlQ2hpbGRyZW5IZWlnaHQoY2F0ZWdvcnlDb250ZW50KSk7XG4gICAgfVxuICAgIFxuICAgIGZ1bmN0aW9uIGhhbmRsZUxpc3ROdW1iZXJpbmcoKVxuICAgIHtcbiAgICAgICAgalF1ZXJ5KFwib2xbc3RhcnRdXCIpLmVhY2goZnVuY3Rpb24oKVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgdmFsID0gcGFyc2VGbG9hdChqUXVlcnkodGhpcykuYXR0cihcInN0YXJ0XCIpKSAtIDE7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyh2YWwpO1xuICAgICAgICAgICAgalF1ZXJ5KHRoaXMpLmNzcyhcImNvdW50ZXItaW5jcmVtZW50XCIsIFwib2wtY291bnRlciBcIiArIHZhbCk7XG4gICAgICAgIH0pOyAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIGZ1bmN0aW9uIGhhbmRsZVByb2R1Y3RzR3JpZCgpXG4gICAge1xuICAgICAgICB2YXIgcHJvZHVjdEdyaWQgPSBqUXVlcnkoXCIucHJvZHVjdC1ncmlkXCIpO1xuICAgICAgICBcbiAgICAgICAgaWYgKCFwcm9kdWN0R3JpZC5sZW5ndGgpXG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIFxuICAgICAgICB2YXIgc2hvd01vcmUgPSBqUXVlcnkoXCIucHJvZHVjdHMtc2hvdy1tb3JlXCIsIHByb2R1Y3RHcmlkKTtcbiAgICAgICAgXG4gICAgICAgIHNob3dNb3JlLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oKVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgYnRuID0galF1ZXJ5KHRoaXMpO1xuICAgICAgICAgICAgdmFyIG1vcmVQcm9kdWN0cyA9IGpRdWVyeShcIi5wb3N0cy1saXN0LWNvbnRlbnQubW9yZS1wcm9kdWN0c1wiLCBwcm9kdWN0R3JpZCk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChtb3JlUHJvZHVjdHMuaGFzQ2xhc3MoXCJ2aXNpYmxlXCIpKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGpRdWVyeShcIi5zaG93XCIsIGJ0bikuc2hvdygpOyBcbiAgICAgICAgICAgICAgICBqUXVlcnkoXCIuaGlkZVwiLCBidG4pLmhpZGUoKTsgXG4gICAgICAgICAgICAgICAgbW9yZVByb2R1Y3RzLnJlbW92ZUNsYXNzKFwidmlzaWJsZVwiKTtcbiAgICAgICAgICAgICAgICBtb3JlUHJvZHVjdHMuc2xpZGVVcCgpO1xuICAgICAgICAgICAgICAgIHNjcm9sbFRvKGpRdWVyeShcIi5wb3N0cy1saXN0LWNvbnRlbnRcIiwgcHJvZHVjdEdyaWQpLCAtMzApO1xuICAgICAgICAgICAgfSAgICBcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBqUXVlcnkoXCIuc2hvd1wiLCBidG4pLmhpZGUoKTsgXG4gICAgICAgICAgICAgICAgalF1ZXJ5KFwiLmhpZGVcIiwgYnRuKS5zaG93KCk7IFxuICAgICAgICAgICAgICAgIG1vcmVQcm9kdWN0cy5hZGRDbGFzcyhcInZpc2libGVcIik7XG4gICAgICAgICAgICAgICAgbW9yZVByb2R1Y3RzLnNsaWRlRG93bigpO1xuICAgICAgICAgICAgICAgIHNjcm9sbFRvKGpRdWVyeShcIi5wb3N0cy1saXN0LWNvbnRlbnRcIiwgcHJvZHVjdEdyaWQpLCAtMzApO1xuICAgICAgICAgICAgfSAgIFxuICAgICAgICB9KTtcbiAgICAgICAgXG4gICAgfVxuICAgIFxuICAgIFxuXHRmdW5jdGlvbiBoYW5kbGVTaGFyZUxpbmtzKClcblx0e1xuXHRcdGpRdWVyeShcIi5zaGFyZSAuc29jaWFsLXNoYXJlXCIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oZSlcbiAgICAgICAge1xuXHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0d2luZG93Lm9wZW4oalF1ZXJ5KHRoaXMpLmF0dHIoJ2hyZWYnKSwgJ2ZiU2hhcmVXaW5kb3cnLCAnaGVpZ2h0PTQ1MCwgd2lkdGg9NjUwLCB0b3A9JyArIChqUXVlcnkod2luZG93KS5oZWlnaHQoKSAvIDIgLSAyNzUpICsgJywgbGVmdD0nICsgKGpRdWVyeSh3aW5kb3cpLndpZHRoKCkgLyAyIC0gMjI1KSArICcsIHRvb2xiYXI9MCwgbG9jYXRpb249MCwgbWVudWJhcj0wLCBkaXJlY3Rvcmllcz0wLCBzY3JvbGxiYXJzPTAnKTtcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9KTtcblx0fVxuICAgIFxuICAgIGZ1bmN0aW9uIGhhbmRsZVNwb3RsaWdodFNlYXJjaCgpXG4gICAge1xuICAgICAgICB2YXIgc2VhcmNoQnRuID0galF1ZXJ5KFwiLnNpdGUtaGVhZGVyIC5zZWFyY2gtYnRuIGFcIik7XG4gICAgICAgIHZhciBzZWFyY2hCYXIgPSBqUXVlcnkoXCIuc2VhcmNoLWJhci5zcG90bGlnaHQtc2VhcmNoXCIpO1xuICAgICAgICBcbiAgICAgICAgaWYgKCFzZWFyY2hCdG4ubGVuZ3RoIHx8ICFzZWFyY2hCdG4ubGVuZ3RoKVxuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICBcbiAgICAgICAgZnVuY3Rpb24gaGlkZUJhcigpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHNlYXJjaEJhci5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgICAgIGpRdWVyeShcImlucHV0XCIsIHNlYXJjaEJhcikuYmx1cigpLmF0dHIoXCJyZWFkb25seVwiLCB0cnVlKTtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgalF1ZXJ5KFwiaW5wdXRcIiwgc2VhcmNoQmFyKS5hdHRyKFwicmVhZG9ubHlcIiwgdHJ1ZSk7XG4gICAgICAgIFxuICAgICAgICBzZWFyY2hCdG4ub24oXCJjbGlja1wiLCBmdW5jdGlvbihlKVxuICAgICAgICB7XG4gICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpOyAgICAgICAgICAgIFxuICAgICAgICAgICAgc2VhcmNoQmFyLnRvZ2dsZUNsYXNzKFwiYWN0aXZlXCIpOyAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChzZWFyY2hCYXIuaGFzQ2xhc3MoXCJhY3RpdmVcIikpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgalF1ZXJ5KFwiaW5wdXRcIiwgc2VhcmNoQmFyKS5hdHRyKFwicmVhZG9ubHlcIiwgZmFsc2UpOyBcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB3aW5kb3cuc2V0VGltZW91dChmdW5jdGlvbigpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBqUXVlcnkoXCJpbnB1dFt0eXBlPXRleHRdXCIsIHNlYXJjaEJhcikuZm9jdXMoKTtcbiAgICAgICAgICAgICAgICB9ICw1MDApO1xuICAgICAgICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgalF1ZXJ5KFwiaW5wdXRcIiwgc2VhcmNoQmFyKS5hdHRyKFwicmVhZG9ubHlcIiwgdHJ1ZSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHNlYXJjaEJhci5vbihcImNsaWNrIHRvdWNoIHRvdWNoZW5kXCIsIGZ1bmN0aW9uKGUpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgIH0pOyAgICAgICAgXG4gICAgICAgIFxuICAgICAgICBqUXVlcnkoZG9jdW1lbnQpXG4gICAgICAgICAgICAub24oXCJjbGljayB0b3VjaCB0b3VjaGVuZFwiLCBmdW5jdGlvbigpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaGlkZUJhcigpO1xuICAgICAgICAgICAgfSkgICAgICAgIFxuICAgICAgICAgICAgLm9uKFwia2V5ZG93blwiLCBmdW5jdGlvbihlKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGlmICggZS5rZXlDb2RlID09IDI3IClcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGhpZGVCYXIoKTsgICAgXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2hlY2tFbGVtZW50c1Zpc2liaWxpdHlPblNjcm9sbChlbGVtZW50c0NsYXNzKVxuICAgIHtcbiAgICAgICAgdmFyIGVsZW1lbnRzID0galF1ZXJ5KGVsZW1lbnRzQ2xhc3MpO1xuXG4gICAgICAgIGlmICghZWxlbWVudHMubGVuZ3RoKVxuICAgICAgICAgICAgcmV0dXJuO1xuXG4gICAgICAgIGVsZW1lbnRzXG4gICAgICAgICAgICAuYXR0cihcImRhdGEtb25zY3JlZW5cIiwgMCk7XG5cbiAgICAgICAgdmFyIGlzU2Nyb2xsaW5nICA9IGZhbHNlO1xuXG4gICAgICAgIGpRdWVyeSh3aW5kb3cpLm9uKFwic2Nyb2xsXCIsIGZ1bmN0aW9uKClcbiAgICAgICAge1xuXHRcdFx0aWYgKGlzU2Nyb2xsaW5nKSB7IHJldHVybjsgfVxuXG4gICAgICAgICAgICB2YXIgd2luSGVpZ2h0ID0galF1ZXJ5KHdpbmRvdykuaW5uZXJIZWlnaHQoKTtcbiAgICAgICAgICAgIHZhciBzY3JvbGxUb3AgPSAgalF1ZXJ5KHdpbmRvdykuc2Nyb2xsVG9wKCk7XG4gICAgICAgICAgICB2YXIgc2Nyb2xsQm90dG9tID0gc2Nyb2xsVG9wICsgd2luSGVpZ2h0O1xuXG5cdFx0XHRpc1Njcm9sbGluZyA9IHRydWU7XG5cblx0XHRcdHdpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uKClcblx0XHRcdHtcbiAgICAgICAgICAgICAgICBlbGVtZW50cy5lYWNoKGZ1bmN0aW9uKClcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBlbGVtZW50ID0galF1ZXJ5KHRoaXMpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgaXNWaXNpYmxlID0gZWxlbWVudC5hdHRyKFwiZGF0YS1vbnNjcmVlblwiKTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGVsZW1lbnRIZWlnaHQgPSBlbGVtZW50Lm91dGVySGVpZ2h0KCk7XG4gICAgICAgICAgICAgICAgICAgIHZhciBlbGVtZW50VG9wID0gZWxlbWVudC5vZmZzZXQoKS50b3A7XG4gICAgICAgICAgICAgICAgICAgIHZhciBlbGVtZW50Qm90dG9tID0gZWxlbWVudFRvcCArIGVsZW1lbnRIZWlnaHQ7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKGlzVmlzaWJsZSA9PSB0cnVlKVxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZWxlbWVudEJvdHRvbSA8PSBzY3JvbGxUb3AgfHwgZWxlbWVudFRvcCA+PSBzY3JvbGxCb3R0b20pXG4gICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAudHJpZ2dlcihcImlrOnZpc2libGl0eS1jaGFuZ2VcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRyaWdnZXIoXCJpazpoaWRkZW5cIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoXCJkYXRhLW9uc2NyZWVuXCIsIDApO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZWxlbWVudEJvdHRvbSA+IHNjcm9sbFRvcCAmJiBlbGVtZW50VG9wIDwgc2Nyb2xsVG9wKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZWxlbWVudFRvcCA8IHNjcm9sbEJvdHRvbSAmJiBlbGVtZW50Qm90dG9tID4gc2Nyb2xsQm90dG9tKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoZWxlbWVudFRvcCA8IHNjcm9sbFRvcCAmJiBlbGVtZW50Qm90dG9tID4gc2Nyb2xsQm90dG9tKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50cmlnZ2VyKFwiaWs6dmlzaWJsaXR5LWNoYW5nZVwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAudHJpZ2dlcihcImlrOnZpc2libGVcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoXCJkYXRhLW9uc2NyZWVuXCIsIDEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgLypcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVsZW1lbnRCb3R0b20gPD0gc2Nyb2xsQm90dG9tICYmIGVsZW1lbnRUb3AgPj0gc2Nyb2xsVG9wKVxuICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudC50cmlnZ2VyKFwiaWs6cmV2ZWFsOmZ1bGxcIik7XG4gICAgICAgICAgICAgICAgICAgIGVsc2UqL1xuICAgICAgICAgICAgICAgIH0pO1xuXG5cdFx0XHRcdGlzU2Nyb2xsaW5nID0gZmFsc2U7XG5cdFx0XHR9LCA1ICk7XG4gICAgICAgIH0pO1xuXG5cdFx0alF1ZXJ5KHdpbmRvdykudHJpZ2dlcihcInNjcm9sbFwiKTtcbiAgICB9XG4gICAgZnVuY3Rpb24gaGFuZGxlTWVudSgpXG4gICAge1xuICAgICAgICB2YXIgc2l0ZUhlYWRlciA9IGpRdWVyeShcIi5zaXRlLWhlYWRlclwiKTtcbiAgICAgICAgdmFyIG1lbnVJdGVtcyA9IGpRdWVyeShcIi5tZW51ID4gLm1lbnUtaXRlbS1oYXMtY2hpbGRyZW4gPiBhXCIsIHNpdGVIZWFkZXIpO1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIG1lbnVJdGVtcy5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGUpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDEwMjQpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdmFyIGlzQ2xpY2tlZCA9IGpRdWVyeSh0aGlzKS5wYXJlbnQoKS5oYXNDbGFzcyhcImNsaWNrZWRcIik7XG4gICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBpZiAoIWlzQ2xpY2tlZClcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGUuc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTsgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgalF1ZXJ5KFwiLm1lbnUgPiAubWVudS1pdGVtLWhhcy1jaGlsZHJlblwiKS5yZW1vdmVDbGFzcyhcImNsaWNrZWRcIik7XG4gICAgICAgICAgICAgICAgICAgIGpRdWVyeSh0aGlzKS5wYXJlbnQoKS5hZGRDbGFzcyhcImNsaWNrZWRcIik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICB7XG4vLyAgICAgICAgICAgICAgICAgICAgalF1ZXJ5KHRoaXMpLnBhcmVudCgpLnJlbW92ZUNsYXNzKFwiY2xpY2tlZFwiKTsgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIFxuICAgIH1cblxuXHRmdW5jdGlvbiBoYW5kbGVNb2JpbGVNZW51KClcblx0e1xuICAgICAgICB2YXIgYm9keSA9IGpRdWVyeShcImJvZHlcIik7XG4gICAgICAgIHZhciBzaXRlSGVhZGVyID0galF1ZXJ5KFwiLnNpdGUtaGVhZGVyXCIpO1xuXHRcdHZhciBidG5NZW51ID0galF1ZXJ5KFwiLmJ0bi1tb2JpbGUtbWVudVwiLCBzaXRlSGVhZGVyKTtcblx0XHR2YXIgbWVudUl0ZW1zID0galF1ZXJ5KFwiLm1lbnUtaXRlbSBhXCIsIHNpdGVIZWFkZXIpO1xuXG5cdFx0YnRuTWVudVxuXHRcdFx0Lm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oZSlcblx0XHRcdHtcblx0XHRcdFx0Ym9keS50b2dnbGVDbGFzcyhcIm1lbnUtb3BlbmVkXCIpO1xuXG5cdFx0XHRcdGlmIChib2R5Lmhhc0NsYXNzKFwibWVudS1vcGVuZWRcIikpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRqUXVlcnkodGhpcykuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XG5cdFx0XHRcdFx0c2l0ZUhlYWRlci5hZGRDbGFzcyhcIm1vYmlsZS1hY3RpdmVcIik7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0alF1ZXJ5KHRoaXMpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xuXHRcdFx0XHRcdHNpdGVIZWFkZXIucmVtb3ZlQ2xhc3MoXCJtb2JpbGUtYWN0aXZlXCIpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblxuXHRcdG1lbnVJdGVtc1xuXHRcdFx0Lm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oZSlcblx0XHRcdHtcbiAgICAgICAgICAgICAgICBpZighalF1ZXJ5KHRoaXMpLnBhcmVudCgpLmhhc0NsYXNzKFwibWVudS1pdGVtLWhhcy1jaGlsZHJlblwiKSlcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGJvZHkucmVtb3ZlQ2xhc3MoXCJtZW51LW9wZW5lZFwiKTtcbiAgICAgICAgICAgICAgICAgICAgYnRuTWVudS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgICAgICAgICAgICAgc2l0ZUhlYWRlci5yZW1vdmVDbGFzcyhcIm1vYmlsZS1hY3RpdmVcIik7XG5cbiAgICAgICAgICAgICAgICB9XG5cdFx0XHR9KTtcblx0fTtcblxuICAgIGZ1bmN0aW9uIGhhbmRsZUFuYWx5dGljcygpXG4gICAge1xuICAgICAgICBqUXVlcnkoJy5uYXZpZ2F0aW9uLXRvcCBsaSBhJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSlcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIGxpbmsgPSBqUXVlcnkodGhpcykuYXR0cihcImhyZWZcIik7XG4gICAgICAgICAgICAvL2UucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIGd0YWcoJ2V2ZW50JywgJ1RPUC1tZW51Jywge1xuICAgICAgICAgICAgICAgICdldmVudF9jYXRlZ29yeSc6ICdjbGljaycsXG4gICAgICAgICAgICAgICAgJ2V2ZW50X2xhYmVsJzogalF1ZXJ5KHRoaXMpLmZpbmQoXCJzcGFuXCIpLmh0bWwoKSxcbiAgICAgICAgICAgICAgICAvLydldmVudF9jYWxsYmFjayc6IGZ1bmN0aW9uKCl7ZG9jdW1lbnQubG9jYXRpb24gPSBsaW5rO31cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICBqUXVlcnkoJy5mb290ZXItbmF2IGxpIGEnKS5vbignY2xpY2snLCBmdW5jdGlvbihlKVxuICAgICAgICB7XG4gICAgICAgICAgICAvL2UucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgdmFyIGxpbmsgPSBqUXVlcnkodGhpcykuYXR0cihcImhyZWZcIik7XG5cbiAgICAgICAgICAgIGd0YWcoJ2V2ZW50JywgJ2Zvb3Rlci1tZW51Jywge1xuICAgICAgICAgICAgICAgICdldmVudF9jYXRlZ29yeSc6ICdjbGljaycsXG4gICAgICAgICAgICAgICAgJ2V2ZW50X2xhYmVsJzogalF1ZXJ5KHRoaXMpLmZpbmQoXCJzcGFuXCIpLmh0bWwoKSxcbiAgICAgICAgICAgICAgICAvLydldmVudF9jYWxsYmFjayc6IGZ1bmN0aW9uKCkge2RvY3VtZW50LmxvY2F0aW9uID0gbGluazt9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgalF1ZXJ5KCcucHJvZHVjdHMubGlzdCBsaSAubW9yZSAuZXhwYW5kZXIgLmV4cGFuZCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBwcm9kdWN0X25hbWUgPSBqUXVlcnkodGhpcykuY2xvc2VzdChcImxpXCIpLmZpbmQoXCIubmFtZSBkaXZcIikuaHRtbCgpO1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgZ3RhZygnZXZlbnQnLCAnWm9iYWN6IHdpxJljZWonLCB7XG4gICAgICAgICAgICAgICAgJ2V2ZW50X2NhdGVnb3J5JzogJ2NsaWNrJyxcbiAgICAgICAgICAgICAgICAnZXZlbnRfbGFiZWwnOiBwcm9kdWN0X25hbWUgKyAnICBuYSAnICsgbG9jYXRpb24uaHJlZlxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGpRdWVyeSgnLnByb2R1Y3RzLmxpc3QgbGkgLm1vcmUgLmV4cGFuZGVyIC5mb2xkJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSlcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIHByb2R1Y3RfbmFtZSA9IGpRdWVyeSh0aGlzKS5jbG9zZXN0KFwibGlcIikuZmluZChcIi5uYW1lIGRpdlwiKS5odG1sKCk7XG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICBndGFnKCdldmVudCcsICdab2JhY3ogbW5pZWonLCB7XG4gICAgICAgICAgICAgICAgJ2V2ZW50X2NhdGVnb3J5JzogJ2NsaWNrJyxcbiAgICAgICAgICAgICAgICAnZXZlbnRfbGFiZWwnOiBwcm9kdWN0X25hbWUgKyAnICBuYSAnICsgbG9jYXRpb24uaHJlZlxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGpRdWVyeSgnLnByb2R1Y3RzLmxpc3QgbGkgLmN0YSBhJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSlcbiAgICAgICAge1xuICAgICAgICAgICAgLy9lLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICAgIHZhciBsaW5rID0galF1ZXJ5KHRoaXMpLmF0dHIoXCJocmVmXCIpO1xuICAgICAgICAgICAgdmFyIHByb2R1Y3RfbmFtZSA9IGpRdWVyeSh0aGlzKS5jbG9zZXN0KFwibGlcIikuYXR0cihcImlkXCIpXG4gICAgICAgICAgICB2YXIgY29sb3IgPSBcImJpYcWCeVwiO1xuXG4gICAgICAgICAgICBpZihqUXVlcnkodGhpcykuaGFzQ2xhc3MoXCJidG4taW52ZXJzZVwiKSlcbiAgICAgICAgICAgICAgICBjb2xvciA9IFwiemllbG9ueVwiO1xuXG4gICAgICAgICAgICBndGFnKCdldmVudCcsICdjb21wYXJlLWFjcXVpc2l0aW9uJywge1xuICAgICAgICAgICAgICAgICdldmVudF9jYXRlZ29yeSc6ICdjbGljaycsXG4gICAgICAgICAgICAgICAgJ2V2ZW50X2xhYmVsJzogJ1rFgsOzxbwgd25pb3NlayAtICcgKyBjb2xvciArICcgZ3V6aWsgLSAnICArIHByb2R1Y3RfbmFtZSArICcgbmEgJyArIGxvY2F0aW9uLmhyZWYsXG4gICAgICAgICAgICAgICAgLy8nZXZlbnRfY2FsbGJhY2snOiBmdW5jdGlvbigpe2RvY3VtZW50LmxvY2F0aW9uID0gbGluazt9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgalF1ZXJ5KCcucHJvZHVjdCAuYXR0YWNobWVudHMgdWwgbGkgYScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpXG4gICAgICAgIHtcbiAgICAgICAgICAgIC8vZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICB2YXIgbGluayA9IGpRdWVyeSh0aGlzKS5hdHRyKFwiaHJlZlwiKTtcblxuICAgICAgICAgICAgZ3RhZygnZXZlbnQnLCAnUERGJywge1xuICAgICAgICAgICAgICAgICAgICAnZXZlbnRfY2F0ZWdvcnknIDogJ3BvYnJhbmllJyxcbiAgICAgICAgICAgICAgICAgICAgJ2V2ZW50X2xhYmVsJyA6IGxvY2F0aW9uLmhyZWYsXG4gICAgICAgICAgICAgICAgICAgIC8vJ2V2ZW50X2NhbGxiYWNrJzogZnVuY3Rpb24oKXtkb2N1bWVudC5sb2NhdGlvbiA9IGxpbms7fVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICk7XG4gICAgICAgIH0pO1xuXG5cbiAgICAgICAgalF1ZXJ5KCcuc2hhcmUgLmxpbmtzIGEuc29jaWFsLXNoYXJlJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgbWVkaWEgPSBqUXVlcnkodGhpcykuZmluZChcInNwYW5cIikuaHRtbCgpO1xuXG4gICAgICAgICAgICBndGFnKCdldmVudCcsIG1lZGlhLFxuICAgICAgICAgICAgICAgIHsgJ2V2ZW50X2NhdGVnb3J5JyA6ICdzaGFyZScsICdldmVudF9sYWJlbCcgOiBsb2NhdGlvbi5ocmVmIH1cbiAgICAgICAgICAgICk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGpRdWVyeSgnLnBvc3QtY29udGVudCAuZXh0ZXJuYWwtbGluaycpLm9uKCdjbGljaycsIGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIGxpbmsgPSBqUXVlcnkodGhpcykuYXR0cihcImhyZWZcIik7XG4gICAgICAgICAgICB2YXIgdGV4dCA9IGpRdWVyeSh0aGlzKS50ZXh0KCk7XG4gICAgICAgICAgICBndGFnKFwiZXZlbnRcIiwgXCJsaW5rX3pfYXJ0eWt1bHVcIixcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiZXZlbnRfY2F0ZWdvcnlcIiA6IFwiY2xpY2tcIixcbiAgICAgICAgICAgICAgICAgICAgXCJldmVudF9sYWJlbFwiIDogbGluayArIFwiIHwgXCIgKyB0ZXh0LFxuICAgICAgICAgICAgICAgICAgICAvL1wiZXZlbnRfY2FsbGJhY2tcIjogZnVuY3Rpb24oKXtkb2N1bWVudC5sb2NhdGlvbiA9IGxpbms7fVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICk7XG4gICAgICAgIH0pO1xuXG5cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzY3JvbGxUbyh0YXJnZXRPckludCwgbWFyZ2luLCBzcGVlZClcbiAgICB7XG4gICAgICAgIGZ1bmN0aW9uIGlzSW50KG4peyByZXR1cm4gTnVtYmVyKG4pID09PSBuICYmIG4gJSAxID09PSAwOyB9XG4gICAgICAgIGZ1bmN0aW9uIGlzalF1ZXJ5KG9iaikgeyByZXR1cm4gKG9iaiAmJiAob2JqIGluc3RhbmNlb2YgalF1ZXJ5IHx8IG9iai5jb25zdHJ1Y3Rvci5wcm90b3R5cGUuanF1ZXJ5KSk7IH1cblxuICAgICAgICB2YXIgdGFyZ2V0ID0gdGFyZ2V0T3JJbnQ7XG5cbiAgICAgICAgaWYgKCFpc0ludCh0YXJnZXQpKVxuICAgICAgICB7XG4gICAgICAgICAgICB0YXJnZXQgPSAhaXNqUXVlcnkodGFyZ2V0KSA/IGpRdWVyeSh0YXJnZXQpLm9mZnNldCgpLnRvcCA6IHRhcmdldC5vZmZzZXQoKS50b3A7XG4gICAgICAgIH1cblxuICAgICAgICBtYXJnaW4gPSB0eXBlb2YgbWFyZ2luICE9PSBcInVuZGVmaW5lZFwiID8gbWFyZ2luIDogMDtcbiAgICAgICAgc3BlZWQgPSB0eXBlb2Ygc3BlZWQgIT09IFwidW5kZWZpbmVkXCIgPyBzcGVlZCA6IDc1MDtcblxuICAgICAgICBpZiAobmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvKGlQb2R8aVBob25lfGlQYWQpLykpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGpRdWVyeSgnYm9keScpLmFuaW1hdGUoXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgc2Nyb2xsVG9wOiB0YXJnZXQgKyBtYXJnaW5cbiAgICAgICAgICAgIH0sIHNwZWVkKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlXG4gICAgICAgIHtcbiAgICAgICAgICAgIGpRdWVyeSgnaHRtbCwgYm9keScpLmFuaW1hdGUoXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgc2Nyb2xsVG9wOiB0YXJnZXQgKyBtYXJnaW5cbiAgICAgICAgICAgIH0sIHNwZWVkKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGhhbmRsZVNjcm9sbFRvQ29udGVudCgpXG4gICAge1xuICAgICAgICB2YXIgc2Nyb2xsQnRuID0galF1ZXJ5KFwiLnNjcm9sbC10by1jb250ZW50XCIpO1xuXG4gICAgICAgIGlmICghc2Nyb2xsQnRuLmxlbmd0aClcbiAgICAgICAgICAgIHJldHVybjtcblxuICAgICAgICB2YXIgY29udGVudCA9IGpRdWVyeShcIi5tYWluLWNvbnRlbnRcIik7XG5cbiAgICAgICAgaWYgKCFjb250ZW50Lmxlbmd0aClcbiAgICAgICAgICAgIGNvbnRlbnQgPSBqUXVlcnkoXCIucGFuZWwtbGF5b3V0XCIpO1xuXG4gICAgICAgIGlmICghY29udGVudC5sZW5ndGgpXG4gICAgICAgICAgICByZXR1cm47XG5cbiAgICAgICAgc2Nyb2xsQnRuLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oKVxuICAgICAgICB7XG4gICAgICAgICAgICBzY3JvbGxUbyhjb250ZW50KTtcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBoYW5kbGVNYWdlYm94TmF2aWdhdGlvbigpXG4gICAge1xuICAgICAgICB2YXIgbWVnYWJveGVzID0galF1ZXJ5KFwiLm1lZ2Fib3hcIik7XG5cbiAgICAgICAgaWYgKCFtZWdhYm94ZXMubGVuZ3RoKVxuICAgICAgICAgICAgcmV0dXJuO1xuXG4gICAgICAgIHZhciBhdXRvUGxheSA9IGZhbHNlO1xuICAgICAgICB2YXIgbWVnYWJveEludGVydmFsO1xuXG4gICAgICAgIGZ1bmN0aW9uIHRvZ2dsZUF1dG9QbGF5KG1lZ2Fib3gsIHR1cm5fb24pXG4gICAgICAgIHtcbiAgICAgICAgICAgIHR1cm5fb24gPSB0eXBlb2YgdHVybl9vbiAhPT0gXCJ1bmRlZmluZWRcIiA/IHR1cm5fb24gOiB0cnVlO1xuXG4gICAgICAgICAgICBpZiAodHVybl9vbilcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBjbGVhckludGVydmFsKG1lZ2Fib3hJbnRlcnZhbCk7XG4gICAgICAgICAgICAgICAgbWVnYWJveEludGVydmFsID0gc2V0SW50ZXJ2YWwoZnVuY3Rpb24oKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZ29Ub05leHRTbGlkZShtZWdhYm94LCB0cnVlKTtcbiAgICAgICAgICAgICAgICB9LCAzMDAwKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBjbGVhckludGVydmFsKG1lZ2Fib3hJbnRlcnZhbCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBnZXRBY3RpdmVTbGlkZShtZWdhYm94KVxuICAgICAgICB7XG4gICAgICAgICAgICByZXR1cm4galF1ZXJ5KFwiLmZlYXR1cmVkLXBvc3RzLWNvbnRhaW5lciAuZmVhdHVyZWQtcG9zdC5hY3RpdmVcIik7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBnZXRBY3RpdmVTbGlkZUluZGV4KG1lZ2Fib3gpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHJldHVybiBnZXRBY3RpdmVTbGlkZShtZWdhYm94KS5pbmRleCgpO1xuICAgICAgICB9O1xuXG4gICAgICAgIGZ1bmN0aW9uIGdldFNsaWRlQnlJbmRleChtZWdhYm94LCBpbmRleClcbiAgICAgICAge1xuICAgICAgICAgICAgcmV0dXJuIGpRdWVyeShcIi5mZWF0dXJlZC1wb3N0cy1jb250YWluZXIgLmZlYXR1cmVkLXBvc3RcIiwgbWVnYWJveCkuZXEoaW5kZXgpO1xuICAgICAgICB9O1xuXG4gICAgICAgIGZ1bmN0aW9uIHVwZGF0ZUFjdGl2ZURvdFBvc2l0aW9uKG1lZ2Fib3gpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGpRdWVyeShcIi5kb3RzIC5kb3RcIiwgbWVnYWJveCkucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIilcbiAgICAgICAgICAgICAgICAuZXEoZ2V0QWN0aXZlU2xpZGVJbmRleChtZWdhYm94KSkuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgICAgIH07XG5cbiAgICAgICAgZnVuY3Rpb24gZ29Ub1NsaWRlKG1lZ2Fib3gsIGluZGV4KVxuICAgICAgICB7XG4gICAgICAgICAgICBpZiAoZ2V0QWN0aXZlU2xpZGVJbmRleChtZWdhYm94KSA9PT0gaW5kZXgpXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuXG4gICAgICAgICAgICB2YXIgc2xpZGVzID0galF1ZXJ5KFwiLmZlYXR1cmVkLXBvc3RzLWNvbnRhaW5lciAuZmVhdHVyZWQtcG9zdFwiLCBtZWdhYm94KTtcblxuICAgICAgICAgICAgbWVnYWJveC50cmlnZ2VyKCBcImlrYWxrdWxhdG9yOmJlZm9yZU1lZ2Fib3hDaGFuZ2VcIiApO1xuICAgICAgICAgICAgc2xpZGVzLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICAgICAgc2xpZGVzLmVxKGluZGV4KS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgICAgIHVwZGF0ZUFjdGl2ZURvdFBvc2l0aW9uKG1lZ2Fib3gpO1xuICAgICAgICAgICAgbWVnYWJveC50cmlnZ2VyKCBcImlrYWxrdWxhdG9yOmFmdGVyU2xpZGVDaGFuZ2VcIiApO1xuICAgICAgICB9O1xuXG4gICAgICAgIGZ1bmN0aW9uIGdvVG9QcmV2U2xpZGUobWVnYWJveCwgbG9vcClcbiAgICAgICAge1xuICAgICAgICAgICAgbG9vcCA9IHR5cGVvZiBsb29wICE9PSBcInVuZGVmaW5lZFwiID8gbG9vcCA6IHRydWU7XG4gICAgICAgICAgICB2YXIgc2xpZGVzID0galF1ZXJ5KFwiLmZlYXR1cmVkLXBvc3RzLWNvbnRhaW5lciAuZmVhdHVyZWQtcG9zdFwiLCBtZWdhYm94KTtcblxuICAgICAgICAgICAgdmFyIGFjdGl2ZUluZGV4ID0gZ2V0QWN0aXZlU2xpZGVJbmRleChtZWdhYm94KTtcblxuICAgICAgICAgICAgaWYgKGFjdGl2ZUluZGV4ID49IDEgJiYgZ2V0U2xpZGVCeUluZGV4KG1lZ2Fib3gsIGFjdGl2ZUluZGV4IC0gMSkubGVuZ3RoKVxuICAgICAgICAgICAgICAgIGdvVG9TbGlkZShtZWdhYm94LCBhY3RpdmVJbmRleCAtIDEpO1xuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgIGlmIChsb29wKVxuICAgICAgICAgICAgICAgICAgICBnb1RvU2xpZGUobWVnYWJveCwgc2xpZGVzLmxlbmd0aCAtIDEpO1xuICAgICAgICB9O1xuXG4gICAgICAgIGZ1bmN0aW9uIGdvVG9OZXh0U2xpZGUobWVnYWJveCwgbG9vcClcbiAgICAgICAge1xuICAgICAgICAgICAgbG9vcCA9IHR5cGVvZiBsb29wICE9PSBcInVuZGVmaW5lZFwiID8gbG9vcCA6IHRydWU7XG4gICAgICAgICAgICB2YXIgYWN0aXZlSW5kZXggPSBnZXRBY3RpdmVTbGlkZUluZGV4KG1lZ2Fib3gpO1xuXG4gICAgICAgICAgICBpZiAoZ2V0U2xpZGVCeUluZGV4KG1lZ2Fib3gsIGFjdGl2ZUluZGV4ICsgMSkubGVuZ3RoKVxuICAgICAgICAgICAgICAgIGdvVG9TbGlkZShtZWdhYm94LCBhY3RpdmVJbmRleCArIDEpO1xuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgIGlmIChsb29wKVxuICAgICAgICAgICAgICAgICAgICBnb1RvU2xpZGUobWVnYWJveCwgMCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgbWVnYWJveGVzLmVhY2goZnVuY3Rpb24oKVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgbWVnYWJveCA9IGpRdWVyeSh0aGlzKTtcbiAgICAgICAgICAgIHZhciBkb3RzID0galF1ZXJ5KFwiLm5hdi1wYW5lbCAuZG90XCIsIG1lZ2Fib3gpO1xuICAgICAgICAgICAgdmFyIHByZXYgPSBqUXVlcnkoXCIubmF2LXBhbmVsIC5wcmV2XCIsIG1lZ2Fib3gpO1xuICAgICAgICAgICAgdmFyIG5leHQgPSBqUXVlcnkoXCIubmF2LXBhbmVsIC5uZXh0XCIsIG1lZ2Fib3gpO1xuXG4gICAgICAgICAgICBwcmV2Lm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGdvVG9QcmV2U2xpZGUobWVnYWJveCwgdHJ1ZSk7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgbmV4dC5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBnb1RvTmV4dFNsaWRlKG1lZ2Fib3gsIHRydWUpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIGRvdHMub24oXCJjbGlja1wiLCBmdW5jdGlvbihldm50KVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHZhciBkb3RJbmRleCA9ICBqUXVlcnkodGhpcykuaW5kZXgoKTtcbiAgICAgICAgICAgICAgICBnb1RvU2xpZGUobWVnYWJveCwgZG90SW5kZXgpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIGlmIChhdXRvUGxheSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBqUXVlcnkoXCIuZmVhdHVyZWQtcG9zdHMtY29udGFpbmVyLCAubmF2LXBhbmVsXCIsIG1lZ2Fib3gpXG4gICAgICAgICAgICAgICAgICAgIC5vbihcIm1vdXNlZW50ZXJcIiwgZnVuY3Rpb24oKVxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0b2dnbGVBdXRvUGxheShtZWdhYm94LCBmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIC5vbihcIm1vdXNlbGVhdmVcIiwgZnVuY3Rpb24oKVxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0b2dnbGVBdXRvUGxheShtZWdhYm94LCB0cnVlKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICB0b2dnbGVBdXRvUGxheShtZWdhYm94LCB0cnVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGpRdWVyeShcIi5zZWUtYWxsIC5zZWUtYWxsLWJ0blwiLG1lZ2Fib3gpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGpRdWVyeShcIi5zZWUtYWxsXCIsbWVnYWJveCkudG9nZ2xlQ2xhc3MoXCJhY3RpdmVcIik7XG5cbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHVwZGF0ZVBvc3RGbG9hdGluZ0hlYWRlclZpc2liaWxpdHkodmFsdWUpXG4gICAge1xuICAgICAgICB2YXIgZmxvYXRpbmdIZWFkZXIgPSBqUXVlcnkoXCIuZmxvYXRpbmctaGVhZGVyXCIpO1xuXG4gICAgICAgIGlmICh2YWx1ZSA+PSAwKVxuICAgICAgICAgICAgZmxvYXRpbmdIZWFkZXIuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgICAgIGVsc2VcbiAgICAgICAgICAgIGZsb2F0aW5nSGVhZGVyLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHVwZGF0ZVByb2dyZXNzQmFyKHByb2dyZXNzLCBjb250ZW50LCBjb250ZW50VG9wKVxuICAgIHtcbiAgICAgICAgdmFyIHdpbkhlaWdodCA9IGpRdWVyeSh3aW5kb3cpLmlubmVySGVpZ2h0KCk7XG5cbiAgICAgICAgdmFyIGNvbnRlbnRUb3BQYWRkaW5nID0gY29udGVudC5vZmZzZXQoKS50b3AgLSBjb250ZW50VG9wO1xuICAgICAgICB2YXIgY29udGVudEhlaWdodCA9IGNvbnRlbnRUb3BQYWRkaW5nICsgY29udGVudC5vdXRlckhlaWdodCgpO1xuICAgICAgICB2YXIgdmFsdWUgPSBqUXVlcnkod2luZG93KS5zY3JvbGxUb3AoKSAtIGNvbnRlbnRUb3A7XG4gICAgICAgIHByb2dyZXNzLmF0dHIoXCJtYXhcIiwgY29udGVudEhlaWdodCAtIHdpbkhlaWdodCk7XG4gICAgICAgIHByb2dyZXNzLmF0dHIoXCJ2YWx1ZVwiLCB2YWx1ZSk7XG5cbiAgICAgICAgdXBkYXRlUG9zdEZsb2F0aW5nSGVhZGVyVmlzaWJpbGl0eSh2YWx1ZSk7XG5cbiAgICAgICAgcmV0dXJuIHZhbHVlO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGhhbmRsZU5ld3NsZXR0ZXJTY3JvbGxUb1Jlc3BvbnNlKClcbiAgICB7XG4gICAgICAgIHZhciByZXNwb3NlRWxlbWVudCA9IGpRdWVyeShcIi5tYzR3cC1yZXNwb25zZSAubWM0d3AtYWxlcnRcIik7XG4gICAgICAgIFxuICAgICAgICBpZiAoIXJlc3Bvc2VFbGVtZW50Lmxlbmd0aClcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgXG4gICAgICAgIGlmIChyZXNwb3NlRWxlbWVudC5odG1sICE9PSBcIlwiKVxuICAgICAgICB7XG4gICAgICAgICAgICBzY3JvbGxUbyhyZXNwb3NlRWxlbWVudCwgLTgwKTtcbiAgICAgICAgfVxuICAgIH1cblxuXG4gICAgZnVuY3Rpb24gaGFuZGxlUmVhZGluZ1Byb2dyZXNzKClcbiAgICB7XG4gICAgICAgIHZhciBwcm9ncmVzcyA9IGpRdWVyeShcIi5yZWFkaW5nLXByb2dyZXNzXCIpO1xuICAgICAgICB2YXIgY29udGVudCA9IGpRdWVyeShcIi5tYWluLWNvbnRlbnQgLnBvc3QtY29udGVudFwiKTtcblxuICAgICAgICBpZiAoIXByb2dyZXNzLmxlbmd0aCB8fCAhY29udGVudC5sZW5ndGgpXG4gICAgICAgICAgICByZXR1cm47XG5cbiAgICAgICAgdmFyIG1haW5Db250ZW50ID0gY29udGVudC5jbG9zZXN0KFwiLm1haW4tY29udGVudFwiKTtcblxuICAgICAgICB2YXIgY29udGVudFRvcCA9IG1haW5Db250ZW50Lm9mZnNldCgpLnRvcDtcbiAgICAgICAgdXBkYXRlUHJvZ3Jlc3NCYXIocHJvZ3Jlc3MsIGNvbnRlbnQsIGNvbnRlbnRUb3ApO1xuXG4gICAgICAgIGpRdWVyeShkb2N1bWVudCkub24oXCJzY3JvbGxcIiwgZnVuY3Rpb24oKVxuICAgICAgICB7XG4gICAgICAgICAgICBjb250ZW50VG9wID0gbWFpbkNvbnRlbnQub2Zmc2V0KCkudG9wO1xuICAgICAgICAgICAgdmFyIHZhbHVlID0galF1ZXJ5KHdpbmRvdykuc2Nyb2xsVG9wKCkgLSBjb250ZW50VG9wO1xuICAgICAgICAgICAgcHJvZ3Jlc3MuYXR0cihcInZhbHVlXCIsICB2YWx1ZSk7XG4gICAgICAgICAgICB1cGRhdGVQb3N0RmxvYXRpbmdIZWFkZXJWaXNpYmlsaXR5KHZhbHVlKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgalF1ZXJ5KHdpbmRvdykub24oXCJyZXNpemVcIiwgZnVuY3Rpb24oKVxuICAgICAgICB7XG4gICAgICAgICAgICBjb250ZW50VG9wID0gbWFpbkNvbnRlbnQub2Zmc2V0KCkudG9wO1xuICAgICAgICAgICAgdXBkYXRlUHJvZ3Jlc3NCYXIocHJvZ3Jlc3MsIGNvbnRlbnQsIGNvbnRlbnRUb3ApO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgXG4gICAgZnVuY3Rpb24gaGFuZGxlTW9iaWxlU3dhcE9yZGVyKClcbiAgICB7XG4gICAgICAgIHZhciBjb250YWluZXJzID0galF1ZXJ5KFwiLm1vYmlsZS1zd2FwXCIpO1xuICAgICAgICBcbiAgICAgICAgY29udGFpbmVycy5lYWNoKGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIGN1cnJlbnQgPSBqUXVlcnkodGhpcyk7XG4gICAgICAgICAgICB2YXIgZWxlbWVudHMgPSBqUXVlcnkoXCI+ICpcIiwgY3VycmVudCk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoIDwgNzY4KVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGlmICghY3VycmVudC5oYXNDbGFzcyhcInN3YXBwZWRcIikpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBlbGVtZW50cy5maXJzdCgpLmRldGFjaCgpLmluc2VydEFmdGVyKGVsZW1lbnRzLmxhc3QoKSk7XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnQuYWRkQ2xhc3MoXCJzd2FwcGVkXCIpO1xuICAgICAgICAgICAgICAgIH0gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgIGlmIChjdXJyZW50Lmhhc0NsYXNzKFwic3dhcHBlZFwiKSlcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRzLmZpcnN0KCkuZGV0YWNoKCkuaW5zZXJ0QWZ0ZXIoZWxlbWVudHMubGFzdCgpKTtcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudC5yZW1vdmVDbGFzcyhcInN3YXBwZWRcIik7XG4gICAgICAgICAgICAgICAgfSAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuXG4gICAgZnVuY3Rpb24gZGlzYWJsZUltYWdlc0RyYWdnaW5nKClcbiAgICB7XG4gICAgICAgIGpRdWVyeShcIi5wb3N0LWxpc3RcIikub24oXCJtb3VzZWRvd25cIiwgXCJpbWdcIiwgZnVuY3Rpb24oZXZlbnQpIHsgZXZlbnQucHJldmVudERlZmF1bHQoKTsgfSk7XG4gICAgfVxuICAgIFxuICAgIGZ1bmN0aW9uIG9uUmVzaXplKClcbiAgICB7XHRcbiAgICAgICAgdmFyIGlzUmVzaXppbmcgID0gZmFsc2U7XG5cbiAgICAgICAgalF1ZXJ5KHdpbmRvdykub24oXCJyZXNpemVcIiwgZnVuY3Rpb24gKCkgXG4gICAgICAgIHtcbiAgICAgICAgICAgIGlmIChpc1Jlc2l6aW5nKSB7IHJldHVybjsgfVx0XHRcbiAgICAgICAgICAgIGlzUmVzaXppbmcgPSB0cnVlO1xuXG4gICAgICAgICAgICB3aW5kb3cuc2V0VGltZW91dChmdW5jdGlvbigpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgLy9wbGFjZSBmdW5jdGlvbnMgaGVyZVxuICAgICAgICAgICAgICAgIGhhbmRsZU1vYmlsZVN3YXBPcmRlcigpO1xuICAgICAgICAgICAgICAgIGhhbmRsZUNhdGVnb3J5V2l0aFNpZGViYXJNaW5IZWlnaHQoKTtcblxuICAgICAgICAgICAgICAgIGlzUmVzaXppbmcgPSBmYWxzZTtcbiAgICAgICAgICAgIH0sIDUwICk7XG4gICAgICAgIH0pO1x0XG4gICAgICAgICAgICAgICBcblx0XHRqUXVlcnkod2luZG93KS50cmlnZ2VyKFwicmVzaXplXCIpO1xuICAgIH1cblxuXG5cbiAgICAvKiBfX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fIHB1YmxpYyBtZXRob2RzICovXG5cblx0dmFyIGluaXQgPSBmdW5jdGlvbigpXG5cdHtcbiAgICAgICAgaGFuZGxlTWVudSgpO1xuICAgICAgICBoYW5kbGVNb2JpbGVNZW51KCk7XG4gICAgICAgIGhhbmRsZU1hZ2Vib3hOYXZpZ2F0aW9uKCk7XG4gICAgICAgIGhhbmRsZVNjcm9sbFRvQ29udGVudCgpO1xuICAgICAgICBoYW5kbGVSZWFkaW5nUHJvZ3Jlc3MoKTtcbiAgICAgICAgaGFuZGxlU3BvdGxpZ2h0U2VhcmNoKCk7XG4gICAgICAgIGhhbmRsZVNoYXJlTGlua3MoKTtcbiAgICAgICAgaGFuZGxlUHJvZHVjdHNHcmlkKCk7XG4gICAgICAgIGhhbmRsZUFuYWx5dGljcygpO1xuICAgICAgICBoYW5kbGVMaXN0TnVtYmVyaW5nKCk7XG4gICAgICAgIGhhbmRsZUNhdGVnb3J5V2l0aFNpZGViYXJNaW5IZWlnaHQoKTtcbiAgICAgICAgXG4gICAgICAgIGNoZWNrRWxlbWVudHNWaXNpYmlsaXR5T25TY3JvbGwoXCIuc2Nyb2xsLWhhbmRsZVwiKTtcbiAgICAgICAgZGlzYWJsZUltYWdlc0RyYWdnaW5nKCk7XG4gICAgICAgIGhhbmRsZU5ld3NsZXR0ZXJTY3JvbGxUb1Jlc3BvbnNlKCk7XG5cblx0XHRvblJlc2l6ZSgpO1xuXHRcdGpRdWVyeSh3aW5kb3cpLnRyaWdnZXIoXCJzY3JvbGxcIik7XG5cblx0fTtcblxuXHRpbml0KCk7XG59O1xuXG52YXIgaWthbGt1bGF0b3I7XG5cbmpRdWVyeShkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oJClcbntcblx0aWthbGt1bGF0b3IgPSBuZXcgaWthbGt1bGF0b3IoKTtcblx0cHJvZHVjdF9maWx0ZXJzID0gbmV3IHByb2R1Y3RGaWx0ZXIoKTtcblx0Y29tcGFuaWVzID0gbmV3IGNvbXBhbmllcygpO1xuICAgIHNlYXJjaGJveCA9IG5ldyBzZWFyY2hib3goKTtcbiAgICBwcm9tb2JveCA9IG5ldyBwcm9tb2JveCgpO1xuICAgIGNvbW1lbnRzID0gbmV3IGNvbW1lbnRzKCk7XG59KTtcbiIsImNsYXNzIHBlcmNlbnRPZkNhbGN1bGF0b3Ige1xuICBjb25zdHJ1Y3Rvcih0YXJnZXRJZCkge1xuICAgIHZhciBzZWxmID0gdGhpcztcbiAgICB0aGlzLnRhcmdldElEID0gdGFyZ2V0SWQ7XG4gICAgdGhpcy50YXJnZXQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCh0YXJnZXRJZCk7XG4gICAgdGhpcy50YXJnZXQuaW5uZXJIVE1MID0gJzxmb3JtIGNsYXNzPVwiaWthbENhbGMgaWthbENhbGNfX3BlcmNPZlwiIGFjdGlvbj1cIlwiPjxkaXYgY2xhc3M9XCJyb3dcIj5PYmxpY3ogPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpa2FsQ2FsY19fcGVyY09mX19pbnB1dDFcIiBuYW1lPVwiaWthbENhbGNfX3BlcmNPZl9faW5wdXQxXCI+JSB6IDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiaWthbENhbGNfX3BlcmNPZl9faW5wdXQyXCIgbmFtZT1cImlrYWxDYWxjX19wZXJjT2ZfX2lucHV0MlwiPjwvZGl2PjxkaXYgY2xhc3M9XCJyb3dcIj5XeW5pazogPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpa2FsQ2FsY19fcGVyY09mX19vdXRwdXRcIiBuYW1lPVwiaWthbENhbGNfX3BlcmNPZl9fb3V0cHV0XCIgZGlzYWJsZWQ+PC9kaXY+PC9mb3JtPic7XG5cbiAgICB0aGlzLmlucHV0cyA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiaW5wdXRcIik7XG4gICAgdGhpcy5pbnB1dDEgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19fcGVyY09mX19pbnB1dDEnKVswXTtcbiAgICB0aGlzLmlucHV0MiA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2lrYWxDYWxjX19wZXJjT2ZfX2lucHV0MicpWzBdO1xuICAgIHRoaXMub3V0cHV0ID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnaWthbENhbGNfX3BlcmNPZl9fb3V0cHV0JylbMF07XG5cbiAgICBmb3IobGV0IGkgPSAwOyBpPHRoaXMuaW5wdXRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB0aGlzLnByZXZlbnROb25OdW1lcmljKHRoaXMuaW5wdXRzW2ldKTtcbiAgICB9XG5cbiAgICB0aGlzLmlucHV0MS5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcbiAgICB0aGlzLmlucHV0Mi5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcbiAgfVxuICBwcmV2ZW50Tm9uTnVtZXJpYyhpbnB1dCkge1xuICAgIGlucHV0LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXlwcmVzc1wiLCBmdW5jdGlvbiAoZXZ0KSB7XG4gICAgICBpZiAoZXZ0LndoaWNoIDwgNDYgfHwgZXZ0LndoaWNoID09IDQ3IHx8IGV2dC53aGljaCA+IDU3KSB7XG4gICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG4gIHJlc3VsdCgpIHtcbiAgICBsZXQgcmVzdWx0ID0gZXZhbCgoIHRoaXMuaW5wdXQxLnZhbHVlIC8gMTAwICkgKiB0aGlzLmlucHV0Mi52YWx1ZSk7XG4gICAgcmVzdWx0ID0rIChNYXRoLnJvdW5kKHJlc3VsdCArIFwiZSs2XCIpICsgXCJlLTZcIik7XG4gICAgaWYoIWlzTmFOKHJlc3VsdCkpIHtcbiAgICAgIHRoaXMub3V0cHV0LnZhbHVlID0gcmVzdWx0O1xuICAgIH1cbiAgfVxufVxuXG5jbGFzcyB3aGF0UGVyY2VudENhbGN1bGF0b3Ige1xuICBjb25zdHJ1Y3Rvcih0YXJnZXRJZCkge1xuICAgIHZhciBzZWxmID0gdGhpcztcbiAgICB0aGlzLnRhcmdldElEID0gdGFyZ2V0SWQ7XG4gICAgdGhpcy50YXJnZXQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCh0YXJnZXRJZCk7XG4gICAgdGhpcy50YXJnZXQuaW5uZXJIVE1MID0gJzxmb3JtIGNsYXNzPVwiaWthbENhbGMgaWthbENhbGNfX3doYXRQZXJjXCIgYWN0aW9uPVwiXCI+PGRpdiBjbGFzcz1cInJvd1wiPjxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiaWthbENhbGNfX3doYXRQZXJjX19pbnB1dDFcIiBuYW1lPVwiaWthbENhbGNfX3doYXRQZXJjX19pbnB1dDFcIj4gamVzdCBqYWtpbSBwcm9jZW50ZW0geiA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImlrYWxDYWxjX193aGF0UGVyY19faW5wdXQyXCIgbmFtZT1cImlrYWxDYWxjX193aGF0UGVyY19faW5wdXQyXCI+PC9kaXY+PGRpdiBjbGFzcz1cInJvd1wiPld5bmlrOiA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImlrYWxDYWxjX193aGF0UGVyY19fb3V0cHV0XCIgbmFtZT1cImlrYWxDYWxjX193aGF0UGVyY19fb3V0cHV0XCIgZGlzYWJsZWQ+PC9kaXY+PC9mb3JtPic7XG5cbiAgICB0aGlzLmlucHV0cyA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiaW5wdXRcIik7XG4gICAgdGhpcy5pbnB1dDEgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19fd2hhdFBlcmNfX2lucHV0MScpWzBdO1xuICAgIHRoaXMuaW5wdXQyID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnaWthbENhbGNfX3doYXRQZXJjX19pbnB1dDInKVswXTtcbiAgICB0aGlzLm91dHB1dCA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2lrYWxDYWxjX193aGF0UGVyY19fb3V0cHV0JylbMF07XG5cbiAgICBmb3IobGV0IGkgPSAwOyBpPHRoaXMuaW5wdXRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB0aGlzLnByZXZlbnROb25OdW1lcmljKHRoaXMuaW5wdXRzW2ldKTtcbiAgICB9XG5cbiAgICB0aGlzLmlucHV0MS5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcbiAgICB0aGlzLmlucHV0Mi5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcbiAgfVxuICBwcmV2ZW50Tm9uTnVtZXJpYyhpbnB1dCkge1xuICAgIGlucHV0LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXlwcmVzc1wiLCBmdW5jdGlvbiAoZXZ0KSB7XG4gICAgICBpZiAoZXZ0LndoaWNoIDwgNDYgfHwgZXZ0LndoaWNoID09IDQ3IHx8IGV2dC53aGljaCA+IDU3KSB7XG4gICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG4gIHJlc3VsdCgpIHtcbiAgICBsZXQgcmVzdWx0ID0gZXZhbCggdGhpcy5pbnB1dDEudmFsdWUgLyAodGhpcy5pbnB1dDIudmFsdWUgLyAxMDApICk7XG4gICAgcmVzdWx0ID0rIChNYXRoLnJvdW5kKHJlc3VsdCArIFwiZSs2XCIpICsgXCJlLTZcIik7XG4gICAgaWYoIWlzTmFOKHJlc3VsdCkpIHtcbiAgICAgIHRoaXMub3V0cHV0LnZhbHVlID0gcmVzdWx0O1xuICAgIH1cbiAgfVxufVxuXG5jbGFzcyBwZXJjZW50UmFpc2VDYWxjdWxhdG9yIHtcbiAgY29uc3RydWN0b3IodGFyZ2V0SWQpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgdGhpcy50YXJnZXRJRCA9IHRhcmdldElkO1xuICAgIHRoaXMudGFyZ2V0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQodGFyZ2V0SWQpO1xuICAgIHRoaXMudGFyZ2V0LmlubmVySFRNTCA9ICc8Zm9ybSBjbGFzcz1cImlrYWxDYWxjIGlrYWxDYWxjX19wZXJjUmFpc2VcIiBhY3Rpb249XCJcIj48ZGl2IGNsYXNzPVwicm93XCI+TyBqYWtpIHByb2NlbnQgd3pyb3PFgmEvem1hbGHFgmEgbGljemJhPC9kaXY+PGRpdiBjbGFzcz1cInJvd1wiPnogPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpa2FsQ2FsY19fcGVyY1JhaXNlX19pbnB1dDFcIiBuYW1lPVwiaWthbENhbGNfX3BlcmNSYWlzZV9faW5wdXQxXCI+IGRvIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiaWthbENhbGNfX3BlcmNSYWlzZV9faW5wdXQyXCIgbmFtZT1cImlrYWxDYWxjX19wZXJjUmFpc2VfX2lucHV0MlwiPj88L2Rpdj48ZGl2IGNsYXNzPVwicm93XCI+V3luaWs6IDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiaWthbENhbGNfX3BlcmNSYWlzZV9fb3V0cHV0XCIgbmFtZT1cImlrYWxDYWxjX19wZXJjUmFpc2VfX291dHB1dFwiIGRpc2FibGVkPiU8L2Rpdj48L2Zvcm0+JztcblxuICAgIHRoaXMuaW5wdXRzID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJpbnB1dFwiKTtcbiAgICB0aGlzLmlucHV0MSA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2lrYWxDYWxjX19wZXJjUmFpc2VfX2lucHV0MScpWzBdO1xuICAgIHRoaXMuaW5wdXQyID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnaWthbENhbGNfX3BlcmNSYWlzZV9faW5wdXQyJylbMF07XG4gICAgdGhpcy5vdXRwdXQgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19fcGVyY1JhaXNlX19vdXRwdXQnKVswXTtcblxuICAgIGZvcihsZXQgaSA9IDA7IGk8dGhpcy5pbnB1dHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHRoaXMucHJldmVudE5vbk51bWVyaWModGhpcy5pbnB1dHNbaV0pO1xuICAgIH1cblxuICAgIHRoaXMuaW5wdXQxLmFkZEV2ZW50TGlzdGVuZXIoJ2tleXVwJywgZnVuY3Rpb24oKSB7XG4gICAgICBzZWxmLnJlc3VsdCgpO1xuICAgIH0pO1xuICAgIHRoaXMuaW5wdXQyLmFkZEV2ZW50TGlzdGVuZXIoJ2tleXVwJywgZnVuY3Rpb24oKSB7XG4gICAgICBzZWxmLnJlc3VsdCgpO1xuICAgIH0pO1xuICB9XG4gIHByZXZlbnROb25OdW1lcmljKGlucHV0KSB7XG4gICAgaW5wdXQuYWRkRXZlbnRMaXN0ZW5lcihcImtleXByZXNzXCIsIGZ1bmN0aW9uIChldnQpIHtcbiAgICAgIGlmIChldnQud2hpY2ggPCA0NiB8fCBldnQud2hpY2ggPT0gNDcgfHwgZXZ0LndoaWNoID4gNTcpIHtcbiAgICAgICAgZXZ0LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cbiAgcmVzdWx0KCkge1xuICAgIGxldCByZXN1bHQgPSBldmFsKCAodGhpcy5pbnB1dDIudmFsdWUgKiAxMDApIC8gdGhpcy5pbnB1dDEudmFsdWUgLSAxMDApO1xuICAgIHJlc3VsdCA9KyAoTWF0aC5yb3VuZChyZXN1bHQgKyBcImUrNlwiKSArIFwiZS02XCIpO1xuICAgIGlmKCFpc05hTihyZXN1bHQpKSB7XG4gICAgICB0aGlzLm91dHB1dC52YWx1ZSA9IHJlc3VsdDtcbiAgICB9XG4gIH1cbn1cblxuXG5jbGFzcyBwZXJjZW50QWRkaXRpb25DYWxjdWxhdG9yIHtcbiAgY29uc3RydWN0b3IodGFyZ2V0SWQpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgdGhpcy50YXJnZXRJRCA9IHRhcmdldElkO1xuICAgIHRoaXMudGFyZ2V0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQodGFyZ2V0SWQpO1xuICAgIHRoaXMudGFyZ2V0LmlubmVySFRNTCA9ICc8Zm9ybSBjbGFzcz1cImlrYWxDYWxjIGlrYWxDYWxjX19wZXJjQWRkaXRpb25cIiBhY3Rpb249XCJcIj48ZGl2IGNsYXNzPVwicm93XCI+RG9kYWogcHJvY2VudCBkbyBsaWN6Ynk8L2Rpdj48ZGl2IGNsYXNzPVwicm93XCI+PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpa2FsQ2FsY19fcGVyY0FkZGl0aW9uX19pbnB1dDFcIiBuYW1lPVwiaWthbENhbGNfX3BlcmNBZGRpdGlvbl9faW5wdXQxXCI+ICsgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpa2FsQ2FsY19fcGVyY0FkZGl0aW9uX19pbnB1dDJcIiBuYW1lPVwiaWthbENhbGNfX3BlcmNBZGRpdGlvbl9faW5wdXQyXCI+JTwvZGl2PjxkaXYgY2xhc3M9XCJyb3dcIj5XeW5pazogPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpa2FsQ2FsY19fcGVyY0FkZGl0aW9uX19vdXRwdXRcIiBuYW1lPVwiaWthbENhbGNfX3BlcmNBZGRpdGlvbl9fb3V0cHV0XCIgZGlzYWJsZWQ+PC9kaXY+PC9mb3JtPic7XG5cbiAgICB0aGlzLmlucHV0cyA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiaW5wdXRcIik7XG4gICAgdGhpcy5pbnB1dDEgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19fcGVyY0FkZGl0aW9uX19pbnB1dDEnKVswXTtcbiAgICB0aGlzLmlucHV0MiA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2lrYWxDYWxjX19wZXJjQWRkaXRpb25fX2lucHV0MicpWzBdO1xuICAgIHRoaXMub3V0cHV0ID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnaWthbENhbGNfX3BlcmNBZGRpdGlvbl9fb3V0cHV0JylbMF07XG5cbiAgICBmb3IobGV0IGkgPSAwOyBpPHRoaXMuaW5wdXRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB0aGlzLnByZXZlbnROb25OdW1lcmljKHRoaXMuaW5wdXRzW2ldKTtcbiAgICB9XG5cbiAgICB0aGlzLmlucHV0MS5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcbiAgICB0aGlzLmlucHV0Mi5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcbiAgfVxuICBwcmV2ZW50Tm9uTnVtZXJpYyhpbnB1dCkge1xuICAgIGlucHV0LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXlwcmVzc1wiLCBmdW5jdGlvbiAoZXZ0KSB7XG4gICAgICBpZiAoZXZ0LndoaWNoIDwgNDYgfHwgZXZ0LndoaWNoID09IDQ3IHx8IGV2dC53aGljaCA+IDU3KSB7XG4gICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG4gIHJlc3VsdCgpIHtcbiAgICBsZXQgcmVzdWx0ID0gZXZhbCggdGhpcy5pbnB1dDEudmFsdWUgKiAoIHRoaXMuaW5wdXQyLnZhbHVlIC8gMTAwICsgMSApICk7XG4gICAgcmVzdWx0ID0rIChNYXRoLnJvdW5kKHJlc3VsdCArIFwiZSs2XCIpICsgXCJlLTZcIik7XG4gICAgaWYoIWlzTmFOKHJlc3VsdCkpIHtcbiAgICAgIHRoaXMub3V0cHV0LnZhbHVlID0gcmVzdWx0O1xuICAgIH1cbiAgfVxufVxuXG5jbGFzcyBwZXJjZW50U3VidHJhY3RDYWxjdWxhdG9yIHtcbiAgY29uc3RydWN0b3IodGFyZ2V0SWQpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgdGhpcy50YXJnZXRJRCA9IHRhcmdldElkO1xuICAgIHRoaXMudGFyZ2V0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQodGFyZ2V0SWQpO1xuICAgIHRoaXMudGFyZ2V0LmlubmVySFRNTCA9ICc8Zm9ybSBjbGFzcz1cImlrYWxDYWxjIGlrYWxDYWxjX19wZXJjU3VidHJhY3RcIiBhY3Rpb249XCJcIj48ZGl2IGNsYXNzPVwicm93XCI+T2Rlam1paiBwcm9jZW50IG9kIGxpY3pieTwvZGl2PjxkaXYgY2xhc3M9XCJyb3dcIj48aW5wdXQgdHlwZT1cInRleHRcIiBtaW49XCIwXCIgbWF4PVwiMTAwXCIgY2xhc3M9XCJpa2FsQ2FsY19fcGVyY1N1YnRyYWN0X19pbnB1dDFcIiBuYW1lPVwiaWthbENhbGNfX3BlcmNTdWJ0cmFjdF9faW5wdXQxXCI+IC0gPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpa2FsQ2FsY19fcGVyY1N1YnRyYWN0X19pbnB1dDJcIiBuYW1lPVwiaWthbENhbGNfX3BlcmNTdWJ0cmFjdF9faW5wdXQyXCI+JTwvZGl2PjxkaXYgY2xhc3M9XCJyb3dcIj5XeW5pazogPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpa2FsQ2FsY19fcGVyY1N1YnRyYWN0X19vdXRwdXRcIiBuYW1lPVwiaWthbENhbGNfX3BlcmNTdWJ0cmFjdF9fb3V0cHV0XCIgZGlzYWJsZWQ+PC9kaXY+PC9mb3JtPic7XG5cbiAgICB0aGlzLmlucHV0cyA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiaW5wdXRcIik7XG4gICAgdGhpcy5pbnB1dDEgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19fcGVyY1N1YnRyYWN0X19pbnB1dDEnKVswXTtcbiAgICB0aGlzLmlucHV0MiA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2lrYWxDYWxjX19wZXJjU3VidHJhY3RfX2lucHV0MicpWzBdO1xuICAgIHRoaXMub3V0cHV0ID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnaWthbENhbGNfX3BlcmNTdWJ0cmFjdF9fb3V0cHV0JylbMF07XG5cbiAgICBmb3IobGV0IGkgPSAwOyBpPHRoaXMuaW5wdXRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB0aGlzLnByZXZlbnROb25OdW1lcmljKHRoaXMuaW5wdXRzW2ldKTtcbiAgICB9XG5cbiAgICB0aGlzLmlucHV0MS5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcbiAgICB0aGlzLmlucHV0Mi5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcbiAgfVxuICBwcmV2ZW50Tm9uTnVtZXJpYyhpbnB1dCkge1xuICAgIGlucHV0LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXlwcmVzc1wiLCBmdW5jdGlvbiAoZXZ0KSB7XG4gICAgICBpZiAoZXZ0LndoaWNoIDwgNDYgfHwgZXZ0LndoaWNoID09IDQ3IHx8IGV2dC53aGljaCA+IDU3KSB7XG4gICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG4gIHJlc3VsdCgpIHtcbiAgICBsZXQgcmVzdWx0ID0gZXZhbCggdGhpcy5pbnB1dDEudmFsdWUgKiAoIDEgLSB0aGlzLmlucHV0Mi52YWx1ZSAvIDEwMCApICk7XG4gICAgcmVzdWx0ID0rIChNYXRoLnJvdW5kKHJlc3VsdCArIFwiZSs2XCIpICsgXCJlLTZcIik7XG4gICAgaWYoIWlzTmFOKHJlc3VsdCkpIHtcbiAgICAgIHRoaXMub3V0cHV0LnZhbHVlID0gcmVzdWx0O1xuICAgIH1cbiAgfVxufVxuXG5jbGFzcyBjb21wb3VuZEludGVyZXN0Q2FsY3VsYXRvciB7XG4gIGNvbnN0cnVjdG9yKHRhcmdldElkKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHRoaXMudGFyZ2V0SUQgPSB0YXJnZXRJZDtcbiAgICB0aGlzLnRhcmdldCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHRhcmdldElkKTtcbiAgICB0aGlzLnRhcmdldC5pbm5lckhUTUwgPSAnPGZvcm0gY2xhc3M9XCJpa2FsQ2FsYyBpa2FsQ2FsY19fY29tcEludGVyZXN0XCIgYWN0aW9uPVwiXCI+PGRpdiBjbGFzcz1cInJvd1wiPjxzcGFuPkthcGl0YcWCIHBvY3rEhXRrb3d5PC9zcGFuPiA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImlrYWxDYWxjX19jb21wSW50ZXJlc3RfX2luaXRCYWxhbmNlXCIgbmFtZT1cImlrYWxDYWxjX19jb21wSW50ZXJlc3RfX2luaXRCYWxhbmNlXCI+PC9kaXY+PGRpdiBjbGFzcz1cInJvd1wiPjxzcGFuPk9wcm9jZW50b3dhbmllPC9zcGFuPiA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImlrYWxDYWxjX19jb21wSW50ZXJlc3RfX2ludFJhdGVcIiBuYW1lPVwiaWthbENhbGNfX2NvbXBJbnRlcmVzdF9faW50UmF0ZVwiPjwvZGl2PjxkaXYgY2xhc3M9XCJyb3dcIj48c3Bhbj5LYXBpdGFsaXphY2phPC9zcGFuPiA8c2VsZWN0IGNsYXNzPVwiaWthbENhbGNfX2NvbXBJbnRlcmVzdF9fY29tcEZyZXFcIiBuYW1lPVwiaWthbENhbGNfX2NvbXBJbnRlcmVzdF9fY29tcEZyZXFcIj48b3B0aW9uIHZhbHVlPVwiMzY1XCI+ZHppZW5uYTwvb3B0aW9uPjxvcHRpb24gdmFsdWU9XCI1MlwiPnR5Z29kbmlvd2E8L29wdGlvbj48b3B0aW9uIHZhbHVlPVwiMTJcIj5taWVzacSZY3puYTwvb3B0aW9uPjxvcHRpb24gdmFsdWU9XCI0XCI+a3dhcnRhbG5hPC9vcHRpb24+PG9wdGlvbiB2YWx1ZT1cIjJcIj5ww7PFgnJvY3puYTwvb3B0aW9uPjxvcHRpb24gdmFsdWU9XCIxXCI+cm9jem5hPC9vcHRpb24+PC9zZWxlY3Q+PC9kaXY+PGRpdiBjbGFzcz1cInJvd1wiPjxzcGFuPkxpY3piYSBsYXQ8L3NwYW4+IDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiaWthbENhbGNfX2NvbXBJbnRlcmVzdF9feWVhcnNcIiBuYW1lPVwiaWthbENhbGNfX2NvbXBJbnRlcmVzdF9feWVhcnNcIj48L2Rpdj48ZGl2IGNsYXNzPVwicm93XCI+PHNwYW4+S2FwaXRhxYIga2/FhGNvd3k8L3NwYW4+IDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiaWthbENhbGNfX2NvbXBJbnRlcmVzdF9fb3V0cHV0XCIgbmFtZT1cImlrYWxDYWxjX19jb21wSW50ZXJlc3RfX291dHB1dFwiIGRpc2FibGVkPjwvZGl2PjwvZm9ybT4nO1xuXG4gICAgdGhpcy5pbnB1dHMgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5VGFnTmFtZShcImlucHV0XCIpO1xuICAgIHRoaXMuaW5pdEJhbGFuY2UgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19fY29tcEludGVyZXN0X19pbml0QmFsYW5jZScpWzBdO1xuICAgIHRoaXMuaW50UmF0ZSA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2lrYWxDYWxjX19jb21wSW50ZXJlc3RfX2ludFJhdGUnKVswXTtcbiAgICB0aGlzLmNvbXBGcmVxID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnaWthbENhbGNfX2NvbXBJbnRlcmVzdF9fY29tcEZyZXEnKVswXTtcbiAgICB0aGlzLnllYXJzID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnaWthbENhbGNfX2NvbXBJbnRlcmVzdF9feWVhcnMnKVswXTtcbiAgICB0aGlzLm91dHB1dCA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2lrYWxDYWxjX19jb21wSW50ZXJlc3RfX291dHB1dCcpWzBdO1xuXG4gICAgZm9yKGxldCBpID0gMDsgaTx0aGlzLmlucHV0cy5sZW5ndGg7IGkrKykge1xuICAgICAgdGhpcy5wcmV2ZW50Tm9uTnVtZXJpYyh0aGlzLmlucHV0c1tpXSk7XG4gICAgfVxuXG4gICAgdGhpcy5pbml0QmFsYW5jZS5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcblxuICAgIHRoaXMuaW50UmF0ZS5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcblxuICAgIHRoaXMuY29tcEZyZXEuYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgZnVuY3Rpb24oKSB7XG4gICAgICBzZWxmLnJlc3VsdCgpO1xuICAgIH0pO1xuXG4gICAgdGhpcy55ZWFycy5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcbiAgfVxuICBwcmV2ZW50Tm9uTnVtZXJpYyhpbnB1dCkge1xuICAgIGlucHV0LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXlwcmVzc1wiLCBmdW5jdGlvbiAoZXZ0KSB7XG4gICAgICBpZiAoZXZ0LndoaWNoIDwgNDYgfHwgZXZ0LndoaWNoID09IDQ3IHx8IGV2dC53aGljaCA+IDU3KSB7XG4gICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG4gIHJlc3VsdCgpIHtcbiAgICBjb25zdCBpbml0QmFsYW5jZSA9IHRoaXMuaW5pdEJhbGFuY2UudmFsdWU7XG4gICAgY29uc3QgaW50UmF0ZSA9IHRoaXMuaW50UmF0ZS52YWx1ZSAvIDEwMDtcbiAgICBjb25zdCBjb21wRnJlcSA9IHRoaXMuY29tcEZyZXEudmFsdWU7XG4gICAgY29uc3QgeWVhcnMgPSB0aGlzLnllYXJzLnZhbHVlO1xuXG4gICAgbGV0IGNhbGMxID0gaW50UmF0ZSAvIGNvbXBGcmVxO1xuICAgIGxldCBjYWxjMiA9IDEgKyBjYWxjMTtcbiAgICBsZXQgY2FsYzMgPSBjb21wRnJlcSAqIHllYXJzO1xuICAgIGxldCBjYWxjNCA9IE1hdGgucG93KGNhbGMyLCBjYWxjMyk7XG4gICAgbGV0IGNhbGM1ID0gaW5pdEJhbGFuY2UgKiBjYWxjNDtcblxuICAgIGxldCByZXN1bHQgPSBldmFsKCBjYWxjNSApO1xuICAgIHJlc3VsdCA9KyAoTWF0aC5yb3VuZChyZXN1bHQgKyBcImUrMlwiKSArIFwiZS0yXCIpO1xuICAgIGlmKCFpc05hTihyZXN1bHQpKSB7XG4gICAgICB0aGlzLm91dHB1dC52YWx1ZSA9IHJlc3VsdDtcbiAgICB9XG4gIH1cbn1cblxuXG5jbGFzcyBpbnZlc3RtZW50Q2FsY3VsYXRvciB7XG4gIGNvbnN0cnVjdG9yKHRhcmdldElkKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHRoaXMudGFyZ2V0SUQgPSB0YXJnZXRJZDtcbiAgICBjb25zb2xlLmxvZyh0aGlzLnRhcmdldElEKTtcbiAgICB0aGlzLnRhcmdldCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHRhcmdldElkKTtcbiAgICBjb25zb2xlLmxvZyh0aGlzLnRhcmdldCk7XG4gICAgdGhpcy50YXJnZXQuaW5uZXJIVE1MID0gJzxmb3JtIGNsYXNzPVwiaWthbENhbGMgaWthbENhbGNfX2ludmVzdG1lbnRcIiBhY3Rpb249XCJcIj48ZGl2IGNsYXNzPVwicm93XCI+PHNwYW4+S2FwaXRhxYIgcG9jesSFdGtvd3k8L3NwYW4+PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpa2FsQ2FsY19faW52ZXN0bWVudF9faW5pdEJhbGFuY2VcIiBuYW1lPVwiaWthbENhbGNfX2ludmVzdG1lbnRfX2luaXRCYWxhbmNlXCI+PC9kaXY+PGRpdiBjbGFzcz1cInJvd1wiPjxzcGFuPk9rcmVzIHVtb3d5PC9zcGFuPjxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiaWthbENhbGNfX2ludmVzdG1lbnRfX3RpbWVfX3ZhbHVlXCIgbmFtZT1cImlrYWxDYWxjX19pbnZlc3RtZW50X190aW1lX192YWx1ZVwiPjxzZWxlY3QgY2xhc3M9XCJpa2FsQ2FsY19faW52ZXN0bWVudF9fdGltZV9fdW5pdFwiIG5hbWU9XCJpa2FsQ2FsY19faW52ZXN0bWVudF9fdGltZV9fdW5pdFwiPjxvcHRpb24gdmFsdWU9XCJkYXlzXCI+ZG5pPC9vcHRpb24+PG9wdGlvbiB2YWx1ZT1cIm1vbnRoc1wiPm1pZXNpxIVjZTwvb3B0aW9uPjxvcHRpb24gdmFsdWU9XCJ5ZWFyc1wiPmxhdGE8L29wdGlvbj48L3NlbGVjdD48L2Rpdj48ZGl2IGNsYXNzPVwicm93XCI+PHNwYW4+T3Byb2NlbnRvd2FuaWU8L3NwYW4+PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpa2FsQ2FsY19faW52ZXN0bWVudF9faW50UmF0ZVwiIG5hbWU9XCJpa2FsQ2FsY19faW52ZXN0bWVudF9faW50UmF0ZVwiPjwvZGl2PjxkaXYgY2xhc3M9XCJyb3dcIj48c3Bhbj5LYXBpdGFsaXphY2phPC9zcGFuPjxzZWxlY3QgY2xhc3M9XCJpa2FsQ2FsY19faW52ZXN0bWVudF9fY29tcEZyZXFcIiBuYW1lPVwiaWthbENhbGNfX2ludmVzdG1lbnRfX2NvbXBGcmVxXCI+PG9wdGlvbiB2YWx1ZT1cInllYXJseVwiPnJvY3puYTwvb3B0aW9uPjxvcHRpb24gdmFsdWU9XCJtb250aGx5XCI+bWllc2nEmWN6bmE8L29wdGlvbj48b3B0aW9uIHZhbHVlPVwiZGFpbHlcIj5kemllbm5hPC9vcHRpb24+PG9wdGlvbiB2YWx1ZT1cIm92ZXJhbGxcIj5uYSBrb25pZWMgb2tyZXN1PC9vcHRpb24+PC9zZWxlY3Q+PC9kaXY+PGRpdiBjbGFzcz1cInJvd1wiPjxkaXYgY2xhc3M9XCJjaGVja2JveFwiPjxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBjbGFzcz1cImlrYWxDYWxjX19pbnZlc3RtZW50X190YXhcIiBpZD1cImlrYWxDYWxjX19pbnZlc3RtZW50X190YXhcIiBuYW1lPVwiaWthbENhbGNfX2ludmVzdG1lbnRfX3RheFwiPjxsYWJlbCBmb3I9XCJpa2FsQ2FsY19faW52ZXN0bWVudF9fdGF4XCI+VXd6Z2zEmWRuaWogcG9kYXRlazwvbGFiZWw+PC9kaXY+PC9kaXY+PGRpdiBjbGFzcz1cInJvd1wiPjxsYWJlbCBpZD1cImlrYWxDYWxjX19pbnZlc3RtZW50X19vdXRwdXRcIj5LYXBpdGHFgiBrb8WEY293eTwvbGFiZWw+PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpa2FsQ2FsY19faW52ZXN0bWVudF9fb3V0cHV0XCIgbmFtZT1cImlrYWxDYWxjX19pbnZlc3RtZW50X19vdXRwdXRcIj48L2Rpdj48L2Zvcm0+JztcblxuICAgIHRoaXMuaW5wdXRzID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJpbnB1dFwiKTtcbiAgICB0aGlzLmluaXRCYWxhbmNlID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnaWthbENhbGNfX2ludmVzdG1lbnRfX2luaXRCYWxhbmNlJylbMF07XG4gICAgdGhpcy50aW1lVmFsdWUgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19faW52ZXN0bWVudF9fdGltZV9fdmFsdWUnKVswXTtcbiAgICB0aGlzLnRpbWVVbml0ID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnaWthbENhbGNfX2ludmVzdG1lbnRfX3RpbWVfX3VuaXQnKVswXTtcbiAgICB0aGlzLmludFJhdGUgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19faW52ZXN0bWVudF9faW50UmF0ZScpWzBdO1xuICAgIHRoaXMuY29tcEZyZXEgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19faW52ZXN0bWVudF9fY29tcEZyZXEnKVswXTtcbiAgICB0aGlzLnRheCA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2lrYWxDYWxjX19pbnZlc3RtZW50X190YXgnKVswXTtcbiAgICB0aGlzLm91dHB1dCA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2lrYWxDYWxjX19pbnZlc3RtZW50X19vdXRwdXQnKVswXTtcblxuICAgIGZvcihsZXQgaSA9IDA7IGk8dGhpcy5pbnB1dHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHRoaXMucHJldmVudE5vbk51bWVyaWModGhpcy5pbnB1dHNbaV0pO1xuICAgIH1cblxuICAgIHRoaXMuaW5pdEJhbGFuY2UuYWRkRXZlbnRMaXN0ZW5lcigna2V5dXAnLCBmdW5jdGlvbigpIHtcbiAgICAgIHNlbGYucmVzdWx0KCk7XG4gICAgfSk7XG5cbiAgICB0aGlzLnRpbWVWYWx1ZS5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcblxuICAgIHRoaXMudGltZVVuaXQuYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgZnVuY3Rpb24oKSB7XG4gICAgICBzZWxmLnJlc3VsdCgpO1xuICAgIH0pO1xuXG4gICAgdGhpcy5pbnRSYXRlLmFkZEV2ZW50TGlzdGVuZXIoJ2tleXVwJywgZnVuY3Rpb24oKSB7XG4gICAgICBzZWxmLnJlc3VsdCgpO1xuICAgIH0pO1xuXG4gICAgdGhpcy5jb21wRnJlcS5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcbiAgICAgIHNlbGYucmVzdWx0KCk7XG4gICAgfSk7XG5cbiAgICB0aGlzLnRheC5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcbiAgICAgIHNlbGYucmVzdWx0KCk7XG4gICAgfSk7XG4gIH1cbiAgcHJldmVudE5vbk51bWVyaWMoaW5wdXQpIHtcbiAgICBpbnB1dC5hZGRFdmVudExpc3RlbmVyKFwia2V5cHJlc3NcIiwgZnVuY3Rpb24gKGV2dCkge1xuICAgICAgaWYgKGV2dC53aGljaCA8IDQ2IHx8IGV2dC53aGljaCA9PSA0NyB8fCBldnQud2hpY2ggPiA1Nykge1xuICAgICAgICBldnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuICByZXN1bHQoKSB7XG4gICAgY29uc3QgaW5pdEJhbGFuY2UgPSB0aGlzLmluaXRCYWxhbmNlLnZhbHVlO1xuXG4gICAgbGV0IHRpbWUgPSB0aGlzLnRpbWVWYWx1ZS52YWx1ZTtcbiAgICBjb25zdCB0aW1lVW5pdCA9IHRoaXMudGltZVVuaXQudmFsdWU7XG4gICAgc3dpdGNoICh0aW1lVW5pdCkge1xuICAgICAgY2FzZSAnZGF5cyc6XG4gICAgICAgIHRpbWUgPSB0aW1lIC8gMzY1O1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ21vbnRocyc6XG4gICAgICAgIHRpbWUgPSB0aW1lIC8gMTI7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cblxuICAgIGNvbnN0IGludFJhdGUgPSB0aGlzLmludFJhdGUudmFsdWUqMC4wMTtcbiAgICBsZXQgY29tcEZyZXEgPSB0aGlzLmNvbXBGcmVxLnZhbHVlO1xuICAgIHN3aXRjaChjb21wRnJlcSkge1xuICAgICAgY2FzZSAneWVhcmx5JzpcbiAgICAgICAgY29tcEZyZXEgPSAxO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ21vbnRobHknOlxuICAgICAgICBjb21wRnJlcSA9IDEyO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ2RhaWx5JzpcbiAgICAgICAgY29tcEZyZXEgPSAzNjU7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnb3ZlcmFsbCc6XG4gICAgICAgIGNvbXBGcmVxID0gMS90aW1lO1xuICAgICAgICBicmVhaztcbiAgICB9XG5cbiAgICBjb25zdCB0YXggPSB0aGlzLnRheC5jaGVja2VkO1xuICAgIGxldCB0YXhlZCA9IDA7XG4gICAgbGV0IGNhbGMxID0gMSArIGludFJhdGUgLyBjb21wRnJlcTtcbiAgICBsZXQgY2FsYzIgPSBjb21wRnJlcSAqIHRpbWU7XG4gICAgbGV0IGNhbGMzID0gTWF0aC5wb3coY2FsYzEsIGNhbGMyKTtcbiAgICBsZXQgY2FsYzQgPSBpbml0QmFsYW5jZSAqIGNhbGMzO1xuXG4gICAgaWYodGF4KSB7XG4gICAgICB0YXhlZCA9ICAoKGNhbGM0IC0gaW5pdEJhbGFuY2UpICogMC4xOSk7XG4gICAgfVxuXG4gICAgbGV0IHJlc3VsdCA9IGV2YWwoIGNhbGM0IC0gdGF4ZWQgKTtcbiAgICByZXN1bHQgPSsgKE1hdGgucm91bmQocmVzdWx0ICsgXCJlKzJcIikgKyBcImUtMlwiKTtcbiAgICBpZighaXNOYU4ocmVzdWx0KSkge1xuICAgICAgdGhpcy5vdXRwdXQudmFsdWUgPSByZXN1bHQ7XG4gICAgfVxuICB9XG59XG5cbmNsYXNzIHJlZ3VsYXJTYXZpbmdzQ2FsY3VsYXRvciB7XG4gIGNvbnN0cnVjdG9yKHRhcmdldElkKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHRoaXMudGFyZ2V0SUQgPSB0YXJnZXRJZDtcbiAgICB0aGlzLnRhcmdldCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHRhcmdldElkKTtcbiAgICB0aGlzLnRhcmdldC5pbm5lckhUTUwgPSAnPGZvcm0gY2xhc3M9XCJpa2FsQ2FsYyBpa2FsQ2FsY19fcmVnU2F2aW5nc1wiIGFjdGlvbj1cIlwiPjxkaXYgY2xhc3M9XCJyb3dcIj48c3Bhbj5Ld290YSByZWd1bGFybnljaCBza8WCYWRlazwvc3Bhbj48aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImlrYWxDYWxjX19yZWdTYXZpbmdzX19hbW91bnRcIiBuYW1lPVwiaWthbENhbGNfX3JlZ1NhdmluZ3NfX2Ftb3VudFwiPjwvZGl2PjxkaXYgY2xhc3M9XCJyb3dcIj48c3Bhbj5DesSZc3RvdGxpd2/Fm8SHPC9zcGFuPjxzZWxlY3QgY2xhc3M9XCJpa2FsQ2FsY19fcmVnU2F2aW5nc19fZnJlcVwiIG5hbWU9XCJpa2FsQ2FsY19fcmVnU2F2aW5nc19fZnJlcVwiPjxvcHRpb24gdmFsdWU9XCIxMlwiPk1pZXNpxJljem5pZTwvb3B0aW9uPjxvcHRpb24gdmFsdWU9XCIxXCI+Um9jem5pZTwvb3B0aW9uPjwvc2VsZWN0PjwvZGl2PjxkaXYgY2xhc3M9XCJyb3dcIj48c3Bhbj5Pa3JlcyBvc3pjesSZZHphbmlhPC9zcGFuPjxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiaWthbENhbGNfX3JlZ1NhdmluZ3NfX3llYXJzXCIgbmFtZT1cImlrYWxDYWxjX19yZWdTYXZpbmdzX195ZWFyc1wiPiBsYXQ8L2Rpdj48ZGl2IGNsYXNzPVwicm93XCI+PHNwYW4+U3RvcGEgendyb3R1PC9zcGFuPjxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiaWthbENhbGNfX3JlZ1NhdmluZ3NfX2ludFJhdGVcIiBuYW1lPVwiaWthbENhbGNfX3JlZ1NhdmluZ3NfX2ludFJhdGVcIj48L2Rpdj48ZGl2IGNsYXNzPVwicm93XCI+PHNwYW4+S2FwaXRhbGl6YWNqYTwvc3Bhbj48c2VsZWN0IGNsYXNzPVwiaWthbENhbGNfX3JlZ1NhdmluZ3NfX2NvbXBGcmVxXCIgbmFtZT1cImlrYWxDYWxjX19yZWdTYXZpbmdzX19jb21wRnJlcVwiPjxvcHRpb24gdmFsdWU9XCIxMlwiPk1pZXNpxJljem5pZTwvb3B0aW9uPjxvcHRpb24gdmFsdWU9XCIxXCI+Um9jem5pZTwvb3B0aW9uPjwvc2VsZWN0PjwvZGl2PjxkaXYgY2xhc3M9XCJyb3dcIj48c3Bhbj5JbmZsYWNqYTwvc3Bhbj48aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImlrYWxDYWxjX19yZWdTYXZpbmdzX19pbmZsYXRpb25cIiBuYW1lPVwiaWthbENhbGNfX3JlZ1NhdmluZ3NfX2luZmxhdGlvblwiPjwvZGl2PjxkaXYgY2xhc3M9XCJyb3dcIj48ZGl2IGNsYXNzPVwiY2hlY2tib3hcIj48aW5wdXQgdHlwZT1cImNoZWNrYm94XCIgY2xhc3M9XCJpa2FsQ2FsY19fcmVnU2F2aW5nc19fdGF4XCIgaWQ9XCJpa2FsQ2FsY19fcmVnU2F2aW5nc19fdGF4XCIgbmFtZT1cImlrYWxDYWxjX19yZWdTYXZpbmdzX190YXhcIj48bGFiZWwgZm9yPVwiaWthbENhbGNfX3JlZ1NhdmluZ3NfX3RheFwiPlV3emdsxJlkbmlqIHBvZGF0ZWs8L2xhYmVsPjwvZGl2PjwvZGl2PjxkaXYgY2xhc3M9XCJyb3dcIj48c3Bhbj5XeW5pazwvc3Bhbj48aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImlrYWxDYWxjX19yZWdTYXZpbmdzX19vdXRwdXRcIiBuYW1lPVwiaWthbENhbGNfX3JlZ1NhdmluZ3NfX291dHB1dFwiIGRpc2FibGVkPVwiXCI+PC9kaXY+PC9mb3JtPic7XG5cbiAgICB0aGlzLmlucHV0cyA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiaW5wdXRcIik7XG4gICAgdGhpcy5hbW91bnQgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19fcmVnU2F2aW5nc19fYW1vdW50JylbMF07XG4gICAgdGhpcy5mcmVxID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnaWthbENhbGNfX3JlZ1NhdmluZ3NfX2ZyZXEnKVswXTtcbiAgICB0aGlzLnllYXJzID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnaWthbENhbGNfX3JlZ1NhdmluZ3NfX3llYXJzJylbMF07XG4gICAgdGhpcy5pbnRSYXRlID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnaWthbENhbGNfX3JlZ1NhdmluZ3NfX2ludFJhdGUnKVswXTtcbiAgICB0aGlzLmNvbXBGcmVxID0gdGhpcy50YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnaWthbENhbGNfX3JlZ1NhdmluZ3NfX2NvbXBGcmVxJylbMF07XG4gICAgdGhpcy5pbmZsYXRpb24gPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19fcmVnU2F2aW5nc19faW5mbGF0aW9uJylbMF07XG4gICAgdGhpcy50YXggPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19fcmVnU2F2aW5nc19fdGF4JylbMF07XG4gICAgdGhpcy5vdXRwdXQgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19fcmVnU2F2aW5nc19fb3V0cHV0JylbMF07XG5cbiAgICBmb3IobGV0IGkgPSAwOyBpPHRoaXMuaW5wdXRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB0aGlzLnByZXZlbnROb25OdW1lcmljKHRoaXMuaW5wdXRzW2ldKTtcbiAgICB9XG5cbiAgICB0aGlzLmFtb3VudC5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcblxuICAgIHRoaXMuZnJlcS5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcbiAgICAgIHNlbGYucmVzdWx0KCk7XG4gICAgfSk7XG5cbiAgICB0aGlzLnllYXJzLmFkZEV2ZW50TGlzdGVuZXIoJ2tleXVwJywgZnVuY3Rpb24oKSB7XG4gICAgICBzZWxmLnJlc3VsdCgpO1xuICAgIH0pO1xuXG4gICAgdGhpcy5pbnRSYXRlLmFkZEV2ZW50TGlzdGVuZXIoJ2tleXVwJywgZnVuY3Rpb24oKSB7XG4gICAgICBzZWxmLnJlc3VsdCgpO1xuICAgIH0pO1xuXG4gICAgdGhpcy5jb21wRnJlcS5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcbiAgICAgIHNlbGYucmVzdWx0KCk7XG4gICAgfSk7XG5cbiAgICB0aGlzLmluZmxhdGlvbi5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcblxuICAgIHRoaXMudGF4LmFkZEV2ZW50TGlzdGVuZXIoJ2NoYW5nZScsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcblxuICAgIHRoaXMub3V0cHV0LmFkZEV2ZW50TGlzdGVuZXIoJ2tleXVwJywgZnVuY3Rpb24oKSB7XG4gICAgICBzZWxmLnJlc3VsdCgpO1xuICAgIH0pO1xuICB9XG4gIHByZXZlbnROb25OdW1lcmljKGlucHV0KSB7XG4gICAgaW5wdXQuYWRkRXZlbnRMaXN0ZW5lcihcImtleXByZXNzXCIsIGZ1bmN0aW9uIChldnQpIHtcbiAgICAgIGlmKGlucHV0LmNsYXNzTGlzdC5jb250YWlucygnaWthbENhbGNfX3JlZ1NhdmluZ3NfX2luZmxhdGlvbicpKSB7XG4gICAgICAgIGlmIChldnQud2hpY2ggPCA0NSB8fCAoIGV2dC53aGljaCA+IDQ2ICYmIGV2dC53aGljaCA8IDQ4KSB8fCBldnQud2hpY2ggPiA1Nykge1xuICAgICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAoZXZ0LndoaWNoIDwgNDYgfHwgZXZ0LndoaWNoID09IDQ3IHx8IGV2dC53aGljaCA+IDU3KSB7XG4gICAgICAgICAgZXZ0LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuICByZXN1bHQoKSB7XG4gICAgY29uc3QgYW1vdW50ID0gZXZhbCh0aGlzLmFtb3VudC52YWx1ZSk7XG4gICAgY29uc3QgZnJlcSA9IGV2YWwodGhpcy5mcmVxLnZhbHVlKTtcbiAgICBjb25zdCB5ZWFycyA9IGV2YWwodGhpcy55ZWFycy52YWx1ZSk7XG4gICAgY29uc3QgaW50UmF0ZSA9IGV2YWwodGhpcy5pbnRSYXRlLnZhbHVlKSAqIDAuMDE7XG4gICAgY29uc3QgY29tcEZyZXEgPSBldmFsKHRoaXMuY29tcEZyZXEudmFsdWUpO1xuICAgIGNvbnN0IGluZmxhdGlvbiA9IGV2YWwodGhpcy5pbmZsYXRpb24udmFsdWUpICogMC4wMTtcbiAgICBjb25zdCB0YXggPSB0aGlzLnRheC5jaGVja2VkO1xuXG4gICAgbGV0IHN1YnNjb3JlID0gMDtcblxuICAgIGlmIChjb21wRnJlcSA9PSBmcmVxKSB7XG4gICAgICBjb25zdCBzdWJzY29yZTEgPSAxICsgaW50UmF0ZSAvIGNvbXBGcmVxO1xuICAgICAgY29uc3Qgc3Vic2NvcmUyID0gTWF0aC5wb3coc3Vic2NvcmUxLCAoY29tcEZyZXEgKiB5ZWFycykpIC0gMTtcbiAgICAgIGNvbnN0IHN1YnNjb3JlMyA9IGludFJhdGUgLyBjb21wRnJlcTtcbiAgICAgIHN1YnNjb3JlID0gYW1vdW50ICogc3Vic2NvcmUxICogKHN1YnNjb3JlMiAvIHN1YnNjb3JlMyk7XG4gICAgfSBlbHNlIGlmIChjb21wRnJlcSA8IGZyZXEpIHtcbiAgICAgIGNvbnN0IHN1YnNjb3JlMSA9IChmcmVxIC8gY29tcEZyZXEpICsgKCgoZnJlcSAvIGNvbXBGcmVxIC0gMSkgKiAoaW50UmF0ZSAvIGNvbXBGcmVxKSkgLyAyKTtcbiAgICAgIGNvbnN0IHN1YnNjb3JlMiA9IChNYXRoLnBvdygoMSArIGludFJhdGUgLyBjb21wRnJlcSksIChjb21wRnJlcSAqIHllYXJzKSkgLSAxKSAvIChpbnRSYXRlIC8gY29tcEZyZXEpO1xuICAgICAgc3Vic2NvcmUgPSBhbW91bnQgKiBzdWJzY29yZTEgKiBzdWJzY29yZTI7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IHN1YnNjb3JlMSA9IE1hdGgucG93KCgxICsgaW50UmF0ZSAvIGNvbXBGcmVxKSwgKGNvbXBGcmVxICogeWVhcnMpKSAtIDE7XG4gICAgICBjb25zdCBzdWJzY29yZTIgPSBNYXRoLnBvdygoMSArIGludFJhdGUgLyBjb21wRnJlcSksIChjb21wRnJlcSAvIGZyZXEpKSAtIDE7XG4gICAgICBzdWJzY29yZSA9IGFtb3VudCAqIChzdWJzY29yZTEgLyBzdWJzY29yZTIpO1xuICAgIH1cblxuICAgIGNvbnN0IHNhdmVkQW1vdW50ID0gYW1vdW50ICogZnJlcSAqIHllYXJzO1xuICAgIGNvbnN0IGRpZmZlbmNlID0gc3Vic2NvcmUgLSBzYXZlZEFtb3VudDtcblxuICAgIGxldCB0YXhBbW91bnQgPSAwO1xuICAgIGlmICh0YXgpIHtcbiAgICAgIHRheEFtb3VudCA9IGRpZmZlbmNlICogMC4xOTtcbiAgICB9XG5cbiAgICBsZXQgaW5mbGF0aW9uQW1vdW50ID0gMDtcbiAgICBpZiAoaW5mbGF0aW9uICYmIGluZmxhdGlvbiAhPSAwKSB7XG4gICAgICBpbmZsYXRpb25BbW91bnQgPSBzdWJzY29yZSAqIGluZmxhdGlvbjtcbiAgICB9XG5cbiAgICBsZXQgcmVzdWx0ICA9IGV2YWwoc3Vic2NvcmUgLSB0YXhBbW91bnQgLSBpbmZsYXRpb25BbW91bnQpO1xuICAgIHJlc3VsdCA9KyAoTWF0aC5yb3VuZChyZXN1bHQgKyBcImUrMlwiKSArIFwiZS0yXCIpO1xuICAgIGlmKCFpc05hTihyZXN1bHQpKSB7XG4gICAgICB0aGlzLm91dHB1dC52YWx1ZSA9IHJlc3VsdDtcbiAgICB9XG4gIH1cbn1cblxuXG5jbGFzcyByZWd1bGFyU2F2aW5nc0J5QW1vdW50Q2FsY3VsYXRvciB7XG4gIGNvbnN0cnVjdG9yKHRhcmdldElkKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHRoaXMudGFyZ2V0SUQgPSB0YXJnZXRJZDtcbiAgICB0aGlzLnRhcmdldCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHRhcmdldElkKTtcbiAgICB0aGlzLnRhcmdldC5pbm5lckhUTUwgPSAnPGZvcm0gY2xhc3M9XCJpa2FsQ2FsYyBpa2FsQ2FsY19fcmVnU2F2aW5nc0J5QW1vdW50XCIgYWN0aW9uPVwiXCI+PGRpdiBjbGFzcz1cInJvd1wiPjxzcGFuPkNlbDwvc3Bhbj48aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImlrYWxDYWxjX19yZWdTYXZpbmdzQnlBbW91bnRfX2dvYWxcIiBuYW1lPVwiaWthbENhbGNfX3JlZ1NhdmluZ3NfX2dvYWxcIj48L2Rpdj48ZGl2IGNsYXNzPVwicm93XCI+PHNwYW4+S3dvdGEgcmVndWxhcm55Y2ggc2vFgmFkZWs8L3NwYW4+PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpa2FsQ2FsY19fcmVnU2F2aW5nc0J5QW1vdW50X19hbW91bnRcIiBuYW1lPVwiaWthbENhbGNfX3JlZ1NhdmluZ3NCeUFtb3VudF9fYW1vdW50XCI+PC9kaXY+PGRpdiBjbGFzcz1cInJvd1wiPjxzcGFuPkN6xJlzdG90bGl3b8WbxIc8L3NwYW4+PHNlbGVjdCBjbGFzcz1cImlrYWxDYWxjX19yZWdTYXZpbmdzQnlBbW91bnRfX2ZyZXFcIiBuYW1lPVwiaWthbENhbGNfX3JlZ1NhdmluZ3NCeUFtb3VudF9fZnJlcVwiPjxvcHRpb24gdmFsdWU9XCIxMlwiPk1pZXNpxJljem5pZTwvb3B0aW9uPjxvcHRpb24gdmFsdWU9XCI0XCI+S3dhcnRhbG5pZTwvb3B0aW9uPjxvcHRpb24gdmFsdWU9XCIxXCI+Um9jem5pZTwvb3B0aW9uPjwvc2VsZWN0PjwvZGl2PjxkaXYgY2xhc3M9XCJyb3dcIj48c3Bhbj5TdG9wYSB6d3JvdHU8L3NwYW4+PGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJpa2FsQ2FsY19fcmVnU2F2aW5nc0J5QW1vdW50X19pbnRSYXRlXCIgbmFtZT1cImlrYWxDYWxjX19yZWdTYXZpbmdzQnlBbW91bnRfX2ludFJhdGVcIj48L2Rpdj48ZGl2IGNsYXNzPVwicm93XCI+PHNwYW4+S2FwaXRhbGl6YWNqYTwvc3Bhbj48c2VsZWN0IGNsYXNzPVwiaWthbENhbGNfX3JlZ1NhdmluZ3NCeUFtb3VudF9fY29tcEZyZXFcIiBuYW1lPVwiaWthbENhbGNfX3JlZ1NhdmluZ3NCeUFtb3VudF9fY29tcEZyZXFcIj48b3B0aW9uIHZhbHVlPVwiMTJcIj5NaWVzacSZY3puaWU8L29wdGlvbj48b3B0aW9uIHZhbHVlPVwiMVwiPlJvY3puaWU8L29wdGlvbj48L3NlbGVjdD48L2Rpdj48ZGl2IGNsYXNzPVwicm93XCI+PGRpdiBjbGFzcz1cImlrYWxDYWxjX19yZWdTYXZpbmdzQnlBbW91bnRfX291dHB1dFwiPjwvZGl2PjwvZGl2PjwvZm9ybT4nO1xuXG4gICAgdGhpcy5pbnB1dHMgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5VGFnTmFtZShcImlucHV0XCIpO1xuICAgIHRoaXMuZ29hbCA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2lrYWxDYWxjX19yZWdTYXZpbmdzQnlBbW91bnRfX2dvYWwnKVswXTtcbiAgICB0aGlzLmFtb3VudCA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2lrYWxDYWxjX19yZWdTYXZpbmdzQnlBbW91bnRfX2Ftb3VudCcpWzBdO1xuICAgIHRoaXMuZnJlcSA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2lrYWxDYWxjX19yZWdTYXZpbmdzQnlBbW91bnRfX2ZyZXEnKVswXTtcbiAgICB0aGlzLmludFJhdGUgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19fcmVnU2F2aW5nc0J5QW1vdW50X19pbnRSYXRlJylbMF07XG4gICAgdGhpcy5jb21wRnJlcSA9IHRoaXMudGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2lrYWxDYWxjX19yZWdTYXZpbmdzQnlBbW91bnRfX2NvbXBGcmVxJylbMF07XG4gICAgdGhpcy5vdXRwdXQgPSB0aGlzLnRhcmdldC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdpa2FsQ2FsY19fcmVnU2F2aW5nc0J5QW1vdW50X19vdXRwdXQnKVswXTtcblxuICAgIGZvcihsZXQgaSA9IDA7IGk8dGhpcy5pbnB1dHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHRoaXMucHJldmVudE5vbk51bWVyaWModGhpcy5pbnB1dHNbaV0pO1xuICAgIH1cblxuICAgIHRoaXMuZ29hbC5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcblxuICAgIHRoaXMuYW1vdW50LmFkZEV2ZW50TGlzdGVuZXIoJ2tleXVwJywgZnVuY3Rpb24oKSB7XG4gICAgICBzZWxmLnJlc3VsdCgpO1xuICAgIH0pO1xuXG4gICAgdGhpcy5mcmVxLmFkZEV2ZW50TGlzdGVuZXIoJ2NoYW5nZScsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcblxuICAgIHRoaXMuaW50UmF0ZS5hZGRFdmVudExpc3RlbmVyKCdrZXl1cCcsIGZ1bmN0aW9uKCkge1xuICAgICAgc2VsZi5yZXN1bHQoKTtcbiAgICB9KTtcblxuICAgIHRoaXMuY29tcEZyZXEuYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgZnVuY3Rpb24oKSB7XG4gICAgICBzZWxmLnJlc3VsdCgpO1xuICAgIH0pO1xuXG4gIH1cbiAgcHJldmVudE5vbk51bWVyaWMoaW5wdXQpIHtcbiAgICBpbnB1dC5hZGRFdmVudExpc3RlbmVyKFwia2V5cHJlc3NcIiwgZnVuY3Rpb24gKGV2dCkge1xuICAgICAgaWYgKGV2dC53aGljaCA8IDQ2IHx8IGV2dC53aGljaCA9PSA0NyB8fCBldnQud2hpY2ggPiA1Nykge1xuICAgICAgICBldnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuICByZXN1bHQoKSB7XG4gICAgY29uc3QgZ29hbCA9IGV2YWwodGhpcy5nb2FsLnZhbHVlKTtcbiAgICBjb25zdCBhbW91bnQgPSBldmFsKHRoaXMuYW1vdW50LnZhbHVlKTtcbiAgICBjb25zdCBmcmVxID0gZXZhbCh0aGlzLmZyZXEudmFsdWUpO1xuICAgIGNvbnN0IGludFJhdGUgPSBldmFsKHRoaXMuaW50UmF0ZS52YWx1ZSkgKiAwLjAxO1xuICAgIGNvbnN0IGNvbXBGcmVxID0gZXZhbCh0aGlzLmNvbXBGcmVxLnZhbHVlKTtcblxuICAgIGxldCBzdWJzY29yZSA9IDA7XG4gICAgbGV0IHllYXJzID0gMDtcblxuICAgIGlmIChjb21wRnJlcSA9PSBmcmVxKSB7XG4gICAgICB3aGlsZShzdWJzY29yZSA8IGdvYWwpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ2NvbXBmcmVxPT1mcmVxIHllYXJzOiAnICsgeWVhcnMgKyAnIHNjb3JlOiAnICsgc3Vic2NvcmUpO1xuICAgICAgICB5ZWFycysrO1xuICAgICAgICBjb25zdCBzdWJzY29yZTEgPSAxICsgaW50UmF0ZSAvIGNvbXBGcmVxO1xuICAgICAgICBjb25zdCBzdWJzY29yZTIgPSBNYXRoLnBvdyhzdWJzY29yZTEsIChjb21wRnJlcSAqIHllYXJzKSkgLSAxO1xuICAgICAgICBjb25zdCBzdWJzY29yZTMgPSBpbnRSYXRlIC8gY29tcEZyZXE7XG4gICAgICAgIHN1YnNjb3JlID0gYW1vdW50ICogc3Vic2NvcmUxICogKHN1YnNjb3JlMiAvIHN1YnNjb3JlMyk7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChjb21wRnJlcSA8IGZyZXEpIHtcbiAgICAgIHdoaWxlKHN1YnNjb3JlIDwgZ29hbCkge1xuICAgICAgICBjb25zb2xlLmxvZygnY29tcGZyZXE8ZnJlcSB5ZWFyczogJyArIHllYXJzICsgJyBzY29yZTogJyArIHN1YnNjb3JlKTtcbiAgICAgICAgeWVhcnMrKztcbiAgICAgICAgY29uc3Qgc3Vic2NvcmUxID0gKGZyZXEgLyBjb21wRnJlcSkgKyAoKChmcmVxIC8gY29tcEZyZXEgLSAxKSAqIChpbnRSYXRlIC8gY29tcEZyZXEpKSAvIDIpO1xuICAgICAgICBjb25zdCBzdWJzY29yZTIgPSAoTWF0aC5wb3coKDEgKyBpbnRSYXRlIC8gY29tcEZyZXEpLCAoY29tcEZyZXEgKiB5ZWFycykpIC0gMSkgLyAoaW50UmF0ZSAvIGNvbXBGcmVxKTtcbiAgICAgICAgc3Vic2NvcmUgPSBhbW91bnQgKiBzdWJzY29yZTEgKiBzdWJzY29yZTI7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHdoaWxlKHN1YnNjb3JlIDwgZ29hbCkge1xuICAgICAgICBjb25zb2xlLmxvZygnY29tcGZyZXE+ZnJlcSB5ZWFyczogJyArIHllYXJzICsgJyBzY29yZTogJyArIHN1YnNjb3JlKTtcbiAgICAgICAgeWVhcnMrKztcbiAgICAgICAgY29uc3Qgc3Vic2NvcmUxID0gTWF0aC5wb3coKDEgKyBpbnRSYXRlIC8gY29tcEZyZXEpLCAoY29tcEZyZXEgKiB5ZWFycykpIC0gMTtcbiAgICAgICAgY29uc29sZS5sb2coJ3N1YnNjb3JlMSAnICsgc3Vic2NvcmUxKTtcbiAgICAgICAgY29uc3Qgc3Vic2NvcmUyID0gTWF0aC5wb3coKDEgKyBpbnRSYXRlIC8gY29tcEZyZXEpLCAoY29tcEZyZXEgLyBmcmVxKSkgLSAxO1xuICAgICAgICBjb25zb2xlLmxvZygnc3Vic2NvcmUyICcgKyBzdWJzY29yZTIpO1xuICAgICAgICBzdWJzY29yZSA9IGFtb3VudCAqIChzdWJzY29yZTEgLyBzdWJzY29yZTIpO1xuICAgICAgfVxuICAgIH1cbiAgICAvLyBsZXQgcmVzdWx0ID0gJ011c2lzeiB6YmllcmHEhyBwcnpleiAnICsgeWVhcnMgKyAnIGxhdC4gVXpiaWVyYW5hIGt3b3RhIGLEmWR6aWUgcsOzd25hICcgKyBzdWJzY29yZTtcbiAgICBsZXQgcmVzdWx0ICA9IGV2YWwoc3Vic2NvcmUpO1xuICAgIHJlc3VsdCA9KyAoTWF0aC5yb3VuZChyZXN1bHQgKyBcImUrMlwiKSArIFwiZS0yXCIpO1xuICAgIGlmKCFpc05hTihyZXN1bHQpKSB7XG4gICAgICBpZih5ZWFycyA9PSAxKSB7XG4gICAgICAgIHRoaXMub3V0cHV0LmlubmVySFRNTCA9ICdNdXNpc3ogemJpZXJhxIcgcHJ6ZXogMSByb2suIFV6YmllcmFuYSBrd290YSBixJlkemllIHd5bm9zacSHICcgKyByZXN1bHQ7XG4gICAgICB9IGVsc2UgaWYgKHllYXJzID4gMSAmJiB5ZWFycyA8IDUpIHtcbiAgICAgICAgdGhpcy5vdXRwdXQuaW5uZXJIVE1MID0gJ011c2lzeiB6YmllcmHEhyBwcnpleiAnICsgeWVhcnMgKyAnIGxhdGEuIFV6YmllcmFuYSBrd290YSBixJlkemllIHd5bm9zacSHICcgKyByZXN1bHQ7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLm91dHB1dC5pbm5lckhUTUwgPSAnTXVzaXN6IHpiaWVyYcSHIHByemV6ICcgKyB5ZWFycyArICcgbGF0LiBVemJpZXJhbmEga3dvdGEgYsSZZHppZSB3eW5vc2nEhyAnICsgcmVzdWx0O1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuIiwiY29tbWVudHMgPSBmdW5jdGlvbiAoKVxue1xuXHR2YXIgYXBwbHlTd2l0Y2hlciA9IGZ1bmN0aW9uKClcblx0e1xuXHRcdGpRdWVyeShcIi5jb21tZW50c1wiKS5lYWNoKCBmdW5jdGlvbiAoKVxuXHRcdHtcblx0XHRcdHZhciBjb21tZW50Qm94ID0galF1ZXJ5KHRoaXMpO1xuXG5cdFx0XHRqUXVlcnkoXCIuc2hvdy1hY3Rpb24gYnV0dG9uXCIsIGNvbW1lbnRCb3gpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oKVxuXHRcdFx0e1xuXHRcdFx0XHRqUXVlcnkoXCIuY29tbWVudHMtY29udGVudFwiLCBjb21tZW50Qm94KS5zdG9wKCkuc2xpZGVUb2dnbGUoKTtcblx0XHRcdFx0Y29tbWVudEJveC50b2dnbGVDbGFzcyhcInZpc2libGVcIik7XG5cdFx0XHR9KVxuXHRcdH0pXG5cblx0fVxuXG5cdHZhciBzY3JvbGxUb0NvbW1lbnRzID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0aWYobG9jYXRpb24uaHJlZi5pbmRleE9mKFwicmVwbHl0b2NvbT1cIikgPiAtMSB8fCBsb2NhdGlvbi5ocmVmLmluZGV4T2YoXCIjY29tbWVudFwiKSA+IC0xKVxuXHRcdHtcblx0XHRcdGpRdWVyeShcIi5jb21tZW50cyAuY29tbWVudHMtY29udGVudFwiKS5zaG93KCk7XG5cdFx0XHRqUXVlcnkoXCIuY29tbWVudHNcIikuYWRkQ2xhc3MoXCJ2aXNpYmxlXCIpO1xuXG5cdFx0XHRpZihsb2NhdGlvbi5oYXNoKVxuXHRcdFx0e1xuXHRcdFx0XHRpZihqUXVlcnkobG9jYXRpb24uaGFzaCkubGVuZ3RoKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0alF1ZXJ5KFwiaHRtbCxib2R5XCIpLmFuaW1hdGUoeyBzY3JvbGxUb3A6IGpRdWVyeShsb2NhdGlvbi5oYXNoKS5vZmZzZXQoKS50b3AgLSAxMDB9KVxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG5cblx0dmFyIGhhbmRsZURvUmF0ZVJlbGVhc2UgPSBmdW5jdGlvbigpXG5cdHtcblx0XHRqUXVlcnkoXCIjZG9SYXRlXCIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oKSB7XG5cdFx0XHRqUXVlcnkoXCIuY29tbWVudHMgLmNvbW1lbnRzLWNvbnRlbnRcIikuc2hvdygpO1xuXHRcdFx0alF1ZXJ5KFwiLmNvbW1lbnRzXCIpLmFkZENsYXNzKFwidmlzaWJsZVwiKTtcblx0XHRcdGpRdWVyeShcImh0bWwsYm9keVwiKS5hbmltYXRlKHsgc2Nyb2xsVG9wOiBqUXVlcnkoXCIjcmVzcG9uZFwiKS5vZmZzZXQoKS50b3AgLSAxMDB9KVxuXHRcdH0pO1xuXHR9XG5cblx0dmFyIGRyYXdTdGFycyA9IGZ1bmN0aW9uKClcblx0e1xuXHRcdGpRdWVyeShcIi5zdGFyLXJhdGluZ1wiKS5lYWNoKCBmdW5jdGlvbigpIHtcblx0XHRcdGlmKGpRdWVyeSh0aGlzKS5kYXRhKFwicmF0aW5nXCIpKVxuXHRcdFx0e1xuXHRcdFx0XHR2YXIgcmF0aW5nID0gcGFyc2VGbG9hdChqUXVlcnkodGhpcykuZGF0YShcInJhdGluZ1wiKSk7XG5cdFx0XHRcdHZhciBmdWxscyA9IE1hdGguZmxvb3IocmF0aW5nKTtcblx0XHRcdFx0Zm9yKHZhciBpID0gMDsgaSA8IGZ1bGxzO2kgKyspXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRqUXVlcnkodGhpcykuZmluZChcIi5zdGFyc1wiKS5hcHBlbmQoJzxzcGFuIGNsYXNzPVwiZnVsbFwiPjwvc3Bhbj4nKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZihmdWxscyA8IDUpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHR2YXIgZnJhY3QgPSBNYXRoLnJvdW5kKChyYXRpbmcgLSBmdWxscykgKiAxMDApO1xuXHRcdFx0XHRcdGlmKGZyYWN0ID4gMCkgalF1ZXJ5KHRoaXMpLmZpbmQoXCIuc3RhcnNcIikuYXBwZW5kKCc8c3BhbiBjbGFzcz1cInBhcnRcIj48c3BhbiBzdHlsZT1cIndpZHRoOiAnICsgKGZyYWN0KzEwKSArICclXCI+PC9zcGFuPjwvc3Bhbj4nKTtcblx0XHRcdFx0XHRlbHNlIGpRdWVyeSh0aGlzKS5maW5kKFwiLnN0YXJzXCIpLmFwcGVuZCgnPHNwYW4gY2xhc3M9XCJlbXB0eVwiPjwvc3Bhbj4nKTtcblx0XHRcdFx0XHRmdWxscysrO1xuXHRcdFx0XHRcdHdoaWxlKGZ1bGxzIDwgNSlcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRqUXVlcnkodGhpcykuZmluZChcIi5zdGFyc1wiKS5hcHBlbmQoJzxzcGFuIGNsYXNzPVwiZW1wdHlcIj48L3NwYW4+Jyk7XG5cdFx0XHRcdFx0XHRmdWxscysrO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH0pO1xuXHR9XG5cblx0dmFyIGhhbmRsZVJhdGVJbkZvcm0gPSBmdW5jdGlvbigpXG5cdHtcblx0XHRqUXVlcnkoXCIuc3Rhci1yYXRpbmcuc2VsZWN0YWJsZVwiKS5lYWNoKCBmdW5jdGlvbigpXG5cdFx0e1xuXHRcdFx0dmFyIHJhdGVDb250YWluZXIgPSBqUXVlcnkodGhpcyk7XG5cdFx0XHRqUXVlcnkoXCIuc3RhcnMgPiBzcGFuXCIsIHJhdGVDb250YWluZXIpLmVhY2goIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRqUXVlcnkodGhpcykub24oXCJtb3VzZWVudGVyXCIsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdHZhciBzZWxlY3RlZEluZGV4ID0galF1ZXJ5KHRoaXMpLmluZGV4KCk7XG5cdFx0XHRcdFx0alF1ZXJ5KFwiLnN0YXJzID4gc3BhblwiLHJhdGVDb250YWluZXIpLmVhY2goIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdFx0aWYoalF1ZXJ5KHRoaXMpLmluZGV4KCkgPD0gc2VsZWN0ZWRJbmRleClcblx0XHRcdFx0XHRcdFx0alF1ZXJ5KHRoaXMpLnJlbW92ZUNsYXNzKCkuYWRkQ2xhc3MoXCJmdWxsXCIpO1xuXHRcdFx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdFx0XHRqUXVlcnkodGhpcykucmVtb3ZlQ2xhc3MoKS5hZGRDbGFzcyhcImVtcHR5XCIpO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHRqUXVlcnkodGhpcykub24oXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHR2YXIgc2VsZWN0ZWRJbmRleCA9IGpRdWVyeSh0aGlzKS5pbmRleCgpO1xuXHRcdFx0XHRcdHZhciBzZWxlY3RlZFZhbHVlID0gc2VsZWN0ZWRJbmRleCArIDE7XG5cdFx0XHRcdFx0cmF0ZUNvbnRhaW5lci5kYXRhKCdyYXRpbmcnLCBzZWxlY3RlZEluZGV4KTtcblx0XHRcdFx0XHRqUXVlcnkoJ2lucHV0W25hbWU9cmF0aW5nXScpLnZhbChzZWxlY3RlZFZhbHVlKTtcblx0XHRcdFx0fSk7XG5cblx0XHRcdH0pO1xuXG5cdFx0XHRyYXRlQ29udGFpbmVyLm9uKFwibW91c2VsZWF2ZVwiLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0dmFyIHNlbGVjdGVkSW5kZXggPSBqUXVlcnkodGhpcykuZGF0YSgncmF0aW5nJyk7XG5cdFx0XHRcdGpRdWVyeShcIi5zdGFycyA+IHNwYW5cIixyYXRlQ29udGFpbmVyKS5lYWNoKCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRpZihqUXVlcnkodGhpcykuaW5kZXgoKSA8PSBzZWxlY3RlZEluZGV4KVxuXHRcdFx0XHRcdFx0alF1ZXJ5KHRoaXMpLnJlbW92ZUNsYXNzKCkuYWRkQ2xhc3MoXCJmdWxsXCIpO1xuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdGpRdWVyeSh0aGlzKS5yZW1vdmVDbGFzcygpLmFkZENsYXNzKFwiZW1wdHlcIik7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSk7XG5cblxuXHRcdH0pXG5cdH1cblxuXHR2YXIgaGFuZGxlVGh1bWJzID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLnRodW1icyA+IGJ1dHRvblwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHRpZighalF1ZXJ5KHRoaXMpLmhhc0NsYXNzKFwibG9ja2VkLWFjdGl2ZVwiKSAmJiAhalF1ZXJ5KHRoaXMpLmhhc0NsYXNzKFwibG9ja2VkLWluYWN0aXZlXCIpKVxuXHRcdFx0e1xuXHRcdFx0XHRqUXVlcnkodGhpcykucGFyZW50KCkuZmluZChcImJ1dHRvblwiKS5mYWRlT3V0KCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRqUXVlcnkodGhpcykuZmFkZUluKFwiZmFzdFwiKTtcblx0XHRcdFx0fSlcblx0XHRcdFx0alF1ZXJ5KHRoaXMpLnBhcmVudCgpLmZpbmQoXCJidXR0b25cIikuYWRkQ2xhc3MoXCJsb2NrZWQtaW5hY3RpdmVcIilcblx0XHRcdFx0alF1ZXJ5KHRoaXMpLmFkZENsYXNzKFwiYWN0aXZlXCIpLnJlbW92ZUNsYXNzKFwibG9ja2VkLWluYWN0aXZlXCIpLmFkZENsYXNzKFwibG9ja2VkLWFjdGl2ZVwiKVxuXG5cdFx0XHRcdHZhciBkaXIgPSBcImRvd25cIlxuXHRcdFx0XHRpZihqUXVlcnkodGhpcykuaGFzQ2xhc3MoXCJ1cFwiKSkgZGlyID0gXCJ1cFwiO1xuXHRcdFx0XHR2YXIgcG9zdElkID0galF1ZXJ5KHRoaXMpLmNsb3Nlc3QoXCIudGh1bWJ2b3RlXCIpLmRhdGEoXCJwb3N0aWRcIik7XG5cdFx0XHRcdGpRdWVyeS5wb3N0KFwiL3dwLWpzb24vaWthbC92MS9wb3N0L3ZvdGVcIiwge1wicG9zdFwiOiBwb3N0SWQsIFwiZGlyXCI6IGRpcn0pO1xuXHRcdFx0fVxuXHRcdH0pXG5cdH1cblxuXG5cdC8qIF9fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX18gcHVibGljIG1ldGhvZHMgKi9cblxuXHR2YXIgaW5pdCA9IGZ1bmN0aW9uKClcblx0e1xuXHRcdGhhbmRsZURvUmF0ZVJlbGVhc2UoKTtcblx0XHRzY3JvbGxUb0NvbW1lbnRzKCk7XG5cdFx0YXBwbHlTd2l0Y2hlcigpO1xuXHRcdGRyYXdTdGFycygpO1xuXHRcdGhhbmRsZVJhdGVJbkZvcm0oKTtcblx0XHRoYW5kbGVUaHVtYnMoKTtcblx0fTtcblxuXHRpbml0KCk7XG59O1xuXG52YXIgY29tbWVudHM7XG5cblxuLypcbiAqXHREb3N0b3Nvd2FuYSBuYXR5d25hIGZ1a2NqYSB3b3JkcHJlc3NhICh3cC1pbmNsdWRlcy9qcy9jb21tZW50LXJlcGx5LmpzKVxuICovXG52YXIgYWRkQ29tbWVudCA9IHtcbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBjb21tSWQgVGhlIGNvbW1lbnQgSUQuXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBwYXJlbnRJZCBUaGUgcGFyZW50IElELlxuICAgKiBAcGFyYW0ge3N0cmluZ30gcmVzcG9uZElkIFRoZSByZXNwb25kIElELlxuICAgKiBAcGFyYW0ge3N0cmluZ30gcG9zdElkIFRoZSBwb3N0IElELlxuICAgKiBAcmV0dXJucyB7Ym9vbGVhbn0gQWx3YXlzIHJldHVybnMgZmFsc2UuXG4gICAqL1xuICBtb3ZlRm9ybTogZnVuY3Rpb24oIGNvbW1JZCwgcGFyZW50SWQsIHJlc3BvbmRJZCwgcG9zdElkICkge1xuICBcdHZhciBjYW5jZWwgPSBqUXVlcnkoJyNjYW5jZWwtY29tbWVudC1yZXBseS1saW5rJyk7XG4gICAgY2FuY2VsLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAgICAgalF1ZXJ5KFwiI2NhbmNlbC1jb21tZW50LXJlcGx5LWxpbmsgLnJlcGx5LXRvLWF1dGhvclwiKS5yZW1vdmUoKTtcbiAgICAgIGpRdWVyeShcIiNjYW5jZWwtY29tbWVudC1yZXBseS1saW5rXCIpLmhpZGUoKTtcbiAgICAgIGpRdWVyeSgnI2NvbW1lbnRfcGFyZW50JykudmFsKG51bGwpO1xuICAgICAgalF1ZXJ5KCcuY29tbWVudC1mb3JtLXJhdGluZycpLnNob3coKTtcbiAgICAgIGpRdWVyeSgnI3JhdGluZycpLnZhbCg0KTtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9KVxuXG4gIFx0dmFyIGF1dGhvciA9IGpRdWVyeSgnIycrY29tbUlkKS5maW5kKCcuY29tbWVudC1hdXRob3IgYicpLmh0bWwoKTtcbiAgICBqUXVlcnkoXCJodG1sLGJvZHlcIikuYW5pbWF0ZSh7IHNjcm9sbFRvcDogalF1ZXJ5KFwiI3Jlc3BvbmRcIikub2Zmc2V0KCkudG9wIC0gMTAwfSk7XG4gICAgalF1ZXJ5KFwiI2NhbmNlbC1jb21tZW50LXJlcGx5LWxpbmtcIikuaHRtbChcbiAgICBcdGpRdWVyeShcIiNjYW5jZWwtY29tbWVudC1yZXBseS1saW5rXCIpLnRleHQoKSArICc8c3BhbiBjbGFzcz1cInJlcGx5LXRvLWF1dGhvclwiPiB1xbx5dGtvd25pa293aSAnICsgYXV0aG9yICsgJzwvc3Bhbj4nXG5cdFx0KTtcbiAgICBqUXVlcnkoXCIjY2FuY2VsLWNvbW1lbnQtcmVwbHktbGlua1wiKS5zaG93KCk7XG5cbiAgICBqUXVlcnkoJy5jb21tZW50LWZvcm0tcmF0aW5nJykuaGlkZSgpO1xuICAgIGpRdWVyeSgnI3JhdGluZycpLnZhbChudWxsKTtcblxuICAgIGpRdWVyeSgnI2NvbW1lbnRfcGFyZW50JykudmFsKHBhcmVudElkKTtcblxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxufVxuXG4iLCJjb21wYW5pZXMgPSBmdW5jdGlvbiAoKVxueyAgIFxuXHRmdW5jdGlvbiBoYW5kbGVGaXhlZEZpbHRlcigpXG5cdHtcbiAgICAgICAgdmFyIHBhZ2VIZWFkID0gIGpRdWVyeShcIi5wYWdlLWhlYWRcIik7XG4gICAgICAgIHZhciBwYWdlQ29udGVudCA9ICBqUXVlcnkoXCIucGFnZS1jb250ZW50XCIpO1xuICAgICAgICB2YXIgZmlsdGVyID0gIGpRdWVyeShcIi5maWx0ZXJzXCIpO1xuICAgICAgICBcbiAgICAgICAgaWYgKCFmaWx0ZXIubGVuZ3RoKVxuICAgICAgICAgICAgcmV0dXJuO1xuXG5cdFx0dmFyIHN3aXRjaEF0ID0gcGFnZUhlYWQub2Zmc2V0KCkudG9wICsgcGFnZUhlYWQub3V0ZXJIZWlnaHQoKTtcblxuICAgIFxuICAgICAgICBpZiAoc3dpdGNoQXQgPCBqUXVlcnkod2luZG93KS5zY3JvbGxUb3AoKSAmJiB3aW5kb3cuaW5uZXJXaWR0aCA+PSA3NjgpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGlmICghZmlsdGVyLmhhc0NsYXNzKFwiZml4ZWRcIikpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdmFyIGRpZmYgPSBmaWx0ZXIub3V0ZXJIZWlnaHQoKSAtICAoc3dpdGNoQXQgLSBmaWx0ZXIub2Zmc2V0KCkudG9wKTtcbiAgICAgICAgICAgICAgICBwYWdlQ29udGVudFxuICAgICAgICAgICAgICAgICAgICAuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwicGFkZGluZy10b3BcIiA6IGRpZmYgKyBcInB4XCJcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZpbHRlclxuICAgICAgICAgICAgICAgIC5hZGRDbGFzcyhcImZpeGVkXCIpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2VcbiAgICAgICAge1xuICAgICAgICAgICAgZmlsdGVyLnJlbW92ZUNsYXNzKFwiZml4ZWRcIik7XG4gICAgICAgICAgICBwYWdlQ29udGVudFxuICAgICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgICAgICBcInBhZGRpbmctdG9wXCIgOiBcIlwiXG4gICAgICAgICAgICAgICAgfSk7ICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICB9ICAgIFxuICAgIFxuICAgIFxuICAgIGZ1bmN0aW9uIGZpbHRlckdyaWQoKVxuICAgIHtcbiAgICAgICAgdmFyIGZpbHRlckl0ZW1zID0galF1ZXJ5KFwiLmNvbXBhbmllcyAuZmlsdGVycyAuaXRlbXMtbGlzdCAuY2F0ZWdvcnlcIik7XG4gICAgICAgIHZhciBncmlkSXRlbXMgPSBqUXVlcnkoXCIuY29tcGFuaWVzIC50aWxlcy1ncmlkIC5ncmlkLWl0ZW1cIik7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgIGlmICghZmlsdGVySXRlbXMubGVuZ3RoIHx8ICFncmlkSXRlbXMubGVuZ3RoKSB7IHJldHVybjsgfVxuICAgICAgICAgICAgICAgIFxuICAgICAgICB2YXIgc2VsZWN0ZWRDYXRlZ29yaWVzID0gW107XG4gICAgICAgIFxuICAgICAgICBmaWx0ZXJJdGVtcy5lYWNoKGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgY3VycmVudEl0ZW0gPSBqUXVlcnkodGhpcyk7ICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChjdXJyZW50SXRlbS5oYXNDbGFzcyhcImFjdGl2ZVwiKSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZiAoY3VycmVudEl0ZW0uYXR0cihcImRhdGEtZmlsdGVyXCIpID09IFwiYWxsXCIpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZENhdGVnb3JpZXMgPSBcImFsbFwiO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkQ2F0ZWdvcmllcy5wdXNoKGN1cnJlbnRJdGVtLmF0dHIoXCJkYXRhLWZpbHRlclwiKSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgXG4gICAgICAgIGlmIChzZWxlY3RlZENhdGVnb3JpZXMgPT0gXCJhbGxcIilcbiAgICAgICAge1xuICAgICAgICAgICAgZ3JpZEl0ZW1zLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2VcbiAgICAgICAge1xuICAgICAgICAgICAgZ3JpZEl0ZW1zLmVhY2goZnVuY3Rpb24oKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGN1cnJlbnRJdGVtID0galF1ZXJ5KHRoaXMpO1xuICAgICAgICAgICAgICAgIHZhciBjYXRlZ29yaWVzID0gY3VycmVudEl0ZW0uYXR0cihcImRhdGEtaXRlbS1jYXRlZ29yaWVzXCIpO1xuXG4gICAgICAgICAgICAgICAgdmFyIGl0ZW1DYXRlZ29yaWVzID0gY2F0ZWdvcmllcy5zcGxpdChcIixcIik7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdmFyIGNvbnRhaW5BdExlYXN0T25lRmlsdGVyID0gc2VsZWN0ZWRDYXRlZ29yaWVzLnNvbWUoZnVuY3Rpb24gKHYpIHsgcmV0dXJuIGl0ZW1DYXRlZ29yaWVzLmluZGV4T2YodikgPj0gMDt9ICk7XG5cbiAgICAgICAgICAgICAgICBpZiAoY29udGFpbkF0TGVhc3RPbmVGaWx0ZXIpXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRJdGVtLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudEl0ZW0ucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7XG5cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfSAgXG4gICAgXG4gICAgXG4gICAgXG4gICAgZnVuY3Rpb24gaGFuZGxlRmlsdGVyKGlzQ29uZGl0aW9uT1IpXG4gICAge1xuICAgICAgICBpc0NvbmRpdGlvbk9SID0gdHlwZW9mIGlzQ29uZGl0aW9uT1IgIT09IFwidW5kZWZpbmVkXCIgPyBpc0NvbmRpdGlvbk9SIDogZmFsc2U7XG4gICAgICAgIFxuICAgICAgICB2YXIgZmlsdGVySXRlbXMgPSBqUXVlcnkoXCIuY29tcGFuaWVzIC5maWx0ZXJzIC5pdGVtcy1saXN0IC5jYXRlZ29yeVwiKTtcbiAgICAgICAgXG4gICAgICAgIGlmICghZmlsdGVySXRlbXMubGVuZ3RoKSB7IHJldHVybjsgfVxuICAgICAgICBcbiAgICAgICAgZmlsdGVySXRlbXMub24oXCJjbGlja1wiLCBmdW5jdGlvbihlKVxuICAgICAgICB7XG4gICAgICAgICAgICBjdXJyZW50SXRlbSA9IGpRdWVyeSh0aGlzKTtcbiAgICAgICAgICAgIGNpcnJlbnRJdGVtRmlsdGVyID0gY3VycmVudEl0ZW0uYXR0cihcImRhdGEtZmlsdGVyXCIpOyAgICAgICAgXG4gICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIGlmIChjaXJyZW50SXRlbUZpbHRlciA9PSBcImFsbFwiKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGlmIChjdXJyZW50SXRlbS5oYXNDbGFzcyhcImFjdGl2ZVwiKSkgXG4gICAgICAgICAgICAgICAgeyBcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJJdGVtcy5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudEl0ZW0uYWRkQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGlmIChpc0NvbmRpdGlvbk9SKSAvL29ubHkgbGFzdCBzZWxlY3RlZFxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJJdGVtcy5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgICAgICAgICBlbHNlIC8vYWxsIHNlbGVjdGVkIGV4Y2VwdGVkIFwiYWxsXCJcbiAgICAgICAgICAgICAgICAgICAgZmlsdGVySXRlbXMuZmlsdGVyKCdbZGF0YS1maWx0ZXI9XCJhbGxcIl0nKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTsgXG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgaWYgKGN1cnJlbnRJdGVtLmhhc0NsYXNzKFwiYWN0aXZlXCIpKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGZpbHRlckl0ZW1zLmZpbHRlcihcIi5hY3RpdmVcIikubGVuZ3RoID4gMSlcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVudEl0ZW0ucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudEl0ZW0uYWRkQ2xhc3MoXCJhY3RpdmVcIik7ICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGZpbHRlckdyaWQoKTtcbiAgICAgICAgfSk7XG4gICAgICAgIFxuICAgICAgICBmaWx0ZXJHcmlkKCk7XG4gICAgfSAgICBcbiAgICBcbi8qXG5cdGZ1bmN0aW9uIG9uU2Nyb2xsKClcblx0e1x0XG5cdFx0dmFyIGlzU2Nyb2xsaW5nICA9IGZhbHNlO1xuXG5cdFx0alF1ZXJ5KHdpbmRvdykub24oXCJzY3JvbGxcIiwgZnVuY3Rpb24gKCkgXG5cdFx0e1xuXHRcdFx0aWYgKGlzU2Nyb2xsaW5nKSB7IHJldHVybjsgfVxuXG5cdFx0XHRpc1Njcm9sbGluZyA9IHRydWU7XG5cblx0XHRcdHdpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uKClcblx0XHRcdHtcblx0XHRcdFx0Ly9wbGFjZSBmdW5jdGlvbnMgaGVyZVx0XG4gICAgICAgICAgICAgICAgaGFuZGxlRml4ZWRGaWx0ZXIoKTtcblxuXHRcdFx0XHRpc1Njcm9sbGluZyA9IGZhbHNlO1xuXHRcdFx0fSwgMTAgKTtcblx0XHR9KTtcbiAgICAgICAgXG4gICAgICAgIGpRdWVyeSh3aW5kb3cpLnRyaWdnZXIoXCJzY3JvbGxcIik7ICAgICAgICBcblx0fVx0XG4gICBcbiAgICBcbiAgICBmdW5jdGlvbiBvblJlc2l6ZSgpXG4gICAge1x0XG4gICAgICAgIHZhciBpc1Jlc2l6aW5nICA9IGZhbHNlO1xuXG4gICAgICAgIGpRdWVyeSh3aW5kb3cpLm9uKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIFxuICAgICAgICB7XG4gICAgICAgICAgICBpZiAoaXNSZXNpemluZykgeyByZXR1cm47IH1cdFx0XG4gICAgICAgICAgICBpc1Jlc2l6aW5nID0gdHJ1ZTtcblxuICAgICAgICAgICAgd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24oKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIC8vcGxhY2UgZnVuY3Rpb25zIGhlcmVcdFxuICAgICAgICAgICAgICAgIGhhbmRsZUZpeGVkRmlsdGVyKCk7XG5cbiAgICAgICAgICAgICAgICBpc1Jlc2l6aW5nID0gZmFsc2U7XG4gICAgICAgICAgICB9LCAxMCApO1xuICAgICAgICB9KTtcdFxuICAgICAgICAgICAgICAgXG5cdFx0alF1ZXJ5KHdpbmRvdykudHJpZ2dlcihcInJlc2l6ZVwiKTtcbiAgICB9ICAgIFxuKi9cblxuXHR2YXIgaW5pdCA9IGZ1bmN0aW9uKClcblx0e1xuICAgICAgICBpZiAoIWpRdWVyeShcIi5wYWdlLW1haW4uY29tcGFuaWVzXCIpLmxlbmd0aCkgeyByZXR1cm47IH1cbiAgICAgICAgXG5cdFx0aGFuZGxlRmlsdGVyKHRydWUpO1xuICAgICAgICBvblNjcm9sbCgpO1xuICAgICAgICBvblJlc2l6ZSgpO1xuXG5cdH07XG5cblx0aW5pdCgpO1xufTtcblxudmFyIGNvbXBhbmllcztcblxuIiwiZnVuY3Rpb24gY291bnRkb3duKHRhcmdldElkKSB7XG4gIGNvbnN0IHRhcmdldCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHRhcmdldElkKTtcbiAgbGV0IGRheXMsIGhvdXJzLCBtaW51dGVzLCBzZWNvbmRzO1xuXG4gIGxldCBzdGFydERhdGUgPSB0YXJnZXQuZ2V0QXR0cmlidXRlKCdkYXRhLXN0YXJ0Jyk7XG4gIHN0YXJ0RGF0ZSA9IG5ldyBEYXRlKHN0YXJ0RGF0ZSkuZ2V0VGltZSgpO1xuXG4gIGxldCBlbmREYXRlID0gdGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS1lbmQnKTtcbiAgZW5kRGF0ZSA9IG5ldyBEYXRlKGVuZERhdGUpLmdldFRpbWUoKTtcblxuICBsZXQgbm93RGF0ZSA9IG5ldyBEYXRlKCk7XG4gIG5vd0RhdGUuZ2V0VGltZSgpO1xuXG4gIGxldCBiZWZvcmVNc2cgPSB0YXJnZXQuZ2V0QXR0cmlidXRlKCdkYXRhLWJlZm9yZS1tc2cnKTtcbiAgbGV0IHdoaWxlTXNnID0gdGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS13aGlsZS1tc2cnKTtcbiAgbGV0IGFmdGVyTXNnID0gdGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS1hZnRlci1tc2cnKTtcblxuICB0YXJnZXQuaW5uZXJIVE1MID0gJzxkaXYgY2xhc3M9XCJpa2FsX19jb3VudGRvd25cIj48c21hbGwgY2xhc3M9XCJtc2dcIj48L3NtYWxsPjxicj48ZGl2IGNsYXNzPVwiaWthbF9fY291bnRkb3duX19jZWxsXCI+PHNwYW4gY2xhc3M9XCJpa2FsX19jb3VudGRvd25fX2RheXNcIj48L3NwYW4+IERuaTwvZGl2PjxkaXYgY2xhc3M9XCJpa2FsX19jb3VudGRvd25fX2NlbGxcIj48c3BhbiBjbGFzcz1cImlrYWxfX2NvdW50ZG93bl9faG91cnNcIj48L3NwYW4+IEdvZHppbjwvZGl2PjxkaXYgY2xhc3M9XCJpa2FsX19jb3VudGRvd25fX2NlbGxcIj48c3BhbiBjbGFzcz1cImlrYWxfX2NvdW50ZG93bl9fbWludXRlc1wiPjwvc3Bhbj4gTWludXQ8L2Rpdj48ZGl2IGNsYXNzPVwiaWthbF9fY291bnRkb3duX19jZWxsXCI+PHNwYW4gY2xhc3M9XCJpa2FsX19jb3VudGRvd25fX3NlY29uZHNcIj48L3NwYW4+IFNla3VuZDwvZGl2PjwvZGl2Pic7XG5cbiAgZnVuY3Rpb24gY2hlY2soKSB7XG4gICAgaWYoKCFpc05hTihzdGFydERhdGUpKSAmJiAoc3RhcnREYXRlIC0gbm93RGF0ZSA+IDApKSB7XG4gICAgICAvLyBvYmxpY3phbmllIGN6YXN1IHByemVkIHJvenBvY3rEmWNpZW0gd3lkYXJ6ZW5pYVxuICAgICAgdGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJtc2dcIilbMF0uaW5uZXJIVE1MID0gYmVmb3JlTXNnO1xuICAgICAgY2FsY3VsYXRlKHN0YXJ0RGF0ZSk7XG4gICAgfSBlbHNlIGlmICgoIWlzTmFOKGVuZERhdGUpKSAmJiAoZW5kRGF0ZSAtIG5vd0RhdGUgPiAwKSkge1xuICAgICAgLy8gb2JsaWN6YW5pZSBjemFzdSB3IHRyYWtjaWUgd3lkYXJ6ZW5pYVxuICAgICAgdGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJtc2dcIilbMF0uaW5uZXJIVE1MID0gd2hpbGVNc2c7XG4gICAgICBjYWxjdWxhdGUoZW5kRGF0ZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGluZm9ybWFjamEgcG8gemFrb8WEY3plbml1IHd5ZGFyemVuaWFcbiAgICAgIGlmKGFmdGVyTXNnKSB7XG4gICAgICAgIHRhcmdldC5pbm5lckhUTUwgPSAnPHAgY2xhc3M9XCJpa2FsX19jb3VudGRvd25cIj48c21hbGwgY2xhc3M9XCJtc2dcIj4nICsgYWZ0ZXJNc2cgKyAnPC9zbWFsbD48L3A+JztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRhcmdldC5pbm5lckhUTUwgPSAnJztcbiAgICAgIH1cbiAgICAgIGNsZWFySW50ZXJ2YWwoaW50ZXJ2YWwpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgfVxuXG4gIGNvbnN0IGludGVydmFsID0gc2V0SW50ZXJ2YWwoZnVuY3Rpb24oKSB7XG4gICAgbm93RGF0ZS5nZXRUaW1lKCk7XG4gICAgY2hlY2soKTtcbiAgfSwgMTAwMCk7XG5cbiAgZnVuY3Rpb24gY2FsY3VsYXRlKGNhbGN1bGF0ZWREYXRlKSB7XG4gICAgbm93RGF0ZSA9IG5ldyBEYXRlKCk7XG4gICAgbm93RGF0ZS5nZXRUaW1lKCk7XG5cbiAgICBsZXQgdGltZVJlbWFpbmluZyA9IHBhcnNlSW50KChjYWxjdWxhdGVkRGF0ZSAtIG5vd0RhdGUpIC8gMTAwMCk7XG5cbiAgICBpZiAodGltZVJlbWFpbmluZyA+PSAwKSB7XG4gICAgICBkYXlzID0gcGFyc2VJbnQodGltZVJlbWFpbmluZyAvIDg2NDAwKTtcbiAgICAgIHRpbWVSZW1haW5pbmcgPSAodGltZVJlbWFpbmluZyAlIDg2NDAwKTtcblxuICAgICAgaG91cnMgPSBwYXJzZUludCh0aW1lUmVtYWluaW5nIC8gMzYwMCk7XG4gICAgICB0aW1lUmVtYWluaW5nID0gKHRpbWVSZW1haW5pbmcgJSAzNjAwKTtcblxuICAgICAgbWludXRlcyA9IHBhcnNlSW50KHRpbWVSZW1haW5pbmcgLyA2MCk7XG4gICAgICB0aW1lUmVtYWluaW5nID0gKHRpbWVSZW1haW5pbmcgJSA2MCk7XG5cbiAgICAgIHNlY29uZHMgPSBwYXJzZUludCh0aW1lUmVtYWluaW5nKTtcblxuICAgICAgdGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJpa2FsX19jb3VudGRvd25fX2RheXNcIilbMF0uaW5uZXJIVE1MID0gcGFyc2VJbnQoZGF5cywgMTApO1xuICAgICAgdGFyZ2V0LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJpa2FsX19jb3VudGRvd25fX2hvdXJzXCIpWzBdLmlubmVySFRNTCA9IChcIjBcIiArIGhvdXJzKS5zbGljZSgtMik7XG4gICAgICB0YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcImlrYWxfX2NvdW50ZG93bl9fbWludXRlc1wiKVswXS5pbm5lckhUTUwgPSAoXCIwXCIgKyBtaW51dGVzKS5zbGljZSgtMik7XG4gICAgICB0YXJnZXQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcImlrYWxfX2NvdW50ZG93bl9fc2Vjb25kc1wiKVswXS5pbm5lckhUTUwgPSAoXCIwXCIgKyBzZWNvbmRzKS5zbGljZSgtMik7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gIH1cbn1cblxuLy8gKGZ1bmN0aW9uICgpIHtcbi8vICAgY291bnRkb3duKCcwNC8wMS8yMzMzIDA1OjAwOjAwIFBNJyk7XG4vLyB9KCkpO1xuIiwicHJvZHVjdEZpbHRlciA9IGZ1bmN0aW9uICgpXG57XG5cdHZhciBzb3J0QXR0cmlidXRlID0gbnVsbDtcblx0dmFyIHNvcnRUeXBlID0gbnVsbDtcblx0dmFyIHNvcnREaXJlY3Rpb24gPSBudWxsO1xuXG5cdHZhciBhcHBseVNsaWRlcnMgPSBmdW5jdGlvbigpXG5cdHtcblx0XHRqUXVlcnkoXCIuc2xpZGVyW2RhdGEtZmlsdGVyLW5hbWVdXCIpLmVhY2goIGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHR2YXIgb3B0aW9ucyA9IHt9O1xuXHRcdFx0dmFyIHNsaWRlckNvbnRhaW5lciA9IGpRdWVyeSh0aGlzKTtcblxuXHRcdFx0aWYoalF1ZXJ5KHRoaXMpLmRhdGEoXCJmaWx0ZXItdHlwZVwiKSA9PSBcImJldHdlZW5cIilcblx0XHRcdHtcblx0XHRcdFx0b3B0aW9ucy5yYW5nZSA9IHRydWU7XG5cdFx0XHRcdG9wdGlvbnMudmFsdWVzID0gW107XG5cblx0XHRcdFx0aWYodHlwZW9mKGpRdWVyeSh0aGlzKS5kYXRhKFwidmFsdWUtbWluXCIpKSAhPSBcInVuZGVmaW5lZFwiICYmIGpRdWVyeSh0aGlzKS5kYXRhKFwidmFsdWUtbWluXCIpID49IDAgJiYgalF1ZXJ5KHRoaXMpLmRhdGEoXCJ2YWx1ZS1tYXhcIikpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRvcHRpb25zLnZhbHVlcy5wdXNoKGpRdWVyeSh0aGlzKS5kYXRhKFwidmFsdWUtbWluXCIpKTtcblx0XHRcdFx0XHRvcHRpb25zLnZhbHVlcy5wdXNoKGpRdWVyeSh0aGlzKS5kYXRhKFwidmFsdWUtbWF4XCIpKTtcblx0XHRcdFx0fTtcblx0XHRcdH1cblx0XHRcdGVsc2UgaWYoalF1ZXJ5KHRoaXMpLmRhdGEoXCJmaWx0ZXItdHlwZVwiKSA9PSBcInRvXCIpXG5cdFx0XHR7XG5cdFx0XHRcdG9wdGlvbnMucmFuZ2UgPSBcIm1pblwiO1xuXG5cdFx0XHRcdGlmKGpRdWVyeSh0aGlzKS5kYXRhKFwidmFsdWVcIikpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRvcHRpb25zLnZhbHVlID0galF1ZXJ5KHRoaXMpLmRhdGEoXCJ2YWx1ZVwiKTtcblx0XHRcdFx0fTtcblx0XHRcdH07XG5cblx0XHRcdGlmKGpRdWVyeSh0aGlzKS5kYXRhKFwibWluXCIpICE9IG51bGwpXG5cdFx0XHR7XG5cdFx0XHRcdG9wdGlvbnMubWluID0galF1ZXJ5KHRoaXMpLmRhdGEoXCJtaW5cIik7XG5cdFx0XHR9O1xuXG5cdFx0XHRpZihqUXVlcnkodGhpcykuZGF0YShcIm1heFwiKSlcblx0XHRcdHtcblx0XHRcdFx0b3B0aW9ucy5tYXggPSBqUXVlcnkodGhpcykuZGF0YShcIm1heFwiKTtcblx0XHRcdH07XG5cblxuXG5cdFx0XHR2YXIgcG9pbnRzVmFsdWVzID0gW107XG5cblx0XHRcdGlmKGpRdWVyeSh0aGlzKS5kYXRhKFwic3RlcFwiKSlcblx0XHRcdHtcblxuXHRcdFx0XHR2YXIgc3RlcFZhbCA9IGpRdWVyeSh0aGlzKS5kYXRhKFwic3RlcFwiKTtcblx0XHRcdFx0dmFyIGxlbmd0aCA9IE1hdGguY2VpbCgob3B0aW9ucy5tYXgtb3B0aW9ucy5taW4pL3N0ZXBWYWwpO1xuXHRcdFx0XHR2YXIgc3RhcnQgPSBNYXRoLmZsb29yKG9wdGlvbnMubWluL3N0ZXBWYWwpO1xuXHRcdFx0XHRmb3IodmFyIGkgPSBzdGFydDsgaSA8bGVuZ3RoK3N0YXJ0OyBpKyspXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRpZihpID09IHN0YXJ0ICYmIG9wdGlvbnMubWluIDwgaSpzdGVwVmFsKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHBvaW50c1ZhbHVlcy5wdXNoKG9wdGlvbnMubWluKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0cG9pbnRzVmFsdWVzLnB1c2goaSpzdGVwVmFsKVxuXHRcdFx0XHRcdGlmKGkgPT0gbGVuZ3RoK3N0YXJ0LTEgJiYgb3B0aW9ucy5tYXggPiBpKnN0ZXBWYWwpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0cG9pbnRzVmFsdWVzLnB1c2gob3B0aW9ucy5tYXgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXG5cblx0XHRcdFx0aWYocG9pbnRzVmFsdWVzLmxlbmd0aClcblx0XHRcdFx0e1xuXHRcdFx0XHRcdG9wdGlvbnMubWluID0gMDtcblx0XHRcdFx0XHRvcHRpb25zLm1heCA9IHBvaW50c1ZhbHVlcy5sZW5ndGgtMTtcblxuXHRcdFx0XHRcdGlmKG9wdGlvbnMudmFsdWUpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0b3B0aW9ucy52YWx1ZSA9IHBvaW50c1ZhbHVlcy5pbmRleE9mKG9wdGlvbnMudmFsdWUpO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdGlmKG9wdGlvbnMudmFsdWVzKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdG9wdGlvbnMudmFsdWVzWzBdID0gcG9pbnRzVmFsdWVzLmluZGV4T2Yob3B0aW9ucy52YWx1ZXNbMF0pO1xuXHRcdFx0XHRcdFx0b3B0aW9ucy52YWx1ZXNbMV0gPSBwb2ludHNWYWx1ZXMuaW5kZXhPZihvcHRpb25zLnZhbHVlc1sxXSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblx0XHRcdH1cblxuXG5cdFx0XHRpZihqUXVlcnkodGhpcykuZGF0YShcInByb2dyZXNzaXZlXCIpID09IHRydWUpXG5cdFx0XHR7XG5cdFx0XHRcdHZhciByYW5nZXNDb25maWcgPVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHRbXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogMCwgXCJtYXhcIjogOTAwLCBcInN0ZXBcIjogMTAwfSxcblx0XHRcdFx0XHRcdHtcIm1pblwiOiAxMDAwLCBcIm1heFwiOiA5MDAwLCBcInN0ZXBcIjogMTAwMH0sXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogMTAwMDAsIFwibWF4XCI6IDQ4MDAwLCBcInN0ZXBcIjogMjAwMH0sXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogNTAwMDAsIFwibWF4XCI6IDUwMDAwMDAwLCBcInN0ZXBcIjogNTAwMH0sXG5cdFx0XHRcdFx0XSxcblx0XHRcdFx0XHRpbnZlc3RtZW50czogW1xuXHRcdFx0XHRcdFx0e1wibWluXCI6IDAsIFwibWF4XCI6IDUwMDAsIFwic3RlcFwiOiA1MDB9LFxuXHRcdFx0XHRcdFx0e1wibWluXCI6IDYwMDAsIFwibWF4XCI6IDE5MDAwLCBcInN0ZXBcIjogMTAwMH0sXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogMjAwMDAsIFwibWF4XCI6IDk1MDAwLCBcInN0ZXBcIjogNTAwMH0sXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogMTAwMDAwLCBcIm1heFwiOiA0NzUwMDAsIFwic3RlcFwiOiAyNTAwMH0sXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogNTAwMDAwLCBcIm1heFwiOiAxMDAwMDAwMCwgXCJzdGVwXCI6IDUwMDAwfSxcblx0XHRcdFx0XHRdLFxuXHRcdFx0XHRcdGludmVzdG1lbnRzTW9udGhzOlxuXHRcdFx0XHRcdFtcblx0XHRcdFx0XHRcdHtcIm1pblwiOiAwLCBcIm1heFwiOiAyMywgXCJzdGVwXCI6IDF9LFxuXHRcdFx0XHRcdFx0e1wibWluXCI6IDI0LCBcIm1heFwiOiA2MDAsIFwic3RlcFwiOiAxMn1cblx0XHRcdFx0XHRdXG5cdFx0XHRcdH07XG5cblx0XHRcdFx0dmFyIHJhbmdlcyA9IHJhbmdlc0NvbmZpZy5kZWZhdWx0O1xuXG5cdFx0XHRcdGlmKGpRdWVyeSh0aGlzKS5kYXRhKFwicHJvZ3Jlc3NpdmUtdmFsdWVzXCIpICYmIHJhbmdlc0NvbmZpZ1tqUXVlcnkodGhpcykuZGF0YShcInByb2dyZXNzaXZlLXZhbHVlc1wiKV0pXG5cdFx0XHRcdFx0cmFuZ2VzID0gcmFuZ2VzQ29uZmlnW2pRdWVyeSh0aGlzKS5kYXRhKFwicHJvZ3Jlc3NpdmUtdmFsdWVzXCIpXTtcblxuXHRcdFx0XHRmb3IoaT0wOyBpIDwgcmFuZ2VzLmxlbmd0aDsgaSsrKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0dmFyIGN1cnJlbnQgPSAwO1xuXHRcdFx0XHRcdGlmKG9wdGlvbnMubWluID4gcmFuZ2VzW2ldLm1heCB8fCBvcHRpb25zLm1heCA8IHJhbmdlc1tpXS5taW4pXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0Y29udGludWU7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRjdXJyZW50ID0gcmFuZ2VzW2ldLm1pbjtcblx0XHRcdFx0XHRcdHdoaWxlKGN1cnJlbnQgPCBvcHRpb25zLm1pbilcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0Y3VycmVudCArPSByYW5nZXNbaV0uc3RlcDtcblx0XHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdFx0aWYob3B0aW9ucy5taW4gPiByYW5nZXNbaV0ubWluICYmIGN1cnJlbnQtcmFuZ2VzW2ldLnN0ZXAgPCBvcHRpb25zLm1pbilcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0cG9pbnRzVmFsdWVzLnB1c2gob3B0aW9ucy5taW4pO1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHR3aGlsZShjdXJyZW50IDw9IG9wdGlvbnMubWF4ICYmIGN1cnJlbnQgPD0gcmFuZ2VzW2ldLm1heClcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0cG9pbnRzVmFsdWVzLnB1c2goY3VycmVudCk7XG5cdFx0XHRcdFx0XHRcdGN1cnJlbnQgKz0gcmFuZ2VzW2ldLnN0ZXA7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblxuXHRcdFx0XHRpZihwb2ludHNWYWx1ZXMubGVuZ3RoKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0b3B0aW9ucy5taW4gPSAwO1xuXHRcdFx0XHRcdG9wdGlvbnMubWF4ID0gcG9pbnRzVmFsdWVzLmxlbmd0aC0xO1xuXG5cdFx0XHRcdFx0aWYob3B0aW9ucy52YWx1ZSlcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRvcHRpb25zLnZhbHVlID0gcG9pbnRzVmFsdWVzLmluZGV4T2Yob3B0aW9ucy52YWx1ZSk7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0aWYob3B0aW9ucy52YWx1ZXMpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0b3B0aW9ucy52YWx1ZXNbMF0gPSBwb2ludHNWYWx1ZXMuaW5kZXhPZihvcHRpb25zLnZhbHVlc1swXSk7XG5cdFx0XHRcdFx0XHRvcHRpb25zLnZhbHVlc1sxXSA9IHBvaW50c1ZhbHVlcy5pbmRleE9mKG9wdGlvbnMudmFsdWVzWzFdKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblxuXG5cdFx0XHR9XG5cblx0XHRcdG9wdGlvbnMuc2xpZGUgPSBmdW5jdGlvbiggZXZlbnQsIHVpIClcblx0XHRcdHtcblx0XHRcdFx0aWYodWkudmFsdWUgfHwgcG9pbnRzVmFsdWVzW3VpLnZhbHVlXSlcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHZhciBleFZhbCA9IHVpLnZhbHVlO1xuXG5cdFx0XHRcdFx0aWYocG9pbnRzVmFsdWVzLmxlbmd0aClcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRleFZhbCA9IHBvaW50c1ZhbHVlc1t1aS52YWx1ZV07XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0c2xpZGVyQ29udGFpbmVyLmZpbmQoXCIudmFsdWVcIikuaHRtbCggZm9ybWF0TnVtYmVyKGV4VmFsKSApO1xuXHRcdFx0XHRcdHNsaWRlckNvbnRhaW5lci5wYXJlbnRzKFwiZm9ybVwiKS5maXJzdCgpLmZpbmQoXCJpbnB1dFtuYW1lPSdcIiArIHNsaWRlckNvbnRhaW5lci5kYXRhKFwiZmlsdGVyLW5hbWVcIikgKyBcIiddXCIpLnZhbChleFZhbClcblx0XHRcdFx0fVxuXHRcdFx0XHRpZih1aS52YWx1ZXMpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHR2YXIgZXhWYWxNaW4gPSB1aS52YWx1ZXNbMF07XG5cdFx0XHRcdFx0dmFyIGV4VmFsTWF4ID0gdWkudmFsdWVzWzFdO1xuXG5cdFx0XHRcdFx0aWYocG9pbnRzVmFsdWVzLmxlbmd0aClcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRleFZhbE1pbiA9IHBvaW50c1ZhbHVlc1t1aS52YWx1ZXNbMF1dO1xuXHRcdFx0XHRcdFx0ZXhWYWxNYXggPSBwb2ludHNWYWx1ZXNbdWkudmFsdWVzWzFdXTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRzbGlkZXJDb250YWluZXIuZmluZChcIi52YWx1ZVwiKS5odG1sKCBmb3JtYXROdW1iZXIoZXhWYWxNaW4pICsgXCIgLSBcIiArIGZvcm1hdE51bWJlcihleFZhbE1heCkgKTtcblx0XHRcdFx0XHRzbGlkZXJDb250YWluZXIucGFyZW50cyhcImZvcm1cIikuZmlyc3QoKS5maW5kKFwiaW5wdXRbbmFtZT0nbWluLVwiICsgc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJmaWx0ZXItbmFtZVwiKSArIFwiJ11cIikudmFsKGV4VmFsTWluKVxuXHRcdFx0XHRcdHNsaWRlckNvbnRhaW5lci5wYXJlbnRzKFwiZm9ybVwiKS5maXJzdCgpLmZpbmQoXCJpbnB1dFtuYW1lPSdtYXgtXCIgKyBzbGlkZXJDb250YWluZXIuZGF0YShcImZpbHRlci1uYW1lXCIpICsgXCInXVwiKS52YWwoZXhWYWxNYXgpXG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0b3B0aW9ucy5jaGFuZ2UgPSBmdW5jdGlvbiggZXZlbnQsIHVpIClcblx0XHRcdHtcblx0XHRcdFx0c2xpZGVyQ29udGFpbmVyLnBhcmVudHMoXCJmb3JtXCIpLmZpcnN0KCkudHJpZ2dlcihcImlrYWw6ZmlsdGVyOmNoYW5nZWRcIik7XG5cdFx0XHR9XG5cblx0XHRcdGlmKHBvaW50c1ZhbHVlcylcblx0XHRcdHtcblx0XHRcdFx0aWYocG9pbnRzVmFsdWVzW29wdGlvbnMubWluXSlcblx0XHRcdFx0XHRzbGlkZXJDb250YWluZXIuZmluZChcIi5yYW5nZS1taW4tdmFsdWVcIikuaHRtbChwb2ludHNWYWx1ZXNbb3B0aW9ucy5taW5dKTtcblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdHNsaWRlckNvbnRhaW5lci5maW5kKFwiLnJhbmdlLW1pbi12YWx1ZVwiKS5odG1sKHBvaW50c1ZhbHVlc1sob3B0aW9ucy5taW4gKyAxKV0gKTtcblxuXHRcdFx0XHRzbGlkZXJDb250YWluZXIuZmluZChcIi5yYW5nZS1tYXgtdmFsdWVcIikuaHRtbChwb2ludHNWYWx1ZXNbb3B0aW9ucy5tYXhdKTtcblxuXHRcdFx0fVxuXHRcdFx0ZWxzZVxuXHRcdFx0e1xuXHRcdFx0XHRzbGlkZXJDb250YWluZXIuZmluZChcIi5yYW5nZS1taW4tdmFsdWVcIikuaHRtbChvcHRpb25zLm1pbik7XG5cdFx0XHRcdHNsaWRlckNvbnRhaW5lci5maW5kKFwiLnJhbmdlLW1heC12YWx1ZVwiKS5odG1sKG9wdGlvbnMubWF4KTtcblxuXHRcdFx0fVxuXHRcdFx0c2xpZGVyQ29udGFpbmVyLmZpbmQoXCIuc2xpZGVyLWJhclwiKS5zbGlkZXIob3B0aW9ucyk7XG5cdFx0fSk7XG5cblx0fVxuXG5cdHZhciBhcHBseUlucHV0cyA9IGZ1bmN0aW9uKClcblx0e1xuXHRcdGpRdWVyeShcIi5maWx0ZXJzIGZvcm1cIikuZWFjaCggZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdHZhciBmaWx0ZXJGb3JtID0galF1ZXJ5KHRoaXMpO1xuXHRcdFx0alF1ZXJ5KFwiaW5wdXRbdHlwZT1jaGVja2JveF0sIGlucHV0W3R5cGU9cmFkaW9dLCBpbnB1dFt0eXBlPXRleHRdXCIsIGZpbHRlckZvcm0pLm9uKFwiY2hhbmdlXCIsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRmaWx0ZXJGb3JtLnRyaWdnZXIoXCJpa2FsOmZpbHRlcjpjaGFuZ2VkXCIpO1xuXHRcdFx0fSlcblx0XHR9KTtcblx0fVxuXG5cdHZhciBwcmVwYXJlRm9ybXMgPSBmdW5jdGlvbigpXG5cdHtcblx0XHRqUXVlcnkoXCIuZmlsdGVycyBmb3JtXCIpLmVhY2goIGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHR2YXIgZmlsdGVyRm9ybSA9IGpRdWVyeSh0aGlzKTtcblxuXHRcdFx0ZmlsdGVyRm9ybS5vbihcImlrYWw6ZmlsdGVyOmNoYW5nZWRcIiwgZnVuY3Rpb24oKVxuXHRcdFx0e1xuXHRcdFx0XHR2YXIgbGlzdCA9IGZpbHRlckZvcm0ucGFyZW50cyhcIi5wcm9kdWN0LWxpc3RcIikuZmlyc3QoKS5maW5kKFwiLnByb2R1Y3RzLmxpc3RcIik7XG5cdFx0XHRcdGxpc3QuYWRkQ2xhc3MoXCJsb2FkaW5nXCIpO1xuXHRcdFx0XHRpZihqUXVlcnkoXCIjcGFnZVwiKS5kYXRhKFwicHJvZHVjdC1zbHVnXCIpKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0dmFyIHVybCA9IFwiL3dwLWNvbnRlbnQvdGhlbWVzL2lrYWxrdWxhdG9yL3Byb2R1Y3QtYXBpL3Byb2R1Y3RzLnBocD9wdHlwZT1cIiArIGpRdWVyeShcIiNwYWdlXCIpLmRhdGEoXCJwcm9kdWN0LXNsdWdcIikgKyBcIiZcIiArIGZpbHRlckZvcm0uc2VyaWFsaXplKCk7XG5cdFx0XHRcdFx0alF1ZXJ5LmdldCh1cmwsIGZ1bmN0aW9uKHJlc3BvbnNlKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGxpc3QuZmlyc3QoKS5odG1sKHJlc3BvbnNlKTtcblx0XHRcdFx0XHRcdHJlZnJlc2hTb3J0aW5nKGxpc3QpO1xuXHRcdFx0XHRcdFx0bGlzdC5yZW1vdmVDbGFzcyhcImxvYWRpbmdcIik7XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fSk7XG5cdH1cblxuXHR2YXIgYXBwbHlTb3J0aW5nID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLnByb2R1Y3RzLmxpc3RcIikuZWFjaCggZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdHZhciBsaXN0ID0galF1ZXJ5KHRoaXMpO1xuXHRcdFx0dmFyIGhlYWRlciA9IGxpc3QucGFyZW50KCkuZmluZChcIi5wcm9kdWN0cy5oZWFkZXJcIik7XG5cdFx0XHRqUXVlcnkoXCJ1bCBsaSBkaXYuc29ydGFibGUgYVwiLCBoZWFkZXIpXG4gICAgICAgICAgICAgICAgLmFwcGVuZCgnPHNwYW4gY2xhc3M9XCJhcnJvd1wiPjwvc3Bhbj4nKVxuICAgICAgICAgICAgICAgIC5vbihcImNsaWNrXCIsIGpRdWVyeS5wcm94eShzb3J0TGlzdCwgdGhpcywgbGlzdCkpO1xuXHRcdH0pXG5cdH1cblxuXHR2YXIgc29ydExpc3QgPSBmdW5jdGlvbihsaXN0LCBldmVudClcblx0e1xuXHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cblx0XHR2YXIgc29ydGJ5ID0galF1ZXJ5KGV2ZW50LnRhcmdldCkuZGF0YShcInNvcnQtYnlcIik7XG5cdFx0dmFyIHNvcnR0eXAgPSBqUXVlcnkoZXZlbnQudGFyZ2V0KS5kYXRhKFwic29ydC10eXBlXCIpO1xuXHRcdHZhciBzb3J0ZGlyID0galF1ZXJ5KGV2ZW50LnRhcmdldCkuZGF0YShcInNvcnQtZGlyXCIpO1xuXG5cdFx0aWYoc29ydEF0dHJpYnV0ZSA9PSBzb3J0YnkpXG5cdFx0e1xuXHRcdFx0aWYoc29ydERpcmVjdGlvbiAhPSBcImRlc2NcIilcblx0XHRcdFx0c29ydERpcmVjdGlvbiA9IFwiZGVzY1wiO1xuXHRcdFx0ZWxzZVxuXHRcdFx0XHRzb3J0RGlyZWN0aW9uID0gXCJhc2NcIjtcblx0XHR9XG5cdFx0ZWxzZVxuXHRcdHtcblx0XHRcdGlmKHNvcnRkaXIgPT0gXCJkZXNjXCIpXG5cdFx0XHRcdHNvcnREaXJlY3Rpb24gPSBcImRlc2NcIjtcblx0XHRcdGVsc2Vcblx0XHRcdFx0c29ydERpcmVjdGlvbiA9IFwiYXNjXCI7XG5cdFx0fVxuXG5cdFx0alF1ZXJ5KFwiZGl2LnNvcnRhYmxlXCIpLnJlbW92ZUNsYXNzKFwiZGVzYyBhc2NcIik7XG5cbiAgICAgICAgalF1ZXJ5KGV2ZW50LnRhcmdldCkuY2xvc2VzdChcImRpdi5zb3J0YWJsZVwiKVxuICAgICAgICAgICAgLmFkZENsYXNzKHNvcnREaXJlY3Rpb24pO1xuXG5cdFx0c29ydFR5cGUgPSBzb3J0dHlwO1xuXHRcdHNvcnRBdHRyaWJ1dGUgPSBzb3J0Ynk7XG5cblx0XHRqUXVlcnkoXCIuc29ydGVkLWZpZWxkXCIpLnJlbW92ZUNsYXNzKFwic29ydGVkLWZpZWxkXCIpO1xuXHRcdGpRdWVyeShcIi5cIiArIHNvcnRBdHRyaWJ1dGUpLmFkZENsYXNzKFwic29ydGVkLWZpZWxkXCIpO1xuXG5cdFx0cmVmcmVzaFNvcnRpbmcobGlzdCk7XG5cdH1cblxuXHR2YXIgcmVmcmVzaFNvcnRpbmcgPSBmdW5jdGlvbihsaXN0KVxuXHR7XG5cdFx0bGlzdC5maW5kKFwiID4gdWwgPiBsaVwiKS5zb3J0KGpRdWVyeS5wcm94eShzb3J0ZXIsIHRoaXMsIHNvcnRBdHRyaWJ1dGUsIHNvcnRUeXBlLCBzb3J0RGlyZWN0aW9uKSkuYXBwZW5kVG8obGlzdC5maW5kKFwiID4gdWxcIikpO1xuXHR9XG5cblx0dmFyIHNvcnRlciA9IGZ1bmN0aW9uKHNvcnRieSwgc29ydHR5cGUsIHNvcnRkaXIsIGEsIGIpXG5cdHtcblx0XHRpZihzb3J0dHlwZSA9PSBcIm51bWJlclwiKVxuXHRcdHtcblx0XHRcdGlmKHNvcnRkaXIgPT0gXCJhc2NcIikgcmV0dXJuIChwYXJzZUZsb2F0KGpRdWVyeShiKS5jbG9zZXN0KFwibGlcIikuZGF0YShzb3J0YnkpKSA8IHBhcnNlRmxvYXQoalF1ZXJ5KGEpLmNsb3Nlc3QoXCJsaVwiKS5kYXRhKHNvcnRieSkpKSA/IDEgOiAtMTtcblx0XHRcdGVsc2UgcmV0dXJuIChwYXJzZUZsb2F0KGpRdWVyeShhKS5jbG9zZXN0KFwibGlcIikuZGF0YShzb3J0YnkpKSA8IHBhcnNlRmxvYXQoalF1ZXJ5KGIpLmNsb3Nlc3QoXCJsaVwiKS5kYXRhKHNvcnRieSkpKSA/IDEgOiAtMTtcblx0XHR9XG5cdH1cblxuXHR2YXIgaGFuZGxlRGV0YWlscyA9IGZ1bmN0aW9uKClcblx0e1xuXHRcdGpRdWVyeShkb2N1bWVudCkub24oXCJjbGlja1wiLCBcIi5wcm9kdWN0LWxpc3QgLnByb2R1Y3RzLmxpc3QgdWwgbGkgLm1vcmUgLmV4cGFuZGVyXCIsIGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHRqUXVlcnkodGhpcykudG9nZ2xlQ2xhc3MoXCJ1bmZvbGRlZFwiKVxuXHRcdFx0alF1ZXJ5KHRoaXMpLnBhcmVudCgpLmZpbmQoXCIuZXhwYW5kZWRcIikuc2xpZGVUb2dnbGUoKTtcblx0XHR9KVxuXHR9XG5cblx0dmFyIGhhbmRsZU9wdGlvbmFsID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLmZpbHRlcnMgZm9ybSAuc2hvdy1hbGxcIikuZWFjaCggZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdFx0dmFyIG1vdmVEb3duSGVhZGVyID0gZnVuY3Rpb24oKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0aWYoalF1ZXJ5KFwiLmNvbnRhaW5lci5wcm9kdWN0cy5oZWFkZXJcIikuaGFzQ2xhc3MoXCJmaXhlZFwiKSlcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRqUXVlcnkoXCIuY29udGFpbmVyLnByb2R1Y3RzLmhlYWRlclwiKS5jc3MoXCJ0b3BcIiwgalF1ZXJ5KFwiLnByb2R1Y3QtbGlzdCAuZmlsdGVycy5jb250YWluZXJcIikub3V0ZXJIZWlnaHQoKSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHRcdHZhciBzaG93QWxsQnRuID0galF1ZXJ5KHRoaXMpO1xuXHRcdFx0XHRqUXVlcnkoc2hvd0FsbEJ0bikub24oXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0c2hvd0FsbEJ0bi50b2dnbGVDbGFzcyhcImFjdGl2ZVwiKTtcblx0XHRcdFx0c2V0SW50ZXJ2YWwoIG1vdmVEb3duSGVhZGVyLCAyMCk7XG5cdFx0XHRcdHNob3dBbGxCdG4uY2xvc2VzdChcImZvcm1cIikuZmluZChcIi50b2dnbGFibGVcIikuc2xpZGVUb2dnbGUoIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRcdGNsZWFySW50ZXJ2YWwoIG1vdmVEb3duSGVhZGVyICk7XG5cdFx0XHRcdFx0alF1ZXJ5KHdpbmRvdykudHJpZ2dlcihcInNjcm9sbCB0b3VjaG1vdmVcIik7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSlcblx0XHR9KTtcblx0fVxuXG5cdHZhciBoYW5kbGVUb29sdGlwID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0aWYoalF1ZXJ5KFwiLnRvb2x0aXBcIikubGVuZ3RoKVxuXHRcdHtcblx0XHRcdGpRdWVyeSh3aW5kb3cpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGpRdWVyeShcIi50b29sdGlwXCIpLmNzcyhcInotaW5kZXhcIiwgXCJcIikuZmluZChcIi5jb250ZW50XCIpLnN0b3AoKS5zbGlkZVVwKCk7XG5cdFx0XHR9KTtcblxuXHRcdFx0alF1ZXJ5KFwiLnRvb2x0aXAgLnNob3dcIikub24oXCJjbGlja1wiLCBmdW5jdGlvbihlKVxuXHRcdFx0e1xuXHRcdFx0XHRlLnN0b3BQcm9wYWdhdGlvbigpO1xuXHRcdFx0XHRqUXVlcnkoXCIucHJvZHVjdHMgLnRvb2x0aXBcIikuY3NzKFwiei1pbmRleFwiLCBcIlwiKS5maW5kKFwiLmNvbnRlbnRcIikuc3RvcCgpLnNsaWRlVXAoKTtcblx0XHRcdFx0alF1ZXJ5KHRoaXMpLmNsb3Nlc3QoXCIudG9vbHRpcFwiKS5jc3MoXCJ6LWluZGV4XCIsIDE5KS5maW5kKFwiLmNvbnRlbnRcIikuc3RvcCgpLnNsaWRlVG9nZ2xlKCk7XG5cdFx0XHRcdGlmKGpRdWVyeSh0aGlzKS5jbG9zZXN0KFwiLnRvb2x0aXBcIikuZmluZChcIi5jb250ZW50XCIpLm91dGVyV2lkdGgoKSArIGpRdWVyeSh0aGlzKS5jbG9zZXN0KFwiLnRvb2x0aXBcIikuZmluZChcIi5jb250ZW50XCIpLm9mZnNldCgpLmxlZnQgPiBqUXVlcnkod2luZG93KS53aWR0aCgpKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0alF1ZXJ5KHRoaXMpLmNsb3Nlc3QoXCIudG9vbHRpcFwiKS5maW5kKFwiLmNvbnRlbnRcIikuY3NzKFwibGVmdFwiLCAtalF1ZXJ5KHRoaXMpLmNsb3Nlc3QoXCIudG9vbHRpcFwiKS5maW5kKFwiLmNvbnRlbnRcIikub3V0ZXJXaWR0aCgpLzIgKyA0MCk7XG5cdFx0XHRcdH1cblx0XHRcdH0pXG5cblx0XHR9XG5cdH1cblxuXHR2YXIgYXBwbHlGaXhlZEZpbHRlciA9IGZ1bmN0aW9uKClcblx0e1xuICAgICAgICB2YXIgZmlsdGVyID0gIGpRdWVyeShcIi5wcm9kdWN0LWxpc3QgLmZpbHRlcnNcIik7XG4gICAgICAgIHZhciBwcm9kdWN0SGVhZGVyID0galF1ZXJ5KFwiLnByb2R1Y3QtbGlzdCAucHJvZHVjdHMuaGVhZGVyXCIpO1xuICAgICAgICB2YXIgcHJvZHVjdExpc3QgPSBqUXVlcnkoXCIucHJvZHVjdC1saXN0IC5wcm9kdWN0cy5saXN0XCIpO1xuICAgICAgICBcbiAgICAgICAgaWYgKCFmaWx0ZXIubGVuZ3RoICYmICFwcm9kdWN0SGVhZGVyLmxlbmd0aClcbiAgICAgICAgICAgIHJldHVybjtcblxuXHRcdHZhciBtYXJnaW4gPSBmaWx0ZXIub2Zmc2V0KCkudG9wIC0gcHJvZHVjdExpc3Qub2Zmc2V0KCkudG9wO1xuXG5cdFx0aWYoalF1ZXJ5KHdpbmRvdykud2lkdGgoKSA8IDc2OClcblx0XHR7XG5cdFx0XHRtYXJnaW4gPSAtcHJvZHVjdEhlYWRlci5oZWlnaHQoKTtcblx0XHR9XG5cblx0XHR2YXIgZXhlY09uU2Nyb2xsID0gZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdGlmIChwcm9kdWN0TGlzdC5vZmZzZXQoKS50b3AgKyBtYXJnaW4gPD0galF1ZXJ5KHdpbmRvdykuc2Nyb2xsVG9wKCkgJiYgalF1ZXJ5KHdpbmRvdykud2lkdGgoKSA+PSA3NjgpXG5cdFx0XHR7XG5cdFx0XHRcdHByb2R1Y3RMaXN0XG5cdFx0XHRcdFx0LmNzcyh7XG5cdFx0XHRcdFx0XHRcIm1hcmdpbi10b3BcIiA6IChmaWx0ZXIub3V0ZXJIZWlnaHQodHJ1ZSkgKyBwcm9kdWN0SGVhZGVyLm91dGVySGVpZ2h0KHRydWUpKSArIFwicHhcIlxuXHRcdFx0XHRcdH0pO1xuXG5cdFx0XHRcdGZpbHRlclxuXHRcdFx0XHRcdC5hZGRDbGFzcyhcImZpeGVkXCIpO1xuXG5cdFx0XHRcdHByb2R1Y3RIZWFkZXJcblx0XHRcdFx0XHQuY3NzKHtcblx0XHRcdFx0XHRcdFwidG9wXCIgOiBmaWx0ZXIub3V0ZXJIZWlnaHQoKSArIFwicHhcIlxuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0LmFkZENsYXNzKFwiZml4ZWRcIik7XG5cblx0XHRcdH1cblx0XHRcdGVsc2Vcblx0XHRcdHtcblx0XHRcdFx0ZmlsdGVyLnJlbW92ZUNsYXNzKFwiZml4ZWRcIik7XG5cblx0XHRcdFx0cHJvZHVjdEhlYWRlclxuXHRcdFx0XHRcdC5jc3Moe1xuXHRcdFx0XHRcdFx0XCJ0b3BcIiA6IFwiXCJcblx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdC5yZW1vdmVDbGFzcyhcImZpeGVkXCIpO1xuXG5cdFx0XHRcdHByb2R1Y3RMaXN0XG5cdFx0XHRcdFx0LmNzcyh7XG5cdFx0XHRcdFx0XHRcIm1hcmdpbi10b3BcIiA6IFwiXCJcblx0XHRcdFx0XHR9KTtcblxuXHRcdFx0fVxuXHRcdH1cbiAgICAgICAgXG4gICAgICAgIGpRdWVyeSh3aW5kb3cpLm9uKFwic2Nyb2xsXCIsIGZ1bmN0aW9uKCkge1xuXHQgICAgICAgIGNsZWFyVGltZW91dChleGVjT25TY3JvbGwpO1xuXHQgICAgICAgIHNldFRpbWVvdXQoZXhlY09uU2Nyb2xsLCA1MClcbiAgICAgICAgfSk7XG5cblx0XHRqUXVlcnkod2luZG93KS5vbihcInRvdWNobW92ZVwiLCBleGVjT25TY3JvbGwpO1xuICAgIH1cblxuXHQvKiBfX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fIHB1YmxpYyBtZXRob2RzICovXG5cblx0dmFyIGluaXQgPSBmdW5jdGlvbigpXG5cdHtcblx0XHRhcHBseUlucHV0cygpO1xuXHRcdGFwcGx5U2xpZGVycygpO1xuXHRcdGFwcGx5U29ydGluZygpO1xuXHRcdGFwcGx5Rml4ZWRGaWx0ZXIoKTtcblx0XHRwcmVwYXJlRm9ybXMoKTtcblx0XHRoYW5kbGVEZXRhaWxzKCk7XG5cdFx0aGFuZGxlT3B0aW9uYWwoKTtcblx0XHRoYW5kbGVUb29sdGlwKCk7XG5cdH07XG5cblx0aW5pdCgpO1xufTtcblxudmFyIHByb2R1Y3RGaWx0ZXI7XG5cbmZvcm1hdE51bWJlciA9IGZ1bmN0aW9uIChudW0pIHtcblx0cmV0dXJuIG51bS50b1N0cmluZygpLnJlcGxhY2UoLyhcXGQpKD89KFxcZHszfSkrKD8hXFxkKSkvZywgXCIkMSBcIilcbn0iLCJwcm9tb2JveCA9IGZ1bmN0aW9uICgpXG57XG4gICAgdmFyIGRyYWdnYWJsZUxpc3QsXG4gICAgICAgIHBvc3RzTGlzdCxcbiAgICAgICAgcHJvbW9ib3hFbGVtZW50LFxuICAgICAgICBpbmZvLFxuICAgICAgICBmaWx0ZXJzLFxuICAgICAgICBmaWx0ZXJzUG9zID0gbnVsbCxcbiAgICAgICAgZmlsdGVyc1N0YXJ0UG9zID0gbnVsbCxcbiAgICAgICAgbGlzdFdpZHRoLFxuICAgICAgICBkaXN0YW5zY2VGcm9tUmlnaHQ7XG4gICAgICAgIGRyYWdnaW5nID0gZmFsc2U7XG4gICAgICAgIGl0ZW1XaWR0aCA9IG51bGwsIHNlZUFsbEl0ZW1XaWR0aCA9IG51bGw7XG4gICAgdmFyIHBlcDtcbiAgICBcbiAgICBmdW5jdGlvbiBnZXRQb3N0c0xpc3RXaWR0aChsaXN0KVxuICAgIHtcbiAgICAgICAgdmFyIHdpZHRoID0gMDtcblxuICAgICAgICBqUXVlcnkoXCIucG9zdC1pdGVtOm5vdCguaGlkZSk6bm90KC5hbGwtcHJvbW90aW9ucylcIiwgbGlzdCkuZWFjaChmdW5jdGlvbigpXG4gICAgICAgIHtcbi8vICAgICAgICAgICAgd2lkdGggKz0galF1ZXJ5KHRoaXMpLm91dGVyV2lkdGgodHJ1ZSk7XG4gICAgICAgICAgICB3aWR0aCArPSBpdGVtV2lkdGg7Ly8valF1ZXJ5KHRoaXMpLm91dGVyV2lkdGgoKTsgLy8zMzNcbiAgICAgICAgfSk7XG4gICAgICAgIFxuICAgICAgICB3aWR0aCArPSBzZWVBbGxJdGVtV2lkdGg7XG4gICAgICAgIHdpZHRoICs9IHBhcnNlRmxvYXQobGlzdC5jc3MoXCJwYWRkaW5nLWxlZnRcIikpICsgcGFyc2VGbG9hdChsaXN0LmNzcyhcInBhZGRpbmctcmlnaHRcIikpOyAgICAgICAgXG4gICAgICAgIFxuICAgICAgICByZXR1cm4gd2lkdGg7XG4gICAgfSAgXG4gICAgXG4gICAgZnVuY3Rpb24gdXBkYXRlVmFycygpXG4gICAge1xuICAgICAgICBkcmFnZ2FibGVMaXN0ID0galF1ZXJ5KFwiLmRyYWdnYWJsZS1saXN0XCIpO1xuICAgICAgICBwb3N0c0xpc3QgPSBqUXVlcnkoXCIucG9zdHMtbGlzdC1jb250ZW50XCIsIGRyYWdnYWJsZUxpc3QpO1xuXG4gICAgICAgIGlmICghcG9zdHNMaXN0Lmxlbmd0aClcbiAgICAgICAgICAgIHJldHVybjsgICBcblxuICAgICAgICBwcm9tb2JveEVsZW1lbnQgPSBkcmFnZ2FibGVMaXN0LmNsb3Nlc3QoXCIucHJvbW9ib3hcIik7XG4gICAgICAgIGluZm8gPSBqUXVlcnkoXCIuaW5mb1wiLCBwcm9tb2JveEVsZW1lbnQpO1xuICAgICAgICBcbiAgICAgICAgaWYgKGZpbHRlcnNQb3MgPT09IG51bGwpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGZpbHRlcnMgPSBqUXVlcnkoXCIuZmlsdGVyc1wiLCBkcmFnZ2FibGVMaXN0KTsgIFxuICAgICAgICAgICAgZmlsdGVyc1BvcyA9IGZpbHRlcnMucG9zaXRpb24oKTtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKGZpbHRlcnNTdGFydFBvcyA9PT0gbnVsbClcbiAgICAgICAge1xuICAgICAgICAgICAgZmlsdGVyc1N0YXJ0UG9zID0gZmlsdGVycy5wb3NpdGlvbigpLmxlZnQ7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGl0ZW1XaWR0aCA9PT0gbnVsbClcbiAgICAgICAge1xuICAgICAgICAgICAgaXRlbVdpZHRoID0galF1ZXJ5KFwiLnBvc3QtaXRlbVwiLCBwb3N0c0xpc3QpLm5vdChcIi5hbGwtcHJvbW90aW9uc1wiKS5lcSgwKS5vdXRlcldpZHRoKCk7XG4gICAgICAgICAgICBzZWVBbGxJdGVtV2lkdGggPSBqUXVlcnkoXCIucG9zdC1pdGVtLmFsbC1wcm9tb3Rpb25zXCIsIHBvc3RzTGlzdCkuZXEoMCkub3V0ZXJXaWR0aCgpO1xuICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgIGxpc3RXaWR0aCA9IGdldFBvc3RzTGlzdFdpZHRoKHBvc3RzTGlzdCk7XG4gICAgICAgIHBvc3RzTGlzdC5vdXRlcldpZHRoKGxpc3RXaWR0aCk7XG4gICAgICAgIGRpc3RhbnNjZUZyb21SaWdodCA9IChqUXVlcnkod2luZG93KS5pbm5lcldpZHRoKCkgLSAgKGRyYWdnYWJsZUxpc3Qub2Zmc2V0KCkubGVmdCArIGRyYWdnYWJsZUxpc3Qub3V0ZXJXaWR0aCgpKSk7XG4gICAgfSAgICAgIFxuICAgIFxuICAgIGZ1bmN0aW9uIHVwZGF0ZUdhbGxlcnkoKVxuICAgIHsgIFxuICAgICAgICBpZiAgKHR5cGVvZiBwZXAgIT09IFwidW5kZWZpbmVkXCIpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGpRdWVyeS5wZXAudW5iaW5kKCBwZXAgKTsgXG4gICAgICAgIH1cblxuICAgICAgICBpZighZHJhZ2dhYmxlTGlzdCB8fCAhZHJhZ2dhYmxlTGlzdC5sZW5ndGgpXG4gICAgICAgICAgICByZXR1cm47XG4gICAgXG4gICAgICAgIHVwZGF0ZVZhcnMoKTtcblxuICAgICAgICBpZiAobGlzdFdpZHRoID49ICBkcmFnZ2FibGVMaXN0Lm9mZnNldCgpLmxlZnQgKyBkcmFnZ2FibGVMaXN0Lm91dGVyV2lkdGgoKSlcbiAgICAgICAge1xuICAgICAgICAgICAgY29uc3RyYWluTGVmdCA9IC1saXN0V2lkdGggKyBkcmFnZ2FibGVMaXN0Lm91dGVyV2lkdGgoKSArIGRpc3RhbnNjZUZyb21SaWdodDtcbiAgICAgICAgfVxuICAgICAgICBlbHNlXG4gICAgICAgIGlmIChsaXN0V2lkdGggPj0gZHJhZ2dhYmxlTGlzdC5vdXRlcldpZHRoKCkpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGNvbnN0cmFpbkxlZnQgPSAtbGlzdFdpZHRoICsgZHJhZ2dhYmxlTGlzdC5vdXRlcldpZHRoKCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZVxuLy8gICAgICAgIGlmIChsaXN0V2lkdGggPCBkcmFnZ2FibGVMaXN0Lm91dGVyV2lkdGgoKSlcbiAgICAgICAge1xuICAgICAgICAgICAgY29uc3RyYWluTGVmdCA9IDA7XG4gICAgICAgIH1cblxuICAgICAgICBpZihqUXVlcnkod2luZG93KS53aWR0aCgpIDwgMTAyNClcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIHNwZCA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGVsc2VcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIHNwZCA9IHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgcGVwID0gcG9zdHNMaXN0LnBlcCh7XG4gICAgICAgICAgICBheGlzOiBcInhcIixcbiAgICAgICAgICAgIGRyYWc6IGRyYWcsXG4gICAgICAgICAgICBlYXNpbmc6IG1vdmluZyxcbiAgICAgICAgICAgIHN0YXJ0OiBzdGFydERyYWcsXG4gICAgICAgICAgICByZXZlcnQ6IGZhbHNlLFxuLy8gICAgICAgICAgcmV2ZXJ0SWY6ICAgZnVuY3Rpb24oKXsgcmV0dXJuIGZhbHNlOyB9LFxuICAgICAgICAgICAgY29uc3RyYWluVG86IFswLCAwLCAwLCBjb25zdHJhaW5MZWZ0XSxcbiAgICAgICAgICAgIHNob3VsZFByZXZlbnREZWZhdWx0OiBzcGRcbiAgICAgICAgICB9KTtcbiAgICAgICBcbiAgICAgICAgXG4gICAgICAgIGluZm8uY3NzKHtcbiAgICAgICAgICAgIFwib3BhY2l0eVwiIDogXCJcIixcbiAgICAgICAgICAgIFwidHJhbnNmb3JtXCIgOiBcIlwiXG4gICAgICAgIH0pOyAgICBcblxuICAgICAgICBwb3N0c0xpc3QuY3NzKHtcbiAgICAgICAgICAgIFwibGVmdFwiIDogXCIwXCIsXG4gICAgICAgICAgICBcInRyYW5zZm9ybVwiOiBcIm1hdHJpeCgxLCAwLCAwLCAxLCAwLCAwKVwiXG4gICAgICAgIH0pOyAgICAgICAgICAgXG4gICAgICAgICAgXG4vKiAgICAgIFxuICAgICAgICBmaWx0ZXJzLmNzcyh7XG4gICAgICAgICAgICBcImxlZnRcIiA6IGZpbHRlcnNTdGFydFBvcyArIFwicHhcIlxuICAgICAgICB9KTsgXG4qLyAgICAgICAgXG4gICAgfSAgIFxuICAgIFxuICAgIGZ1bmN0aW9uIHN0YXJ0RHJhZyhlLCBvYmopXG4gICAge1xuLy8gICAgICAgICAgICBpbmZvID0galF1ZXJ5KFwiLmluZm9cIiwgcHJvbW9ib3hFbGVtZW50KTtcbi8vICAgICAgICAgICAgbGlzdFdpZHRoID0gZ2V0UG9zdHNMaXN0V2lkdGgocG9zdHNMaXN0KTtcbi8vICAgICAgICAgICAgcG9zdHNMaXN0LndpZHRoKGxpc3RXaWR0aCk7XG4gICAgfVxuICAgIFxuICAgIGZ1bmN0aW9uIGRyYWcoZSwgb2JqKVxuICAgIHtcbiAgICAgICAgZHJhZ2dpbmcgPSB0cnVlO1xuICAgICAgICBtb3ZpbmcoZSwgb2JqKTtcbiAgICB9XG5cbiAgICAvL2RyYWcgb3IgZWFzaW5nXG4gICAgZnVuY3Rpb24gbW92aW5nKGUsIG9iailcbiAgICB7XG4gICAgICAgIHZhciBkcmFnID0galF1ZXJ5KG9iai5lbCk7XG4gICAgICAgIHZhciBpbmZvV2lkdGggPSBpbmZvLm91dGVyV2lkdGgoKTtcbiAgICAgICAgdmFyIGRyYWdQb3MgPSBkcmFnLnBvc2l0aW9uKCkubGVmdDtcbiAgICAgICAgdmFyIGluZm9EaXN0YW5jZSA9IE1hdGgubWluKE1hdGguYWJzKGRyYWdQb3MpLCBpbmZvV2lkdGgpO1xuXG4gICAgICAgIGlmIChqUXVlcnkod2luZG93KS5pbm5lcldpZHRoKCkgPj0gMTAyNClcbiAgICAgICAge1xuICAgICAgICAgICAgaW5mby5jc3Moe1xuICAgICAgICAgICAgICAgIFwib3BhY2l0eVwiIDogMS0gaW5mb0Rpc3RhbmNlL2luZm9XaWR0aCxcbiAgICAgICAgICAgICAgICBcInRyYW5zZm9ybVwiIDogXCJzY2FsZShcIiArICgxIC0gaW5mb0Rpc3RhbmNlL2luZm9XaWR0aC8yKSArIFwiKVwiXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlXG4gICAgICAgIHtcbiAgICAgICAgICAgIGluZm8uY3NzKHtcbiAgICAgICAgICAgICAgICBcIm9wYWNpdHlcIiA6IFwiXCIsXG4gICAgICAgICAgICAgICAgXCJ0cmFuc2Zvcm1cIiA6IFwiXCJcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICBmaWx0ZXJzLmNzcyh7XG4gICAgICAgICAgICBcImxlZnRcIiA6IE1hdGgubWF4KChmaWx0ZXJzUG9zLmxlZnQgLSBNYXRoLmFicyhkcmFnUG9zKSksIDApICsgXCJweFwiXG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBcblxuICAgIGZ1bmN0aW9uIGhhbmRsZURyYWdnYWJsZVBvc3RzTGlzdCgpXG4gICAge1xuICAgICAgICBkcmFnZ2luZyA9IGZhbHNlO1xuICAgICAgICB1cGRhdGVWYXJzKCk7XG4gICAgICAgIFxuICAgICAgICBpZiAoIXBvc3RzTGlzdC5sZW5ndGgpXG4gICAgICAgICAgICByZXR1cm47ICBcblxuICAgICAgICB1cGRhdGVHYWxsZXJ5KCk7XG4gICAgICAgIFxuICAgICAgICBqUXVlcnkod2luZG93KS5vbihcInJlc2l6ZVwiLCBmdW5jdGlvbigpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHVwZGF0ZUdhbGxlcnkoKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy9jbGljayB3aGVuIGNsaWNrLCBkcmFnIHdoZW4gZHJhZ1xuICAgICAgICBqUXVlcnkoXCJhXCIsIHBvc3RzTGlzdClcbiAgICAgICAgICAgIC5vbihcIm1vdXNlZG93biBwb2ludGVyZG93biB0b3VjaHN0YXJ0XCIsZnVuY3Rpb24oZSkge1xuICAgICAgICAgICAgICAgIGRyYWdnaW5nID0gZmFsc2U7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLm9uKFwiY2xpY2sgdG91Y2hcIixmdW5jdGlvbihlKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGlmIChkcmFnZ2luZylcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICB9ICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIDtcbiAgICB9XG5cblx0dmFyIGFwcGx5RmlsdGVyID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLnByb21vYm94XCIpLmVhY2goIGZ1bmN0aW9uICgpXG5cdFx0e1xuXHRcdFx0dmFyIHByb21vYm94ID0galF1ZXJ5KHRoaXMpO1xuXHRcdFx0alF1ZXJ5KFwiLmZpbHRlcnMgLnZhbHVlXCIsIHByb21vYm94KS5vbihcImNsaWNrIHRvdWNoIHRvdWNoZW5kXCIsIGZ1bmN0aW9uKGUpXG5cdFx0XHR7XG4gICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICAgICAgalF1ZXJ5KFwiLmZpbHRlcnMgdWxcIiwgcHJvbW9ib3gpLnN0b3AoKS5mYWRlSW4oKTtcblx0XHRcdH0pO1xuXG5cdFx0XHRqUXVlcnkoXCIuZmlsdGVycyB1bCBsaVwiLCBwcm9tb2JveCkub24oXCJjbGljayB0b3VjaCB0b3VjaGVuZFwiLCBmdW5jdGlvbihlKVxuXHRcdFx0e1xuICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgICAgIGpRdWVyeSh0aGlzKS50b2dnbGVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaGFzaCA9IFwiI2ZpbHRydWotXCIgKyBqUXVlcnkodGhpcykuaHRtbCgpO1xuXHRcdFx0XHRhcHBseUZpbHRlclRvQ29udGVudChwcm9tb2JveCk7XG5cdFx0XHR9KTtcblxuICAgICAgICAgICAgLy9qUXVlcnkoZG9jdW1lbnQpLm9uKFwiY2xpY2sgdG91Y2ggdG91Y2hlbmRcIiwgZnVuY3Rpb24oZSlcbiAgICAgICAgICAgIC8ve1xuICAgICAgICAgICAgLy8gICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgIC8vfSlcblxuICAgICAgICAgICAgdmFyIGhpZGVGaWx0ZXIgPSBmdW5jdGlvbigpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgalF1ZXJ5KFwiLmZpbHRlcnMgdWxcIiwgcHJvbW9ib3gpLnN0b3AoKS5mYWRlT3V0KCk7XG5cbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGZpbHRlcnMgIT09IFwidW5kZWZpbmVkXCIgJiYgcGFyc2VJbnQocG9zdHNMaXN0LmNzcyhcImxlZnRcIikpID09IDApXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJzLmNzcyh7XG4gICAgICAgICAgICAgICAgICAgICAgICBcImxlZnRcIiA6IGZpbHRlcnNTdGFydFBvcyArIFwicHhcIlxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cblx0XHRcdGpRdWVyeShcIi5maWx0ZXJzIHVsXCIsIHByb21vYm94KS5vbihcIm1vdXNlbGVhdmVcIiwgaGlkZUZpbHRlcik7XG4gICAgICAgICAgICBqUXVlcnkoZG9jdW1lbnQpLm9uKFwiY2xpY2sgdG91Y2hlbmRcIiwgaGlkZUZpbHRlcilcbiAgICAgICAgfSk7XG5cdH1cblxuXHR2YXIgYXBwbHlGaWx0ZXJUb0NvbnRlbnQgPSBmdW5jdGlvbihwcm9tb2JveClcblx0e1xuXHRcdHZhciBjYXRzID0gW107XG4gICAgICAgIHZhciBwb3N0SXRlbXNMZW5ndGggPSBqUXVlcnkoXCIucG9zdC1pdGVtXCIsIHByb21vYm94KS5sZW5ndGg7XG5cblx0XHRqUXVlcnkoXCIuZmlsdGVycyB1bCBsaVwiLCBwcm9tb2JveCkuZWFjaCggZnVuY3Rpb24oKSB7XG5cdFx0XHRpZihqUXVlcnkodGhpcykuaGFzQ2xhc3MoXCJhY3RpdmVcIikpXG5cdFx0XHRcdGNhdHMucHVzaChqUXVlcnkodGhpcykuYXR0cihcImRhdGEtdHlwZVwiKSk7XG5cdFx0fSk7XG5cblx0XHRpZighY2F0cy5sZW5ndGgpXG4gICAgICAgIHtcblx0XHRcdGpRdWVyeShcIi5wb3N0LWl0ZW1cIiwgcHJvbW9ib3gpLnJlbW92ZUNsYXNzKCBcImhpZGVcIiApO1xuICAgICAgICAgICAgdXBkYXRlR2FsbGVyeSgpO1xuICAgICAgICB9XG5cdFx0ZWxzZVxuXHRcdGpRdWVyeShcIi5wb3N0LWl0ZW1cIiwgcHJvbW9ib3gpLmVhY2goIGZ1bmN0aW9uKGluZGV4LCBlbGVtZW50KVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgY3VycmVudEl0ZW0gPSBqUXVlcnkodGhpcyk7XG4gICAgICAgICAgICBpZiAoIWN1cnJlbnRJdGVtLmhhc0NsYXNzKFwiYWxsLXByb21vdGlvbnNcIikpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdmFyIHBvc3RDYXRzID0gY3VycmVudEl0ZW0uYXR0cihcImRhdGEtdHlwZVwiKS50b1N0cmluZygpLnNwbGl0KFwiLFwiKTtcbiAgICAgICAgICAgICAgICB2YXIgZGlzYWJsZVBvc3QgPSB0cnVlO1xuICAgICAgICAgICAgICAgIGZvcihpIGluIGNhdHMpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBpZihwb3N0Q2F0cy5pbmRleE9mKGNhdHNbaV0udG9TdHJpbmcoKSkgPiAtMSlcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVQb3N0ID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmKGRpc2FibGVQb3N0KSBjdXJyZW50SXRlbS5hZGRDbGFzcyggXCJoaWRlXCIgKTtcbiAgICAgICAgICAgICAgICBlbHNlIGpRdWVyeSh0aGlzKS5yZW1vdmVDbGFzcyggXCJoaWRlXCIgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKGluZGV4ID09IHBvc3RJdGVtc0xlbmd0aCAtMSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB1cGRhdGVHYWxsZXJ5KCk7XG4gICAgICAgICAgICB9ICAgICAgICAgICAgXG5cdFx0fSk7XG4vLyAgICAgICAgdXBkYXRlVmFycygpO1xuLy8gICAgICAgICAgICB1cGRhdGVHYWxsZXJ5KCk7XG5cdH1cblxuXHQvKiBfX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fIHB1YmxpYyBtZXRob2RzICovXG5cblx0dmFyIGluaXQgPSBmdW5jdGlvbigpXG5cdHtcbiAgICAgICAgdXBkYXRlR2FsbGVyeSgpO1xuXHRcdGFwcGx5RmlsdGVyKCk7XG4gICAgICAgIGhhbmRsZURyYWdnYWJsZVBvc3RzTGlzdCgpO1xuXHR9O1xuXG5cdGluaXQoKTtcbn07XG5cbnZhciBwcm9tb2JveDtcbiIsImRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJET01Db250ZW50TG9hZGVkXCIsZnVuY3Rpb24oKXtcblxuICBmdW5jdGlvbiBnZXRDb29raWUobmFtZSkge1xuICAgIHZhciB2YWx1ZSA9IFwiOyBcIiArIGRvY3VtZW50LmNvb2tpZTtcbiAgICB2YXIgcGFydHMgPSB2YWx1ZS5zcGxpdChcIjsgXCIgKyBuYW1lICsgXCI9XCIpO1xuICAgIGlmIChwYXJ0cy5sZW5ndGggPT0gMikgcmV0dXJuIHBhcnRzLnBvcCgpLnNwbGl0KFwiO1wiKS5zaGlmdCgpO1xuICB9XG5cbiAgbGV0IGdpZCA9IGdldENvb2tpZSgnX2dpZCcpO1xuXG4gIGlmKGdpZCkge1xuICAgIGNvbnN0IHVybHMgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnYScpO1xuXG4gICAgaWYoZ2lkLmluY2x1ZGVzKCdHQScpKSB7XG4gICAgICBnaWQgPSBnaWQuc3Vic3RyKDYpO1xuICAgIH1cblxuICAgIGZvcihsZXQgaSA9IDA7IGk8dXJscy5sZW5ndGg7IGkrKykge1xuICAgICAgbGV0IGhyZWYgPSB1cmxzW2ldLmdldEF0dHJpYnV0ZSgnaHJlZicpO1xuICAgICAgaWYoaHJlZi5pbmNsdWRlcygnYWZmcGx1cy5wbC9jJykgJiYgaHJlZi5pbmNsdWRlcygnPycpKSB7XG4gICAgICAgIGhyZWYgKz0gJyZjaWQ9JyArIGdpZDtcbiAgICAgICAgdXJsc1tpXS5zZXRBdHRyaWJ1dGUoJ2hyZWYnLCBocmVmKTtcbiAgICAgIH0gZWxzZSBpZihocmVmLmluY2x1ZGVzKCdhZmZwbHVzLnBsL2MnKSkge1xuICAgICAgICBocmVmICs9ICc/Y2lkPScgKyBnaWQ7XG4gICAgICAgIHVybHNbaV0uc2V0QXR0cmlidXRlKCdocmVmJywgaHJlZik7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbn0pXG4iLCJzZWFyY2hib3ggPSBmdW5jdGlvbiAoKVxue1xuXHR2YXIgY3VycmVudFBhdGggPSBcIlwiO1xuXHR2YXIgY3VycmVudExldmVsID0gMTtcbiAgICBcbiAgICAvLyBnZXQgY2xvc2VzdCB0dSBudW0gbnVtYmVyIGZyb20gYXJyYXlcbiAgICBmdW5jdGlvbiBjbG9zZXN0KG51bSwgYXJyKVxuICAgIHtcbiAgICAgICAgdmFyIGN1cnIgPSBhcnJbMF07XG4gICAgICAgIHZhciBpbmRleCA9IDA7XG4gICAgICAgIHZhciBkaWZmID0gTWF0aC5hYnMobnVtIC0gY3Vycik7XG4gICAgICAgIGZvciAodmFyIHZhbCA9IDA7IHZhbCA8IGFyci5sZW5ndGg7IHZhbCsrKSB7XG4gICAgICAgICAgICB2YXIgbmV3ZGlmZiA9IE1hdGguYWJzKG51bSAtIGFyclt2YWxdKTtcbiAgICAgICAgICAgIGlmIChuZXdkaWZmIDwgZGlmZikge1xuICAgICAgICAgICAgICAgIGRpZmYgPSBuZXdkaWZmO1xuICAgICAgICAgICAgICAgIGN1cnIgPSBhcnJbdmFsXTtcbiAgICAgICAgICAgICAgICBpbmRleCA9IHZhbDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gaW5kZXg7XG4gICAgfSAgICBcblxuXHR2YXIgYXBwbHlTbGlkZXJzID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLnNlYXJjaGJveCAuc2xpZGVyXCIpLmVhY2goIGZ1bmN0aW9uKClcblx0XHR7XG5cdFx0XHR2YXIgb3B0aW9ucyA9IHt9O1xuXHRcdFx0dmFyIHNsaWRlckNvbnRhaW5lciA9IGpRdWVyeSh0aGlzKTtcblxuXHRcdFx0aWYoc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJmaWx0ZXItdHlwZVwiKSA9PSBcImJldHdlZW5cIilcblx0XHRcdHtcblx0XHRcdFx0b3B0aW9ucy5yYW5nZSA9IHRydWU7XG5cdFx0XHRcdG9wdGlvbnMudmFsdWVzID0gW107XG5cdFx0XHRcdGlmKHNsaWRlckNvbnRhaW5lci5kYXRhKFwidmFsdWUtbWluXCIpICYmIHNsaWRlckNvbnRhaW5lci5kYXRhKFwidmFsdWUtbWF4XCIpKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0b3B0aW9ucy52YWx1ZXMucHVzaChzbGlkZXJDb250YWluZXIuZGF0YShcInZhbHVlLW1pblwiKSk7XG5cdFx0XHRcdFx0b3B0aW9ucy52YWx1ZXMucHVzaChzbGlkZXJDb250YWluZXIuZGF0YShcInZhbHVlLW1heFwiKSk7XG5cdFx0XHRcdH07XG5cdFx0XHR9XG5cdFx0XHRlbHNlIGlmKHNsaWRlckNvbnRhaW5lci5kYXRhKFwiZmlsdGVyLXR5cGVcIikgPT0gXCJ0b1wiKVxuXHRcdFx0eyAgICAgICAgICAgICAgICBcblx0XHRcdFx0b3B0aW9ucy5yYW5nZSA9IFwibWluXCI7XG5cdFx0XHRcdGlmKHNsaWRlckNvbnRhaW5lci5kYXRhKFwidmFsdWVcIikpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRvcHRpb25zLnZhbHVlID0gc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJ2YWx1ZVwiKTtcblx0XHRcdFx0fTtcblx0XHRcdH07XG4gICAgICAgICAgICBcblx0XHRcdGlmKHNsaWRlckNvbnRhaW5lci5kYXRhKFwibWluXCIpICE9IG51bGwpXG5cdFx0XHR7XG5cdFx0XHRcdG9wdGlvbnMubWluID0gc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJtaW5cIik7XG5cdFx0XHR9O1xuXG5cdFx0XHRpZihzbGlkZXJDb250YWluZXIuZGF0YShcIm1heFwiKSlcblx0XHRcdHtcblx0XHRcdFx0b3B0aW9ucy5tYXggPSBzbGlkZXJDb250YWluZXIuZGF0YShcIm1heFwiKTtcblx0XHRcdH07XG5cdFx0XHR2YXIgcG9pbnRzVmFsdWVzID0gW107XG5cblx0XHRcdGlmKHNsaWRlckNvbnRhaW5lci5kYXRhKFwic3RlcFwiKSlcblx0XHRcdHtcblxuXHRcdFx0XHR2YXIgc3RlcFZhbCA9IHNsaWRlckNvbnRhaW5lci5kYXRhKFwic3RlcFwiKTtcblx0XHRcdFx0dmFyIGxlbmd0aCA9IE1hdGguY2VpbCgob3B0aW9ucy5tYXgtb3B0aW9ucy5taW4pL3N0ZXBWYWwpO1xuXHRcdFx0XHR2YXIgc3RhcnQgPSBNYXRoLmZsb29yKG9wdGlvbnMubWluL3N0ZXBWYWwpO1xuICAgICAgICAgICAgICAgIFxuXHRcdFx0XHRmb3IodmFyIGkgPSBzdGFydDsgaSA8bGVuZ3RoK3N0YXJ0OyBpKyspXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRpZihpID09IHN0YXJ0ICYmIG9wdGlvbnMubWluIDwgaSpzdGVwVmFsKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHBvaW50c1ZhbHVlcy5wdXNoKG9wdGlvbnMubWluKTtcblx0XHRcdFx0XHR9XG4gICAgICAgICAgICAgICAgICAgIFxuXHRcdFx0XHRcdHBvaW50c1ZhbHVlcy5wdXNoKGkqc3RlcFZhbCk7XG4gICAgICAgICAgICAgICAgICAgIFxuXHRcdFx0XHRcdGlmKGkgPT0gbGVuZ3RoK3N0YXJ0LTEgJiYgb3B0aW9ucy5tYXggPiBpKnN0ZXBWYWwpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0cG9pbnRzVmFsdWVzLnB1c2gob3B0aW9ucy5tYXgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXG5cblx0XHRcdFx0aWYocG9pbnRzVmFsdWVzLmxlbmd0aClcblx0XHRcdFx0e1xuXHRcdFx0XHRcdG9wdGlvbnMubWluID0gMDtcblx0XHRcdFx0XHRvcHRpb25zLm1heCA9IHBvaW50c1ZhbHVlcy5sZW5ndGgtMTtcblxuXHRcdFx0XHRcdGlmKG9wdGlvbnMudmFsdWUpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0b3B0aW9ucy52YWx1ZSA9IHBvaW50c1ZhbHVlcy5pbmRleE9mKG9wdGlvbnMudmFsdWUpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRpZihzbGlkZXJDb250YWluZXIuZGF0YShcInByb2dyZXNzaXZlXCIpID09IHRydWUpXG5cdFx0XHR7XG5cdFx0XHRcdHZhciByYW5nZXNDb25maWcgPVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHRcdFtcblx0XHRcdFx0XHRcdFx0e1wibWluXCI6IDAsIFwibWF4XCI6IDkwMCwgXCJzdGVwXCI6IDEwMH0sXG5cdFx0XHRcdFx0XHRcdHtcIm1pblwiOiAxMDAwLCBcIm1heFwiOiA5MDAwLCBcInN0ZXBcIjogMTAwMH0sXG5cdFx0XHRcdFx0XHRcdHtcIm1pblwiOiAxMDAwMCwgXCJtYXhcIjogNDgwMDAsIFwic3RlcFwiOiAyMDAwfSxcblx0XHRcdFx0XHRcdFx0e1wibWluXCI6IDUwMDAwLCBcIm1heFwiOiA1MDAwMDAwMCwgXCJzdGVwXCI6IDUwMDB9LFxuXHRcdFx0XHRcdFx0XSxcblx0XHRcdFx0XHRpbnZlc3RtZW50czogW1xuXHRcdFx0XHRcdFx0e1wibWluXCI6IDAsIFwibWF4XCI6IDUwMDAsIFwic3RlcFwiOiA1MDB9LFxuXHRcdFx0XHRcdFx0e1wibWluXCI6IDYwMDAsIFwibWF4XCI6IDE5MDAwLCBcInN0ZXBcIjogMTAwMH0sXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogMjAwMDAsIFwibWF4XCI6IDk1MDAwLCBcInN0ZXBcIjogNTAwMH0sXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogMTAwMDAwLCBcIm1heFwiOiA0NzUwMDAsIFwic3RlcFwiOiAyNTAwMH0sXG5cdFx0XHRcdFx0XHR7XCJtaW5cIjogNTAwMDAwLCBcIm1heFwiOiAxMDAwMDAwMCwgXCJzdGVwXCI6IDUwMDAwfSxcblx0XHRcdFx0XHRdLFxuXHRcdFx0XHRcdGludmVzdG1lbnRzTW9udGhzOlxuXHRcdFx0XHRcdFx0W1xuXHRcdFx0XHRcdFx0XHR7XCJtaW5cIjogMSwgXCJtYXhcIjogMjMsIFwic3RlcFwiOiAxfSxcblx0XHRcdFx0XHRcdFx0e1wibWluXCI6IDI0LCBcIm1heFwiOiA2MDAsIFwic3RlcFwiOiAxMn1cblx0XHRcdFx0XHRcdF1cblx0XHRcdFx0fTtcblxuXHRcdFx0XHR2YXIgcmFuZ2VzID0gcmFuZ2VzQ29uZmlnLmRlZmF1bHQ7XG5cblx0XHRcdFx0aWYoc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJwcm9ncmVzc2l2ZS12YWx1ZXNcIikgJiYgcmFuZ2VzQ29uZmlnW3NsaWRlckNvbnRhaW5lci5kYXRhKFwicHJvZ3Jlc3NpdmUtdmFsdWVzXCIpXSlcblx0XHRcdFx0XHRyYW5nZXMgPSByYW5nZXNDb25maWdbc2xpZGVyQ29udGFpbmVyLmRhdGEoXCJwcm9ncmVzc2l2ZS12YWx1ZXNcIildO1xuXG5cdFx0XHRcdGZvcihpPTA7IGkgPCByYW5nZXMubGVuZ3RoOyBpKyspXG5cdFx0XHRcdHtcblx0XHRcdFx0XHR2YXIgY3VycmVudCA9IDA7XG5cdFx0XHRcdFx0aWYob3B0aW9ucy5taW4gPiByYW5nZXNbaV0ubWF4IHx8IG9wdGlvbnMubWF4IDwgcmFuZ2VzW2ldLm1pbilcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRjb250aW51ZTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdHsgXG5cdFx0XHRcdFx0XHRjdXJyZW50ID0gcmFuZ2VzW2ldLm1pbjtcblx0XHRcdFx0XHRcdHdoaWxlKGN1cnJlbnQgPCBvcHRpb25zLm1pbilcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0Y3VycmVudCArPSByYW5nZXNbaV0uc3RlcDtcblx0XHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdFx0aWYob3B0aW9ucy5taW4gPiByYW5nZXNbaV0ubWluICYmIGN1cnJlbnQtcmFuZ2VzW2ldLnN0ZXAgPCBvcHRpb25zLm1pbilcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0cG9pbnRzVmFsdWVzLnB1c2gob3B0aW9ucy5taW4pO1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHR3aGlsZShjdXJyZW50IDw9IG9wdGlvbnMubWF4ICYmIGN1cnJlbnQgPD0gcmFuZ2VzW2ldLm1heClcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0cG9pbnRzVmFsdWVzLnB1c2goY3VycmVudCk7XG5cdFx0XHRcdFx0XHRcdGN1cnJlbnQgKz0gcmFuZ2VzW2ldLnN0ZXA7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblx0XHRcdFx0aWYocG9pbnRzVmFsdWVzLmxlbmd0aClcblx0XHRcdFx0e1xuXHRcdFx0XHRcdG9wdGlvbnMubWluID0gMDtcblx0XHRcdFx0XHRvcHRpb25zLm1heCA9IHBvaW50c1ZhbHVlcy5sZW5ndGgtMTtcbi8vXHRcdFx0XHRcdG9wdGlvbnMubWF4ID0gcG9pbnRzVmFsdWVzW3BvaW50c1ZhbHVlcy5sZW5ndGgtMV07XG5cblx0XHRcdFx0XHRpZihvcHRpb25zLnZhbHVlKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdG9wdGlvbnMudmFsdWUgPSBwb2ludHNWYWx1ZXMuaW5kZXhPZihvcHRpb25zLnZhbHVlKTtcbi8vXHRcdFx0XHRcdFx0b3B0aW9ucy52YWx1ZSA9IHBvaW50c1ZhbHVlc1twb2ludHNWYWx1ZXMuaW5kZXhPZihvcHRpb25zLnZhbHVlKV07XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdG9wdGlvbnMuc2xpZGUgPSBmdW5jdGlvbiggZXZlbnQsIHVpIClcblx0XHRcdHtcblx0XHRcdFx0aWYodWkudmFsdWUpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHR2YXIgZXhWYWwgPSB1aS52YWx1ZTtcblxuXHRcdFx0XHRcdGlmKHBvaW50c1ZhbHVlcy5sZW5ndGgpXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0ZXhWYWwgPSBwb2ludHNWYWx1ZXNbdWkudmFsdWVdO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdHNsaWRlckNvbnRhaW5lci5maW5kKFwiLnZhbHVlXCIpLmh0bWwoIGZvcm1hdE51bWJlcihleFZhbCkgKTtcblx0XHRcdFx0XHRzbGlkZXJDb250YWluZXIucGFyZW50cyhcIltkYXRhLWxldmVsXVwiKS5maXJzdCgpLmZpbmQoXCJpbnB1dFwiKS52YWwoZXhWYWwpO1xuICAgICAgICAgICAgICAgICAgICBzbGlkZXJDb250YWluZXIuZGF0YShcInZhbHVlXCIsIGV4VmFsKTtcbiAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdGlmKHVpLnZhbHVlcylcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHNsaWRlckNvbnRhaW5lci5maW5kKFwiLnZhbHVlXCIpLmh0bWwoIGZvcm1hdE51bWJlcih1aS52YWx1ZXNbMF0pICsgXCIgLSBcIiArIGZvcm1hdE51bWJlcih1aS52YWx1ZXNbMV0pICk7XG5cdFx0XHRcdFx0c2xpZGVyQ29udGFpbmVyLnBhcmVudHMoXCJbZGF0YS1sZXZlbF1cIikuZmlyc3QoKS5maW5kKFwiaW5wdXRbbmFtZV49J21pbi0nXVwiKS52YWwodWkudmFsdWVzWzBdKS50cmlnZ2VyKFwiY2hhbmdlXCIpO1xuXHRcdFx0XHRcdHNsaWRlckNvbnRhaW5lci5wYXJlbnRzKFwiW2RhdGEtbGV2ZWxdXCIpLmZpcnN0KCkuZmluZChcImlucHV0W25hbWVePSdtYXgtJ11cIikudmFsKHVpLnZhbHVlc1sxXSkudHJpZ2dlcihcImNoYW5nZVwiKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRvcHRpb25zLmNoYW5nZSA9IGZ1bmN0aW9uKCBldmVudCwgdWkgKVxuXHRcdFx0e1xuXHRcdFx0XHRzbGlkZXJDb250YWluZXIucGFyZW50cyhcIltkYXRhLWxldmVsXVwiKS5maXJzdCgpLnRyaWdnZXIoXCJpa2FsOmZpbHRlcjpjaGFuZ2VkXCIpO1xuXHRcdFx0fVxuXG5cblx0XHRcdHNsaWRlckNvbnRhaW5lci5jbG9zZXN0KFwiW2RhdGEtbGV2ZWxdXCIpLmZpbmQoXCJpbnB1dFwiKS5vbihcImNoYW5nZVwiLCBmdW5jdGlvbigpXG5cdFx0XHR7XG4gICAgICAgICAgICAgICAgdmFyIGlucHV0ID0galF1ZXJ5KHRoaXMpO1xuXG5cdFx0XHRcdGlmKHBhcnNlRmxvYXQoaW5wdXQuYXR0cihcIm1pblwiKSkgJiYgaW5wdXQudmFsKCkgPCBwYXJzZUZsb2F0KGlucHV0LmF0dHIoXCJtaW5cIikpKVxuXHRcdFx0XHRcdGlucHV0LnZhbChwYXJzZUZsb2F0KGlucHV0LmF0dHIoXCJtaW5cIikpKTtcblxuXHRcdFx0XHRpZihwYXJzZUZsb2F0KGlucHV0LmF0dHIoXCJtYXhcIikpICYmIGlucHV0LnZhbCgpID4gcGFyc2VGbG9hdChpbnB1dC5hdHRyKFwibWF4XCIpKSlcblx0XHRcdFx0XHRpbnB1dC52YWwocGFyc2VGbG9hdChpbnB1dC5hdHRyKFwibWF4XCIpKSk7XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgdmFyIHZhbCA9IGlucHV0LnZhbCgpO1xuICAgICAgICAgICAgICAgIGlmKHBvaW50c1ZhbHVlcy5sZW5ndGgpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICB2YWwgPSBjbG9zZXN0KGlucHV0LnZhbCgpLCBwb2ludHNWYWx1ZXMpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBzbGlkZXJDb250YWluZXIuZmluZChcIi5zbGlkZXItYmFyXCIpLnNsaWRlcihcInZhbHVlXCIsIHZhbCk7XG5cdFx0XHRcdHNsaWRlckNvbnRhaW5lci5maW5kKFwiLnZhbHVlXCIpLmh0bWwoIGZvcm1hdE51bWJlcihpbnB1dC52YWwoKSkgKTtcblx0XHRcdFx0c2xpZGVyQ29udGFpbmVyLmNsb3Nlc3QoXCJbZGF0YS1sZXZlbF1cIikudHJpZ2dlcihcImlrYWw6ZmlsdGVyOmNoYW5nZWRcIik7XG5cdFx0XHR9KTtcbiAgICAgICAgICAgIFxuXHRcdFx0c2xpZGVyQ29udGFpbmVyLmZpbmQoXCIuc2xpZGVyLWJhclwiKS5zbGlkZXIob3B0aW9ucyk7ICAgXG4gICAgICAgICAgICBzbGlkZXJDb250YWluZXIuY2xvc2VzdChcIltkYXRhLWxldmVsXVwiKS5maW5kKFwiaW5wdXRcIikudHJpZ2dlcihcImNoYW5nZVwiKTtcblx0XHR9KTtcbiAgICAgICAgXG5cblx0fVxuXG5cdHZhciBhcHBseUNob2ljZXMgPSBmdW5jdGlvbigpXG5cdHtcblx0XHRqUXVlcnkoXCIuc2VhcmNoYm94IC5jaG9pY2VcIikuZWFjaCggZnVuY3Rpb24oKVxuXHRcdHtcblx0XHRcdHZhciBjaG9pY2VyID0galF1ZXJ5KHRoaXMpO1xuXHRcdFx0alF1ZXJ5KFwiLnZhbHVlXCIsIGNob2ljZXIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oZSlcblx0XHRcdHtcblx0XHRcdFx0alF1ZXJ5KFwiLnRvcHBlZFwiKS5zdG9wKCkuZmFkZU91dCgpLnJlbW92ZUNsYXNzKFwidG9wcGVkXCIpO1xuXHRcdFx0XHRqUXVlcnkoXCJ1bFwiLCBjaG9pY2VyKS5zdG9wKCkuZmFkZUluKCkuYWRkQ2xhc3MoXCJ0b3BwZWRcIik7XG5cdFx0XHR9KVxuXG5cdFx0XHRqUXVlcnkoXCJsaVtkYXRhLXZhbHVlXVwiLCBjaG9pY2VyKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGUpXG5cdFx0XHR7XG5cdFx0XHRcdGUuc3RvcFByb3BhZ2F0aW9uKCk7XG5cdFx0XHRcdHZhciBvcHRpb25WYWx1ZSA9IGpRdWVyeSh0aGlzKS5kYXRhKFwidmFsdWVcIik7XG5cdFx0XHRcdHZhciB0YXJnZXRQYXRoID0galF1ZXJ5KHRoaXMpLmRhdGEoXCJ0YXJnZXQtcGF0aFwiKTtcblx0XHRcdFx0dmFyIHRhcmdldExldmVsID0galF1ZXJ5KHRoaXMpLmRhdGEoXCJ0YXJnZXQtbGV2ZWxcIik7XG5cdFx0XHRcdGpRdWVyeShcInVsXCIsIGNob2ljZXIpLnN0b3AoKS5mYWRlT3V0KCkucmVtb3ZlQ2xhc3MoXCJ0b3BwZWRcIik7XG5cdFx0XHRcdGpRdWVyeShcIi52YWx1ZSA+IHNwYW5cIiwgY2hvaWNlcikuaHRtbChqUXVlcnkodGhpcykuaHRtbCgpKTtcblxuXHRcdFx0XHRjaG9pY2VyLmZpcnN0KCkuZmluZChcImlucHV0XCIpLnZhbChvcHRpb25WYWx1ZSlcblx0XHRcdFx0c2V0UGF0aCh0YXJnZXRQYXRoLCB0YXJnZXRMZXZlbCk7XG5cdFx0XHR9KTtcblx0XHR9KTtcblx0fVxuXG5cdHZhciBhcHBseVRvb2x0aXBzID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLnRvb2x0aXAtdG9nZ2xlclwiKS5lYWNoKCBmdW5jdGlvbigpIHtcblx0XHRcdHZhciB0b2dnbGVyID0galF1ZXJ5KHRoaXMpO1xuXHRcdFx0alF1ZXJ5KFwiLmRvLXRvZ2dsZVwiLCB0b2dnbGVyKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGUpXG5cdFx0XHR7XG5cdFx0XHRcdGpRdWVyeShcIi50b3BwZWRcIikuc3RvcCgpLmZhZGVPdXQoKS5yZW1vdmVDbGFzcyhcInRvcHBlZFwiKTtcblx0XHRcdFx0alF1ZXJ5KFwiLnRvb2x0aXBcIiwgdG9nZ2xlcikuZmFkZUluKCkuYWRkQ2xhc3MoXCJ0b3BwZWRcIik7XG5cblx0XHRcdFx0dG9nZ2xlci5wYXJlbnRzKFwiW2RhdGEtbGV2ZWxdXCIpLmZpcnN0KCkub25lKFwiaWthbDpmaWx0ZXI6Y2hhbmdlZFwiLCBmdW5jdGlvbigpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHRqUXVlcnkoXCIudG9vbHRpcFwiLCB0b2dnbGVyKS5mYWRlT3V0KCkucmVtb3ZlQ2xhc3MoXCJ0b3BwZWRcIik7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSk7XG5cdFx0fSk7XG5cdH1cblxuXHR2YXIgaGFuZGxlU3VibWl0ID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLnN1Ym1pdC1zZWFyY2hcIikub24oXCJjbGlja1wiLCBmdW5jdGlvbigpXG5cdFx0e1xuXHRcdFx0dmFyIHBhdGhQYXJ0cyA9IGdldFBhdGhQYXJ0cygpO1xuXHRcdFx0dmFyIHF1ZXJ5UGFyYW1zID0ge307XG5cdFx0XHRmb3IodmFyIGkgPSAwOyBpPGN1cnJlbnRMZXZlbDsgaSsrKVxuXHRcdFx0e1xuXHRcdFx0XHRqUXVlcnkoXCJbZGF0YS1sZXZlbD0nXCIgKyBwYXJzZUludChpKzEpICtcIiddXCIpLmVhY2goIGZ1bmN0aW9uKClcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHZhciBsdmwgPSBqUXVlcnkodGhpcykuZGF0YShcImxldmVsXCIpO1xuXHRcdFx0XHRcdGlmKGx2bCA9PSBpKzEgJiYgalF1ZXJ5KHRoaXMpLmRhdGEoXCJwYXRoXCIpLmluZGV4T2YocGF0aFBhcnRzW2ldKSA9PSAwKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHZhciBjdXJyRWwgPSBqUXVlcnkodGhpcyk7XG5cdFx0XHRcdFx0XHRqUXVlcnkoXCJpbnB1dFwiLCBjdXJyRWwpLmVhY2goIGZ1bmN0aW9uKClcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0aWYoalF1ZXJ5KHRoaXMpLmRhdGEoXCJwcm9wLW5hbWVcIikpXG5cdFx0XHRcdFx0XHRcdFx0cXVlcnlQYXJhbXNbalF1ZXJ5KHRoaXMpLmRhdGEoXCJwcm9wLW5hbWVcIildID0galF1ZXJ5KHRoaXMpLnZhbCgpO1xuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblx0XHRcdH1cblx0XHRcdHZhciB1cmwgPSBcIlwiO1xuXHRcdFx0aWYocGF0aFBhcnRzWzFdID09IFwicXVpY2tsb2Fuc1wiKSB1cmwgPSBcIi9maW5hbnNvd2FuaWUvY2h3aWxvd2tpL1wiO1xuXHRcdFx0aWYocGF0aFBhcnRzWzFdID09IFwibG9hbnNcIikgdXJsID0gXCIvZmluYW5zb3dhbmllL3BvenljemtpLW5hLXJhdHkvXCI7XG5cdFx0XHRpZihwYXRoUGFydHNbMV0gPT0gXCJjcmVkaXRzXCIpIHVybCA9IFwiL2ZpbmFuc293YW5pZS9rcmVkeXR5LWdvdG93a293ZS9cIjtcblx0XHRcdGlmKHBhdGhQYXJ0c1sxXSA9PSBcImludmVzdG1lbnRzXCIpIHVybCA9IFwiL2xva2F0eS9cIjtcblxuXHRcdFx0aWYocGF0aFBhcnRzWzFdID09IFwiYWNjb3VudHNcIilcblx0XHRcdHtcblx0XHRcdFx0aWYoalF1ZXJ5KFwiaW5wdXRbbmFtZT0nYWNjb3VudHMudHlwZSddXCIpLnZhbCgpID09IFwicGVyc29uYWxcIikgdXJsID0gXCIva29udGEtYmFua293ZS9rb250YS1vc29iaXN0ZS9cIjtcblx0XHRcdFx0aWYoalF1ZXJ5KFwiaW5wdXRbbmFtZT0nYWNjb3VudHMudHlwZSddXCIpLnZhbCgpID09IFwiYnVzaW5lc3NcIikgdXJsID0gXCIva29udGEtYmFua293ZS9rb250YS1maXJtb3dlL1wiO1xuXHRcdFx0XHRpZihqUXVlcnkoXCJpbnB1dFtuYW1lPSdhY2NvdW50cy50eXBlJ11cIikudmFsKCkgPT0gXCJjdXJyZW5jeVwiKSB1cmwgPSBcIi9rb250YS1iYW5rb3dlL2tvbnRhLXdhbHV0b3dlL1wiO1xuXHRcdFx0XHRpZihqUXVlcnkoXCJpbnB1dFtuYW1lPSdhY2NvdW50cy50eXBlJ11cIikudmFsKCkgPT0gXCJzYXZpbmdcIikgdXJsID0gXCIva29udGEtYmFua293ZS9rb250YS1vc3pjemVkbm9zY2lvd2UvXCI7XG5cdFx0XHR9XG5cdFx0XHR1cmwgKz0gXCI/XCI7XG5cbi8vXHRcdFx0Y29uc29sZS5sb2cocXVlcnlQYXJhbXMpO1xuXHRcdFx0alF1ZXJ5LmVhY2gocXVlcnlQYXJhbXMsICBmdW5jdGlvbihrLHYpXG5cdFx0XHR7XG5cdFx0XHRcdHVybCArPSBrICsgXCI9XCIgKyB2ICsgXCImXCI7XG5cdFx0XHR9KVxuXG5cdFx0XHRsb2NhdGlvbi5ocmVmID0gdXJsO1xuXG5cdFx0fSlcblx0fVxuXG5cdHZhciBoYW5kbGVTY3JvbGxEb3duID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0alF1ZXJ5KFwiLnNlYXJjaGJveCAuY3RhIGFcIikub24oXCJjbGlja1wiLCBmdW5jdGlvbihlKVxuXHRcdHtcblx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHRcdHdpbmRvdy5sb2NhdGlvbi5oYXNoID0galF1ZXJ5KHRoaXMpLmF0dHIoXCJocmVmXCIpO1xuXHRcdFx0alF1ZXJ5KFwiYm9keSxodG1sXCIpLmFuaW1hdGUoeyBzY3JvbGxUb3A6IGpRdWVyeShcIi5tYWluLWNvbnRlbnQucGFnZS1jb250ZW50XCIpLm9mZnNldCgpLnRvcCB9KTtcblx0XHR9KVxuXHR9XG5cblx0dmFyIGhhbmRsZVppbmRleCA9IGZ1bmN0aW9uKClcblx0e1xuXHRcdGpRdWVyeShcIi5zZWFyY2hib3ggW2RhdGEtbGV2ZWxdXCIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oZSlcblx0XHR7XG5cdFx0XHRlLnN0b3BQcm9wYWdhdGlvbigpO1xuXG5cdFx0XHRqUXVlcnkoXCIuc2VhcmNoYm94IFtkYXRhLWxldmVsXVwiKS5jc3MoXCJ6LWluZGV4XCIsIFwiXCIpO1xuXHRcdFx0alF1ZXJ5KHRoaXMpLmNzcyhcInotaW5kZXhcIiwgMjApO1xuXHRcdH0pXG5cdH1cblxuXG5cdHZhciBzZXRQYXRoID0gZnVuY3Rpb24odGFyZ2V0UGF0aCwgdGFyZ2V0TGV2ZWwpXG5cdHtcblx0XHRpZih0eXBlb2YodGFyZ2V0UGF0aCkgIT0gXCJ1bmRlZmluZWRcIiAmJiB0eXBlb2YodGFyZ2V0TGV2ZWwpICE9IFwidW5kZWZpbmVkXCIpXG5cdFx0e1xuXHRcdFx0Y3VycmVudFBhdGggPSB0YXJnZXRQYXRoO1xuXHRcdFx0Y3VycmVudExldmVsID0gdGFyZ2V0TGV2ZWw7XG5cblx0XHRcdHZhciBwYXRoUGFydHMgPSBnZXRQYXRoUGFydHMoKTtcblxuXHRcdFx0Zm9yKHZhciBpID0gMDsgaTxjdXJyZW50TGV2ZWw7IGkrKylcblx0XHRcdHtcblx0XHRcdFx0alF1ZXJ5KFwiW2RhdGEtbGV2ZWw9J1wiICsgcGFyc2VJbnQoaSsxKSArXCInXVwiKS5lYWNoKCBmdW5jdGlvbigpXG5cdFx0XHRcdHtcblx0XHRcdFx0XHR2YXIgbHZsID0galF1ZXJ5KHRoaXMpLmRhdGEoXCJsZXZlbFwiKTtcblx0XHRcdFx0XHRpZihsdmwgPT0gaSsxICYmIGpRdWVyeSh0aGlzKS5kYXRhKFwicGF0aFwiKS5pbmRleE9mKHBhdGhQYXJ0c1tpXSkgPT0gMClcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHR2YXIgY3VyckVsID0galF1ZXJ5KHRoaXMpO1xuXHRcdFx0XHRcdFx0c2V0VGltZW91dChmdW5jdGlvbigpXG5cdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdGN1cnJFbC5mYWRlSW4oXCJmYXN0XCIpLmNzcyhcImRpc3BsYXlcIiwgXCJpbmxpbmUtYmxvY2tcIik7XG5cdFx0XHRcdFx0XHR9LCAzNTApXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdGpRdWVyeSh0aGlzKS5mYWRlT3V0KDMwMCk7XG5cblx0XHRcdFx0fSk7XG5cdFx0XHR9XG5cblx0XHR9XG5cdH1cblxuXHR2YXIgZ2V0UGF0aFBhcnRzID0gZnVuY3Rpb24oKVxuXHR7XG5cdFx0dmFyIHNsaWNlZFBhdGggPSBjdXJyZW50UGF0aC5zcGxpdChcIi5cIik7XG5cdFx0dmFyIHBhdGhQYXJ0cyA9IFtcIlwiXTtcblx0XHR2YXIgdG1wUGF0aCA9IFwiXCI7XG5cblx0XHRmb3IodmFyIGsgaW4gc2xpY2VkUGF0aClcblx0XHR7XG5cdFx0XHRpZihrID09IDApIHRtcFBhdGggPSBzbGljZWRQYXRoWzBdO1xuXHRcdFx0ZWxzZSB0bXBQYXRoICs9IFwiLlwiICsgc2xpY2VkUGF0aCBba107XG5cdFx0XHRwYXRoUGFydHMucHVzaCh0bXBQYXRoKTtcblx0XHR9XG5cblx0XHRyZXR1cm4gcGF0aFBhcnRzO1xuXHR9XG5cblx0dmFyIGhhbmRsZUNsaWNrT3V0c2lkZSA9IGZ1bmN0aW9uKClcblx0e1xuXHRcdGpRdWVyeSh3aW5kb3cpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oKSB7XG5cdFx0XHRqUXVlcnkoXCIudG9wcGVkXCIpLnN0b3AoKS5mYWRlT3V0KCkucmVtb3ZlQ2xhc3MoXCJ0b3BwZWRcIik7XG5cdFx0fSk7XG5cdH1cblxuXHQvKiBfX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fX19fIHB1YmxpYyBtZXRob2RzICovXG5cblx0dmFyIGluaXQgPSBmdW5jdGlvbigpXG5cdHtcblx0XHRhcHBseUNob2ljZXMoKTtcblx0XHRhcHBseVRvb2x0aXBzKCk7XG5cdFx0YXBwbHlTbGlkZXJzKCk7XG5cdFx0aGFuZGxlU3VibWl0KCk7XG5cdFx0aGFuZGxlU2Nyb2xsRG93bigpO1xuXHRcdGhhbmRsZVppbmRleCgpO1xuXHRcdGhhbmRsZUNsaWNrT3V0c2lkZSgpO1xuXHR9O1xuXG5cdGluaXQoKTtcbn07XG5cbnZhciBzZWFyY2hib3g7XG4iXX0=
