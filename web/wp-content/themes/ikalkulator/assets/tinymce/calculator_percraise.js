(function() {
  tinymce.create("tinymce.plugins.ik_calc_percraise", {
    init : function(ed, url) {
      ed.addButton("ik_calc_percraise", {
        title : "iKalkulator calculator percent raise",
        cmd : "ik_calc_percraise",
        image : "/wp-content/themes/ikalkulator/assets/icons/grey/percent.svg"
      });
      ed.addCommand("ik_calc_percraise", function() {
        var selected_text = ed.selection.getContent();
        var return_text = "[ik_calc_percraise]";
        ed.execCommand("mceInsertContent", 0, return_text);
      });
    },
    createControl : function(n, cm) {
      return null;
    },
    getInfo : function() {
      return {
        longname : "iKalkulator calculator percent raise",
        author : "asper",
        version : "1"
      };
    }
  });
  tinymce.PluginManager.add("ik_calc_percraise", tinymce.plugins.ik_calc_percraise);
})();
