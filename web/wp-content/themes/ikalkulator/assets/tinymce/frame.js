(function() {
  tinymce.create("tinymce.plugins.ik_frame", {
    init : function(ed, url) {
      ed.addButton("ik_frame", {
        title : "iKalkulator frame",
        cmd : "ik_frame",
        image : "/wp-content/themes/ikalkulator/assets/icons/grey/square.svg"
      });
      ed.addCommand("ik_frame", function() {
        var selected_text = ed.selection.getContent();
        var return_text = "[ik_frame header=\"Sample header\" button_link=\"https://ikalkulator.pl\" button_label=\"Więcej\"]" + selected_text + "[/ik_frame]";
        ed.execCommand("mceInsertContent", 0, return_text);
      });
    },
    createControl : function(n, cm) {
      return null;
    },
    getInfo : function() {
      return {
        longname : "iKalkulator frames",
        author : "asper",
        version : "1"
      };
    }
  });
  tinymce.PluginManager.add("ik_frame", tinymce.plugins.ik_frame);
})();
