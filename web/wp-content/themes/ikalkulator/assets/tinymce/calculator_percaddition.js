(function() {
  tinymce.create("tinymce.plugins.ik_calc_percaddition", {
    init : function(ed, url) {
      ed.addButton("ik_calc_percaddition", {
        title : "iKalkulator calculator addition",
        cmd : "ik_calc_percaddition",
        image : "/wp-content/themes/ikalkulator/assets/icons/grey/percent.svg"
      });
      ed.addCommand("ik_calc_percaddition", function() {
        var selected_text = ed.selection.getContent();
        var return_text = "[ik_calc_percaddition]";
        ed.execCommand("mceInsertContent", 0, return_text);
      });
    },
    createControl : function(n, cm) {
      return null;
    },
    getInfo : function() {
      return {
        longname : "iKalkulator calculator percent addition",
        author : "asper",
        version : "1"
      };
    }
  });
  tinymce.PluginManager.add("ik_calc_percaddition", tinymce.plugins.ik_calc_percaddition);
})();
