(function() {
	tinymce.create("tinymce.plugins.ik_countdown", {
		init : function(ed, url) {
			ed.addButton("ik_countdown", {
				title : "iKalkulator countdown",
				cmd : "ik_countdown",
				image : "/wp-content/themes/ikalkulator/assets/icons/grey/clock.svg"
			});
			ed.addCommand("ik_countdown", function() {
				var return_text = "[ik_countdown start-date=\"12/05/2022 13:03:00\" end-date=\"2025/12/25 13:03:10\" before-msg=\"Promocja rozpoczyna się za:\" while-msg=\"Promocja kończy się za:\" after-msg=\"Promocja zakończyła się\"]";
				ed.execCommand("mceInsertContent", 0, return_text);
			});
		},
		createControl : function(n, cm) {
			return null;
		},
		getInfo : function() {
			return {
				longname : "ikalkulator countdown",
				author : "asper",
				version : "1"
			};
		}
	});
	tinymce.PluginManager.add("ik_countdown", tinymce.plugins.ik_countdown);
})();
