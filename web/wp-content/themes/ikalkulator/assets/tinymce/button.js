(function() {
	tinymce.create("tinymce.plugins.ik_button", {
		init : function(ed, url) {
			ed.addButton("ik_button", {
				title : "iKalkulator button",
				cmd : "ik_button",
				image : "/wp-content/themes/ikalkulator/assets/icons/grey/square.svg"
			});
			ed.addCommand("ik_button", function() {
				var return_text = "[ik_button type=\"white\" text=\"Zobacz\" url=\"https://ikalkulator.pl\"]";
				ed.execCommand("mceInsertContent", 0, return_text);
			});
		},
		createControl : function(n, cm) {
			return null;
		},
		getInfo : function() {
			return {
				longname : "ikalkulator button",
				author : "we code",
				version : "1"
			};
		}
	});
	tinymce.PluginManager.add("ik_button", tinymce.plugins.ik_button);
})();