(function() {
  tinymce.create("tinymce.plugins.ik_calc_percsubtract", {
    init : function(ed, url) {
      ed.addButton("ik_calc_percsubtract", {
        title : "iKalkulator calculator percent subtract",
        cmd : "ik_calc_percsubtract",
        image : "/wp-content/themes/ikalkulator/assets/icons/grey/percent.svg"
      });
      ed.addCommand("ik_calc_percsubtract", function() {
        var selected_text = ed.selection.getContent();
        var return_text = "[ik_calc_percsubtract]";
        ed.execCommand("mceInsertContent", 0, return_text);
      });
    },
    createControl : function(n, cm) {
      return null;
    },
    getInfo : function() {
      return {
        longname : "iKalkulator calculator percent subtract",
        author : "asper",
        version : "1"
      };
    }
  });
  tinymce.PluginManager.add("ik_calc_percsubtract", tinymce.plugins.ik_calc_percsubtract);
})();
