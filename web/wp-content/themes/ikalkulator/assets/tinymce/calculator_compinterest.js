(function() {
  tinymce.create("tinymce.plugins.ik_calc_compinterest", {
    init : function(ed, url) {
      ed.addButton("ik_calc_compinterest", {
        title : "iKalkulator calculator compound interest",
        cmd : "ik_calc_compinterest",
        image : "/wp-content/themes/ikalkulator/assets/icons/grey/dollar-sign.svg"
      });
      ed.addCommand("ik_calc_compinterest", function() {
        var selected_text = ed.selection.getContent();
        var return_text = "[ik_calc_compinterest]";
        ed.execCommand("mceInsertContent", 0, return_text);
      });
    },
    createControl : function(n, cm) {
      return null;
    },
    getInfo : function() {
      return {
        longname : "iKalkulator calculator compound interest",
        author : "asper",
        version : "1"
      };
    }
  });
  tinymce.PluginManager.add("ik_calc_compinterest", tinymce.plugins.ik_calc_compinterest);
})();
