(function() {
  tinymce.create("tinymce.plugins.ik_calc_investment", {
    init : function(ed, url) {
      ed.addButton("ik_calc_investment", {
        title : "iKalkulator calculator investment",
        cmd : "ik_calc_investment",
        image : "/wp-content/themes/ikalkulator/assets/icons/grey/dollar-sign.svg"
      });
      ed.addCommand("ik_calc_investment", function() {
        var selected_text = ed.selection.getContent();
        var return_text = "[ik_calc_investment]";
        ed.execCommand("mceInsertContent", 0, return_text);
      });
    },
    createControl : function(n, cm) {
      return null;
    },
    getInfo : function() {
      return {
        longname : "iKalkulator calculator investment",
        author : "asper",
        version : "1"
      };
    }
  });
  tinymce.PluginManager.add("ik_calc_investment", tinymce.plugins.ik_calc_investment);
})();
