(function() {
  tinymce.create("tinymce.plugins.ik_calc_percof", {
    init : function(ed, url) {
      ed.addButton("ik_calc_percof", {
        title : "iKalkulator calculator percent of",
        cmd : "ik_calc_percof",
        image : "/wp-content/themes/ikalkulator/assets/icons/grey/percent.svg"
      });
      ed.addCommand("ik_calc_percof", function() {
        var selected_text = ed.selection.getContent();
        var return_text = "[ik_calc_percof]";
        ed.execCommand("mceInsertContent", 0, return_text);
      });
    },
    createControl : function(n, cm) {
      return null;
    },
    getInfo : function() {
      return {
        longname : "iKalkulator calculator percent of",
        author : "asper",
        version : "1"
      };
    }
  });
  tinymce.PluginManager.add("ik_calc_percof", tinymce.plugins.ik_calc_percof);
})();
