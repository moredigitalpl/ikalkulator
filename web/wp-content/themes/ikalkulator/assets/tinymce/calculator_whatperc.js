(function() {
  tinymce.create("tinymce.plugins.ik_calc_whatperc", {
    init : function(ed, url) {
      ed.addButton("ik_calc_whatperc", {
        title : "iKalkulator calculator what percent",
        cmd : "ik_calc_whatperc",
        image : "/wp-content/themes/ikalkulator/assets/icons/grey/percent.svg"
      });
      ed.addCommand("ik_calc_whatperc", function() {
        var selected_text = ed.selection.getContent();
        var return_text = "[ik_calc_whatperc]";
        ed.execCommand("mceInsertContent", 0, return_text);
      });
    },
    createControl : function(n, cm) {
      return null;
    },
    getInfo : function() {
      return {
        longname : "iKalkulator calculator what percent",
        author : "asper",
        version : "1"
      };
    }
  });
  tinymce.PluginManager.add("ik_calc_whatperc", tinymce.plugins.ik_calc_whatperc);
})();
