<?php

$minLoanAmount = 0;
$maxLoanAmount = 10000;
$minQuickloanAmount = 99999999999;
$maxQuickloanAmount = 0;
$minQuickloanPeriod = 99999999999;
$maxQuickloanPeriod = 0;
$minLoanAmount = 99999999999;
$maxLoanAmount = 0;
$minLoanPeriod = 99999999999;
$maxLoanPeriod = 0;
$minCreditAmount = 99999999999;
$maxCreditAmount = 0;
$minCreditPeriod = 99999999999;
$maxCreditPeriod = 0;

$args = array(
	'order'            => 'DESC',
	'post_type'        => ['quickloans', 'loans', 'credits'],
	'post_status'      => 'publish',
	'posts_per_page'   => -1,
	'suppress_filters' => true
);

$posts_array = get_posts( $args );

foreach($posts_array as $post)
{
	switch($post->post_type)
	{
		case 'quickloans':
			if(get_field('product_min_amount') < $minQuickloanAmount) $minQuickloanAmount = get_field('product_min_amount');
			if(get_field('product_max_amount') > $maxQuickloanAmount) $maxQuickloanAmount = get_field('product_max_amount');
			if(get_field('product_min_period') < $minQuickloanPeriod) $minQuickloanPeriod = get_field('product_min_period');
			if(get_field('product_max_period') > $maxQuickloanPeriod) $maxQuickloanPeriod = get_field('product_max_period');
			break;

		case 'loans':
			if(get_field('product_min_amount') < $minLoanAmount) $minLoanAmount = get_field('product_min_amount');
			if(get_field('product_max_amount') > $maxLoanAmount) $maxLoanAmount = get_field('product_max_amount');
			if(get_field('product_min_period') < $minLoanPeriod) $minLoanPeriod = get_field('product_min_period');
			if(get_field('product_max_period') > $maxLoanPeriod) $maxLoanPeriod = get_field('product_max_period');
			break;

		case 'credits':
			if(get_field('product_min_amount') < $minCreditAmount) $minCreditAmount = get_field('product_min_amount');
			if(get_field('product_max_amount') > $maxCreditAmount) $maxCreditAmount = get_field('product_max_amount');
			if(get_field('product_min_period') < $minCreditPeriod) $minCreditPeriod = get_field('product_min_period');
			if(get_field('product_max_period') > $maxCreditPeriod) $maxCreditPeriod = get_field('product_max_period');
			break;
	}
}
wp_reset_postdata();

?>
<div class="searchbox">
	<h1>Porównaj produkty finansowe</h1>
	<div class="bg"><img src="/wp-content/themes/ikalkulator/assets/img/home.svg" alt="iKalkulator - wyszukiwarka produktów"/></div>
	<div class="choice" data-path="" data-level="1">
		<h2>Szukam</h2>
		<div class="value">
			<span>Wybierz</span>
			<ul class="select">
				<li data-target-path="quickloans" data-target-level="2" data-value="quickloans">chwilówki</li>
				<li data-target-path="loans" data-target-level="2" data-value="loans">pożyczki ratalnej</li>
				<li data-target-path="credits" data-target-level="2" data-value="credits">kredytu</li>
				<li data-target-path="accounts" data-target-level="2" data-value="accounts">konta</li>
				<li data-target-path="investments" data-target-level="2" data-value="investments">lokaty</li>
			</ul>
		</div>
	</div>
	<div data-path="quickloans" data-level="2">
		<div class="tooltip-toggler">
			<div class="label">na</div>
			<div class="toggler value">
				<span class="do-toggle"></span><span><input type="number" pattern="\d*" name="quickloans.amount" data-prop-name="amount" value="1000" max="<?php echo $maxQuickloanAmount;?>" min="<?php echo $minQuickloanAmount;?>" /></span>
				<div class="tooltip">
					<div class="slider"  data-filter-type="to" data-step="100" data-min="<?php echo $minQuickloanAmount;?>" data-max="<?php echo $maxQuickloanAmount;?>" data-value="1000">
						<div class="label">Wysokość pożyczki</div><div class="value">1000</div>
						<div class="slider-bar"></div>
						<div class="range-min"><span class="range-min-value"><?php echo number_format($minQuickloanAmount, 0, "", " ");?></span>&nbsp;zł</div>
						<div class="range-max"><span class="range-max-value"><?php echo number_format($maxQuickloanAmount, 0, "", " ");?></span>&nbsp;zł</div>
					</div>
				</div>
			</div>
			<div class="text">zł</div>
		</div>
	</div>
	<div data-path="quickloans" data-level="2">
		<div class="tooltip-toggler">
			<div class="label">z okresem spłaty</div>
			<div class="toggler value">
				<span class="do-toggle"></span><span><input type="number" pattern="\d*" name="quickloans.period" data-prop-name="period" value="30"  max="<?php echo $maxQuickloanPeriod;?>" min="<?php echo $minQuickloanPeriod;?>" /></span>

				<div class="tooltip">
					<div class="slider" data-filter-type="to" data-min="<?php echo $minQuickloanPeriod;?>" data-max="<?php echo $maxQuickloanPeriod;?>" data-value="30">
						<div class="label">Długość pożyczki</div><div class="value">30</div>
						<div class="slider-bar"></div>
						<div class="range-min"><span class="range-min-value"><?php echo $minQuickloanPeriod;?></span>&nbsp;dni</div>
						<div class="range-max"><span class="range-max-value"><?php echo $maxQuickloanPeriod;?></span>&nbsp;dni</div>
					</div>
				</div>
			</div>
			<div class="text">dni</div>

		</div>
	</div>
	<div data-path="loans" data-level="2">
		<div class="tooltip-toggler">
			<div class="label">na</div>
			<div class="toggler value">
				<span class="do-toggle"></span><span><input type="number" pattern="\d*" name="loans.amount"  data-prop-name="amount" value="2000" max="<?php echo $maxLoanAmount;?>" min="<?php echo $minLoanAmount;?>" /></span>

				<div class="tooltip">
					<div class="slider" data-filter-type="to" data-step="100" data-min="<?php echo $minLoanAmount;?>" data-max="<?php echo $maxLoanAmount;?>" data-value="2000">
						<div class="label">Wysokość pożyczki</div><div class="value">2 000</div>
						<div class="slider-bar"></div>
						<div class="range-min"><span class="range-min-value"><?php echo number_format($minLoanAmount, 0, "", " ");?></span>&nbsp;zł</div>
						<div class="range-max"><span class="range-max-value"><?php echo number_format($maxLoanAmount, 0, "", " ");?></span>&nbsp;zł</div>
						<input type="hidden" name="amount" />
					</div>
				</div>
			</div>
			<div class="text">zł</div>
		</div>
	</div>
	<div data-path="loans" data-level="2">
		<div class="tooltip-toggler">
			<div class="label">z okresem spłaty</div>
			<div class="toggler value">
				<span class="do-toggle"></span><span><input type="number" pattern="\d*" name="loans.period" data-prop-name="period" value="12"  max="<?php echo $maxLoanPeriod;?>" min="<?php echo $minLoanPeriod;?>"/></span>

				<div class="tooltip">
					<div class="slider" data-filter-type="to" data-min="<?php echo $minLoanPeriod;?>" data-max="<?php echo $maxLoanPeriod;?>" data-value="12">
						<div class="label">Długość pożyczki</div><div class="value">12</div>
						<div class="slider-bar"></div>
						<div class="range-min"><span class="range-min-value"><?php echo $minLoanPeriod;?></span>&nbsp;mies.</div>
						<div class="range-max"><span class="range-max-value"><?php echo $maxLoanPeriod;?></span>&nbsp;mies.</div>
					</div>
				</div>
			</div>
			<div class="text">mies.</div>

		</div>
	</div>
	<div class="choice" data-path="investments" data-level="2">
		<div class="text">na</div>
		<div class="value">
			<span>1-3</span><input type="hidden" name="investments.period" data-prop-name="range" value="1-3"/>
			<ul class="select">
				<li data-value="1-3">1-3</li>
				<li data-value="3-6">4-6</li>
				<li data-value="6-12">6-12</li>
				<li data-value="12-">12+</li>
			</ul>
		</div>
		<div class="text">mies.</div>
	</div>

	<div data-path="credits" data-level="2">
		<div class="tooltip-toggler">
			<div class="label">na</div>
			<div class="toggler value">
				<span class="do-toggle"></span><span><input type="number" pattern="\d*" name="credits.amount" data-prop-name="amount" value="10000"  max="<?php echo $maxCreditAmount;?>" min="<?php echo $minCreditAmount;?>" /></span></span>

				<div class="tooltip">
					<div class="slider" data-filter-type="to" data-progressive="true" data-min="<?php echo $minCreditAmount;?>" data-max="<?php echo $maxCreditAmount;?>" data-value="10000">
						<div class="label">Wysokość kredytu</div><div class="value">10 000</div>
						<div class="slider-bar"></div>
						<div class="range-min"><span class="range-min-value"><?php echo number_format($minCreditAmount, 0, "", " ");?></span>&nbsp;zł</div>
						<div class="range-max"><span class="range-max-value"><?php echo number_format($maxCreditAmount, 0, "", " ");?></span>&nbsp;zł</div>
					</div>
				</div>
			</div>
			<div class="text">zł</div>
		</div>
	</div>
	<div data-path="credits" data-level="2">
		<div class="tooltip-toggler">
			<div class="label">z okresem spłaty</div>
			<div class="toggler value">
				<span class="do-toggle"></span><span><input type="number" pattern="\d*" name="credits.period" data-prop-name="period" value="12" max="<?php echo $maxCreditPeriod;?>" min="<?php echo $minCreditPeriod;?>"/></span>
				<div class="tooltip">
					<div class="slider" data-filter-type="to" data-min="<?php echo $minCreditPeriod;?>" data-max="<?php echo $maxCreditPeriod;?>" data-value="12">
						<div class="label">Długość pożyczki</div><div class="value">12</div>
						<div class="slider-bar"></div>
						<div class="range-min"><span class="range-min-value"><?php echo $minCreditPeriod;?></span>&nbsp;mies.</div>
						<div class="range-max"><span class="range-max-value"><?php echo $maxCreditPeriod;?></span>&nbsp;mies.</div>
					</div>
				</div>
			</div>
			<div class="text">mies.</div>

		</div>
	</div>

	<div class="choice" data-path="accounts" data-level="2">
		<div class="value">
			<span>osobistego</span>
			<input type="hidden" name="accounts.type" value="personal" />
			<ul class="select">
				<li data-value="personal">osobistego</li>
				<li data-value="business">firmowego</li>
				<li data-value="saving">oszczędnościowego</li>
				<li data-value="currency">walutowego</li>
			</ul>
		</div>
		<input type="hidden" name="accounts.type" />

	</div>

	<div class="cta">
		<div class="submit-search btn-inverse">Pokaż</div>
		<a href="#sprawdz-jak-to-dziala">Sprawdź jak to działa</a>
	</div>
</div>
