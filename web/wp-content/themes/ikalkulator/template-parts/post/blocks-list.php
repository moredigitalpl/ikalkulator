<?php
    function create_post_items_block($block_title, $title_link, $posts, $thumbnail = "post-list-big", $classes = "")
    {       
        $html = '<div class="post-items-block">' .
                    '<div class="block-title">';
        $html .=        $title_link ? '<a href="'. $title_link .'">' : "";
        $html .=            '<h2 class="title">'. $block_title . '</h2>';
        $html .=        $title_link ? '</a>' : "";
        $html .=    '</div>'.
                    '<div class="block-content">'.
                        '<div class="posts post-list posts-defined-list col-3">        
                            <div class="posts-list-content">';   
                                $items = "";
                                foreach ($posts as $post_item)
                                {
                                    $items .= create_post_list_item($post_item, "post-list-medium", "", "h3");
                                }                                
        $html .=                $items;
        $html .=            '</div>'.
                        '</div>'.
                    '</div>'.
                '</div>';
        
        return $html;        
    }
 

    $posts_categories = get_query_var("posts_categories_id");
    $posts_categories = $posts_categories ? $posts_categories : array();
    
    $posts_tags = get_query_var("posts_tags_id");
    $posts_tags = $posts_tags ? $posts_tags : array();
    
    $excluded_posts = get_query_var("excluded_posts");
    $excluded_posts = $excluded_posts ? $excluded_posts : array();

    $excluded_posts_ids = array();
    
    foreach ($excluded_posts as $ex_post)
    {
        $excluded_posts_ids[] = $ex_post->ID;
    }
    
    $posts_blockslist = "";
    
    foreach ($posts_categories as $cat_id)
    {
        $cat_title = get_cat_name($cat_id);
        $cat_link = get_category_link($cat_id);
        
        $args = array(
           "category__in"	   => $cat_id,
           "post__not_in"      => $excluded_posts_ids,
           "posts_per_page"    => 3,
        );          
        
        $category_posts = get_posts($args);
        $post_ids_to_exclude = array();
        
        foreach ($category_posts as $post_item)
        {
            $post_ids_to_exclude[] = $post_item->ID;
        }
        
        $excluded_posts_ids = array_merge($excluded_posts_ids, $post_ids_to_exclude);
        
        if (count($category_posts))
        {
            $posts_blockslist .= create_post_items_block($cat_title, $cat_link, $category_posts);
        }        
    }
    

    foreach ($posts_tags as $tag_id)
    {
        $tag = get_tag($tag_id);
        $tag_title = "#".$tag->name;
        $tag_link = get_tag_link($tag->term_id);
       
        $args = array(
           "tag__in"            => $tag_id,
           "post__not_in"      => $excluded_posts_ids,
           "posts_per_page"    => 3,
        );          
        
        $tag_posts = get_posts($args);
        $post_ids_to_exclude = array();
        
        foreach ($tag_posts as $post_item)
        {
            $post_ids_to_exclude[] = $post_item->ID;
        }  
        
        $excluded_posts_ids = array_merge($excluded_posts_ids, $post_ids_to_exclude);
        
        if (count($tag_posts))
        {
            $posts_blockslist .= create_post_items_block($tag_title, $tag_link, $tag_posts);
        }
    }
?>
<div class="posts-blocks-list"> 
    <?php echo $posts_blockslist; ?>
</div>

