<?php
    $popular_posts = get_most_popular_posts();

function create_popular_post_item($post, $thumbnail = "post-list-small")
{
    $post_meta = get_post_custom_meta($post, $thumbnail);

    $result =  '<div class="post-item">
                    <div class="item-content">
                        <a href="'. $post_meta["url"] .'">
                            <div class="image">
                                    <img src="' . $post_meta["featured_image"] . '" alt="' . $post_meta["title"] . '"/>
                            </div>
                            <h3 class="title">'. $post_meta["title"] .'</h3>
                        </a>
                    </div>
                </div>';

    return $result;
}
?>
<aside class="aside-panel scroll-handle">
    <div class="search-block">
        <?php
        echo create_search_bar();
        ?>
    </div>
    <?php get_sidebar("right"); ?>
    <?php
    if ($popular_posts)
    { ?>
    <div class="popular-posts-list">
        <h4 class="block-title">Najczęściej czytane</h4>
            <div class="posts-list-content">
            <?php
                $index = 0;
                foreach($popular_posts as $post)
                {
                    echo create_popular_post_item($post, "post-list-small");

                    if (++$index == 5)
                    {
                        break;
                    }
                }
            ?>
            </div>
    </div>
    <?php
    } ?>
</aside>
