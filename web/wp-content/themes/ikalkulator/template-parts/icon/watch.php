<div class="watch-icon svg-icon">
    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         width="16px" height="24px" viewBox="4 0 16 24" enable-background="new 4 0 16 24" xml:space="preserve">
        <circle fill="none" stroke="#84929C" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" cx="12" cy="12" r="7"/>
        <polyline fill="none" stroke="#84929C" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="12,9 12,12 
                  13.5,13.5 "/>
        <path fill="none" stroke="#84929C" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M16.51,17.35l-0.35,3.83
              c-0.094,1.034-0.963,1.824-2,1.82H9.83c-1.038,0.004-1.907-0.786-2-1.82l-0.35-3.83 M7.49,6.65l0.35-3.83
              C7.933,1.79,8.796,1.001,9.83,1h4.35c1.038-0.004,1.906,0.786,2,1.82l0.35,3.83"/>
    </svg>
</div>