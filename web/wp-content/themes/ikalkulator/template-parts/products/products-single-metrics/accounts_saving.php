<?php
$percentage = "";
if( have_rows('product_global_interests', $post) ):
	while( have_rows('product_global_interests') ) : the_row();
		$percentageMin = 100;
		$percentageMax = 0;

		if($percentageMax < get_sub_field('product_investment_interest'))
			$percentageMax = get_sub_field('product_investment_interest');

		if($percentageMin > get_sub_field('product_investment_interest'))
			$percentageMin = get_sub_field('product_investment_interest');
	endwhile;
endif;

?>
<div class="product-info">
	<div class="labels n3">
		<div class="kind"></div>
		<div class="amount">Kwota</div>
		<div class="interest">Oprocentowanie</div>
	</div>
	<div class="data n3">
		<div class="kind"><?php if( get_field('product_logo')['url']) { ?>
				<img src="<?php echo get_field('product_logo')['url']; ?>" itemprop="image" alt="Logo <?php echo( get_field('product_account_name')); ?>" />
				<?php
					$schemaData["logo"] = get_field('product_logo')['url'];
				}
			?>

			<div>
				<?php echo( get_field('product_account_name')); ?>
			</div>
		</div>
		<div class="amount"><?php echo( number_format(get_field('product_min_amount'), 0, "", "&nbsp;")); ?> - <?php echo( number_format(get_field('product_max_amount'), 0, "", "&nbsp;")); ?> zł</div>
		<div class="interest data-row">
			<?php

			if($percentageMin != $percentageMax)
			{
				$schemaData["annualPercentageRate"] = ["@type" => "QuantitativeValue"];
				$schemaData["annualPercentageRate"]["minValue"] = $percentageMin;
				$schemaData["annualPercentageRate"]["maxValue"] = $percentageMax;

				echo $percentageMin . " - " . $percentageMax .  "&nbsp;%";
			}
			else
			{
				echo $percentageMax .  "&nbsp;%";
				$schemaData["annualPercentageRate"] = $percentageMax;
			}

			?><br/>
		<small><?php echo( get_field('product_validity_period')); ?></small><br/>
			<?php
			if( get_field('product_rules'))
			{
				echo '<div class="tooltip"><div class="content">'.get_field('product_rules').'<span class="close"></span></div><span class="show"></span></div>';
			}
			?>
		</div>
	</div>
</div>