<?php
	$schemaData["loanType"] = "Pożyczka długoterminowa na raty";
?>
<div class="product-info">
	<div class="labels n4">
		<div class="kind"></div>
		<div class="period">Okres trwania</div>
		<div class="amount">Kwota pożyczki</div>
		<div class="nobik">Bez BIK</div>
	</div>
	<div class="data n4">
		<div class="kind"><?php if( get_field('product_logo')['url']) { ?>
				<img src="<?php echo get_field('product_logo')['url']; ?>" itemprop="image" alt="Logo <?php echo( get_field('product_name')); ?>" />
				<?php
				$schemaData["logo"] = get_field('product_logo')['url'];
			}
				?>
				<?php $schemaData["loanTerm"] = [
				"@type" => "QuantitativeValue",
				"value" => get_field('product_max_period'),
			    "unitCode" => "MON"
			]; ?>
			</div>
		<div class="period"><?php echo( get_field('product_min_period')); ?> - <?php echo( get_field('product_max_period')); ?> mies.</div>

		<?php
			$schemaData["amount"][] = [
				"@type" => "MonetaryAmount",
				"name" => "max. kwota pożyczki",
				"value" => get_field('product_max_amount'),
				"currency" => "PLN"
			];
		?>
		<div class="amount"><?php echo( number_format(get_field('product_min_amount'), 0, "", "&nbsp;")); ?> - <?php echo( number_format(get_field('product_max_amount'), 0, "", "&nbsp;")); ?> zł</div>
		<div class="nobik"><?php echo get_field('product_no_bik') ? "TAK" : "NIE"; ?></div>
	</div>
</div>


<?php

	if( get_field('product_cost_rules') )
	{
		$schemaData["feesAndCommissionsSpecification"] = get_field('product_cost_rules');
	}

	if(get_field("product_min_rrso") != null || get_field("product_max_rrso") != null)
	{
		$schemaData["annualPercentageRate"] = [
			"@type" => "QuantitativeValue",
			"name" => "Oprocentowanie zmienne",

		];

		if(get_field("product_min_rrso"))
		{
			$schemaData["annualPercentageRate"]["minValue"] = get_field("product_min_rrso");
		}

		if(get_field("product_max_rrso"))
		{
			$schemaData["annualPercentageRate"]["maxValue"] = get_field("product_max_rrso");
		}
	}
