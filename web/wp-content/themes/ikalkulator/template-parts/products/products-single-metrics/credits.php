<?php
	$schemaData["loanType"] = "Kredyt gotówkowy";
?>
<div class="product-info">
	<div class="labels n3">
		<div class="kind"></div>
		<div class="period">Okres trwania</div>
		<div class="amount">Kwota pożyczki</div>
	</div>
	<div class="data n3">
		<div class="kind"><?php if( get_field('product_logo')['url']) { ?>
				<img src="<?php echo get_field('product_logo')['url']; ?>" itemprop="image" alt="Logo <?php echo( get_field('product_name')); ?>" />
				<?php
					$schemaData["logo"] = get_field('product_logo')['url'];
				}
			?>

			<div><?php echo( get_field('product_name')); ?></div></div>
		<?php $schemaData["loanTerm"] = [
			"@type" => "QuantitativeValue",
			"minValue" => get_field('product_min_period'),
			"maxValue" => get_field('product_max_period'),
		    "unitCode" => "MON"
		]; ?>
		<div class="period"><?php echo( get_field('product_min_period')); ?> - <span><?php echo( get_field('product_max_period')); ?></span> <span>mies.</span></div>

		<?php
		$schemaData["amount"][] = [
			"@type" => "MonetaryAmount",
			"name" => "min. kwota kredytu",
			"value" => get_field('product_min_amount'),
			"currency" => "PLN"
		];
		[
			"@type" => "MonetaryAmount",
			"name" => "max. kwota kredytu",
			"value" => get_field('product_max_amount'),
			"currency" => "PLN"
		];
		?>
		<div class="amount"><?php echo( number_format(get_field('product_min_amount'), 0, "", "&nbsp;")); ?> - <?php echo( number_format(get_field('product_max_amount'), 0, "", "&nbsp;")); ?> zł</div>
	</div>
</div>

<?php

if (get_field("product_min_rrso") != null || get_field("product_max_rrso") != null)
{
	$schemaData["annualPercentageRate"] = [
			"@type" => "QuantitativeValue",
			"name" => "Oprocentowanie zmienne",
		];

	if (get_field("product_min_rrso"))
	{
		$schemaData["annualPercentageRate"]["minValue"] = get_field("product_min_rrso");
	}

	if (get_field("product_max_rrso"))
	{
		$schemaData["annualPercentageRate"]["maxValue"] = get_field("product_max_rrso");
	}
}

