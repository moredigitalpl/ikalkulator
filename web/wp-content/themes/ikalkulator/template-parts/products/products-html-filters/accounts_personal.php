

<div class="filters container">
	<form>
		<h2>Dopasuj kryteria</h2>

		<div class="options">
			<div class="option">
				<div class="checkbox">
					<input type="checkbox" name="freeatms" id="freeatms" value="true" <?php if($freeatms) echo 'checked'; ?>/>
					<label for="freeatms">
						Darmowe bankomaty
					</label>
				</div>
			</div>
			<div class="option">
				<div class="checkbox">
					<input type="checkbox" name="freeaccount" id="freeaccount" value="true" <?php if($freeaccount) echo 'checked'; ?>/>
					<label for="freeaccount">
						Darmowe konto
					</label>
				</div>
			</div>
			<div class="option">
				<div class="checkbox">
					<input type="checkbox" name="freecard" id="freecard" value="true" <?php if($freecard) echo 'checked'; ?>/>
					<label for="freecard">
						Darmowa karta
					</label>
				</div>
			</div>
			<div class="option">
				<div class="checkbox">
					<input type="checkbox" name="promoted" id="promoted" value="true" <?php if($promoted) echo 'checked'; ?>/>
					<label for="promoted">
						Promocja
					</label>
				</div>
			</div>
			<div class="option">
				<div class="checkbox">
					<input type="checkbox" name="premium" id="premium" value="true" <?php if($premium) echo 'checked'; ?>/>
					<label for="premium">
						Konto premium
					</label>
				</div>
			</div>
		</div>
	</form>
</div>
