

<div class="filters container">
	<form>
		<h2>Dopasuj kryteria</h2>

		<div class="options hidden">
			<div class="option">
				<div class="checkbox">
					<input type="checkbox" name="nonewaccount" id="nonewaccount" value="true" <?php if($nonewaccount) echo 'checked'; ?>/>
					<label for="nonewaccount">
						Bez zakładania konta osobistego
					</label>
				</div>
			</div>
			<div class="option">
				<div class="checkbox">
				<?php
					foreach($currenciesAvailable as $ca)
					{
						echo '<div class="checkbox"><input type="radio" name="currency" id="currency'.$ca.'" value="'.$ca;

						if($ca == $currency) echo ' checked /> ';
						else echo '"/>

										<label for="currency'.$ca.'">'.$ca.'</label>
						</div>';
					}
				?>
				</div>
			</div>
		</div>
	</form>
</div>
