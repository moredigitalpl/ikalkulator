<div class="filters container">
	<form>
		<h2>Dopasuj kryteria </h2>

		<div class="sliders visible">
			<div class="slider" data-progressive="true" data-filter-name="amount" data-filter-type="to" data-min="<?php echo $minAmount;?>" data-max="<?php echo $maxAmount;?>" data-value="<?php echo $amount;?>">
				<h3>Wysokość pożyczki <div class="value"><?php echo number_format($amount, 0, ".", " "); ?></div></h3>

				<div class="slider-bar"></div>
				<div class="range-min"><span class="range-min-value"><?php echo number_format($minAmount, 0, ".", " "); ?></span> zł</div>
				<div class="range-max"><span class="range-max-value"><?php echo number_format($maxAmount, 0, ".", " "); ?></span> zł</div>
				<input type="hidden" name="amount" />
			</div>

			<div class="slider" data-filter-name="period" data-filter-type="to" data-min="<?php echo $minMonths;?>" data-max="<?php echo $maxMonths;?>" data-value="<?php echo $period;?>">
				<h3>Okres spłaty <div class="value"><?php echo $period; ?></div></h3>

				<div class="slider-bar"></div>
				<div class="range-min"><span class="range-min-value"><?php echo $minMonths;?></span> mies.</div>
				<div class="range-max"><span class="range-max-value"><?php echo $maxMonths;?></span> mies.</div>
				<input type="hidden" name="period" />
			</div>
		</div>
	</form>
</div>
