<?php

$amount = $_GET["amount"] ? $_GET["amount"] : 5000;
$minperiod = $_GET["min-period"] ? $_GET["min-period"] : 1;
$maxperiod = $_GET["max-period"] ? $_GET["max-period"] : 12;
$nonewaccount = $_GET["nonewaccount"] ? $_GET["nonewaccount"] : null;
$nonewresources = $_GET["nonewresources"] ? $_GET["nonewresources"] : null;
$nonewclient = $_GET["nonewclient"] ? $_GET["nonewclient"] : null;

$minAmount = 999999999;
$maxAmount = 0;
$minMonths = 999999999;
$maxMonths = 0;


$args = array(
	'post_type'        => 'investments',
	'post_status'      => 'publish',
	'posts_per_page'   => -1,
	'suppress_filters' => true,
	'meta_query'	=> array(
		'relation'		=> 'OR',
		array(
			'key'	  	=> 'product_inactive',
			'compare' => 'NOT EXISTS'
		),
		array(
			'key'	  	=> 'product_inactive',
			'value'	  	=> '1',
			'compare' 	=> '!=',
		),
	),
);
$posts_array = get_posts( $args );

foreach ( $posts_array as $post ) : setup_postdata( $post );

	if(get_field('product_max_amount') > $maxAmount) $maxAmount = get_field('product_max_amount');
	if(get_field('product_min_amount') < $minAmount) $minAmount = get_field('product_min_amount');
	if(get_field('product_min_period') < $minMonths) $minMonths = get_field('product_min_period');
	if(get_field('product_max_period') > $maxMonths) $maxMonths = get_field('product_max_period');
endforeach;
$maxMonths = ceil($maxMonths/30);
$minMonths = floor($minMonths/30);

if(!$minperiod) $minperiod = 1;
if(!$maxperiod) $maxperiod = 12;

if(isset($_GET['range']))
{
	$ranges = explode("-", $_GET['range']);

	$ranges[0] = intval($ranges[0]);
	$ranges[1] = intval($ranges[1]);
	if(is_integer($ranges[0]))
	{
		if($ranges[0] >= $minMonths)
		{
			if($ranges[0] <= $maxMonths)
				$minperiod = $ranges[0];
			else
				$minperiod = $maxMonths;
		}

		if($ranges[1] && $ranges[1] <= $maxMonths)
		{
			if($ranges[1] >= $minMonths)
				$maxperiod = $ranges[1];
			else
				$maxperiod = $minMonths;
		}
		else if(!$ranges[1])
		{
			$maxperiod = $maxMonths;
		}
	}
}