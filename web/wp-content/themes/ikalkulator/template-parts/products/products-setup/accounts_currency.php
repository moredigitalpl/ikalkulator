<?php

$freeatms = $_GET["freeatms"] ? $_GET["freeatms"] : null;
$freeaccount = $_GET["freeaccount"] ? $_GET["freeaccount"] : null;
$freecard = $_GET["freecard"] ? $_GET["freecard"] : null;
$nonewaccount = $_GET["nonewaccount"] ? $_GET["nonewaccount"] : null;
$currency = $_GET["currency"] ? $_GET["currency"] : null;

$args = array(
	'post_type'        => 'accounts_currency',
	'post_status'      => 'publish',
	'posts_per_page'   => -1,
	'suppress_filters' => true,
	'meta_query'	=> array(
		'relation'		=> 'OR',
		array(
			'key'	  	=> 'product_inactive',
			'compare' => 'NOT EXISTS'
		),
		array(
			'key'	  	=> 'product_inactive',
			'value'	  	=> '1',
			'compare' 	=> '!=',
		),
	),
);
$posts_array = get_posts( $args );

$currenciesAvailable = [];
foreach ( $posts_array as $post ) : setup_postdata( $post );
	if( have_rows('product_currencies', $post) ):
		while( have_rows('product_currencies') ) : the_row();
			if(!in_array(get_sub_field("product_currency_symbol"), $currenciesAvailable))
				$currenciesAvailable []= get_sub_field("product_currency_symbol");
		endwhile;
	endif;

endforeach;
