<?php
$grid_items = get_query_var("grid");

if( $grid )
{ ?>     
    <div class="tiles-grid">    
        <div class="grid-content">
        <?php foreach ($grid_items as $item)
        { 
            $item_title = $item["grid_item_title"];         
            $item_image = $item["grid_item_image"];         
            $item_url = $item["grid_item_url"];

            ?>    
            <div class="grid-item">
                <a class="item-content" href="<?php echo $item_url; ?>">
                    <h2 class="title"><?php echo $item_title; ?></h2>
                    <div class="image">
                        <span style="background-image: url(<?php echo $item_image; ?>);"></span>
                    </div>
                    <span class="read-more">Zobacz więcej</span>
                </a>
            </div>
        <?php 
        }?>  
        </div>     
    </div>
<?php
}?>