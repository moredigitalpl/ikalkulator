<div class="filters">
    <div class="filters-content">
        <h2 class="title">Kategorie</h2>
        <div class="content">
            <div class="items-list">
                <div class="category active" data-filter="all">Wszystkie</div>
                <?php 
                    $categories = get_post_type_categories("company", "company_type");
                    echo create_categories_labels_list($categories, "", false, $item_el = "div", "data-filter");
                ?>
            </div>
        </div>
    </div>
</div>
