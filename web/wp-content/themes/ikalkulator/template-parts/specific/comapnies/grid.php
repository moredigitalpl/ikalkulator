<?php
$args = array(
    "post_type"			=> "company",
    "posts_per_page"	=> -1,
);

$grid_items = get_posts($args);

?> 
<div class="tiles-grid">    
    <div class="grid-content">
    <?php foreach ($grid_items as $item)
    { 
        $post_meta =  get_post_custom_meta($item, "full");
        $item_title = $post_meta["title"];         
        $item_image = $post_meta["featured_image"];         
        $item_url = $post_meta["url"];
        
        $item_categories = get_post_categories($item, "company_type");
        $categories = categories_comma_separated($item_categories, null, true);
        
        ?>    
        <div class="grid-item" data-item-categories="<?php echo $categories; ?>">
            <div class="content-wrap">
                <a class="item-content" href="<?php echo $item_url; ?>">
                    <h2 class="title"><?php echo $item_title; ?></h2>
                    <div class="image">
                        <span style="background-image: url(<?php echo $item_image; ?>);"></span>
                    </div>
                    <span class="read-more">Zobacz więcej</span>
                </a>
            </div>
        </div>
    <?php 
    }?>  
    </div>     
</div>

