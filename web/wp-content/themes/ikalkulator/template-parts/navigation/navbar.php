<?php
    $from = "";
    if (is_category())
    {
        $taxonomy = get_current_taxonomy();
        $from  = $taxonomy->taxonomy ."_". $taxonomy->term_id;
    }


    if (get_field("navbar_visibility", $from) )
    {
        $selected_menu = get_field("navbar_menu", $from);
        $menu = wp_get_nav_menu_object($selected_menu);
    ?>
        <nav class="page-navbar">
            <div class="content">
                <?php
                if ($menu)
                {
                    wp_nav_menu( array(
                        "menu"				=> $menu->slug,
                        "container_class"	=> "",
                        "container"			=> "",
                        "link_before"       => "<span>",
                        "link_after"		=> "</span>",
                        "items_wrap"		=> '<ul class="%2$s">%3$s</ul>',
                    ));
                } ?>
            </div>
        </nav>
    <?php
    }
