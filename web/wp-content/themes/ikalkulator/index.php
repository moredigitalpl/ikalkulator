<?php get_header(); ?>
<main class="page-main" role="main">
    <div class="main-content page-content">
		<?php the_content(); ?>
	</div>
</main>
<?php get_footer(); ?>