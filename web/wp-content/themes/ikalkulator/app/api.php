<?php

	function api_rewrite() {
		add_rewrite_rule('api/([^/]*)/?', 'wp-content/themes/ikalkulator/inc/products-$matches[1].php');
	}
	add_action('init', 'api_rewrite');

	add_action( 'rest_api_init', function () {
		register_rest_route( 'ikal/v1', '/post/vote', array(
			'methods' => 'POST',
			'callback' => 'ikal_post_vote',
		));
	});

	function ikal_post_vote(WP_REST_Request $request)
	{
		$postId = $request['post'];
		$dir = $request['dir'];

		if(get_post($postId)->post_type == "post")
		{
			if($dir == "up")
			{
				$metaName = 'thumbsup';
			}
			else if($dir == "down")
			{
				$metaName = 'thumbsdown';
			}
			else
			{
				return;
			}            
            
			$votes = (int)get_post_meta($postId, $metaName, true);
			if($votes) $votes++;
			else $votes = 1;
			update_post_meta($postId, $metaName, (int)$votes);
            
            $thumbsup = (int)get_post_meta($postId, 'thumbsup', true);
            $thumbsdown = (int)get_post_meta($postId, 'thumbsdown', true);
            
            if (!$thumbsup) $thumbsup = 0;
            if (!$thumbsdown) $thumbsdown = 0;
            
            $thumbsfinal = $thumbsup - $thumbsdown;
            update_post_meta($postId, "thumbsfinal", (int)$thumbsfinal);
            
			return $votes;
            
            
		}
	}