<?php
// change this to true if you want external links to open in a new window (target="_blank")
$txfx_iel_use_target_blank = false;

function wp_get_domain_name_from_uri($uri){
	preg_match("/^(http:\/\/)?([^\/]+)/i", $uri, $matches);
	$host = $matches[2];
	preg_match("/[^\.\/]+\.[^\.\/]+$/", $host, $matches);
	return $matches[0];	   
}


function parse_external_links($matches){
	if ( wp_get_domain_name_from_uri($matches[3]) != wp_get_domain_name_from_uri($_SERVER["HTTP_HOST"]) ){
		return '<a href="' . $matches[2] . '//' . $matches[3] . '"' . $matches[1] . $matches[4] . ' class="external-link">' . $matches[5] . '</a>';	 
	} else {
		return '<a href="' . $matches[2] . '//' . $matches[3] . '"' . $matches[1] . $matches[4] . '>' . $matches[5] . '</a>';
	}
}
	

function wp_external_links($text) {
	global $txfx_iel_use_target_blank;
	$pattern = '/<a (.*?)href="(.*?)\/\/(.*?)"(.*?)>(.*?)<\/a>/i';
	$text = preg_replace_callback($pattern,'parse_external_links',$text);

	$pattern2 = '/<a (.*?) class="external-link"(.*?)>(.*?)<img (.*?)<\/a>/i';
	if ( $txfx_iel_use_target_blank )
		$text = preg_replace($pattern2, '<a $1 $2 target="_blank">$3<img $4</a>', $text);
	else
		$text = preg_replace($pattern2, '<a $1 $2>$3<img $4</a>', $text);
	return $text;
}

// filters have high priority to make sure that any markup plugins like Textile or Markdown have already created the HTML links
add_filter('the_content', 'wp_external_links', 999);
add_filter('the_excerpt', 'wp_external_links', 999);

// delete this one if you don't want it run on comments
//add_filter('comment_text', 'wp_external_links', 999);
?>