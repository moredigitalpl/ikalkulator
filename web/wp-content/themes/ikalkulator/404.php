<?php get_header(); ?>
<main class="page-main error-404">
    <div class="main-content page-content">
        <h2 class="title">404 nie znaleziono strony</h2>
        <p class="info">
            Wróć do <a href="<?php echo esc_url(home_url("/")); ?>"><?php _e( "strony głownej", "wcd" ); ?></a>
        </p>
	</div>
</main>
<?php get_footer(); ?>
