<?php
/* Template Name: Grid */ 

$grid = get_field("grid");
set_query_var("grid", $grid);
?>
<?php get_header(); ?>
<main class="page-main grid" role="main">
	<?php get_template_part( "template-parts/head/head" ); ?>
    <div class="main-content page-content">
        <?php get_template_part( "template-parts/specific/grid" ); ?>
		<?php the_content(); ?>
	</div>
</main>
<?php get_footer(); ?>
