companies = function ()
{   
	function handleFixedFilter()
	{
        var pageHead =  jQuery(".page-head");
        var pageContent =  jQuery(".page-content");
        var filter =  jQuery(".filters");
        
        if (!filter.length)
            return;

		var switchAt = pageHead.offset().top + pageHead.outerHeight();

    
        if (switchAt < jQuery(window).scrollTop() && window.innerWidth >= 768)
        {
            if (!filter.hasClass("fixed"))
            {
                var diff = filter.outerHeight() -  (switchAt - filter.offset().top);
                pageContent
                    .css({
                        "padding-top" : diff + "px"
                    });
            }

            filter
                .addClass("fixed");
        }
        else
        {
            filter.removeClass("fixed");
            pageContent
                .css({
                    "padding-top" : ""
                });            
        }
    }    
    
    
    function filterGrid()
    {
        var filterItems = jQuery(".companies .filters .items-list .category");
        var gridItems = jQuery(".companies .tiles-grid .grid-item");
                
        if (!filterItems.length || !gridItems.length) { return; }
                
        var selectedCategories = [];
        
        filterItems.each(function()
        {
            currentItem = jQuery(this);            
            
            if (currentItem.hasClass("active"))
            {
                if (currentItem.attr("data-filter") == "all")
                {
                    selectedCategories = "all";
                    return false;
                }
                else
                {
                    selectedCategories.push(currentItem.attr("data-filter"));
                }
            }
        });
        
        if (selectedCategories == "all")
        {
            gridItems.addClass("active");
        }
        else
        {
            gridItems.each(function()
            {
                currentItem = jQuery(this);
                var categories = currentItem.attr("data-item-categories");

                var itemCategories = categories.split(",");
                
                var containAtLeastOneFilter = selectedCategories.some(function (v) { return itemCategories.indexOf(v) >= 0;} );

                if (containAtLeastOneFilter)
                    currentItem.addClass("active");
                else
                    currentItem.removeClass("active");

            });
        }
    }  
    
    
    
    function handleFilter(isConditionOR)
    {
        isConditionOR = typeof isConditionOR !== "undefined" ? isConditionOR : false;
        
        var filterItems = jQuery(".companies .filters .items-list .category");
        
        if (!filterItems.length) { return; }
        
        filterItems.on("click", function(e)
        {
            currentItem = jQuery(this);
            cirrentItemFilter = currentItem.attr("data-filter");        
                  
            if (cirrentItemFilter == "all")
            {
                if (currentItem.hasClass("active")) 
                { 
                    return;
                }
                else
                {
                    filterItems.removeClass("active");
                    currentItem.addClass("active");
                }
            }
            else
            {
                if (isConditionOR) //only last selected
                    filterItems.removeClass("active");
                else //all selected excepted "all"
                    filterItems.filter('[data-filter="all"]').removeClass("active"); 
                
                if (currentItem.hasClass("active"))
                {
                    if (filterItems.filter(".active").length > 1)
                    {
                        currentItem.removeClass("active");
                    }
                }
                else
                {
                    currentItem.addClass("active");                    
                }
            }
            
            filterGrid();
        });
        
        filterGrid();
    }    
    
/*
	function onScroll()
	{	
		var isScrolling  = false;

		jQuery(window).on("scroll", function () 
		{
			if (isScrolling) { return; }

			isScrolling = true;

			window.setTimeout(function()
			{
				//place functions here	
                handleFixedFilter();

				isScrolling = false;
			}, 10 );
		});
        
        jQuery(window).trigger("scroll");        
	}	
   
    
    function onResize()
    {	
        var isResizing  = false;

        jQuery(window).on("resize", function () 
        {
            if (isResizing) { return; }		
            isResizing = true;

            window.setTimeout(function()
            {
                //place functions here	
                handleFixedFilter();

                isResizing = false;
            }, 10 );
        });	
               
		jQuery(window).trigger("resize");
    }    
*/

	var init = function()
	{
        if (!jQuery(".page-main.companies").length) { return; }
        
		handleFilter(true);
        onScroll();
        onResize();

	};

	init();
};

var companies;

