comments = function ()
{
	var applySwitcher = function()
	{
		jQuery(".comments").each( function ()
		{
			var commentBox = jQuery(this);

			jQuery(".show-action button", commentBox).on("click", function()
			{
				jQuery(".comments-content", commentBox).stop().slideToggle();
				commentBox.toggleClass("visible");
			})
		})

	}

	var scrollToComments = function()
	{
		if(location.href.indexOf("replytocom=") > -1 || location.href.indexOf("#comment") > -1)
		{
			jQuery(".comments .comments-content").show();
			jQuery(".comments").addClass("visible");

			if(location.hash)
			{
				if(jQuery(location.hash).length)
				{
					jQuery("html,body").animate({ scrollTop: jQuery(location.hash).offset().top - 100})
				}
			}
		}
	}

	var handleDoRateRelease = function()
	{
		jQuery("#doRate").on("click", function() {
			jQuery(".comments .comments-content").show();
			jQuery(".comments").addClass("visible");
			jQuery("html,body").animate({ scrollTop: jQuery("#respond").offset().top - 100})
		});
	}

	var drawStars = function()
	{
		jQuery(".star-rating").each( function() {
			if(jQuery(this).data("rating"))
			{
				var rating = parseFloat(jQuery(this).data("rating"));
				var fulls = Math.floor(rating);
				for(var i = 0; i < fulls;i ++)
				{
					jQuery(this).find(".stars").append('<span class="full"></span>');
				}
				if(fulls < 5)
				{
					var fract = Math.round((rating - fulls) * 100);
					if(fract > 0) jQuery(this).find(".stars").append('<span class="part"><span style="width: ' + (fract+10) + '%"></span></span>');
					else jQuery(this).find(".stars").append('<span class="empty"></span>');
					fulls++;
					while(fulls < 5)
					{
						jQuery(this).find(".stars").append('<span class="empty"></span>');
						fulls++;
					}
				}
			}
		});
	}

	var handleRateInForm = function()
	{
		jQuery(".star-rating.selectable").each( function()
		{
			var rateContainer = jQuery(this);
			jQuery(".stars > span", rateContainer).each( function() {
				jQuery(this).on("mouseenter", function() {
					var selectedIndex = jQuery(this).index();
					jQuery(".stars > span",rateContainer).each( function() {
						if(jQuery(this).index() <= selectedIndex)
							jQuery(this).removeClass().addClass("full");
						else
							jQuery(this).removeClass().addClass("empty");
					});
				});

				jQuery(this).on("click", function() {
					var selectedIndex = jQuery(this).index();
					var selectedValue = selectedIndex + 1;
					rateContainer.data('rating', selectedIndex);
					jQuery('input[name=rating]').val(selectedValue);
				});

			});

			rateContainer.on("mouseleave", function() {
				var selectedIndex = jQuery(this).data('rating');
				jQuery(".stars > span",rateContainer).each( function() {
					if(jQuery(this).index() <= selectedIndex)
						jQuery(this).removeClass().addClass("full");
					else
						jQuery(this).removeClass().addClass("empty");
				});
			});


		})
	}

	var handleThumbs = function()
	{
		jQuery(".thumbs > button").on("click", function()
		{
			if(!jQuery(this).hasClass("locked-active") && !jQuery(this).hasClass("locked-inactive"))
			{
				jQuery(this).parent().find("button").fadeOut( function() {
					jQuery(this).fadeIn("fast");
				})
				jQuery(this).parent().find("button").addClass("locked-inactive")
				jQuery(this).addClass("active").removeClass("locked-inactive").addClass("locked-active")

				var dir = "down"
				if(jQuery(this).hasClass("up")) dir = "up";
				var postId = jQuery(this).closest(".thumbvote").data("postid");
				jQuery.post("/wp-json/ikal/v1/post/vote", {"post": postId, "dir": dir});
			}
		})
	}


	/* _________________________________________________________________________ public methods */

	var init = function()
	{
		handleDoRateRelease();
		scrollToComments();
		applySwitcher();
		drawStars();
		handleRateInForm();
		handleThumbs();
	};

	init();
};

var comments;


/*
 *	Dostosowana natywna fukcja wordpressa (wp-includes/js/comment-reply.js)
 */
var addComment = {
  /**
   * @param {string} commId The comment ID.
   * @param {string} parentId The parent ID.
   * @param {string} respondId The respond ID.
   * @param {string} postId The post ID.
   * @returns {boolean} Always returns false.
   */
  moveForm: function( commId, parentId, respondId, postId ) {
  	var cancel = jQuery('#cancel-comment-reply-link');
    cancel.on('click', function() {
      jQuery("#cancel-comment-reply-link .reply-to-author").remove();
      jQuery("#cancel-comment-reply-link").hide();
      jQuery('#comment_parent').val(null);
      jQuery('.comment-form-rating').show();
      jQuery('#rating').val(4);
      return false;
    })

  	var author = jQuery('#'+commId).find('.comment-author b').html();
    jQuery("html,body").animate({ scrollTop: jQuery("#respond").offset().top - 100});
    jQuery("#cancel-comment-reply-link").html(
    	jQuery("#cancel-comment-reply-link").text() + '<span class="reply-to-author"> użytkownikowi ' + author + '</span>'
		);
    jQuery("#cancel-comment-reply-link").show();

    jQuery('.comment-form-rating').hide();
    jQuery('#rating').val(null);

    jQuery('#comment_parent').val(parentId);

    return false;
  }
}

