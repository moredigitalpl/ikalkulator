ikalkulator = function ()
{
    function calculateChildrenHeight(container)
    {	
        var topOffset = bottomOffset = 0;

        container.children().each(function(i, child)
        {
            var element = jQuery(child),
            eTopOffset = element.offset().top,
            eBottomOffset = eTopOffset + element.outerHeight();

            if (eTopOffset < topOffset)
                topOffset = eTopOffset;

            if (eBottomOffset > bottomOffset)
                bottomOffset = eBottomOffset;
        });

        return  bottomOffset - topOffset - container.offset().top + parseInt(container.css("padding-bottom"));
    }    
    
    function handleCategoryWithSidebarMinHeight()
    {
        var sidebar = jQuery("body.category section.category-content .aside-panel");
        if (!sidebar.length) { return; }
        
        var categoryContent = jQuery("body.category section.category-content");        
        categoryContent.css("min-height", calculateChildrenHeight(categoryContent));
    }
    
    function handleListNumbering()
    {
        jQuery("ol[start]").each(function()
        {
            var val = parseFloat(jQuery(this).attr("start")) - 1;
            console.log(val);
            jQuery(this).css("counter-increment", "ol-counter " + val);
        });        
    }
    
    function handleProductsGrid()
    {
        var productGrid = jQuery(".product-grid");
        
        if (!productGrid.length)
            return;
        
        var showMore = jQuery(".products-show-more", productGrid);
        
        showMore.on("click", function()
        {
            var btn = jQuery(this);
            var moreProducts = jQuery(".posts-list-content.more-products", productGrid);
            
            if (moreProducts.hasClass("visible"))
            {
                jQuery(".show", btn).show(); 
                jQuery(".hide", btn).hide(); 
                moreProducts.removeClass("visible");
                moreProducts.slideUp();
                scrollTo(jQuery(".posts-list-content", productGrid), -30);
            }    
            else
            {
                jQuery(".show", btn).hide(); 
                jQuery(".hide", btn).show(); 
                moreProducts.addClass("visible");
                moreProducts.slideDown();
                scrollTo(jQuery(".posts-list-content", productGrid), -30);
            }   
        });
        
    }
    
    
	function handleShareLinks()
	{
		jQuery(".share .social-share").on("click", function(e)
        {
			e.preventDefault();
			window.open(jQuery(this).attr('href'), 'fbShareWindow', 'height=450, width=650, top=' + (jQuery(window).height() / 2 - 275) + ', left=' + (jQuery(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
			return false;
		});
	}
    
    function handleSpotlightSearch()
    {
        var searchBtn = jQuery(".site-header .search-btn a");
        var searchBar = jQuery(".search-bar.spotlight-search");
        
        if (!searchBtn.length || !searchBtn.length)
            return;
        
        function hideBar()
        {
            searchBar.removeClass("active");
            jQuery("input", searchBar).blur().attr("readonly", true);
        }
        
        jQuery("input", searchBar).attr("readonly", true);
        
        searchBtn.on("click", function(e)
        {
            e.stopPropagation();
            e.preventDefault();            
            searchBar.toggleClass("active");  
            
            if (searchBar.hasClass("active"))
            {
                jQuery("input", searchBar).attr("readonly", false); 
                
                window.setTimeout(function()
                {
                    jQuery("input[type=text]", searchBar).focus();
                } ,500);
                
        }
            else
                jQuery("input", searchBar).attr("readonly", true);
        });

        searchBar.on("click touch touchend", function(e)
        {
            e.stopPropagation();
        });        
        
        jQuery(document)
            .on("click touch touchend", function()
            {
                hideBar();
            })        
            .on("keydown", function(e)
            {
                if ( e.keyCode == 27 )
                {
                    hideBar();    
                }
            });
    }

    function checkElementsVisibilityOnScroll(elementsClass)
    {
        var elements = jQuery(elementsClass);

        if (!elements.length)
            return;

        elements
            .attr("data-onscreen", 0);

        var isScrolling  = false;

        jQuery(window).on("scroll", function()
        {
			if (isScrolling) { return; }

            var winHeight = jQuery(window).innerHeight();
            var scrollTop =  jQuery(window).scrollTop();
            var scrollBottom = scrollTop + winHeight;

			isScrolling = true;

			window.setTimeout(function()
			{
                elements.each(function()
                {
                    var element = jQuery(this);
                    var isVisible = element.attr("data-onscreen");
                    var elementHeight = element.outerHeight();
                    var elementTop = element.offset().top;
                    var elementBottom = elementTop + elementHeight;

                    if (isVisible == true)
                    {
                        if (elementBottom <= scrollTop || elementTop >= scrollBottom)
                        {
                            element
                                .trigger("ik:visiblity-change")
                                .trigger("ik:hidden")
                                .attr("data-onscreen", 0);
                        }
                    }
                    else
                    {
                        if (
                                (elementBottom > scrollTop && elementTop < scrollTop)
                                ||
                                (elementTop < scrollBottom && elementBottom > scrollBottom)
                                ||
                                (elementTop < scrollTop && elementBottom > scrollBottom)
                            )
                        {
                            element
                                .trigger("ik:visiblity-change")
                                .trigger("ik:visible")
                                .attr("data-onscreen", 1);
                        }
                    }

                    /*
                    if (elementBottom <= scrollBottom && elementTop >= scrollTop)
                        element.trigger("ik:reveal:full");
                    else*/
                });

				isScrolling = false;
			}, 5 );
        });

		jQuery(window).trigger("scroll");
    }
    function handleMenu()
    {
        var siteHeader = jQuery(".site-header");
        var menuItems = jQuery(".menu > .menu-item-has-children > a", siteHeader);
        
        
        menuItems.on("click", function(e)
        {
            if (window.innerWidth < 1024)
            {
                var isClicked = jQuery(this).parent().hasClass("clicked");
               
                
                if (!isClicked)
                {
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    e.stopPropagation();              
                    
                    jQuery(".menu > .menu-item-has-children").removeClass("clicked");
                    jQuery(this).parent().addClass("clicked");
                }
                else
                {
//                    jQuery(this).parent().removeClass("clicked");                    
                }
            }
        });
        
    }

	function handleMobileMenu()
	{
        var body = jQuery("body");
        var siteHeader = jQuery(".site-header");
		var btnMenu = jQuery(".btn-mobile-menu", siteHeader);
		var menuItems = jQuery(".menu-item a", siteHeader);

		btnMenu
			.on("click", function(e)
			{
				body.toggleClass("menu-opened");

				if (body.hasClass("menu-opened"))
				{
					jQuery(this).addClass("active");
					siteHeader.addClass("mobile-active");
				}
				else
				{
					jQuery(this).removeClass("active");
					siteHeader.removeClass("mobile-active");
				}
			});

		menuItems
			.on("click", function(e)
			{
                if(!jQuery(this).parent().hasClass("menu-item-has-children"))
                {
                    body.removeClass("menu-opened");
                    btnMenu.removeClass("active");
                    siteHeader.removeClass("mobile-active");

                }
			});
	};

    function handleAnalytics()
    {
        jQuery('.navigation-top li a').on('click', function(e)
        {
            var link = jQuery(this).attr("href");
            //e.preventDefault();
            gtag('event', 'TOP-menu', {
                'event_category': 'click',
                'event_label': jQuery(this).find("span").html(),
                //'event_callback': function(){document.location = link;}
            });
        });

        jQuery('.footer-nav li a').on('click', function(e)
        {
            //e.preventDefault();

            var link = jQuery(this).attr("href");

            gtag('event', 'footer-menu', {
                'event_category': 'click',
                'event_label': jQuery(this).find("span").html(),
                //'event_callback': function() {document.location = link;}
            });
        });

        jQuery('.products.list li .more .expander .expand').on('click', function(e)
        {
            var product_name = jQuery(this).closest("li").find(".name div").html();
            e.preventDefault();
            gtag('event', 'Zobacz więcej', {
                'event_category': 'click',
                'event_label': product_name + '  na ' + location.href
            });
        });

        jQuery('.products.list li .more .expander .fold').on('click', function(e)
        {
            var product_name = jQuery(this).closest("li").find(".name div").html();
            e.preventDefault();
            gtag('event', 'Zobacz mniej', {
                'event_category': 'click',
                'event_label': product_name + '  na ' + location.href
            });
        });

        jQuery('.products.list li .cta a').on('click', function(e)
        {
            //e.preventDefault();

            var link = jQuery(this).attr("href");
            var product_name = jQuery(this).closest("li").attr("id")
            var color = "biały";

            if(jQuery(this).hasClass("btn-inverse"))
                color = "zielony";

            gtag('event', 'compare-acquisition', {
                'event_category': 'click',
                'event_label': 'Złóż wniosek - ' + color + ' guzik - '  + product_name + ' na ' + location.href,
                //'event_callback': function(){document.location = link;}
            });
        });

        jQuery('.product .attachments ul li a').on('click', function(e)
        {
            //e.preventDefault();

            var link = jQuery(this).attr("href");

            gtag('event', 'PDF', {
                    'event_category' : 'pobranie',
                    'event_label' : location.href,
                    //'event_callback': function(){document.location = link;}
                }
            );
        });


        jQuery('.share .links a.social-share').on('click', function()
        {
            var media = jQuery(this).find("span").html();

            gtag('event', media,
                { 'event_category' : 'share', 'event_label' : location.href }
            );
        });

        jQuery('.post-content .external-link').on('click', function()
        {
            var link = jQuery(this).attr("href");
            var text = jQuery(this).text();
            gtag("event", "link_z_artykulu",
                {
                    "event_category" : "click",
                    "event_label" : link + " | " + text,
                    //"event_callback": function(){document.location = link;}
                }
            );
        });


    }

    function scrollTo(targetOrInt, margin, speed)
    {
        function isInt(n){ return Number(n) === n && n % 1 === 0; }
        function isjQuery(obj) { return (obj && (obj instanceof jQuery || obj.constructor.prototype.jquery)); }

        var target = targetOrInt;

        if (!isInt(target))
        {
            target = !isjQuery(target) ? jQuery(target).offset().top : target.offset().top;
        }

        margin = typeof margin !== "undefined" ? margin : 0;
        speed = typeof speed !== "undefined" ? speed : 750;

        if (navigator.userAgent.match(/(iPod|iPhone|iPad)/))
        {
            jQuery('body').animate(
            {
                scrollTop: target + margin
            }, speed);
        }
        else
        {
            jQuery('html, body').animate(
            {
                scrollTop: target + margin
            }, speed);
        }
    }

    function handleScrollToContent()
    {
        var scrollBtn = jQuery(".scroll-to-content");

        if (!scrollBtn.length)
            return;

        var content = jQuery(".main-content");

        if (!content.length)
            content = jQuery(".panel-layout");

        if (!content.length)
            return;

        scrollBtn.on("click", function()
        {
            scrollTo(content);
        })
    }

    function handleMageboxNavigation()
    {
        var megaboxes = jQuery(".megabox");

        if (!megaboxes.length)
            return;

        var autoPlay = false;
        var megaboxInterval;

        function toggleAutoPlay(megabox, turn_on)
        {
            turn_on = typeof turn_on !== "undefined" ? turn_on : true;

            if (turn_on)
            {
                clearInterval(megaboxInterval);
                megaboxInterval = setInterval(function()
                {
                    goToNextSlide(megabox, true);
                }, 3000);
            }
            else
            {
                clearInterval(megaboxInterval);
            }
        }

        function getActiveSlide(megabox)
        {
            return jQuery(".featured-posts-container .featured-post.active");
        }

        function getActiveSlideIndex(megabox)
        {
            return getActiveSlide(megabox).index();
        };

        function getSlideByIndex(megabox, index)
        {
            return jQuery(".featured-posts-container .featured-post", megabox).eq(index);
        };

        function updateActiveDotPosition(megabox)
        {
            jQuery(".dots .dot", megabox).removeClass("active")
                .eq(getActiveSlideIndex(megabox)).addClass("active");
        };

        function goToSlide(megabox, index)
        {
            if (getActiveSlideIndex(megabox) === index)
                return;

            var slides = jQuery(".featured-posts-container .featured-post", megabox);

            megabox.trigger( "ikalkulator:beforeMegaboxChange" );
            slides.removeClass("active");
            slides.eq(index).addClass("active");
            updateActiveDotPosition(megabox);
            megabox.trigger( "ikalkulator:afterSlideChange" );
        };

        function goToPrevSlide(megabox, loop)
        {
            loop = typeof loop !== "undefined" ? loop : true;
            var slides = jQuery(".featured-posts-container .featured-post", megabox);

            var activeIndex = getActiveSlideIndex(megabox);

            if (activeIndex >= 1 && getSlideByIndex(megabox, activeIndex - 1).length)
                goToSlide(megabox, activeIndex - 1);
            else
                if (loop)
                    goToSlide(megabox, slides.length - 1);
        };

        function goToNextSlide(megabox, loop)
        {
            loop = typeof loop !== "undefined" ? loop : true;
            var activeIndex = getActiveSlideIndex(megabox);

            if (getSlideByIndex(megabox, activeIndex + 1).length)
                goToSlide(megabox, activeIndex + 1);
            else
                if (loop)
                    goToSlide(megabox, 0);
        };

        megaboxes.each(function()
        {
            var megabox = jQuery(this);
            var dots = jQuery(".nav-panel .dot", megabox);
            var prev = jQuery(".nav-panel .prev", megabox);
            var next = jQuery(".nav-panel .next", megabox);

            prev.on("click", function()
            {
                goToPrevSlide(megabox, true);
            });

            next.on("click", function()
            {
                goToNextSlide(megabox, true);
            });

            dots.on("click", function(evnt)
            {
                var dotIndex =  jQuery(this).index();
                goToSlide(megabox, dotIndex);
            });

            if (autoPlay)
            {
                jQuery(".featured-posts-container, .nav-panel", megabox)
                    .on("mouseenter", function()
                    {
                        toggleAutoPlay(megabox, false);
                    })
                    .on("mouseleave", function()
                    {
                        toggleAutoPlay(megabox, true);
                    });

                toggleAutoPlay(megabox, true);
            }
            jQuery(".see-all .see-all-btn",megabox).on("click", function()
            {
                jQuery(".see-all",megabox).toggleClass("active");

            });

        });
    }

    function updatePostFloatingHeaderVisibility(value)
    {
        var floatingHeader = jQuery(".floating-header");

        if (value >= 0)
            floatingHeader.addClass("active");
        else
            floatingHeader.removeClass("active");
    }

    function updateProgressBar(progress, content, contentTop)
    {
        var winHeight = jQuery(window).innerHeight();

        var contentTopPadding = content.offset().top - contentTop;
        var contentHeight = contentTopPadding + content.outerHeight();
        var value = jQuery(window).scrollTop() - contentTop;
        progress.attr("max", contentHeight - winHeight);
        progress.attr("value", value);

        updatePostFloatingHeaderVisibility(value);

        return value;
    }

    function handleNewsletterScrollToResponse()
    {
        var resposeElement = jQuery(".mc4wp-response .mc4wp-alert");
        
        if (!resposeElement.length)
            return;
        
        if (resposeElement.html !== "")
        {
            scrollTo(resposeElement, -80);
        }
    }


    function handleReadingProgress()
    {
        var progress = jQuery(".reading-progress");
        var content = jQuery(".main-content .post-content");

        if (!progress.length || !content.length)
            return;

        var mainContent = content.closest(".main-content");

        var contentTop = mainContent.offset().top;
        updateProgressBar(progress, content, contentTop);

        jQuery(document).on("scroll", function()
        {
            contentTop = mainContent.offset().top;
            var value = jQuery(window).scrollTop() - contentTop;
            progress.attr("value",  value);
            updatePostFloatingHeaderVisibility(value);
        });

        jQuery(window).on("resize", function()
        {
            contentTop = mainContent.offset().top;
            updateProgressBar(progress, content, contentTop);
        });
    }
    
    function handleMobileSwapOrder()
    {
        var containers = jQuery(".mobile-swap");
        
        containers.each(function()
        {
            var current = jQuery(this);
            var elements = jQuery("> *", current);
            
            
            if (window.innerWidth < 768)
            {
                if (!current.hasClass("swapped"))
                {
                    elements.first().detach().insertAfter(elements.last());
                    current.addClass("swapped");
                }                
            }
            else
            {
                 if (current.hasClass("swapped"))
                {
                    elements.first().detach().insertAfter(elements.last());
                    current.removeClass("swapped");
                }                 
            }
        });
    }


    function disableImagesDragging()
    {
        jQuery(".post-list").on("mousedown", "img", function(event) { event.preventDefault(); });
    }
    
    function onResize()
    {	
        var isResizing  = false;

        jQuery(window).on("resize", function () 
        {
            if (isResizing) { return; }		
            isResizing = true;

            window.setTimeout(function()
            {
                //place functions here
                handleMobileSwapOrder();
                handleCategoryWithSidebarMinHeight();

                isResizing = false;
            }, 50 );
        });	
               
		jQuery(window).trigger("resize");
    }



    /* _________________________________________________________________________ public methods */

	var init = function()
	{
        handleMenu();
        handleMobileMenu();
        handleMageboxNavigation();
        handleScrollToContent();
        handleReadingProgress();
        handleSpotlightSearch();
        handleShareLinks();
        handleProductsGrid();
        handleAnalytics();
        handleListNumbering();
        handleCategoryWithSidebarMinHeight();
        
        checkElementsVisibilityOnScroll(".scroll-handle");
        disableImagesDragging();
        handleNewsletterScrollToResponse();

		onResize();
		jQuery(window).trigger("scroll");

	};

	init();
};

var ikalkulator;

jQuery(document).ready(function($)
{
	ikalkulator = new ikalkulator();
	product_filters = new productFilter();
	companies = new companies();
    searchbox = new searchbox();
    promobox = new promobox();
    comments = new comments();
});
