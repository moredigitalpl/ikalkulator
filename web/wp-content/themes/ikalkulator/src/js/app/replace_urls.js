document.addEventListener("DOMContentLoaded",function(){

  function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
  }

  let gid = getCookie('_gid');

  if(gid) {
    const urls = document.getElementsByTagName('a');

    if(gid.includes('GA')) {
      gid = gid.substr(6);
    }

    for(let i = 0; i<urls.length; i++) {
      let href = urls[i].getAttribute('href');
      if(href.includes('affplus.pl/c') && href.includes('?')) {
        href += '&cid=' + gid;
        urls[i].setAttribute('href', href);
      } else if(href.includes('affplus.pl/c')) {
        href += '?cid=' + gid;
        urls[i].setAttribute('href', href);
      }
    }
  }

})
