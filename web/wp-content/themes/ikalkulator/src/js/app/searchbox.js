searchbox = function ()
{
	var currentPath = "";
	var currentLevel = 1;
    
    // get closest tu num number from array
    function closest(num, arr)
    {
        var curr = arr[0];
        var index = 0;
        var diff = Math.abs(num - curr);
        for (var val = 0; val < arr.length; val++) {
            var newdiff = Math.abs(num - arr[val]);
            if (newdiff < diff) {
                diff = newdiff;
                curr = arr[val];
                index = val;
            }
        }
        return index;
    }    

	var applySliders = function()
	{
		jQuery(".searchbox .slider").each( function()
		{
			var options = {};
			var sliderContainer = jQuery(this);

			if(sliderContainer.data("filter-type") == "between")
			{
				options.range = true;
				options.values = [];
				if(sliderContainer.data("value-min") && sliderContainer.data("value-max"))
				{
					options.values.push(sliderContainer.data("value-min"));
					options.values.push(sliderContainer.data("value-max"));
				};
			}
			else if(sliderContainer.data("filter-type") == "to")
			{                
				options.range = "min";
				if(sliderContainer.data("value"))
				{
					options.value = sliderContainer.data("value");
				};
			};
            
			if(sliderContainer.data("min") != null)
			{
				options.min = sliderContainer.data("min");
			};

			if(sliderContainer.data("max"))
			{
				options.max = sliderContainer.data("max");
			};
			var pointsValues = [];

			if(sliderContainer.data("step"))
			{

				var stepVal = sliderContainer.data("step");
				var length = Math.ceil((options.max-options.min)/stepVal);
				var start = Math.floor(options.min/stepVal);
                
				for(var i = start; i <length+start; i++)
				{
					if(i == start && options.min < i*stepVal)
					{
						pointsValues.push(options.min);
					}
                    
					pointsValues.push(i*stepVal);
                    
					if(i == length+start-1 && options.max > i*stepVal)
					{
						pointsValues.push(options.max);
					}
				}


				if(pointsValues.length)
				{
					options.min = 0;
					options.max = pointsValues.length-1;

					if(options.value)
					{
						options.value = pointsValues.indexOf(options.value);
					}
				}
			}

			if(sliderContainer.data("progressive") == true)
			{
				var rangesConfig =
				{
					default:
						[
							{"min": 0, "max": 900, "step": 100},
							{"min": 1000, "max": 9000, "step": 1000},
							{"min": 10000, "max": 48000, "step": 2000},
							{"min": 50000, "max": 50000000, "step": 5000},
						],
					investments: [
						{"min": 0, "max": 5000, "step": 500},
						{"min": 6000, "max": 19000, "step": 1000},
						{"min": 20000, "max": 95000, "step": 5000},
						{"min": 100000, "max": 475000, "step": 25000},
						{"min": 500000, "max": 10000000, "step": 50000},
					],
					investmentsMonths:
						[
							{"min": 1, "max": 23, "step": 1},
							{"min": 24, "max": 600, "step": 12}
						]
				};

				var ranges = rangesConfig.default;

				if(sliderContainer.data("progressive-values") && rangesConfig[sliderContainer.data("progressive-values")])
					ranges = rangesConfig[sliderContainer.data("progressive-values")];

				for(i=0; i < ranges.length; i++)
				{
					var current = 0;
					if(options.min > ranges[i].max || options.max < ranges[i].min)
					{
						continue;
					}
					else
					{ 
						current = ranges[i].min;
						while(current < options.min)
						{
							current += ranges[i].step;
						}

						if(options.min > ranges[i].min && current-ranges[i].step < options.min)
						{
							pointsValues.push(options.min);
						}

						while(current <= options.max && current <= ranges[i].max)
						{
							pointsValues.push(current);
							current += ranges[i].step;
						}
					}
				}

				if(pointsValues.length)
				{
					options.min = 0;
					options.max = pointsValues.length-1;
//					options.max = pointsValues[pointsValues.length-1];

					if(options.value)
					{
						options.value = pointsValues.indexOf(options.value);
//						options.value = pointsValues[pointsValues.indexOf(options.value)];
					}
				}
			}

			options.slide = function( event, ui )
			{
				if(ui.value)
				{
					var exVal = ui.value;

					if(pointsValues.length)
					{
						exVal = pointsValues[ui.value];
					}

					sliderContainer.find(".value").html( formatNumber(exVal) );
					sliderContainer.parents("[data-level]").first().find("input").val(exVal);
                    sliderContainer.data("value", exVal);
                }
				if(ui.values)
				{
					sliderContainer.find(".value").html( formatNumber(ui.values[0]) + " - " + formatNumber(ui.values[1]) );
					sliderContainer.parents("[data-level]").first().find("input[name^='min-']").val(ui.values[0]).trigger("change");
					sliderContainer.parents("[data-level]").first().find("input[name^='max-']").val(ui.values[1]).trigger("change");
				}
			}

			options.change = function( event, ui )
			{
				sliderContainer.parents("[data-level]").first().trigger("ikal:filter:changed");
			}


			sliderContainer.closest("[data-level]").find("input").on("change", function()
			{
                var input = jQuery(this);

				if(parseFloat(input.attr("min")) && input.val() < parseFloat(input.attr("min")))
					input.val(parseFloat(input.attr("min")));

				if(parseFloat(input.attr("max")) && input.val() > parseFloat(input.attr("max")))
					input.val(parseFloat(input.attr("max")));
                
                var val = input.val();
                if(pointsValues.length)
                {
                    val = closest(input.val(), pointsValues);
                }
                sliderContainer.find(".slider-bar").slider("value", val);
				sliderContainer.find(".value").html( formatNumber(input.val()) );
				sliderContainer.closest("[data-level]").trigger("ikal:filter:changed");
			});
            
			sliderContainer.find(".slider-bar").slider(options);   
            sliderContainer.closest("[data-level]").find("input").trigger("change");
		});
        

	}

	var applyChoices = function()
	{
		jQuery(".searchbox .choice").each( function()
		{
			var choicer = jQuery(this);
			jQuery(".value", choicer).on("click", function(e)
			{
				jQuery(".topped").stop().fadeOut().removeClass("topped");
				jQuery("ul", choicer).stop().fadeIn().addClass("topped");
			})

			jQuery("li[data-value]", choicer).on("click", function(e)
			{
				e.stopPropagation();
				var optionValue = jQuery(this).data("value");
				var targetPath = jQuery(this).data("target-path");
				var targetLevel = jQuery(this).data("target-level");
				jQuery("ul", choicer).stop().fadeOut().removeClass("topped");
				jQuery(".value > span", choicer).html(jQuery(this).html());

				choicer.first().find("input").val(optionValue)
				setPath(targetPath, targetLevel);
			});
		});
	}

	var applyTooltips = function()
	{
		jQuery(".tooltip-toggler").each( function() {
			var toggler = jQuery(this);
			jQuery(".do-toggle", toggler).on("click", function(e)
			{
				jQuery(".topped").stop().fadeOut().removeClass("topped");
				jQuery(".tooltip", toggler).fadeIn().addClass("topped");

				toggler.parents("[data-level]").first().one("ikal:filter:changed", function()
				{
					jQuery(".tooltip", toggler).fadeOut().removeClass("topped");
				});
			});
		});
	}

	var handleSubmit = function()
	{
		jQuery(".submit-search").on("click", function()
		{
			var pathParts = getPathParts();
			var queryParams = {};
			for(var i = 0; i<currentLevel; i++)
			{
				jQuery("[data-level='" + parseInt(i+1) +"']").each( function()
				{
					var lvl = jQuery(this).data("level");
					if(lvl == i+1 && jQuery(this).data("path").indexOf(pathParts[i]) == 0)
					{
						var currEl = jQuery(this);
						jQuery("input", currEl).each( function()
						{
							if(jQuery(this).data("prop-name"))
								queryParams[jQuery(this).data("prop-name")] = jQuery(this).val();
						});
					}
				});
			}
			var url = "";
			if(pathParts[1] == "quickloans") url = "/finansowanie/chwilowki/";
			if(pathParts[1] == "loans") url = "/finansowanie/pozyczki-na-raty/";
			if(pathParts[1] == "credits") url = "/finansowanie/kredyty-gotowkowe/";
			if(pathParts[1] == "investments") url = "/lokaty/";

			if(pathParts[1] == "accounts")
			{
				if(jQuery("input[name='accounts.type']").val() == "personal") url = "/konta-bankowe/konta-osobiste/";
				if(jQuery("input[name='accounts.type']").val() == "business") url = "/konta-bankowe/konta-firmowe/";
				if(jQuery("input[name='accounts.type']").val() == "currency") url = "/konta-bankowe/konta-walutowe/";
				if(jQuery("input[name='accounts.type']").val() == "saving") url = "/konta-bankowe/konta-oszczednosciowe/";
			}
			url += "?";

//			console.log(queryParams);
			jQuery.each(queryParams,  function(k,v)
			{
				url += k + "=" + v + "&";
			})

			location.href = url;

		})
	}

	var handleScrollDown = function()
	{
		jQuery(".searchbox .cta a").on("click", function(e)
		{
			e.preventDefault();
			window.location.hash = jQuery(this).attr("href");
			jQuery("body,html").animate({ scrollTop: jQuery(".main-content.page-content").offset().top });
		})
	}

	var handleZindex = function()
	{
		jQuery(".searchbox [data-level]").on("click", function(e)
		{
			e.stopPropagation();

			jQuery(".searchbox [data-level]").css("z-index", "");
			jQuery(this).css("z-index", 20);
		})
	}


	var setPath = function(targetPath, targetLevel)
	{
		if(typeof(targetPath) != "undefined" && typeof(targetLevel) != "undefined")
		{
			currentPath = targetPath;
			currentLevel = targetLevel;

			var pathParts = getPathParts();

			for(var i = 0; i<currentLevel; i++)
			{
				jQuery("[data-level='" + parseInt(i+1) +"']").each( function()
				{
					var lvl = jQuery(this).data("level");
					if(lvl == i+1 && jQuery(this).data("path").indexOf(pathParts[i]) == 0)
					{
						var currEl = jQuery(this);
						setTimeout(function()
						{
							currEl.fadeIn("fast").css("display", "inline-block");
						}, 350)
					}
					else
						jQuery(this).fadeOut(300);

				});
			}

		}
	}

	var getPathParts = function()
	{
		var slicedPath = currentPath.split(".");
		var pathParts = [""];
		var tmpPath = "";

		for(var k in slicedPath)
		{
			if(k == 0) tmpPath = slicedPath[0];
			else tmpPath += "." + slicedPath [k];
			pathParts.push(tmpPath);
		}

		return pathParts;
	}

	var handleClickOutside = function()
	{
		jQuery(window).on("click", function() {
			jQuery(".topped").stop().fadeOut().removeClass("topped");
		});
	}

	/* _________________________________________________________________________ public methods */

	var init = function()
	{
		applyChoices();
		applyTooltips();
		applySliders();
		handleSubmit();
		handleScrollDown();
		handleZindex();
		handleClickOutside();
	};

	init();
};

var searchbox;
