function countdown(targetId) {
  const target = document.getElementById(targetId);
  let days, hours, minutes, seconds;

  let startDate = target.getAttribute('data-start');
  startDate = new Date(startDate).getTime();

  let endDate = target.getAttribute('data-end');
  endDate = new Date(endDate).getTime();

  let nowDate = new Date();
  nowDate.getTime();

  let beforeMsg = target.getAttribute('data-before-msg');
  let whileMsg = target.getAttribute('data-while-msg');
  let afterMsg = target.getAttribute('data-after-msg');

  target.innerHTML = '<div class="ikal__countdown"><small class="msg"></small><br><div class="ikal__countdown__cell"><span class="ikal__countdown__days"></span> Dni</div><div class="ikal__countdown__cell"><span class="ikal__countdown__hours"></span> Godzin</div><div class="ikal__countdown__cell"><span class="ikal__countdown__minutes"></span> Minut</div><div class="ikal__countdown__cell"><span class="ikal__countdown__seconds"></span> Sekund</div></div>';

  function check() {
    if((!isNaN(startDate)) && (startDate - nowDate > 0)) {
      // obliczanie czasu przed rozpoczęciem wydarzenia
      target.getElementsByClassName("msg")[0].innerHTML = beforeMsg;
      calculate(startDate);
    } else if ((!isNaN(endDate)) && (endDate - nowDate > 0)) {
      // obliczanie czasu w trakcie wydarzenia
      target.getElementsByClassName("msg")[0].innerHTML = whileMsg;
      calculate(endDate);
    } else {
      // informacja po zakończeniu wydarzenia
      if(afterMsg) {
        target.innerHTML = '<p class="ikal__countdown"><small class="msg">' + afterMsg + '</small></p>';
      } else {
        target.innerHTML = '';
      }
      clearInterval(interval);
      return;
    }
  }

  const interval = setInterval(function() {
    nowDate.getTime();
    check();
  }, 1000);

  function calculate(calculatedDate) {
    nowDate = new Date();
    nowDate.getTime();

    let timeRemaining = parseInt((calculatedDate - nowDate) / 1000);

    if (timeRemaining >= 0) {
      days = parseInt(timeRemaining / 86400);
      timeRemaining = (timeRemaining % 86400);

      hours = parseInt(timeRemaining / 3600);
      timeRemaining = (timeRemaining % 3600);

      minutes = parseInt(timeRemaining / 60);
      timeRemaining = (timeRemaining % 60);

      seconds = parseInt(timeRemaining);

      target.getElementsByClassName("ikal__countdown__days")[0].innerHTML = parseInt(days, 10);
      target.getElementsByClassName("ikal__countdown__hours")[0].innerHTML = ("0" + hours).slice(-2);
      target.getElementsByClassName("ikal__countdown__minutes")[0].innerHTML = ("0" + minutes).slice(-2);
      target.getElementsByClassName("ikal__countdown__seconds")[0].innerHTML = ("0" + seconds).slice(-2);
    } else {
      return;
    }
  }
}

// (function () {
//   countdown('04/01/2333 05:00:00 PM');
// }());
