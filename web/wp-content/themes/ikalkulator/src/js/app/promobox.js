promobox = function ()
{
    var draggableList,
        postsList,
        promoboxElement,
        info,
        filters,
        filtersPos = null,
        filtersStartPos = null,
        listWidth,
        distansceFromRight;
        dragging = false;
        itemWidth = null, seeAllItemWidth = null;
    var pep;
    
    function getPostsListWidth(list)
    {
        var width = 0;

        jQuery(".post-item:not(.hide):not(.all-promotions)", list).each(function()
        {
//            width += jQuery(this).outerWidth(true);
            width += itemWidth;///jQuery(this).outerWidth(); //333
        });
        
        width += seeAllItemWidth;
        width += parseFloat(list.css("padding-left")) + parseFloat(list.css("padding-right"));        
        
        return width;
    }  
    
    function updateVars()
    {
        draggableList = jQuery(".draggable-list");
        postsList = jQuery(".posts-list-content", draggableList);

        if (!postsList.length)
            return;   

        promoboxElement = draggableList.closest(".promobox");
        info = jQuery(".info", promoboxElement);
        
        if (filtersPos === null)
        {
            filters = jQuery(".filters", draggableList);  
            filtersPos = filters.position();
        }
        
        if (filtersStartPos === null)
        {
            filtersStartPos = filters.position().left;
        }
        if (itemWidth === null)
        {
            itemWidth = jQuery(".post-item", postsList).not(".all-promotions").eq(0).outerWidth();
            seeAllItemWidth = jQuery(".post-item.all-promotions", postsList).eq(0).outerWidth();
        }
                
        listWidth = getPostsListWidth(postsList);
        postsList.outerWidth(listWidth);
        distansceFromRight = (jQuery(window).innerWidth() -  (draggableList.offset().left + draggableList.outerWidth()));
    }      
    
    function updateGallery()
    {  
        if  (typeof pep !== "undefined")
        {
            jQuery.pep.unbind( pep ); 
        }

        if(!draggableList || !draggableList.length)
            return;
    
        updateVars();

        if (listWidth >=  draggableList.offset().left + draggableList.outerWidth())
        {
            constrainLeft = -listWidth + draggableList.outerWidth() + distansceFromRight;
        }
        else
        if (listWidth >= draggableList.outerWidth())
        {
            constrainLeft = -listWidth + draggableList.outerWidth();
        }
        else
//        if (listWidth < draggableList.outerWidth())
        {
            constrainLeft = 0;
        }

        if(jQuery(window).width() < 1024)
        {
            var spd = false;
        }
        else
        {
            var spd = true;
        }
                
        pep = postsList.pep({
            axis: "x",
            drag: drag,
            easing: moving,
            start: startDrag,
            revert: false,
//          revertIf:   function(){ return false; },
            constrainTo: [0, 0, 0, constrainLeft],
            shouldPreventDefault: spd
          });
       
        
        info.css({
            "opacity" : "",
            "transform" : ""
        });    

        postsList.css({
            "left" : "0",
            "transform": "matrix(1, 0, 0, 1, 0, 0)"
        });           
          
/*      
        filters.css({
            "left" : filtersStartPos + "px"
        }); 
*/        
    }   
    
    function startDrag(e, obj)
    {
//            info = jQuery(".info", promoboxElement);
//            listWidth = getPostsListWidth(postsList);
//            postsList.width(listWidth);
    }
    
    function drag(e, obj)
    {
        dragging = true;
        moving(e, obj);
    }

    //drag or easing
    function moving(e, obj)
    {
        var drag = jQuery(obj.el);
        var infoWidth = info.outerWidth();
        var dragPos = drag.position().left;
        var infoDistance = Math.min(Math.abs(dragPos), infoWidth);

        if (jQuery(window).innerWidth() >= 1024)
        {
            info.css({
                "opacity" : 1- infoDistance/infoWidth,
                "transform" : "scale(" + (1 - infoDistance/infoWidth/2) + ")"
            });
        }
        else
        {
            info.css({
                "opacity" : "",
                "transform" : ""
            });
        }
        
        filters.css({
            "left" : Math.max((filtersPos.left - Math.abs(dragPos)), 0) + "px"
        });
    }
    

    function handleDraggablePostsList()
    {
        dragging = false;
        updateVars();
        
        if (!postsList.length)
            return;  

        updateGallery();
        
        jQuery(window).on("resize", function()
        {
            updateGallery();
        });

        //click when click, drag when drag
        jQuery("a", postsList)
            .on("mousedown pointerdown touchstart",function(e) {
                dragging = false;
            })
            .on("click touch",function(e)
            {
                if (dragging)
                {
                    e.preventDefault();
                }                
            })
            ;
    }

	var applyFilter = function()
	{
		jQuery(".promobox").each( function ()
		{
			var promobox = jQuery(this);
			jQuery(".filters .value", promobox).on("click touch touchend", function(e)
			{
                e.preventDefault();
                e.stopPropagation();
                jQuery(".filters ul", promobox).stop().fadeIn();
			});

			jQuery(".filters ul li", promobox).on("click touch touchend", function(e)
			{
                e.preventDefault();
                e.stopPropagation();
                jQuery(this).toggleClass("active");
                window.location.hash = "#filtruj-" + jQuery(this).html();
				applyFilterToContent(promobox);
			});

            //jQuery(document).on("click touch touchend", function(e)
            //{
            //    e.stopPropagation();
            //})

            var hideFilter = function()
            {
                jQuery(".filters ul", promobox).stop().fadeOut();

                if (typeof filters !== "undefined" && parseInt(postsList.css("left")) == 0)
                {
                    filters.css({
                        "left" : filtersStartPos + "px"
                    });
                }
            }

			jQuery(".filters ul", promobox).on("mouseleave", hideFilter);
            jQuery(document).on("click touchend", hideFilter)
        });
	}

	var applyFilterToContent = function(promobox)
	{
		var cats = [];
        var postItemsLength = jQuery(".post-item", promobox).length;

		jQuery(".filters ul li", promobox).each( function() {
			if(jQuery(this).hasClass("active"))
				cats.push(jQuery(this).attr("data-type"));
		});

		if(!cats.length)
        {
			jQuery(".post-item", promobox).removeClass( "hide" );
            updateGallery();
        }
		else
		jQuery(".post-item", promobox).each( function(index, element)
        {
            var currentItem = jQuery(this);
            if (!currentItem.hasClass("all-promotions"))
            {
                var postCats = currentItem.attr("data-type").toString().split(",");
                var disablePost = true;
                for(i in cats)
                {
                    if(postCats.indexOf(cats[i].toString()) > -1)
                        disablePost = false;
                }
                if(disablePost) currentItem.addClass( "hide" );
                else jQuery(this).removeClass( "hide" );
            }
            
            if (index == postItemsLength -1)
            {
                updateGallery();
            }            
		});
//        updateVars();
//            updateGallery();
	}

	/* _________________________________________________________________________ public methods */

	var init = function()
	{
        updateGallery();
		applyFilter();
        handleDraggablePostsList();
	};

	init();
};

var promobox;
