class percentOfCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__percOf" action=""><div class="row">Oblicz <input type="text" class="ikalCalc__percOf__input1" name="ikalCalc__percOf__input1">% z <input type="text" class="ikalCalc__percOf__input2" name="ikalCalc__percOf__input2"></div><div class="row">Wynik: <input type="text" class="ikalCalc__percOf__output" name="ikalCalc__percOf__output" disabled></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.input1 = this.target.getElementsByClassName('ikalCalc__percOf__input1')[0];
    this.input2 = this.target.getElementsByClassName('ikalCalc__percOf__input2')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__percOf__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.input1.addEventListener('keyup', function() {
      self.result();
    });
    this.input2.addEventListener('keyup', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    let result = eval(( this.input1.value / 100 ) * this.input2.value);
    result =+ (Math.round(result + "e+6") + "e-6");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}

class whatPercentCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__whatPerc" action=""><div class="row"><input type="text" class="ikalCalc__whatPerc__input1" name="ikalCalc__whatPerc__input1"> jest jakim procentem z <input type="text" class="ikalCalc__whatPerc__input2" name="ikalCalc__whatPerc__input2"></div><div class="row">Wynik: <input type="text" class="ikalCalc__whatPerc__output" name="ikalCalc__whatPerc__output" disabled></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.input1 = this.target.getElementsByClassName('ikalCalc__whatPerc__input1')[0];
    this.input2 = this.target.getElementsByClassName('ikalCalc__whatPerc__input2')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__whatPerc__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.input1.addEventListener('keyup', function() {
      self.result();
    });
    this.input2.addEventListener('keyup', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    let result = eval( this.input1.value / (this.input2.value / 100) );
    result =+ (Math.round(result + "e+6") + "e-6");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}

class percentRaiseCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__percRaise" action=""><div class="row">O jaki procent wzrosła/zmalała liczba</div><div class="row">z <input type="text" class="ikalCalc__percRaise__input1" name="ikalCalc__percRaise__input1"> do <input type="text" class="ikalCalc__percRaise__input2" name="ikalCalc__percRaise__input2">?</div><div class="row">Wynik: <input type="text" class="ikalCalc__percRaise__output" name="ikalCalc__percRaise__output" disabled>%</div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.input1 = this.target.getElementsByClassName('ikalCalc__percRaise__input1')[0];
    this.input2 = this.target.getElementsByClassName('ikalCalc__percRaise__input2')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__percRaise__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.input1.addEventListener('keyup', function() {
      self.result();
    });
    this.input2.addEventListener('keyup', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    let result = eval( (this.input2.value * 100) / this.input1.value - 100);
    result =+ (Math.round(result + "e+6") + "e-6");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}


class percentAdditionCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__percAddition" action=""><div class="row">Dodaj procent do liczby</div><div class="row"><input type="text" class="ikalCalc__percAddition__input1" name="ikalCalc__percAddition__input1"> + <input type="text" class="ikalCalc__percAddition__input2" name="ikalCalc__percAddition__input2">%</div><div class="row">Wynik: <input type="text" class="ikalCalc__percAddition__output" name="ikalCalc__percAddition__output" disabled></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.input1 = this.target.getElementsByClassName('ikalCalc__percAddition__input1')[0];
    this.input2 = this.target.getElementsByClassName('ikalCalc__percAddition__input2')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__percAddition__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.input1.addEventListener('keyup', function() {
      self.result();
    });
    this.input2.addEventListener('keyup', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    let result = eval( this.input1.value * ( this.input2.value / 100 + 1 ) );
    result =+ (Math.round(result + "e+6") + "e-6");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}

class percentSubtractCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__percSubtract" action=""><div class="row">Odejmij procent od liczby</div><div class="row"><input type="text" min="0" max="100" class="ikalCalc__percSubtract__input1" name="ikalCalc__percSubtract__input1"> - <input type="text" class="ikalCalc__percSubtract__input2" name="ikalCalc__percSubtract__input2">%</div><div class="row">Wynik: <input type="text" class="ikalCalc__percSubtract__output" name="ikalCalc__percSubtract__output" disabled></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.input1 = this.target.getElementsByClassName('ikalCalc__percSubtract__input1')[0];
    this.input2 = this.target.getElementsByClassName('ikalCalc__percSubtract__input2')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__percSubtract__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.input1.addEventListener('keyup', function() {
      self.result();
    });
    this.input2.addEventListener('keyup', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    let result = eval( this.input1.value * ( 1 - this.input2.value / 100 ) );
    result =+ (Math.round(result + "e+6") + "e-6");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}

class compoundInterestCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__compInterest" action=""><div class="row"><span>Kapitał początkowy</span> <input type="text" class="ikalCalc__compInterest__initBalance" name="ikalCalc__compInterest__initBalance"></div><div class="row"><span>Oprocentowanie</span> <input type="text" class="ikalCalc__compInterest__intRate" name="ikalCalc__compInterest__intRate"></div><div class="row"><span>Kapitalizacja</span> <select class="ikalCalc__compInterest__compFreq" name="ikalCalc__compInterest__compFreq"><option value="365">dzienna</option><option value="52">tygodniowa</option><option value="12">miesięczna</option><option value="4">kwartalna</option><option value="2">półroczna</option><option value="1">roczna</option></select></div><div class="row"><span>Liczba lat</span> <input type="text" class="ikalCalc__compInterest__years" name="ikalCalc__compInterest__years"></div><div class="row"><span>Kapitał końcowy</span> <input type="text" class="ikalCalc__compInterest__output" name="ikalCalc__compInterest__output" disabled></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.initBalance = this.target.getElementsByClassName('ikalCalc__compInterest__initBalance')[0];
    this.intRate = this.target.getElementsByClassName('ikalCalc__compInterest__intRate')[0];
    this.compFreq = this.target.getElementsByClassName('ikalCalc__compInterest__compFreq')[0];
    this.years = this.target.getElementsByClassName('ikalCalc__compInterest__years')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__compInterest__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.initBalance.addEventListener('keyup', function() {
      self.result();
    });

    this.intRate.addEventListener('keyup', function() {
      self.result();
    });

    this.compFreq.addEventListener('change', function() {
      self.result();
    });

    this.years.addEventListener('keyup', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    const initBalance = this.initBalance.value;
    const intRate = this.intRate.value / 100;
    const compFreq = this.compFreq.value;
    const years = this.years.value;

    let calc1 = intRate / compFreq;
    let calc2 = 1 + calc1;
    let calc3 = compFreq * years;
    let calc4 = Math.pow(calc2, calc3);
    let calc5 = initBalance * calc4;

    let result = eval( calc5 );
    result =+ (Math.round(result + "e+2") + "e-2");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}


class investmentCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    console.log(this.targetID);
    this.target = document.getElementById(targetId);
    console.log(this.target);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__investment" action=""><div class="row"><span>Kapitał początkowy</span><input type="text" class="ikalCalc__investment__initBalance" name="ikalCalc__investment__initBalance"></div><div class="row"><span>Okres umowy</span><input type="text" class="ikalCalc__investment__time__value" name="ikalCalc__investment__time__value"><select class="ikalCalc__investment__time__unit" name="ikalCalc__investment__time__unit"><option value="days">dni</option><option value="months">miesiące</option><option value="years">lata</option></select></div><div class="row"><span>Oprocentowanie</span><input type="text" class="ikalCalc__investment__intRate" name="ikalCalc__investment__intRate"></div><div class="row"><span>Kapitalizacja</span><select class="ikalCalc__investment__compFreq" name="ikalCalc__investment__compFreq"><option value="yearly">roczna</option><option value="monthly">miesięczna</option><option value="daily">dzienna</option><option value="overall">na koniec okresu</option></select></div><div class="row"><div class="checkbox"><input type="checkbox" class="ikalCalc__investment__tax" id="ikalCalc__investment__tax" name="ikalCalc__investment__tax"><label for="ikalCalc__investment__tax">Uwzględnij podatek</label></div></div><div class="row"><label id="ikalCalc__investment__output">Kapitał końcowy</label><input type="text" class="ikalCalc__investment__output" name="ikalCalc__investment__output"></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.initBalance = this.target.getElementsByClassName('ikalCalc__investment__initBalance')[0];
    this.timeValue = this.target.getElementsByClassName('ikalCalc__investment__time__value')[0];
    this.timeUnit = this.target.getElementsByClassName('ikalCalc__investment__time__unit')[0];
    this.intRate = this.target.getElementsByClassName('ikalCalc__investment__intRate')[0];
    this.compFreq = this.target.getElementsByClassName('ikalCalc__investment__compFreq')[0];
    this.tax = this.target.getElementsByClassName('ikalCalc__investment__tax')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__investment__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.initBalance.addEventListener('keyup', function() {
      self.result();
    });

    this.timeValue.addEventListener('keyup', function() {
      self.result();
    });

    this.timeUnit.addEventListener('change', function() {
      self.result();
    });

    this.intRate.addEventListener('keyup', function() {
      self.result();
    });

    this.compFreq.addEventListener('change', function() {
      self.result();
    });

    this.tax.addEventListener('change', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    const initBalance = this.initBalance.value;

    let time = this.timeValue.value;
    const timeUnit = this.timeUnit.value;
    switch (timeUnit) {
      case 'days':
        time = time / 365;
        break;
      case 'months':
        time = time / 12;
        break;
    }

    const intRate = this.intRate.value*0.01;
    let compFreq = this.compFreq.value;
    switch(compFreq) {
      case 'yearly':
        compFreq = 1;
        break;
      case 'monthly':
        compFreq = 12;
        break;
      case 'daily':
        compFreq = 365;
        break;
      case 'overall':
        compFreq = 1/time;
        break;
    }

    const tax = this.tax.checked;
    let taxed = 0;
    let calc1 = 1 + intRate / compFreq;
    let calc2 = compFreq * time;
    let calc3 = Math.pow(calc1, calc2);
    let calc4 = initBalance * calc3;

    if(tax) {
      taxed =  ((calc4 - initBalance) * 0.19);
    }

    let result = eval( calc4 - taxed );
    result =+ (Math.round(result + "e+2") + "e-2");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}

class regularSavingsCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__regSavings" action=""><div class="row"><span>Kwota regularnych składek</span><input type="text" class="ikalCalc__regSavings__amount" name="ikalCalc__regSavings__amount"></div><div class="row"><span>Częstotliwość</span><select class="ikalCalc__regSavings__freq" name="ikalCalc__regSavings__freq"><option value="12">Miesięcznie</option><option value="1">Rocznie</option></select></div><div class="row"><span>Okres oszczędzania</span><input type="text" class="ikalCalc__regSavings__years" name="ikalCalc__regSavings__years"> lat</div><div class="row"><span>Stopa zwrotu</span><input type="text" class="ikalCalc__regSavings__intRate" name="ikalCalc__regSavings__intRate"></div><div class="row"><span>Kapitalizacja</span><select class="ikalCalc__regSavings__compFreq" name="ikalCalc__regSavings__compFreq"><option value="12">Miesięcznie</option><option value="1">Rocznie</option></select></div><div class="row"><span>Inflacja</span><input type="text" class="ikalCalc__regSavings__inflation" name="ikalCalc__regSavings__inflation"></div><div class="row"><div class="checkbox"><input type="checkbox" class="ikalCalc__regSavings__tax" id="ikalCalc__regSavings__tax" name="ikalCalc__regSavings__tax"><label for="ikalCalc__regSavings__tax">Uwzględnij podatek</label></div></div><div class="row"><span>Wynik</span><input type="text" class="ikalCalc__regSavings__output" name="ikalCalc__regSavings__output" disabled=""></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.amount = this.target.getElementsByClassName('ikalCalc__regSavings__amount')[0];
    this.freq = this.target.getElementsByClassName('ikalCalc__regSavings__freq')[0];
    this.years = this.target.getElementsByClassName('ikalCalc__regSavings__years')[0];
    this.intRate = this.target.getElementsByClassName('ikalCalc__regSavings__intRate')[0];
    this.compFreq = this.target.getElementsByClassName('ikalCalc__regSavings__compFreq')[0];
    this.inflation = this.target.getElementsByClassName('ikalCalc__regSavings__inflation')[0];
    this.tax = this.target.getElementsByClassName('ikalCalc__regSavings__tax')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__regSavings__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.amount.addEventListener('keyup', function() {
      self.result();
    });

    this.freq.addEventListener('change', function() {
      self.result();
    });

    this.years.addEventListener('keyup', function() {
      self.result();
    });

    this.intRate.addEventListener('keyup', function() {
      self.result();
    });

    this.compFreq.addEventListener('change', function() {
      self.result();
    });

    this.inflation.addEventListener('keyup', function() {
      self.result();
    });

    this.tax.addEventListener('change', function() {
      self.result();
    });

    this.output.addEventListener('keyup', function() {
      self.result();
    });
  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if(input.classList.contains('ikalCalc__regSavings__inflation')) {
        if (evt.which < 45 || ( evt.which > 46 && evt.which < 48) || evt.which > 57) {
          evt.preventDefault();
        }
      } else {
        if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
          evt.preventDefault();
        }
      }
    });
  }
  result() {
    const amount = eval(this.amount.value);
    const freq = eval(this.freq.value);
    const years = eval(this.years.value);
    const intRate = eval(this.intRate.value) * 0.01;
    const compFreq = eval(this.compFreq.value);
    const inflation = eval(this.inflation.value) * 0.01;
    const tax = this.tax.checked;

    let subscore = 0;

    if (compFreq == freq) {
      const subscore1 = 1 + intRate / compFreq;
      const subscore2 = Math.pow(subscore1, (compFreq * years)) - 1;
      const subscore3 = intRate / compFreq;
      subscore = amount * subscore1 * (subscore2 / subscore3);
    } else if (compFreq < freq) {
      const subscore1 = (freq / compFreq) + (((freq / compFreq - 1) * (intRate / compFreq)) / 2);
      const subscore2 = (Math.pow((1 + intRate / compFreq), (compFreq * years)) - 1) / (intRate / compFreq);
      subscore = amount * subscore1 * subscore2;
    } else {
      const subscore1 = Math.pow((1 + intRate / compFreq), (compFreq * years)) - 1;
      const subscore2 = Math.pow((1 + intRate / compFreq), (compFreq / freq)) - 1;
      subscore = amount * (subscore1 / subscore2);
    }

    const savedAmount = amount * freq * years;
    const diffence = subscore - savedAmount;

    let taxAmount = 0;
    if (tax) {
      taxAmount = diffence * 0.19;
    }

    let inflationAmount = 0;
    if (inflation && inflation != 0) {
      inflationAmount = subscore * inflation;
    }

    let result  = eval(subscore - taxAmount - inflationAmount);
    result =+ (Math.round(result + "e+2") + "e-2");
    if(!isNaN(result)) {
      this.output.value = result;
    }
  }
}


class regularSavingsByAmountCalculator {
  constructor(targetId) {
    var self = this;
    this.targetID = targetId;
    this.target = document.getElementById(targetId);
    this.target.innerHTML = '<form class="ikalCalc ikalCalc__regSavingsByAmount" action=""><div class="row"><span>Cel</span><input type="text" class="ikalCalc__regSavingsByAmount__goal" name="ikalCalc__regSavings__goal"></div><div class="row"><span>Kwota regularnych składek</span><input type="text" class="ikalCalc__regSavingsByAmount__amount" name="ikalCalc__regSavingsByAmount__amount"></div><div class="row"><span>Częstotliwość</span><select class="ikalCalc__regSavingsByAmount__freq" name="ikalCalc__regSavingsByAmount__freq"><option value="12">Miesięcznie</option><option value="4">Kwartalnie</option><option value="1">Rocznie</option></select></div><div class="row"><span>Stopa zwrotu</span><input type="text" class="ikalCalc__regSavingsByAmount__intRate" name="ikalCalc__regSavingsByAmount__intRate"></div><div class="row"><span>Kapitalizacja</span><select class="ikalCalc__regSavingsByAmount__compFreq" name="ikalCalc__regSavingsByAmount__compFreq"><option value="12">Miesięcznie</option><option value="1">Rocznie</option></select></div><div class="row"><div class="ikalCalc__regSavingsByAmount__output"></div></div></form>';

    this.inputs = this.target.getElementsByTagName("input");
    this.goal = this.target.getElementsByClassName('ikalCalc__regSavingsByAmount__goal')[0];
    this.amount = this.target.getElementsByClassName('ikalCalc__regSavingsByAmount__amount')[0];
    this.freq = this.target.getElementsByClassName('ikalCalc__regSavingsByAmount__freq')[0];
    this.intRate = this.target.getElementsByClassName('ikalCalc__regSavingsByAmount__intRate')[0];
    this.compFreq = this.target.getElementsByClassName('ikalCalc__regSavingsByAmount__compFreq')[0];
    this.output = this.target.getElementsByClassName('ikalCalc__regSavingsByAmount__output')[0];

    for(let i = 0; i<this.inputs.length; i++) {
      this.preventNonNumeric(this.inputs[i]);
    }

    this.goal.addEventListener('keyup', function() {
      self.result();
    });

    this.amount.addEventListener('keyup', function() {
      self.result();
    });

    this.freq.addEventListener('change', function() {
      self.result();
    });

    this.intRate.addEventListener('keyup', function() {
      self.result();
    });

    this.compFreq.addEventListener('change', function() {
      self.result();
    });

  }
  preventNonNumeric(input) {
    input.addEventListener("keypress", function (evt) {
      if (evt.which < 46 || evt.which == 47 || evt.which > 57) {
        evt.preventDefault();
      }
    });
  }
  result() {
    const goal = eval(this.goal.value);
    const amount = eval(this.amount.value);
    const freq = eval(this.freq.value);
    const intRate = eval(this.intRate.value) * 0.01;
    const compFreq = eval(this.compFreq.value);

    let subscore = 0;
    let years = 0;

    if (compFreq == freq) {
      while(subscore < goal) {
        console.log('compfreq==freq years: ' + years + ' score: ' + subscore);
        years++;
        const subscore1 = 1 + intRate / compFreq;
        const subscore2 = Math.pow(subscore1, (compFreq * years)) - 1;
        const subscore3 = intRate / compFreq;
        subscore = amount * subscore1 * (subscore2 / subscore3);
      }
    } else if (compFreq < freq) {
      while(subscore < goal) {
        console.log('compfreq<freq years: ' + years + ' score: ' + subscore);
        years++;
        const subscore1 = (freq / compFreq) + (((freq / compFreq - 1) * (intRate / compFreq)) / 2);
        const subscore2 = (Math.pow((1 + intRate / compFreq), (compFreq * years)) - 1) / (intRate / compFreq);
        subscore = amount * subscore1 * subscore2;
      }
    } else {
      while(subscore < goal) {
        console.log('compfreq>freq years: ' + years + ' score: ' + subscore);
        years++;
        const subscore1 = Math.pow((1 + intRate / compFreq), (compFreq * years)) - 1;
        console.log('subscore1 ' + subscore1);
        const subscore2 = Math.pow((1 + intRate / compFreq), (compFreq / freq)) - 1;
        console.log('subscore2 ' + subscore2);
        subscore = amount * (subscore1 / subscore2);
      }
    }
    // let result = 'Musisz zbierać przez ' + years + ' lat. Uzbierana kwota będzie równa ' + subscore;
    let result  = eval(subscore);
    result =+ (Math.round(result + "e+2") + "e-2");
    if(!isNaN(result)) {
      if(years == 1) {
        this.output.innerHTML = 'Musisz zbierać przez 1 rok. Uzbierana kwota będzie wynosić ' + result;
      } else if (years > 1 && years < 5) {
        this.output.innerHTML = 'Musisz zbierać przez ' + years + ' lata. Uzbierana kwota będzie wynosić ' + result;
      } else {
        this.output.innerHTML = 'Musisz zbierać przez ' + years + ' lat. Uzbierana kwota będzie wynosić ' + result;
      }
    }
  }
}
