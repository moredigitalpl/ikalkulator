<?php

$bodyClass = "archive-quickloans";
get_header();
include('template-parts/products/products-setup/quickloans.php');
?>


<div id="page" class="product-list quickloans" data-product-slug="quickloans">
    <div class="page-head ">
        <div class="content container">
            <?php the_breadcrumbs(); ?>
            <h1 class="title">Chwilówki <span class="count">(<?php echo product_count_display(count($posts_array)); ?>)</span> </h1>

            <div class="description">
                <?php the_field('products_descriptions_quickloans', 'options'); ?>
            </div>
            
        </div>
    </div>

<?php


    include('template-parts/products/products-html-filters/quickloans.php');

?>
    <div class="container products header"><div class="mobile-label">Sortuj wg</div><ul><li>
        <div class="image data-row">
        </div>
        <div class="amount data-row">
            Kwota
        </div>
        <div class="maxperiod data-row">
            Okres spłat
        </div>
        <div class="cost data-row sortable">
            <a href="#" data-sort-by="cost" data-sort-type="number">Koszt</a>
        </div>
        <div class="rank data-row sortable">
            <a href="#" data-sort-by="rank" data-sort-type="number">Ocena</a>
        </div>
        <div class="cta data-row">
            Złóż wniosek
        </div>
    </li></ul></div>
    <div class="container products list">
    <?php
    include('template-parts/products/products-html-list/quickloans.php');
        echo '</div>';

        ?>
        <div class="container description main-content">
            <div class="content">
                <?php the_field('products_descriptions_quickloans_bottom', 'options'); ?>
            </div>
        </div>
</div>
    <script type="application/ld+json">
        <?php echo json_encode($schemaData); ?>
    </script>


<?php get_footer(); ?>
