<?php if ( is_active_sidebar( "sidebar-bottom" ) ) : ?>
	<div class="sidebar sidebar-bottom widget-area" role="complementary">
		<?php dynamic_sidebar( "sidebar-bottom" ); ?>
	</div>
<?php endif; ?>