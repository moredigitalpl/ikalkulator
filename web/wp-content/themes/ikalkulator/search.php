<?php
get_header();
?>
<main class="page-main" role="main">
    <div class="page-head category-head">
        <div class="content">
            <h1 class="title">Wyniki wyszukiwania</h1>
            <div class="description">
                Poniżej prezentujemy wyniki wyszukiwania dla "<strong><?php the_search_query(); ?></strong>"
            </div>
        </div>
    </div>
    <div class="main-content category-content">
        <?php
            $paged = get_query_var( "paged" ) ? absint( get_query_var( "paged" ) ) : 1;
            $show_products = isset($_GET["products"]) && $_GET["products"] == 1 ? true : false;            
            
            if ($show_products && $paged == 1)
            {
//                get_search_query()

                $args = array(
                    "post_type"  => array(
                        "accounts_business",
                        "accounts_currency",
                        "accounts_personal",
                        "accounts_saving",
                        "credits",
                        "investments",
                        "loans",
                        "quickloans",
                    ),
                    "s" => get_search_query(),
                    "posts_per_page" => -1,
                );

                $products_query = new WP_Query($args);
                
                $items_count = 0;
                $visible_products = "";
                $hidden_products = "";

                if ($products_query->have_posts()):
                    while( $products_query->have_posts()): $products_query->the_post();
                    {
                        $items_count++;
                        $post_type = get_post_type();
                        
                        if ($items_count < 4)
                        {
                            $visible_products .= create_product_list_item($post, "post-list-big", "type-product {$post_type}");
                        }
                        elseif ($items_count == 4) //to switch visiblity when 3 cols
                        {
                            $visible_products .= create_product_list_item($post, "post-list-big", "type-product {$post_type} fill");
                            $hidden_products .= create_product_list_item($post, "post-list-big", "type-product {$post_type} fill");   
                        }
                        else
                        {
                            
                            $hidden_products .= create_product_list_item($post, "post-list-big", "type-product {$post_type}");                            
                        }
                    }
                    endwhile;    
                endif;
                ?>
                <div class="product-grid">
                    <div class="posts-list-content">
                        <?php echo $visible_products; ?>
                    </div>   
                    <?php
                    if ($items_count > 4)
                    { ?>
                        <div class="posts-list-content more-products">
                            <?php echo $hidden_products; ?>                        
                        </div>  
                        <div class="products-show-more">
                            <span class="show">Rozwiń</span>
                            <span class="hide" style="display: none;">Zwiń</span>
                        </div>
                    <?php
                    } ?>
                </div><?php                          
            }    
        ?>
                    
                    
            
        <div class="posts post-list">
            <?php
            if ( have_posts() )
            { ?>
                <div class="posts-list-content">
                    <?php
                    $outdated_items = "";
                    $items = "";
                    while ( have_posts() ) : the_post();
                        $product_item = create_post_list_item($post);

                        if (strpos($product_item, "outdated") !== false)
                        {
                            $outdated_items .= $product_item;
                        }
                        else
                        {
                            $items .= $product_item;
                        }                    
                    endwhile;
                    echo $items;
                    echo $outdated_items;   
                    ?>
                </div>
                <?php
                build_posts_pagination(new WP_Query( array( "s" => get_search_query(), "post_type"  => array("post") )) );
                wp_reset_postdata();
            } ?>
        </div>
    </div>
</main>
<?php get_footer();
