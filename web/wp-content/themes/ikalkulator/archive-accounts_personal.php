<?php

$bodyClass = "archive-accounts_personal";
get_header();
include('template-parts/products/products-setup/accounts_personal.php');
?>

<div id="page" class="product-list accounts_personal" data-product-slug="accounts_personal">
    <div class="page-head">
        <div class="content">
            <?php the_breadcrumbs(); ?>
            <h1 class="title">Konta osobiste <span class="count">(<?php echo product_count_display(count($posts_array)); ?>)</span> </h1>

            <div class="description">
                <?php the_field('products_descriptions_accounts_personal', 'options'); ?>
            </div>
            
        </div>
    </div>
    <?php

    include('template-parts/products/products-html-filters/accounts_personal.php');

    ?>
    <div class="container products header"><div class="mobile-label">Sortuj wg</div><ul><li>
                <div class="name data-row">
                    Nazwa produktu
                </div>
                <div class="payment data-row sortable">
                    <a href="#" data-sort-by="payment" data-sort-type="number">Konto</a>
                </div>
                <div class="cardpayment data-row sortable">
                    <a href="#" data-sort-by="cardpayment" data-sort-type="number">Karta</a>
                </div>
                <div class="atmpayment data-row sortable">
                    <a href="#" data-sort-by="atmpayment" data-sort-type="number">Bankomaty</a>
                </div>
                <div class="rank data-row sortable">
                    <a href="#" data-sort-by="rank" data-sort-type="number">Ocena</a>
                </div>
                <div class="cta data-row">
                    Złóż wniosek
                </div>
            </li></ul></div>
    <div class="container products list">
        <?php
        include('template-parts/products/products-html-list/accounts_personal.php');
        echo '</div>';

        ?>
        <div class="container description main-content">
            <div class="content">
             <?php the_field('products_descriptions_accounts_personal_bottom', 'options'); ?>
            </div>
        </div>
    </div>

    <?php get_footer(); ?>
