<?php

$bodyClass = "archive-loans";
get_header();
include('template-parts/products/products-setup/loans.php');
?>

<div id="page" class="product-list loans" data-product-slug="loans">
    <div class="page-head">
        <div class="content">
            <?php the_breadcrumbs(); ?>
            <h1 class="title">Pożyczki ratalne <span class="count">(<?php echo product_count_display(count($posts_array)); ?>)</span> </h1>

            <div class="description">
                <?php the_field('products_descriptions_loans', 'options'); ?>
            </div>
            
        </div>
    </div>
    <?php
    include('template-parts/products/products-html-filters/loans.php');
    ?>
    <div class="container products header"><div class="mobile-label">Sortuj wg</div><ul><li>
                <div class="image data-row">
                </div>
                <div class="amount data-row">
                    Kwota
                </div>
                <div class="maxperiod data-row">
                    Okres spłat
                </div>
                <div class="cost data-row sortable">
                    <a href="#" data-sort-by="cost" data-sort-type="number">Wysokość raty</a>
                </div>
                <div class="rank data-row sortable">
                    <a href="#" data-sort-by="rank" data-sort-type="number">Ocena</a>
                </div>
                <div class="cta data-row">
                    Złóż wniosek
                </div>
            </li></ul></div>
    <div class="container products list">
        <?php
        include('template-parts/products/products-html-list/loans.php');
        echo '</div>';

        ?>
        <div class="container description main-content">
            <div class="content">
                <?php the_field('products_descriptions_loans_bottom', 'options'); ?>
            </div>
        </div>
    </div>


    <?php get_footer(); ?>
