<?php

$bodyClass = "archive-companies";
get_header();
?>
<main class="page-main companies" role="main">
    <div class="page-head ">
        <div class="content container">
            <?php the_breadcrumbs(); ?>
            <h1 class="title">Baza firm</h1>
            <div class="description">
                <?php the_field("companies_description", "options"); ?>
            </div>            
        </div>
    </div>

<?php

    include("template-parts/specific/comapnies/filters.php");

?>
    <div class="main-content page-content">
		<?php
        include("template-parts/specific/comapnies/grid.php");
        ?>
	</div>    
<?php get_footer(); ?>
