<?php
/*
Plugin Name: ikalkulator widgets
Description: Widgets for ikalkulator
Version: 1.0
Author: we code&trade;
Author URI: http://wecode.agency
*/

add_action("after_setup_theme", "wecode_load_widgets_textdomain");
function wecode_load_widgets_textdomain()
{
	load_plugin_textdomain("wecode-widgets", false, dirname( plugin_basename( __FILE__ ) ) . "/languages");

	return;
}

add_action("admin_head", "wecode_widgets_custom_style");
function wecode_widgets_custom_style()
{
	/* hide "Image Search" button" */
	echo "<style>
		.siteorigin-widget-form .siteorigin-widget-field-type-media .media-field-wrapper .find-image-button
		{
			display: none !important;
		}
	</style>";
}

function wecode_widgets($folders){
    $folders[] = __DIR__ . '/widgets/';
    return $folders;
}
add_filter('siteorigin_widgets_widget_folders', 'wecode_widgets');

function add_wecode_widgets_collection( $widgets ) {
    $handle = opendir(__DIR__ . '/widgets/');
     while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            $widgets[$entry] = true;
        }
    }
    closedir($handle);
    $widgets['so-video-widget'] = true;
    return $widgets;
}
add_filter('siteorigin_widgets_active_widgets', 'add_wecode_widgets_collection');


/* _____________________________________________________________________________ */

function wecodeWidgets_getPostCategories($post, $term = "category")
{
    $categories = get_the_terms($post->ID, $term );
    return $categories;
}

function wecodeWidgets_getPostImgURL($post, $type = "thumbnail")
{
    $path = "";

    if (has_post_thumbnail($post->ID) )
    {
        $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $type );
        $path = $image[0];
    }

    return $path;
}

function wecodeWidgets_getPostMeta($post, $thumbnail="thumbnail")
{
    $post_meta = array(
        "categories"        => wecodeWidgets_getPostCategories($post),
        "featured_image"    => wecodeWidgets_getPostImgURL($post, $thumbnail),
        "title"             => get_the_title($post->ID),
//        "excerpt"           => get_the_excerpt($post->ID),
        "content"           => get_the_content($post),
        "url"               => get_permalink($post->ID),
        "excerpt"           => get_permalink($post->ID),
    );

    return $post_meta;
}



function wecodeWidgets__getPosts($catsID, $number = -1)
{
    $queryParams = array(
        "post_type"			=> "post",
        "category"			=> implode (",", $catsID),
        "posts_per_page"	=> $number,
        'orderby'			=> 'date',
        'order'				=> 'DESC',
    );

    $posts = get_posts($queryParams);

    return $posts;
}