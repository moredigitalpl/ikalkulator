<?php

/*
Widget Name: ikalkulator - Wyróżniony tekst
Description: Widżet umożliwiający wstawienie wyróżónionego tekstu
Author: we code&trade;
Author URI: http://wecode.agency/
*/

class Wecode_Em_Text_Block_Widget extends SiteOrigin_Widget
{
	function tinymce_editor_buttons($buttons)
	{
		return array(
			"undo",
			"redo",
			"separator",
			"bold",
			"italic",
			"underline",
			"link",
			"alignleft",
			"aligncenter",
			"alignright",
			"alignjustify",
		);
	}	
	
    function __construct()
    {
		$fields = array(		
			"content" => array(
				"type" => "tinymce",
				"label" => "Treść",
				"default" => "",
				"rows" => 10,
				"default_editor" => "tinymce",
				"button_filters" => array(
//					"mce_buttons" => array( $this, "tinymce_editor_buttons" ),
				)
			),	            
			
			/*
			"autop" => array(
				"type" => "checkbox",
				"default" => true,
				"label" => __("Automatically add paragraphs", "itcraft-widgets"),
			),
			*/	            
		);
		
        parent::__construct(
            "wecode-em-text-block-widget",
            __("ikalkulator - Wyróżniony tekst", "wecode-widgets"),
            array(
				"description" => __("Widżet umożliwiający wstawienie wyróżónionego tekstu.", "wecode-widgets"),
 			),
			array(
			),
            $fields
        );
    }
    
	/*
	function unwpautop($string) {
		$string = str_replace("<p>", "", $string);
		$string = str_replace(array("<br />", "<br>", "<br/>"), "\n", $string);
		$string = str_replace("</p>", "\n\n", $string);

		return $string;
	}
	*/    
	    
	public function get_template_variables( $instance, $args ) {
		$instance = wp_parse_args(
			$instance,
			array(  "content" => "" )
		);

//		$instance["content"] = $this->unwpautop( $instance["content"] );
		$instance["content"] = apply_filters( "widget_text", $instance["content"] );

		// Run some known stuff
		if( !empty($GLOBALS["wp_embed"]) ) {
			$instance["content"] = $GLOBALS["wp_embed"]->run_shortcode( $instance["content"] );
			$instance["content"] = $GLOBALS["wp_embed"]->autoembed( $instance["content"] );
		}
		if (function_exists("wp_make_content_images_responsive")) {
			$instance["content"] = wp_make_content_images_responsive( $instance["content"] );
		}
		/*
		if( $instance["autop"] ) {
			$instance["content"] = wpautop( $instance["content"] );
		}
		*/
		$instance["content"] = do_shortcode( shortcode_unautop( $instance["content"] ) );

		return array(
			"content" => $instance["content"],
		);
	}    
    
    function get_style_name($instance) {
        return "";
    }
	
	function get_template_name($instance) {
		return "default";
	}
	
	function get_template_dir($instance) {
		return "tpl";
	}
}
siteorigin_widget_register("wecode-em-text-block-widget", __FILE__, "Wecode_Em_Text_Block_Widget");


function wecode_em_text_block_activate_widget( $widgets ) {
    $widgets[] = "wecode-em-text-block-widget";
    return $widgets;
}
add_filter("siteorigin_widgets_active_widgets", "wecode_em_text_block_activate_widget");
