<?php

/*
Widget Name: ikalkulator - Obraz
Description: Blok dający możliwość wstawienia zdjęcia wraz z opisem.
Author: we code™
*/

class Wecode_Image_Widget extends SiteOrigin_Widget
{
    function __construct()
    {
		$fields = array(
			"image" => array(
				"type" => "media",
				"label" => __( "Obraz", "wecode-widgets" ),
				"description" => __( "brak grafiki spowoduje, że element się nie wyswietli", "wecode-widgets" ),
				"choose" => __( "Wybierz", "wecode-widgets" ),
				"update" => __( "Ustaw", "wecode-widgets" ),
				"library" => "image",
				"fallback" => false
			),
            
            "description_type" => array(
                "type" => "radio",
                "label" => "Opis",
                "default" => "no-description",
                
                "options" => array(
                    "no-description" => "Bez opisu",
                    "image-description" => "Opis obrazka",
                    "custom-description" => "Własny opis"
                ),
                "state_emitter" => array(
					"callback" => "select",
					"args" => array( "description_type" )
				)
            ),           

			"image_description" => array(
				"type" => "text",
				"label" => __("Opis", "wecode-widgets"),
				"default" => "",
				"state_handler" => array(
					"description_type[custom-description]" => array("show"),
					"_else[description_type]" => array("hide"),
				),                
			),
		);

        parent::__construct(
            "wecode-image-widget",
            __("ikalkulator - Obraz", "wecode-widgets"),
            array(
				"description" => __("Blok dający możliwość wstawienia zdjęcia wraz z opisem.", "wecode-widgets"),
 			),
			array(
			),
            $fields
        );
    }

    function get_style_name($instance) {
        return "";
    }

	function get_template_name($instance) {
		return "default";
	}

	function get_template_dir($instance) {
		return "tpl";
	}
}
siteorigin_widget_register("wecode-image-widget", __FILE__, "wecode_Image_Widget");


function wecode_image_activate_widget( $widgets ) {
    $widgets[] = "wecode-image-widget";
    return $widgets;
}
add_filter("siteorigin_widgets_active_widgets", "wecode_image_activate_widget");